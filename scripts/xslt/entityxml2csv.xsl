<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:rng="http://relaxng.org/ns/structure/1.0"
    xmlns:tei="http://www.tei-c.org/ns/1.0"
    xmlns:teiex="http://www.tei-c.org/ns/Examples"
    xmlns:app="http://sub.uni-goettingen.de/met/standards/application-profiles/app-info#"
    xmlns:doc="https://sub.uni-goettingen.de/met/formats/simpledoc#"
    xmlns:output="http://www.w3.org/2010/xslt-xquery-serialization"
    xmlns:md="http://markdown/functions"
    xmlns:exml="https://sub.uni-goettingen.de/met/standards/entity-xml#"
    xmlns:dc="http://purl.org/dc/elements/1.1/"
    xmlns:foaf="http://xmlns.com/foaf/0.1/" 
    xmlns:gndo="https://d-nb.info/standards/elementset/gnd#" 
    xmlns:owl="http://www.w3.org/2002/07/owl#"
    xmlns:skos="http://www.w3.org/2004/02/skos/core#" 
    xmlns:geo="http://www.opengis.net/ont/geosparql#" 
    xmlns:dnb="https://www.dnb.de"
    xmlns:wgs84="http://www.w3.org/2003/01/geo/wgs84_pos#"
    xmlns:es="https://sub.uni-goettingen.de/met/standards/entity-xml-staging#"
    xmlns:utils="https://sub.uni-goettingen.de/met/xslt/functions/entity-xml#"
    exclude-result-prefixes="xs app" 
    xpath-default-namespace="https://sub.uni-goettingen.de/met/standards/entity-xml#"
    version="3.0">
    
    <xsl:param name="agency" select="'false'" />
    <xsl:param name="schema-path" select="'../../schema/entityXML.rng'" />
    <xsl:param name="ignore-attributes" select="'true'" />
    <xsl:param name="trenner" select="';'" />
    
    <xsl:output method="text"/>
    
    <xsl:template match="/">
        <xsl:variable name="csv-xml">
            <xsl:apply-templates select="entityXML" mode="csv-xml"/>
        </xsl:variable>
        <xsl:variable name="cleanup">
            <xsl:apply-templates select="$csv-xml" mode="cleanup" />
        </xsl:variable>
        <xsl:apply-templates select="$cleanup" mode="csv" />
    </xsl:template>
    
    <xsl:template match="entityXML" mode="csv-xml">
        <csv>
            <xsl:apply-templates select="collection/data/list" mode="csv-xml"/>    
        </csv>
    </xsl:template>
    
    <xsl:template match="data/list/title" mode="csv-xml"></xsl:template>
    
    <xsl:template match="data/list/abstract" mode="csv-xml"></xsl:template>
    
    <xsl:template match="data/list/element()[not(name() = ('title', 'abstract') )]" mode="csv-xml">
        <row>
            <cell name="_id"><xsl:value-of select="data(@xml:id)"/></cell>
            <cell name="_uri"><xsl:value-of select="data(@gndo:uri)"/></cell>
            <cell name="_agency"><xsl:value-of select="data(@agency)"/></cell>
            <xsl:variable name="nodes" select="(exml:*, gndo:*, dc:*, foaf:*, owl:*, skos:*, geo:*, dnb:*, wgs84:*, es:*)[not(name() = 'revision')]"/>
            <xsl:for-each-group select="$nodes" group-by="./name()">
                <cell name="{distinct-values(current-group()/name())}">
                    <xsl:apply-templates mode="csv-xml-properties" select="current-group()"/>
                    
                </cell>
            </xsl:for-each-group>
        </row>
    </xsl:template>
    
    <xsl:template match="gndo:preferredName" mode="csv-xml-properties">
        <xsl:value-of select="normalize-space(data(.))"/>
    </xsl:template>
    
    <xsl:template match="element()" mode="csv-xml-properties">
        <xsl:variable name="name" select="./name()"/>
        <xsl:variable name="same" select="parent::node()/child::node()[name() = $name]"/>
        <xsl:choose>
            <xsl:when test="count($same) &gt; 1">
                <item>
                    <xsl:value-of select="normalize-space(data(.))"/>
                    <xsl:apply-templates mode="csv-xml-properties" select="@*" />
                </item>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="normalize-space(data(.))"/>
                <xsl:apply-templates mode="csv-xml-properties" select="@*" />
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="text()" mode="csv-xml-properties">
        <xsl:value-of select="normalize-space(data(.))"/>
    </xsl:template>
    
    <xsl:template match="@*" mode="csv-xml-properties">
        <xsl:if test="not($ignore-attributes = 'true')">
            <attr name="{name()}"><xsl:value-of select="data(.)"/></attr>
        </xsl:if>
    </xsl:template>
    
    
    
    
    
    
    <xsl:template match="*:cell" mode="cleanup">
        <xsl:copy>
            <xsl:apply-templates select="@*" mode="cleanup"/>
            <xsl:apply-templates select="text()" mode="cleanup"/>
            <xsl:choose>
                <xsl:when test="*:item">
                    <xsl:for-each select="*:item">
                        <xsl:apply-templates select="." mode="cleanup" />
                        <xsl:if test="following-sibling::element()"><xsl:text>, </xsl:text></xsl:if>
                    </xsl:for-each>
                </xsl:when>
                <xsl:when test="*:attr">
                    <xsl:text> (</xsl:text>
                    <xsl:for-each select="*:attr">
                        <xsl:apply-templates select="." mode="cleanup"/>
                    </xsl:for-each>
                    <xsl:text>)</xsl:text>
                </xsl:when>
                <!--<xsl:otherwise>
                    <xsl:value-of select="data(.)"/>
                </xsl:otherwise>-->
            </xsl:choose>
        </xsl:copy>
    </xsl:template>
    
    <xsl:template match="*:item" mode="cleanup">
        <xsl:text>$</xsl:text>
        <xsl:value-of select="count(preceding-sibling::element())+1"/>
        <xsl:value-of select="normalize-space(text())"/>
        <xsl:if test="*:attr">
            <xsl:text> (</xsl:text>
            <xsl:for-each select="*:attr">
                <xsl:apply-templates select="." mode="cleanup"/>
                
            </xsl:for-each>
            <xsl:text>)</xsl:text>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="*:attr" mode="cleanup">
        <xsl:text>@</xsl:text>
        <xsl:value-of select="data(@name)"/>
        <xsl:text>=</xsl:text>
        <xsl:value-of select="normalize-space(data(.))"/>
        <xsl:if test="following-sibling::element()"><xsl:text>, </xsl:text></xsl:if>
    </xsl:template>
    
    <xsl:template match="node()|@*" mode="cleanup">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()" mode="cleanup" />
        </xsl:copy>
    </xsl:template>
    
    <xsl:template match="*:csv" mode="csv">
        <xsl:variable name="terms" select="distinct-values(root()//*:cell/data(@name))"></xsl:variable>
        <xsl:text>&#10;</xsl:text>
        <xsl:for-each select="$terms">
            <xsl:value-of select="."/>
            <xsl:value-of select="$trenner"/>
        </xsl:for-each>
        <xsl:apply-templates mode="csv" />
    </xsl:template>
    
    <xsl:template match="*:row" mode="csv">
        <xsl:variable name="children" select="*:cell"/>
        <xsl:variable name="terms" select="distinct-values(root()//*:cell/data(@name))"></xsl:variable>
        <xsl:text>&#10;</xsl:text>
        <xsl:for-each select="$terms">
            <xsl:variable name="term" select="."/>
            <xsl:apply-templates mode="csv" select="$children[@name = $term]"/>
            <xsl:text></xsl:text>
            <xsl:value-of select="$trenner"/>
        </xsl:for-each>
        
    </xsl:template>
    
    <xsl:template match="*:cell[not(starts-with(@name, '_'))]" mode="csv">
        <xsl:variable name="pPat">"</xsl:variable>
        <xsl:value-of select="replace(text(), $pPat, '\\')"/>
    </xsl:template>
    
    <xsl:template match="text()[normalize-space(.) = '']" mode="csv"></xsl:template>
    
    
</xsl:stylesheet>