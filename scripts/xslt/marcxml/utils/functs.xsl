<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:marc="http://www.loc.gov/MARC21/slim"
    xmlns:geo="http://www.opengis.net/ont/geosparql#"
    xmlns:gndo="https://d-nb.info/standards/elementset/gnd#"
    xpath-default-namespace="https://sub.uni-goettingen.de/met/standards/entity-xml#"
    exclude-result-prefixes="xs"
    version="3.0">
    
    <xsl:function name="marc:controllfield-008">
        <xsl:param name="record" />
        <xsl:variable name="creation-date">
            <xsl:choose>
                <xsl:when test="$record/revision/change">
                    <xsl:value-of select="format-date(xs:date(data($record/revision/change[last()]/@when)), '[Y01]-[M01]-[D01]')"/>
                </xsl:when>
                <xsl:when test="$record/ancestor::collection/metadata/revision/change">
                    <xsl:value-of select="format-date(xs:date(data($record/ancestor::collection/metadata/revision[1]/change[last()]/@when)), '[Y01]-[M01]-[D01]')"/>
                </xsl:when>
                <xsl:otherwise><xsl:value-of select="xs:string(format-date(current-date(), '[Y01]-[M01]-[D01]'))"/></xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="p00-05">                            <!-- 00-05 = Datum der Ersterfassung im Format JJMMTT -->
            <xsl:value-of select="translate($creation-date, '-', '')"/>
        </xsl:variable>              
        <xsl:variable name="p06">n</xsl:variable>               <!-- 06 Direkte oder indirekte geografische Unterteilung: n = nicht anwendbar -->
        <xsl:variable name="p07">|</xsl:variable>               <!-- 07 Umschriftsstandard: | = keine Angabe -->
        <xsl:variable name="p08">|</xsl:variable>               <!-- 08 Sprache des Katalogs: | = keine Angabe -->
        <xsl:variable name="p09">a</xsl:variable>               <!-- 09 Art des Datensatzes: a = default, b = Hinweissatz -->
        <xsl:variable name="p10">z</xsl:variable>               <!-- 10 Formalerschließungsregeln: z = andere -->
        <xsl:variable name="p11">z</xsl:variable>               <!-- 11 Schlagwortkatalog/Thesaurus: z = andere, n = nicht anwendbar -->
        <xsl:variable name="p12">n</xsl:variable>               <!-- 12 Art des Gesamttitels: n = nicht anwendbar -->
        <xsl:variable name="p13">n</xsl:variable>               <!-- 13 gezählter oder ungezählter Gesamttitels: n = nicht anwendbar -->
        <xsl:variable name="p14">a</xsl:variable>               <!-- 14 Verwendung der Ansetzung - Haupt- oder Nebeneintragung: a = geeignet, b = nicht geeignet -->
        <xsl:variable name="p15">|</xsl:variable>               <!-- 15 Verwendung der Anseztung - Nebeneintragung unter einem Schlagwortkatalog/Thesaurus: a = wenn als Schlagwort verwendet, b = wenn nicht als Schlagwort verwendet, | = keine Angabe möglich -->
        <xsl:variable name="p16">n</xsl:variable>               <!-- 16 Verwendung der Ansetzung - Nebeneintragung unter dem Gesamttitels: n = nicht anwendbar -->
        <xsl:variable name="p17">n</xsl:variable>               <!-- 17 Art der Schlagwortunterteilung: n = nicht anwendbar -->
        <xsl:variable name="p18-27" xml:space="preserve">          </xsl:variable>  <!-- nicht definiert - 10 Leerzeichen -->
        <xsl:variable name="p28" xml:space="preserve"> </xsl:variable>  <!-- 28 Art der amtlichen Agentur: o amtliche Agentur - Art unbestimmt, Leerzeichen = keine amtliche Agentur (Default-Wert ist das Leerzeichen) -->
        <xsl:variable name="p29">|</xsl:variable>               <!-- 29 Verweisungsbewertung: | = keine Angabe -->
        <xsl:variable name="p30" xml:space="preserve"> </xsl:variable>               <!-- nicht definiert - 1 Leerzeichen -->
        <xsl:variable name="p31">a</xsl:variable>               <!-- 31 Datensatz in Bearbeitung: a = Datensatz kann verwendet werden -->
        <xsl:variable name="p32">                               <!-- 32 Nicht individualisierter Personenname: n = default, a = Individualisierter Personenname, b = nicht-individulaisierter Personenname -->
            <xsl:choose>
                <xsl:when test="$record[self::person]">a</xsl:when>
                <xsl:otherwise>n</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="p33">n</xsl:variable>               <!-- 33 Level der Normierung: a = vollständige Normierung, c = provisorisch, n = nicht anwendbar -->
        <xsl:variable name="p34-37" xml:space="preserve">    </xsl:variable>  <!-- nicht definiert - 4 Leerzeichen -->
        <xsl:variable name="p38">|</xsl:variable>               <!-- 38 Modifizierter Datensatz: | = keine Angabe -->
        <xsl:variable name="p39">u</xsl:variable>               <!-- 39 Katalogisierungsquelle: c = Kooperatives Katalogisierungsprogramm, d = other, u = unknown -->
        <xsl:value-of select="concat(
            $p00-05, 
            $p06, 
            $p07,
            $p08, 
            $p09, 
            $p10, 
            $p11, 
            $p12, 
            $p13, 
            $p14, 
            $p15,
            $p16,
            $p17,
            $p18-27,
            $p28,
            $p29,
            $p30,
            $p31,
            $p32,
            $p33,
            $p34-37,
            $p38,
            $p39)"/>
    </xsl:function>
    
    <xsl:function name="marc:record-leader">
        <xsl:param name="record" />
        <xsl:variable name="p00-04">00000</xsl:variable>                    <!-- 00-04 = Länge des Datensatzes - 00000 -->
        <xsl:variable name="p05">                                           <!-- 05 = Status des Datensatzes -->
            <xsl:choose>
                <xsl:when test="$record/@agency = 'add'">n</xsl:when>
                <!-- Umlenkung "c" implementieren -->
                <xsl:when test="$record/@agency = 'remove'">d</xsl:when>
                <xsl:otherwise>|</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="p06">z</xsl:variable>                           <!-- 06 = Art des Datensatzes -->
        <xsl:variable name="p07-08" xml:space="preserve">  </xsl:variable>  <!-- 07-08 = nicht definiert - immer 00 -->
        <xsl:variable name="p09">a</xsl:variable>                           <!-- 09 = Zeichenkodierungsschema UTF-8 -->
        <xsl:variable name="p10">2</xsl:variable>                           <!-- 10 = Indikatorzähler - immer 2 -->
        <xsl:variable name="p11">2</xsl:variable>                           <!-- 11 = Unterfeldcode-Länge - immer 2 -->
        <xsl:variable name="p12-16">00000</xsl:variable>                    <!-- 12-16 = Datenanfangsadresse-Länge - immer 2 -->
        <xsl:variable name="p17">n</xsl:variable>                           <!-- 17 = Vollständigkeit des Datensatzes -->
        <xsl:variable name="p18">c</xsl:variable>                           <!-- 18 = Interpunktion -->
        <xsl:variable name="p19" xml:space="preserve"> </xsl:variable>      <!-- 19 = nicht definiert - immer Leerzeichen -->
        <xsl:variable name="p20">4</xsl:variable>                           <!-- 20 = Länge des Feldlängenabschnitts - immer 4 -->
        <xsl:variable name="p21">5</xsl:variable>                           <!-- 21 = Länge des Zeichenanfangspositionsabeschnittes - immer 5 -->
        <xsl:variable name="p22">0</xsl:variable>                           <!-- 22 = Lände des anwendungsdefinierten Abschnittes - immer 0 -->
        <xsl:variable name="p23">0</xsl:variable>                           <!-- 23 = nicht definiert - immer 0 -->
        <xsl:value-of select="concat(
            $p00-04, 
            $p05, 
            $p06, 
            $p07-08, 
            $p09, 
            $p10, 
            $p11, 
            $p12-16,
            $p17,
            $p18,
            $p19,
            $p20,
            $p21,
            $p22,
            $p23)"/>
    </xsl:function>
    
    <xsl:function name="geo:dec2gms">
        <xsl:param name="dec"/>
        <xsl:variable name="tokens" select="tokenize($dec, '\.')" />
        <xsl:variable name="grad" select="xs:int($tokens[1])" />
        <xsl:variable name="nachkomma" select="xs:float('0.'||$tokens[2])" />
        <xsl:variable name="x" select="$nachkomma*60"/>
        <xsl:variable name="minuten" select="xs:int(tokenize(xs:string($x), '\.')[1])"/>
        <xsl:variable name="sekunden" select="format-number(($x - $minuten)*60, '00.#')"/>
        <xsl:variable name="result">
            <xsl:value-of select="format-number($grad, '000')"/>
            <xsl:text> </xsl:text>
            <xsl:value-of select="format-number($minuten, '00')"/>
            <xsl:text> </xsl:text>
            <xsl:value-of select="$sekunden"/>
        </xsl:variable>
        <xsl:value-of select="string-join($result)"/>
    </xsl:function>
    
    <xsl:function name="gndo:get-entityfacts">
        <xsl:param name="uri" />
        <xsl:variable name="id" select="tokenize($uri, '/')[last()]"/>
        <xsl:variable name="data" select="unparsed-text('https://hub.culturegraph.org/entityfacts/'||$id)"/>
        <xsl:copy-of select="parse-json($data)"/>
    </xsl:function>
    
    <xsl:function name="gndo:types-from-mapping">
        <xsl:param name="mapping" />
        <xsl:param name="record-type" />
        <xsl:variable name="mapping-term" select="$mapping//*:term[@type = $record-type]"/>
        <xsl:variable name="tokens" select="tokenize($mapping-term/text(), ' ')"/>
        <xsl:for-each select="$tokens">
            <xsl:value-of select="."/>
        </xsl:for-each>
    </xsl:function>
    
</xsl:stylesheet>