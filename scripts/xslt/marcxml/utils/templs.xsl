<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:gndo="https://d-nb.info/standards/elementset/gnd#"
    xmlns="http://www.loc.gov/MARC21/slim"
    exclude-result-prefixes="xs gndo"
    version="2.0">
    
    <xsl:template name="gndo:ref">
        <xsl:if test="@gndo:ref">
            <xsl:variable name="ref" select="data(@gndo:ref)"/>
            <xsl:variable name="de-588" select="substring-after($ref, '/gnd/')" />
            <!--
                <xsl:variable name="de-101" select="format-number(xs:int(replace($de-588, '\-', '')), '000000000')" />
                <subfield code="0">(DE-101)<xsl:value-of select="$de-101"/></subfield>
            -->
            <subfield code="0">(DE-588)<xsl:value-of select="$de-588"/></subfield>
            <subfield code="0"><xsl:value-of select="$ref"/></subfield>
        </xsl:if>
    </xsl:template>
    
</xsl:stylesheet>