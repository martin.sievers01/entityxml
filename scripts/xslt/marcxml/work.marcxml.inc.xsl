<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:geo="http://www.opengis.net/ont/geosparql#"
    xmlns:gndo="https://d-nb.info/standards/elementset/gnd#"
    xmlns:foaf="http://xmlns.com/foaf/0.1/"
    xmlns:marc="http://www.loc.gov/MARC21/slim"
    xmlns:exml="https://sub.uni-goettingen.de/met/standards/entity-xml#"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns="http://www.loc.gov/MARC21/slim"
    exclude-result-prefixes="xs geo gndo exml marc foaf" 
    xpath-default-namespace="https://sub.uni-goettingen.de/met/standards/entity-xml#"
    version="2.0">
    
    
    
    <!-- 
        INCLUDE Module (not standalone): Work Records 2 Marc-XML 
        **********************************************************
        - uses external functions from /utils/functs.xsl
    -->
    
    <xsl:template match="exml:work/gndo:preferredName">
        <xsl:variable name="ind1"><xsl:text>1</xsl:text></xsl:variable>
        <datafield tag="100" ind1="{$ind1}" ind2=" ">
            <xsl:choose>
                <xsl:when test="$requests = true()">
                    <xsl:try>
                        <xsl:if test="parent::element()/gndo:firstAuthor[@gndo:ref]">
                            <xsl:variable name="author" select="gndo:get-entityfacts(data(parent::element()/gndo:firstAuthor/@gndo:ref))"/>
                            <xsl:variable name="dob" select="tokenize($author?dateOfBirth, ' ')[last()]"/>
                            <xsl:variable name="dod" select="tokenize($author?dateOfDeath, ' ')[last()]"/>
                            <subfield code="a">
                                <xsl:value-of select="$author?preferredName"/>
                            </subfield>
                            <subfield code="d"><xsl:value-of select="$dob"/><xsl:text>-</xsl:text><xsl:value-of select="$dod"/></subfield>
                        </xsl:if>
                        <xsl:catch>
                            <subfield code="a">
                                <xsl:value-of select="normalize-space(parent::element()/gndo:firstAuthor/text())"/>
                            </subfield>
                        </xsl:catch>
                    </xsl:try>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:if test="parent::element()/gndo:firstAuthor">
                        <subfield code="a">
                            <xsl:value-of select="normalize-space(parent::element()/gndo:firstAuthor/text())"/>
                        </subfield>
                    </xsl:if>
                </xsl:otherwise>
            </xsl:choose>
            <subfield code="t"><xsl:value-of select="normalize-space(text())"/></subfield>
        </datafield>
    </xsl:template>
    
</xsl:stylesheet>