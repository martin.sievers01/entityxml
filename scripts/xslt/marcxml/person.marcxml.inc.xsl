<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:geo="http://www.opengis.net/ont/geosparql#"
    xmlns:gndo="https://d-nb.info/standards/elementset/gnd#"
    xmlns:foaf="http://xmlns.com/foaf/0.1/"
    xmlns:marc="http://www.loc.gov/MARC21/slim"
    xmlns:exml="https://sub.uni-goettingen.de/met/standards/entity-xml#"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns="http://www.loc.gov/MARC21/slim"
    exclude-result-prefixes="xs geo gndo exml marc foaf" 
    xpath-default-namespace="https://sub.uni-goettingen.de/met/standards/entity-xml#"
    version="2.0">
    
    <!--<xsl:include href="utils/functs.xsl"/>
    <xsl:include href="utils/templs.xsl"/>
    <xsl:include href="utils/mappings.xsl"/>-->
    
    <!-- 
        INCLUDE Module (not standalone): Person Records 2 Marc-XML 
        **********************************************************
        - uses external named templated from /utils/templs.xsl
    -->
    
    <!--
        Templates
        ########################################
    -->
    <xsl:template match="exml:person" mode="person">
        <record type="Authority" xmlns="http://www.loc.gov/MARC21/slim">
            <xsl:call-template name="record-leader" />
            <xsl:apply-templates select="@*"/>
            <xsl:variable name="types" select="gndo:record-types($gndo:record-types, 'person', ./@gndo:type)"/>
            <xsl:for-each select="$types">
                <xsl:variable name="tokens" select="tokenize(., ':')"/>
                <datafield tag="075" ind1=" " ind2=" " xmlns="http://www.loc.gov/MARC21/slim">
                    <subfield code="b"><xsl:value-of select="$tokens[2]"/></subfield>
                    <subfield code="2"><xsl:value-of select="$tokens[1]"/></subfield>
                </datafield>
            </xsl:for-each>
            <xsl:apply-templates select="node()[not(self::gndo:dateOfBirth or self::gndo:dateOfDeath)]"/>
            <xsl:if test="gndo:dateOfBirth[@iso-date] or gndo:dateOfDeath[@iso-date]">
                <xsl:call-template name="dateOfBirthAndDeath.standard" />
                <xsl:call-template name="dateOfBirthAndDeath.exact" />
            </xsl:if>
            <!-- Tp Datensätze erfordern eine Ländercodeangabe. Wenn keine vorhanden ist, dann wird der Code "ZZ" als Platzhalter gesetzt. -->
            <xsl:if test="not(gndo:geographicAreaCode)">
                <datafield tag="043" ind1=" " ind2=" " xmlns="http://www.loc.gov/MARC21/slim">
                    <subfield code="c">ZZ</subfield>
                </datafield>
            </xsl:if>
        </record>
    </xsl:template>
    
    <xsl:template match="exml:person/gndo:academicDegree">
        <datafield tag="550" ind1=" " ind2=" " xmlns="http://www.loc.gov/MARC21/slim">
            <subfield code="a"><xsl:value-of select="normalize-space(text())"/></subfield>
            <subfield code="4">akad</subfield>
            <subfield code="4">https://d-nb.info/standards/elementset/gnd#academicDegree</subfield>
            <subfield code="w">r</subfield>
            <subfield code="i">Akademischer Grad</subfield>
        </datafield>
    </xsl:template>
    
    <xsl:template match="exml:person/gndo:affiliation">
        <datafield tag="510" ind1=" " ind2=" " xmlns="http://www.loc.gov/MARC21/slim">
            <xsl:call-template name="gndo:ref" />
            <subfield code="a"><xsl:value-of select="normalize-space(text())"/></subfield>
            <subfield code="4">affi</subfield>
            <subfield code="4">https://d-nb.info/standards/elementset/gnd#affiliation</subfield>
            <subfield code="w">r</subfield>
            <subfield code="i">Affiliation</subfield>
            <subfield code="e">Affiliation</subfield>
            <!--<subfield code="9"></subfield>-->
        </datafield>
    </xsl:template>
    
    <xsl:template match="exml:person/gndo:fieldOfStudy">
        <datafield tag="550" ind1=" " ind2=" " xmlns="http://www.loc.gov/MARC21/slim">
            <xsl:call-template name="gndo:ref" />
            <subfield code="a"><xsl:value-of select="normalize-space(text())"/></subfield>
            <subfield code="4">stud</subfield>
            <subfield code="4">https://d-nb.info/standards/elementset/gnd#fieldOfStudy</subfield>
            <subfield code="w">r</subfield>
            <subfield code="i">Studienfach</subfield>
        </datafield>
    </xsl:template>
    
    <xsl:template match="exml:person/gndo:functionOrRole">
        <datafield tag="550" ind1=" " ind2=" " xmlns="http://www.loc.gov/MARC21/slim">
            <xsl:call-template name="gndo:ref" />
            <subfield code="a"><xsl:value-of select="normalize-space(text())"/></subfield>
            <subfield code="4">funk</subfield>
            <subfield code="4">https://d-nb.info/standards/elementset/gnd#functionOrRole</subfield>
            <subfield code="w">r</subfield>
            <subfield code="i">Funktion oder Rolle</subfield>
        </datafield>
    </xsl:template>
    
    <xsl:template match="exml:person/gndo:gender[@gndo:term]">
        <xsl:variable name="gender" select="tokenize(data(@gndo:term), ' ')"/>
        <xsl:variable name="codes">
            <gender>
                <xsl:for-each select="$gender">
                    <code>
                        <xsl:choose>
                            <xsl:when test=". = 'https://d-nb.info/standards/vocab/gnd/gender#female'">2</xsl:when>
                            <xsl:when test=". = 'https://d-nb.info/standards/vocab/gnd/gender#male'">1</xsl:when>
                            <xsl:when test=". = 'https://d-nb.info/standards/vocab/gnd/gender#notKnown'">0</xsl:when>
                            <xsl:otherwise>9</xsl:otherwise>
                        </xsl:choose>
                    </code>
                </xsl:for-each>
            </gender>
        </xsl:variable>
        <datafield tag="375" ind1=" " ind2=" " xmlns="http://www.loc.gov/MARC21/slim">
            <subfield code="a"><xsl:value-of select="string-join($codes//*:code/text(), ';')"/></subfield>
            <subfield code="2">iso5218</subfield>
        </datafield>
    </xsl:template>
    
    <xsl:template match="exml:person/gndo:periodOfActivity">
        <xsl:variable name="from" select="normalize-space(data(@iso-from))"/>
        <xsl:variable name="to" select="normalize-space(data(@iso-to))"/>
        <datafield tag="548" ind1=" " ind2=" " xmlns="http://www.loc.gov/MARC21/slim">
            <subfield code="a"><xsl:value-of select="$from"/><xsl:text>-</xsl:text><xsl:value-of select="$to"/></subfield>
            <subfield code="4">datw</subfield>
            <subfield code="4">https://d-nb.info/standards/elementset/gnd#periodOfActivity</subfield>
            <subfield code="w">r</subfield>
            <subfield code="i">Wirkungszeitraum</subfield>
        </datafield>
    </xsl:template>
    
    <xsl:template match="exml:person/gndo:placeOfActivity">
        <datafield tag="551" ind1=" " ind2=" " xmlns="http://www.loc.gov/MARC21/slim">
            <xsl:call-template name="gndo:ref" />
            <subfield code="a"><xsl:value-of select="normalize-space(text())"/></subfield>
            <subfield code="4">ortg</subfield>
            <subfield code="4">https://d-nb.info/standards/elementset/gnd#placeOfBirth</subfield>
            <subfield code="w">r</subfield>
            <subfield code="i">Wirkungsort</subfield>
        </datafield>
    </xsl:template>
    
    <xsl:template match="exml:person/gndo:placeOfBirth">
        <datafield tag="551" ind1=" " ind2=" " xmlns="http://www.loc.gov/MARC21/slim">
            <xsl:call-template name="gndo:ref" />
            <subfield code="a"><xsl:value-of select="normalize-space(text())"/></subfield>
            <subfield code="4">ortg</subfield>
            <subfield code="4">https://d-nb.info/standards/elementset/gnd#placeOfBirth</subfield>
            <subfield code="w">r</subfield>
            <subfield code="i">Geburtsort</subfield>
        </datafield>
    </xsl:template>
    
    <xsl:template match="exml:person/gndo:placeOfDeath">
        <datafield tag="551" ind1=" " ind2=" " xmlns="http://www.loc.gov/MARC21/slim">
            <xsl:call-template name="gndo:ref" />
            <subfield code="a"><xsl:value-of select="normalize-space(text())"/></subfield>
            <subfield code="4">orts</subfield>
            <subfield code="4">https://d-nb.info/standards/elementset/gnd#placeOfDeath</subfield>
            <subfield code="w">r</subfield>
            <subfield code="i">Sterbeort</subfield>
        </datafield>
    </xsl:template>
    
    <xsl:template match="exml:person/gndo:placeOfExile">
        <datafield tag="551" ind1=" " ind2=" " xmlns="http://www.loc.gov/MARC21/slim">
            <xsl:call-template name="gndo:ref" />
            <subfield code="a"><xsl:value-of select="normalize-space(text())"/></subfield>
            <subfield code="4">ortx</subfield>
            <subfield code="4">https://d-nb.info/standards/elementset/gnd#placeOfExile</subfield>
            <subfield code="w">r</subfield>
            <subfield code="i">Exil</subfield>
        </datafield>
    </xsl:template>
    
    <xsl:template match="exml:person/gndo:preferredName">
        <datafield tag="100" ind1="1" ind2=" " xmlns="http://www.loc.gov/MARC21/slim">
            <subfield code="a">
                <xsl:call-template name="person-name"/>
            </subfield>
            <xsl:apply-templates select="gndo:*[not(name() = ('gndo:personalName', 'gndo:forename', 'gndo:surname', 'gndo:prefix'))]" />
            <xsl:if test="parent::node()/gndo:dateOfBirth[@iso-date] or parent::node()/gndo:dateOfDeath[@iso-date]">
                <subfield code="d"><xsl:call-template name="person-dates-of-living" /></subfield>
            </xsl:if>
        </datafield>
    </xsl:template>
    
    <xsl:template match="exml:person/gndo:professionOrOccupation">
        <datafield tag="550" ind1=" " ind2=" " xmlns="http://www.loc.gov/MARC21/slim">
            <xsl:call-template name="gndo:ref" />
            <subfield code="a"><xsl:value-of select="normalize-space(text())"/></subfield>
            <subfield code="4">
                <xsl:choose>
                    <xsl:when test="@gndo:type='significant'">berc</xsl:when>
                    <xsl:otherwise>beru</xsl:otherwise>
                </xsl:choose>
            </subfield>
            <subfield code="4">https://d-nb.info/standards/elementset/gnd#professionOrOccupation</subfield>
            <subfield code="w">r</subfield>
            <subfield code="i">
                <xsl:choose>
                    <xsl:when test="@gndo:type='significant'">Charakteristischer Beruf</xsl:when>
                    <xsl:otherwise>Beruf</xsl:otherwise>
                </xsl:choose>
            </subfield>
        </datafield>
    </xsl:template>
    
   
    <!-- 
        ACHTUNG!!!! 
        ###########
        `gndo:publication` ist hochgradig experimentell. Mal schauen, ob man das nicht einfacher lösen kann -->
    <!--
   <xsl:template match="exml:person/gndo:publication">
        <!-\- IMPLEMENTED but spooky! -\->
        <xsl:variable name="title" select="normalize-space(exml:title/text())"/>
        <xsl:variable name="date" select="normalize-space(exml:date/text())"/>
        <datafield tag="672" ind1=" " ind2=" " xmlns="http://www.loc.gov/MARC21/slim">
            <xsl:call-template name="gndo:ref" />
            <xsl:choose>
                <xsl:when test="@editor='true'">
                    <subfield code="a">Hrsg. von: <xsl:apply-templates select="exml:title" mode="publication-elements"/></subfield>
                </xsl:when>
                <xsl:otherwise>
                    <subfield code="a"><xsl:apply-templates select="exml:author|exml:title" mode="publication-elements"/></subfield>
                </xsl:otherwise>
            </xsl:choose>
            <xsl:apply-templates select="exml:date" mode="publication-elements" />
        </datafield>
    </xsl:template>-->
    
    
    <xsl:template match="exml:person/gndo:publication[not(@role) or @role ='author']">
        <xsl:variable name="title" select="normalize-space(exml:title/text())"/>
        <xsl:variable name="date" select="normalize-space(exml:date/text())"/>
        <datafield tag="672" ind1=" " ind2=" " xmlns="http://www.loc.gov/MARC21/slim">
            <xsl:call-template name="gndo:ref" />
            <subfield code="a"><xsl:apply-templates select="exml:title" mode="publication-elements"/></subfield>
            <xsl:apply-templates select="exml:date" mode="publication-elements" />
        </datafield>
    </xsl:template>
    
    <xsl:template match="exml:person/gndo:publication[@role ='editor']">
        <xsl:variable name="title" select="normalize-space(exml:title/text())"/>
        <xsl:variable name="date" select="normalize-space(exml:date/text())"/>
        <datafield tag="672" ind1=" " ind2=" " xmlns="http://www.loc.gov/MARC21/slim">
            <xsl:call-template name="gndo:ref" />
            <subfield code="a"><xsl:text>Hrsg. von: </xsl:text><xsl:apply-templates select="exml:title" mode="publication-elements"/></subfield>
            <xsl:apply-templates select="exml:date" mode="publication-elements" />
        </datafield>
    </xsl:template>
    
    <xsl:template match="exml:person/gndo:publication[@role ='about'][exml:author or gndo:firstAuthor]">
        <xsl:variable name="title" select="normalize-space(exml:title/text())"/>
        <xsl:variable name="date" select="normalize-space(exml:date/text())"/>
        <datafield tag="672" ind1=" " ind2=" " xmlns="http://www.loc.gov/MARC21/slim">
            <xsl:call-template name="gndo:ref" />
            <subfield code="a"><xsl:apply-templates select="exml:author|gndo:firstAuthor|exml:title" mode="publication-elements"/></subfield>
            <xsl:apply-templates select="exml:date" mode="publication-elements" />
        </datafield>
    </xsl:template>
    
    <xsl:template match="exml:person/gndo:publication/exml:title" mode="publication-elements">
        <xsl:value-of select="normalize-space(text())"/>
    </xsl:template>
    
    <xsl:template match="exml:person/gndo:publication/exml:author | exml:person/gndo:publication/gndo:firstAuthor" mode="publication-elements">
        <xsl:value-of select="normalize-space(text())"/>
        <xsl:text>: </xsl:text>
    </xsl:template>
    
    <xsl:template match="exml:person/gndo:publication/exml:date" mode="publication-elements">
        <subfield code="f" xmlns="http://www.loc.gov/MARC21/slim"><xsl:value-of select="normalize-space(text())"/></subfield>
    </xsl:template>
    
    <!-- ########### ACHTUNG!!!! ENDE ##########-->
        
    <xsl:template match="exml:person/gndo:pseudonym">
        <datafield tag="500" ind1="1" ind2=" " xmlns="http://www.loc.gov/MARC21/slim">
            <xsl:call-template name="gndo:ref" />
            <subfield code="a"><xsl:value-of select="normalize-space(text())"/></subfield>
            <subfield code="4">pseu</subfield>
            <subfield code="4">https://d-nb.info/standards/elementset/gnd#pseudonym</subfield>
            <subfield code="w">r</subfield>
            <subfield code="i">Pseudonym</subfield>
            <subfield code="e">Pseudonym</subfield>
        </datafield>
    </xsl:template>
    
    <xsl:template match="exml:person/gndo:titleOfNobility">
        <datafield tag="550" ind1=" " ind2=" " xmlns="http://www.loc.gov/MARC21/slim">
            <xsl:call-template name="gndo:ref" />
            <subfield code="a"><xsl:value-of select="normalize-space(text())"/></subfield>
            <subfield code="4">adel</subfield>
            <subfield code="4">https://d-nb.info/standards/elementset/gnd#titleOfNobility</subfield>
            <subfield code="w">r</subfield>
            <subfield code="i">Adelstitel</subfield>
        </datafield>
    </xsl:template>
    
    <xsl:template match="exml:person/gndo:variantName">
        <datafield tag="400" ind1="1" ind2=" " xmlns="http://www.loc.gov/MARC21/slim">
            <subfield code="a">
                <xsl:call-template name="person-name"/>
            </subfield>
            <xsl:apply-templates select="gndo:*[not(name() = ('gndo:personalName', 'gndo:forename', 'gndo:surname', 'gndo:prefix', 'gndo:nameAddition', 'gndo:epithetGenericNameTitleOrTerritory'))]" />
            <xsl:call-template name="additions-and-titles" />
            <xsl:if test="parent::node()/gndo:dateOfBirth[@iso-date] or parent::node()/gndo:dateOfDeath[@iso-date]">
                <subfield code="d"><xsl:call-template name="person-dates-of-living" /></subfield>
            </xsl:if>
        </datafield>
    </xsl:template>
    
    
    <!-- 
        Name Parts
        *****************************************
    -->
    
    <xsl:template match="exml:person//gndo:surname | exml:person//gndo:personalName">
        <xsl:value-of select="normalize-space(./text())"/>
    </xsl:template>
    
    <xsl:template match="exml:person//gndo:forename">
        <xsl:text>, </xsl:text>
        <xsl:value-of select="normalize-space(./text())"/>
    </xsl:template>
    
    <xsl:template match="exml:person//gndo:prefix">
        <xsl:text> &#152;</xsl:text>
        <xsl:value-of select="normalize-space(./text())"/>
        <xsl:text>&#156;</xsl:text>
    </xsl:template>
    
    <xsl:template match="exml:person//gndo:counting">
        <xsl:variable name="value" select="normalize-space(./text())"/>
        <subfield code="b" xmlns="http://www.loc.gov/MARC21/slim">
            <xsl:value-of select="$value"/>
            <xsl:choose>
                <xsl:when test="matches($value, '[IVXLCDMA0-9]+?') and not(substring($value, string-length($value), 1)='.')">
                    <xsl:text>.</xsl:text>
                </xsl:when>
                <xsl:otherwise/>
            </xsl:choose>
        </subfield>
    </xsl:template>
    
    
    <!-- 
        NAMED Templates
        ##############################################################################################################
    -->
    <xsl:template name="person-name">
        <xsl:choose>
            <xsl:when test="gndo:personalName">
                <xsl:apply-templates select="gndo:personalName"/>
            </xsl:when>
            <xsl:when test="gndo:surname and gndo:forename">
                <xsl:apply-templates select="gndo:surname"></xsl:apply-templates>
                <xsl:apply-templates select="gndo:forename"></xsl:apply-templates>
                <xsl:apply-templates select="gndo:prefix"></xsl:apply-templates>
            </xsl:when>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template name="additions-and-titles">
        <xsl:variable name="items" select="(gndo:nameAddition, gndo:epithetGenericNameTitleOrTerritory)"/>
        <xsl:variable name="values" select="for $item in $items return normalize-space($item/text())"/>
        <subfield code="c" xmlns="http://www.loc.gov/MARC21/slim">
            <xsl:value-of select="string-join($values, '; ')"/>
        </subfield>
    </xsl:template>
    
    <xsl:template name="person-dates-of-living">
        <xsl:if test="parent::node()/gndo:dateOfBirth[@iso-date]"><xsl:value-of select="tokenize(parent::node()/gndo:dateOfBirth/@iso-date, '-')[1]"/></xsl:if><xsl:if test="parent::node()/gndo:dateOfBirth[@iso-date]"><xsl:text>-</xsl:text><xsl:value-of select="tokenize(parent::node()/gndo:dateOfDeath/@iso-date, '-')[1]"/></xsl:if>
    </xsl:template>
    
    <xsl:template name="dateOfBirthAndDeath.standard">
        <datafield tag="548" ind1=" " ind2=" " xmlns="http://www.loc.gov/MARC21/slim">
            <subfield code="a">
                <xsl:value-of select="tokenize(gndo:dateOfBirth/@iso-date, '-')[1]"/><xsl:text>-</xsl:text><xsl:value-of select="tokenize(gndo:dateOfDeath/@iso-date, '-')[1]"/>
            </subfield>
            <subfield code="4">datl</subfield>
            <subfield code="4">https://d-nb.info/standards/elementset/gnd#dateOfBirthAndDeath</subfield>
            <subfield code="w">r</subfield>
            <subfield code="i">Lebensdaten</subfield>
        </datafield>
    </xsl:template>
    
    <xsl:template name="dateOfBirthAndDeath.exact">
        <datafield tag="548" ind1=" " ind2=" " xmlns="http://www.loc.gov/MARC21/slim">
            <subfield code="a">
                <xsl:value-of select="string-join(reverse(tokenize(gndo:dateOfBirth/@iso-date, '-')), '.')"/><xsl:text>-</xsl:text><xsl:value-of select="string-join(reverse(tokenize(gndo:dateOfDeath/@iso-date, '-')), '.')"/>
            </subfield>
            <subfield code="4">datx</subfield>
            <subfield code="4">https://d-nb.info/standards/elementset/gnd#dateOfBirthAndDeath</subfield>
            <subfield code="w">r</subfield>
            <subfield code="i">Exakte Lebensdaten</subfield>
        </datafield>
    </xsl:template>
    
</xsl:stylesheet>