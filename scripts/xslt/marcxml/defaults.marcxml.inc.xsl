<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:geo="http://www.opengis.net/ont/geosparql#"
    xmlns:gndo="https://d-nb.info/standards/elementset/gnd#"
    xmlns:marc="http://www.loc.gov/MARC21/slim"
    xmlns:exml="https://sub.uni-goettingen.de/met/standards/entity-xml#"
    xmlns:foaf="http://xmlns.com/foaf/0.1/"
    xmlns:owl="http://www.w3.org/2002/07/owl#" 
    xmlns:skos="http://www.w3.org/2004/02/skos/core#" 
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="exml foaf geo gndo marc owl skos xs" 
    xpath-default-namespace="https://sub.uni-goettingen.de/met/standards/entity-xml#"
    version="2.0">
    
    <!-- 
        INCLUDE Module (not standalone): General Default Properties 2 Marc-XML 
        **********************************************************************
    -->
    
    <xsl:template match="collection/data/list/element()/@gndo:uri">
        <xsl:variable name="id" select="tokenize(., '/')[last()]"/>
        <datafield tag="024" ind1="7" ind2=" " xmlns="http://www.loc.gov/MARC21/slim">
            <subfield code="a"><xsl:value-of select="$id"/></subfield>
            <subfield code="0"><xsl:value-of select="."/></subfield>
            <subfield code="2">gnd</subfield>
        </datafield>
    </xsl:template>
    
    <xsl:template match="gndo:biographicalOrHistoricalInformation">
        <datafield tag="678" ind1=" " ind2=" " xmlns="http://www.loc.gov/MARC21/slim">
            <subfield code="b"><xsl:value-of select="normalize-space(text())"/></subfield>
        </datafield>
    </xsl:template>
    
    <!-- https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DE-BE -->
    <xsl:template match="gndo:geographicAreaCode[@gndo:term]">    
        <datafield tag="043" ind1=" " ind2=" " xmlns="http://www.loc.gov/MARC21/slim">
            <subfield code="c"><xsl:value-of select="normalize-space(substring-after(@gndo:term, 'geographic-area-code#'))"/></subfield>
        </datafield>
    </xsl:template>
    
    <xsl:template match="gndo:homepage|foaf:page">
        <datafield tag="670" ind1=" " ind2=" " xmlns="http://www.loc.gov/MARC21/slim">
            <subfield code="a">Homepage</subfield>
            <xsl:if test="@iso-date">
                <subfield code="b"><xsl:text>Stand: </xsl:text><xsl:value-of select="data(@iso-date)"/></subfield>
            </xsl:if>
            <subfield code="u"><xsl:value-of select="normalize-space(text())"/></subfield>
        </datafield>
    </xsl:template>
    
    <xsl:template match="gndo:subjectCategory">
        <xsl:variable name="notation" select="substring-after(data(@gndo:term), '#')"/>
        <datafield tag="065" ind1=" " ind2=" " xmlns="http://www.loc.gov/MARC21/slim">
            <subfield code="a"><xsl:value-of select="$notation"/></subfield>
            <subfield code="2">sswd</subfield>
        </datafield>
    </xsl:template>
    
    <xsl:template match="owl:sameAs">
        <xsl:variable name="url" select="text()"/>
        <xsl:variable name="parsed" select="owl:parse-url($url)" />
        <xsl:variable name="type">
            <xsl:choose>
                <xsl:when test="@type"><xsl:value-of select="data(@type)"/></xsl:when>
                <xsl:otherwise><xsl:value-of select="tokenize($parsed, ':')[1]"/></xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="id">
            <xsl:choose>
                <xsl:when test="@xml:id"><xsl:value-of select="data(@xml:id)"/></xsl:when>
                <xsl:otherwise><xsl:value-of select="tokenize($parsed, ':')[2]"/></xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <datafield tag="024" ind1="7" ind2=" " xmlns="http://www.loc.gov/MARC21/slim">
            <xsl:if test="not($id = '')">
                <subfield code="a"><xsl:value-of select="$id"/></subfield>
            </xsl:if>
            <subfield code="0"><xsl:value-of select="$url"/></subfield>
            <xsl:if test="not($type = '')">
                <subfield code="2"><xsl:value-of select="$type"/></subfield>
            </xsl:if>
        </datafield>
    </xsl:template>
    
    <xsl:template match="data//source">
        <datafield tag="670" ind1=" " ind2=" " xmlns="http://www.loc.gov/MARC21/slim">
            <subfield code="a"><xsl:value-of select="normalize-space(text())"/></subfield>
            <xsl:if test="@url">
                <subfield code="u"><xsl:value-of select="normalize-space(data(@url))"/></subfield>
            </xsl:if>
        </datafield>
    </xsl:template>
    
    <xsl:template match="skos:note[not(@gndo:type)]">
        <datafield tag="680" ind1=" " ind2=" " xmlns="http://www.loc.gov/MARC21/slim">
            <subfield code="a"><xsl:value-of select="normalize-space(text())"/></subfield>
        </datafield>
    </xsl:template>
    
    <xsl:template match="skos:note[@gndo:type = 'internal']">
        <xsl:variable name="agency" select="ancestor::collection/metadata/agency"/>
        <datafield tag="667" ind1=" " ind2=" " xmlns="http://www.loc.gov/MARC21/slim">
            <subfield code="a"><xsl:value-of select="normalize-space(text())"/></subfield>
            <xsl:if test="not(empty($agency))">
                <subfield code="u"><xsl:value-of select="normalize-space(data($agency/@isil))"/></subfield>
            </xsl:if>
        </datafield>
    </xsl:template>
    
    
    
    <!-- Funtions used by this module -->
    <xsl:function name="owl:parse-url">
        <xsl:param name="url" />
        <xsl:variable name="data">
            <xsl:choose>
                <xsl:when test="contains($url, 'lccn.loc.gov')">
                    <xsl:text>lccn</xsl:text>
                    <xsl:text>:</xsl:text>
                    <xsl:value-of select="substring-after($url, 'lccn.loc.gov/')"/>
                </xsl:when>
                <xsl:when test="contains($url, 'id.loc.gov/authorities/names')">
                    <xsl:text>lcnaf</xsl:text>
                    <xsl:text>:</xsl:text>
                    <xsl:value-of select="substring-after($url, 'authorities/names/')"/>
                </xsl:when>
                <xsl:when test="contains($url, 'viaf.org')">
                    <xsl:text>viaf</xsl:text>
                    <xsl:text>:</xsl:text>
                    <xsl:value-of select="substring-after($url, 'viaf/')"/>
                </xsl:when>
                <xsl:when test="contains($url, 'orcid.org')">
                    <xsl:text>orcid</xsl:text>
                    <xsl:text>:</xsl:text>
                    <xsl:value-of select="substring-after($url, 'orcid.org/')"/>
                </xsl:when>
                <xsl:when test="contains($url, 'wikidata.org')">
                    <xsl:text>wikidata</xsl:text>
                    <xsl:text>:</xsl:text>
                    <xsl:value-of select="substring-after($url, 'wikidata.org/wiki/')"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:text>:</xsl:text>
                    <xsl:value-of select="tokenize($url, '/')[last()]"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:value-of select="string-join($data)"/>
    </xsl:function>
        
</xsl:stylesheet>