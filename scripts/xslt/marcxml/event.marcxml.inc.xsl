<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:geo="http://www.opengis.net/ont/geosparql#"
    xmlns:gndo="https://d-nb.info/standards/elementset/gnd#"
    xmlns:foaf="http://xmlns.com/foaf/0.1/"
    xmlns:marc="http://www.loc.gov/MARC21/slim"
    xmlns:exml="https://sub.uni-goettingen.de/met/standards/entity-xml#"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs geo gndo exml marc foaf" 
    xpath-default-namespace="https://sub.uni-goettingen.de/met/standards/entity-xml#"
    version="2.0">
    
    <!-- 
        INCLUDE Module (not standalone): Event Records 2 Marc-XML 
        **********************************************************
        - uses external named templated from /utils/templs.xsl
    -->
    
    <xsl:template match="exml:event/@gndo:uri">
        <xsl:variable name="id" select="tokenize(., '/')[last()]"/>
        <datafield tag="024" ind1="7" ind2=" " xmlns="http://www.loc.gov/MARC21/slim">
            <subfield code="a"><xsl:value-of select="$id"/></subfield>
            <subfield code="0"><xsl:value-of select="."/></subfield>
            <subfield code="2">gnd</subfield>
        </datafield>
    </xsl:template>

    
</xsl:stylesheet>