<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:geo="http://www.opengis.net/ont/geosparql#"
    xmlns:gndo="https://d-nb.info/standards/elementset/gnd#"
    xmlns:marc="http://www.loc.gov/MARC21/slim"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns="http://www.loc.gov/MARC21/slim"
    exclude-result-prefixes="xs geo gndo marc" 
    xpath-default-namespace="https://sub.uni-goettingen.de/met/standards/entity-xml#"
    version="3.0">
    
    <!-- Import named template module, global functions module and mapping module -->
    <xsl:import href="utils/functs.xsl"/>
    <xsl:import href="utils/templs.xsl"/>
    <xsl:import href="utils/mappings.xsl"/>
    
    <!-- Include globals conversion modules -->
    <xsl:include href="defaults.marcxml.inc.xsl"/>
    
    <!-- Include all Class conversion modules -->
    <xsl:include href="event.marcxml.inc.xsl"/>
    <xsl:include href="corporateBody.marcxml.inc.xsl"/>
    <xsl:include href="person.marcxml.inc.xsl"/>
    <xsl:include href="place.marcxml.inc.xsl"/>
    <xsl:include href="work.marcxml.inc.xsl"/>
    
    <!-- parameter -->
    <xsl:param name="agency-isil">DE-000</xsl:param> <!-- Pseudo default ISIL -->
    <xsl:param name="records"/> <!-- record agency modes separated by whitespace: 'create', 'update', 'merge' -->
    <xsl:param name="ignore-records" select="true()"/> <!-- records with @agency='ignore' will be ignored when converting without a specified record agency mode -->
    <xsl:param name="base-uri">http://sub.uni-goettingen.de/gnd-agentur/entity/</xsl:param>
    <xsl:param name="requests" select="false()"/>
    
    <xsl:output omit-xml-declaration="yes" indent="yes"/>
    
    
    <!-- 
        INITAL STEP: Convert all collections to MARC-XML 
        #############################################################################
    -->
    <xsl:template match="/">
        <xsl:variable name="conversion">
            <xsl:apply-templates />
        </xsl:variable>
        <xsl:apply-templates select="$conversion" mode="sort-props" />
        <!--<xsl:copy-of select="$conversion"></xsl:copy-of>-->
    </xsl:template>
    
    <xsl:template match="collection">
        <collection xmlns="http://www.loc.gov/MARC21/slim">
            <xsl:choose>
                <xsl:when test="@id">
                    <xsl:attribute name="id" select="data(@id)||'-'||current-dateTime()" />
                </xsl:when>
                <xsl:otherwise>
                    <xsl:attribute name="id" select="data(metadata/provider/@id)||'-'||current-dateTime()" />
                </xsl:otherwise>
            </xsl:choose>
            <xsl:choose>
                <xsl:when test="empty($records) or not(normalize-space($records) = '')">
                    <xsl:apply-templates select="data/list/element()[@agency = tokenize($records)]"/>
                </xsl:when>
                <xsl:when test="$ignore-records = true()">
                    <xsl:apply-templates select="data/list/element()[not(name() = ('title', 'abstract'))][not(@agency = 'ignore')]"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:apply-templates select="data/list/element()[not(name() = ('title', 'abstract'))]"/>
                </xsl:otherwise>
            </xsl:choose>
        </collection>
    </xsl:template>
    
    <xsl:template match="collection/data/list/element()[not(name() = ('abstract', 'title'))]">
        <xsl:choose>
            <!-- Sonderfall: Person -->
            <xsl:when test="self::person">
                <xsl:apply-templates select="." mode="person"/>
            </xsl:when>
            <!-- Default -->
            <xsl:otherwise>
                <record type="Authority" xmlns="http://www.loc.gov/MARC21/slim">
                    <!--<xsl:apply-templates select="ancestor::collection/metadata" mode="leader" />-->
                    <xsl:call-template name="record-leader" />
                    <xsl:variable name="types" select="gndo:record-types($gndo:record-types, ./local-name(), ./@gndo:type)"/>
                    <xsl:apply-templates select="@*"/>
                    <xsl:for-each select="$types">
                        <xsl:variable name="tokens" select="tokenize(., ':')"/>
                        <datafield tag="075" ind1="" ind2=" " xmlns="http://www.loc.gov/MARC21/slim">
                            <subfield code="b"><xsl:value-of select="$tokens[2]"/></subfield>
                            <subfield code="2"><xsl:value-of select="$tokens[1]"/></subfield>
                        </datafield>
                    </xsl:for-each>
                    <xsl:apply-templates select="node()"/>
                </record>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="collection/data/list/element()[not(@gndo:uri)]/@*:id">
        <xsl:variable name="provider-title" select="normalize-space(ancestor::collection/metadata/provider/title/text())"/>
        <xsl:variable name="ind1">
            <xsl:choose>
                <xsl:when test="not($provider-title = '')">7</xsl:when>
                <xsl:otherwise>8</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <datafield tag="024" ind1="{$ind1}" ind2=" " xmlns="http://www.loc.gov/MARC21/slim">
            <subfield code="a"><xsl:value-of select="normalize-space(data(.))"/></subfield>
            <!--<subfield code="0"></subfield>-->
            <xsl:if test="$ind1 = '7'">
                <subfield code="2"><xsl:value-of select="$provider-title"/></subfield>
            </xsl:if>
        </datafield>     
    </xsl:template>
    
    <xsl:template match="node()|@*">
        <xsl:apply-templates select="node()|@*"/>
    </xsl:template>
    
    <xsl:template name="record-leader">
        <leader xmlns="http://www.loc.gov/MARC21/slim"><xsl:value-of select="marc:record-leader(.)"/></leader>
        <controlfield tag="001" xmlns="http://www.loc.gov/MARC21/slim">
            <xsl:choose>
                <xsl:when test=".[@gndo:uri]"><xsl:value-of select="tokenize(data(./@gndo:uri), '/')[last()]"/></xsl:when>
                <xsl:otherwise><xsl:value-of select="data(./@*:id)"/></xsl:otherwise>
            </xsl:choose>
        </controlfield>
        <controlfield tag="003" xmlns="http://www.loc.gov/MARC21/slim">
            <xsl:variable name="agency-metadata" select="(./ancestor::collection)[1]/metadata/agency"/>
            <xsl:choose>
                <xsl:when test="$agency-metadata and $agency-metadata[@isil]">
                    <xsl:value-of select="data($agency-metadata/@isil)"/>
                </xsl:when>
                <xsl:when test="$agency-isil">
                    <xsl:value-of select="$agency-isil"/>
                </xsl:when>
                <xsl:otherwise />
            </xsl:choose>
        </controlfield>
        <controlfield tag="005" xmlns="http://www.loc.gov/MARC21/slim">
            <xsl:value-of select="format-dateTime(current-dateTime(), '[Y0001][M01][D01][H01][m01][s01].0')"/>
        </controlfield>
        <controlfield tag="008" xmlns="http://www.loc.gov/MARC21/slim">
            <xsl:value-of select="marc:controllfield-008(.)"/>
        </controlfield>
    </xsl:template>
    
    
    <!-- 
        END-STEP: Sort all datafields by tag 
        #############################################################################
    -->
    <xsl:template match="/" mode="sort-props">
        <xsl:apply-templates mode="sort-props"/>
    </xsl:template>
    
    <xsl:template match="marc:record" mode="sort-props">
        <xsl:copy>
            <xsl:apply-templates select="@*" mode="sort-props" />
            <xsl:copy-of select="marc:leader" />
            <xsl:apply-templates select="marc:controlfield" mode="sort-props"/>
            <xsl:apply-templates select="marc:datafield" mode="sort-props">
                <xsl:sort select="data(./@tag)" />
            </xsl:apply-templates>
        </xsl:copy>
    </xsl:template>
    
    <xsl:template match="@*|node()" mode="sort-props">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()" mode="sort-props" />
        </xsl:copy>
    </xsl:template>
    
</xsl:stylesheet>