<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:rng="http://relaxng.org/ns/structure/1.0"
    xmlns:output="http://www.w3.org/2010/xslt-xquery-serialization"
    xmlns:es="https://sub.uni-goettingen.de/met/standards/entity-xml-staging#"
    xmlns:utils="https://sub.uni-goettingen.de/met/xslt/functions/entity-xml#"
    exclude-result-prefixes="xs" 
    xpath-default-namespace="https://sub.uni-goettingen.de/met/standards/entity-xml#"
    version="3.0">
    
    <!-- entityXML straight: strips all elements from custom namespaces aka. ready for GND! -->
    
    <xsl:param name="staging" select="true()" />
    <xsl:param name="schema-path" select="'../../schema/entityXML.rng'" />
    
    <xsl:variable name="schema" select="doc($schema-path)"/>
    <xsl:variable name="defined-element-names" select="utils:schema-elements( $schema )"></xsl:variable>
    <xsl:variable name="defined-attribute-names" select="utils:schema-attributes( $schema )" />
    
    <xsl:output method="xml" indent="true" />
    
    <xsl:function name="utils:schema-element-names">
        <xsl:param name="schema" />
        <xsl:for-each select="($schema//rng:element, $schema//rng:attribute)">
            <xsl:value-of select="data(./@name)"/>
        </xsl:for-each>
    </xsl:function>
    
    <xsl:function name="utils:schema-elements">
        <xsl:param name="schema" />
        <xsl:for-each select="$schema//rng:element">
            <xsl:value-of select="data(./@name)"/>
        </xsl:for-each>
    </xsl:function>
    
    <xsl:function name="utils:schema-attributes">
        <xsl:param name="schema" />
        <xsl:for-each select="$schema//rng:attribute">
            <xsl:value-of select="data(./@name)"/>
        </xsl:for-each>
    </xsl:function>
    
    <xsl:template match="/">
        <xsl:choose>
            <xsl:when test="$staging = true()">
                <xsl:apply-templates mode="staging" />
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates mode="default" />
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    
    <!-- DEFAULT MODE: Ignore all elements from custom namespaces -->
    <xsl:template match="@*|node()" mode="default">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()" mode="default" />
        </xsl:copy>
    </xsl:template>
    
    <xsl:template match="element()[not(name() = $defined-element-names)]" mode="default">
        <!--<xsl:apply-templates select="@*|node()" mode="custom-namespaces" />-->
    </xsl:template>
    
    <!-- CUSTOM NAMESPACES Handling -->
    <!--<xsl:template match="@*" mode="custom-namespaces" />
    <xsl:template match="text()" mode="custom-namespaces" />-->
    
    
    <!-- STAGING MODE: If "staging" is active elements from xmlns:es are not ignored -->
    <xsl:template match="element()[self::es:*]" mode="staging">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()" mode="staging"/>
        </xsl:copy>
    </xsl:template>
    
    <xsl:template match="@*|node()" mode="staging">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()" mode="staging" />
        </xsl:copy>
    </xsl:template>
    
    <xsl:template match="element()[not(self::es:*)][not(name() = $defined-element-names)]" mode="staging" />
    
   
    
   
</xsl:stylesheet>