<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:geo="http://www.opengis.net/ont/geosparql#"
    xmlns:gndo="https://d-nb.info/standards/elementset/gnd#"
    xmlns:marc="http://www.loc.gov/MARC21/slim"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs geo gndo marc" 
    xpath-default-namespace="https://sub.uni-goettingen.de/met/standards/entity-xml#"
    version="2.0">
    
    <!--<xsl:output method="xml" indent="yes"/>-->
    <!-- 
        Update Conversion: 0.5.0 > 0.5.1
        ################################
        This XSLT converts entityXML resources from v0.5.0 to v0.5.1
    -->
    
   
    <xsl:template match="/">
        <xsl:apply-templates/>
    </xsl:template>
    
    <!-- 
        Record IDs to @xml:id
        ***************************************************
        - element attribute name change
    -->
    <xsl:template match="list/element()[@id]">
        <xsl:copy>
            <xsl:attribute name="xml:id" select="data(@id)"></xsl:attribute>
            <xsl:apply-templates select="@*[not(name() = 'id')]|node()" />
        </xsl:copy>
    </xsl:template>

    <!-- 
        Person: gndo:dateOfActivity > gndo:periodOfActivity
        ***************************************************
        - element name change 
        - no @iso-date anymore
    -->
    <xsl:template match="gndo:dateOfActivity">
        <gndo:periodOfActivity>
            <xsl:apply-templates select="@*[not(name() = 'iso-date')]" />
            <xsl:apply-templates select="node()[not(self::attribute())]" />
        </gndo:periodOfActivity>
    </xsl:template>
    
    <!-- 
        Person: gndo:page > gndo:homepage
        ***************************************************
        - element name change
    -->
    <xsl:template match="gndo:page">
        <gndo:homepage>
            <xsl:apply-templates select="@*|node()" />
        </gndo:homepage>
    </xsl:template>
    
    <!-- 
        Sort revision entries by dates
        ***************************************************
    -->
    <xsl:template match="revision">
        <xsl:copy>
            <xsl:apply-templates select="@*" />
            <xsl:for-each select="change">
                <xsl:sort select="xs:date(./@when)" order="descending" />
                <xsl:copy-of select="." />
            </xsl:for-each>
        </xsl:copy>
    </xsl:template>
    
    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()" />
        </xsl:copy>
    </xsl:template>
    
</xsl:stylesheet>