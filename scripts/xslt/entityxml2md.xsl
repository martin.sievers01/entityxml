<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:rng="http://relaxng.org/ns/structure/1.0"
    xmlns:tei="http://www.tei-c.org/ns/1.0"
    xmlns:teiex="http://www.tei-c.org/ns/Examples"
    xmlns:app="http://sub.uni-goettingen.de/met/standards/application-profiles/app-info#"
    xmlns:doc="https://sub.uni-goettingen.de/met/formats/simpledoc#"
    xmlns:output="http://www.w3.org/2010/xslt-xquery-serialization"
    xmlns:md="http://markdown/functions"
    xmlns:exml="https://sub.uni-goettingen.de/met/standards/entity-xml#"
    xmlns:dc="http://purl.org/dc/elements/1.1/"
    xmlns:foaf="http://xmlns.com/foaf/0.1/" 
    xmlns:gndo="https://d-nb.info/standards/elementset/gnd#" 
    xmlns:owl="http://www.w3.org/2002/07/owl#"
    xmlns:skos="http://www.w3.org/2004/02/skos/core#" 
    xmlns:geo="http://www.opengis.net/ont/geosparql#" 
    xmlns:dnb="https://www.dnb.de"
    xmlns:wgs84="http://www.w3.org/2003/01/geo/wgs84_pos#"
    xmlns:es="https://sub.uni-goettingen.de/met/standards/entity-xml-staging#"
    xmlns:utils="https://sub.uni-goettingen.de/met/xslt/functions/entity-xml#"
    exclude-result-prefixes="xs app" 
    xpath-default-namespace="https://sub.uni-goettingen.de/met/standards/entity-xml#"
    version="3.0">
    
    <xsl:param name="agency" select="'false'" />
    <xsl:param name="schema-path" select="'../../schema/entityXML.rng'" />
    
    <xsl:output method="text" />
    
    <xsl:function name="utils:schema-element-names">
        <xsl:param name="schema" />
        <xsl:for-each select="($schema//rng:element, $schema//rng:attribute)">
            <xsl:value-of select="data(./@name)"/>
        </xsl:for-each>
    </xsl:function>
    
    <xsl:template match="/">
        <xsl:apply-templates select="entityXML"/>
    </xsl:template>
    
    <xsl:template match="entityXML">
        <xsl:apply-templates select="collection/data/list"/>
    </xsl:template>
    
    <xsl:template match="data/list/title">
        <xsl:text>&#10;# </xsl:text>
        <xsl:value-of select="normalize-space(text())"/>
        <xsl:if test="following-sibling::element()[1][not(name() = 'abstract')]">&#10;</xsl:if>
    </xsl:template>
    
    <xsl:template match="data/list/abstract">
        <xsl:text>&#10;&gt; </xsl:text>
        <xsl:value-of select="normalize-space( data(text()) )"/>
        <xsl:text>&#10;</xsl:text>
    </xsl:template>
    
    <xsl:template match="data/list/element()[not(name() = ('title', 'abstract') )]">
        <xsl:text>&#10;## </xsl:text>
        <xsl:value-of select="data(@xml:id)"/>
        <xsl:text> (</xsl:text>
        <xsl:value-of select="name()"/>
        <xsl:text>)</xsl:text>
        <xsl:text>&#10;</xsl:text>
        <xsl:value-of select="replace(path(), 'Q\{.+?\}', '')"/>
        <xsl:text>&#10;</xsl:text>
        <xsl:variable name="nodes">
            <xsl:choose>
                <xsl:when test="$agency = 'true'">
                    <xsl:variable name="schema" select="doc($schema-path)"/>
                    <xsl:copy-of select="child::element()[name() = utils:schema-element-names($schema)][not(name() = 'revision')]"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:copy-of select="child::element()" />
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <!-- (exml:*, gndo:*, dc:*, foaf:*, owl:*, skos:*, geo:*, dnb:*, wgs84:*, es:*)[not(name() = 'revision')] -->
        
        <xsl:for-each select="$nodes/*">
            <xsl:value-of select="exml:get-property(., '', false())"/>
            <xsl:text>&#10;</xsl:text>
        </xsl:for-each>
        <!--<xsl:apply-templates select="(exml:*, gndo:*, dc:*, foaf:*, owl:*, skos:*, geo:*, dnb:*, wgs84:*)[not(name() = 'revision')]"/>-->
        <xsl:text>&#10;---&#10;&#10;</xsl:text>
    </xsl:template>
      
    <xsl:function name="exml:get-property">
        <xsl:param name="nodes" />
        <xsl:param name="parent-name" />
        <xsl:param name="is-attr" />
        <xsl:for-each select="$nodes">
            <xsl:variable name="node-name">
                <xsl:choose>
                    <xsl:when test="$is-attr">
                        <xsl:text>@</xsl:text>
                        <xsl:value-of select="./name()"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="./name()"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <xsl:variable name="name">
                <xsl:choose>
                    <xsl:when test="$parent-name = ''">
                        <xsl:value-of select="$node-name"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="$parent-name||'/'||$node-name" />
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            
            <xsl:choose>
                <xsl:when test="./@* or ./child::element()">
                    <xsl:if test="./@* and not(./child::element())">
                        <xsl:text>&#10;</xsl:text>
                        <xsl:value-of select="$name" />
                        <xsl:text>: </xsl:text>
                        <xsl:value-of select="exml:md-url( normalize-space(./text()) )"/>
                    </xsl:if>
                    <xsl:value-of select="exml:get-property((./@*), $name, true())"/>
                    <xsl:value-of select="exml:get-property((./child::element()), $name, false())"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:text>&#10;</xsl:text>
                    <xsl:value-of select="$name" />
                    <xsl:text>: </xsl:text>
                    <xsl:value-of select="exml:md-url( normalize-space( data(.)) )"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:for-each>
    </xsl:function>
    
    <xsl:function name="exml:md-url">
        <xsl:param name="value" />
        <xsl:choose>
            <xsl:when test="matches($value, '^(http|https)://(.+)$')">
                <xsl:text>(</xsl:text>
                <xsl:value-of select="$value"/>
                <xsl:text>)</xsl:text>
                <xsl:text>[</xsl:text>
                <xsl:value-of select="$value"/>
                <xsl:text>]</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$value"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>
    

</xsl:stylesheet>