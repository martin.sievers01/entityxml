xquery version "3.1";

import module namespace entities="http://sub.uni-goettingen.de/met/apps/entitystore/entities" at "modules/entities.xqm";

declare namespace entityxml="https://sub.uni-goettingen.de/met/standards/entity-xml#";
declare namespace gndo="https://d-nb.info/standards/elementset/gnd#" ;
declare namespace foaf="http://xmlns.com/foaf/0.1/";
declare namespace output = "http://www.w3.org/2010/xslt-xquery-serialization"; 

declare option output:method "html"; 

declare variable $mode as xs:string external;
declare variable $assets-dir as xs:string external := "../../assets/";
declare variable $schema_dir as xs:string external;
declare variable $declared-names := ();


declare function local:transform( $nodes as node()* ){
    for $node in $nodes
    return 
        typeswitch($node)
            case text() return $node
            case comment() return $node
            case processing-instruction() return $node
            
            case element(entityxml:entityXML) return 
                <html>
                    <head>
                        <title>Structured View</title>
                        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                        <script>{ unparsed-text($assets-dir||'js/entityxml.js') }</script>
                        <!-- <script src="../js/entityxml.js"></script> -->
                        <style>{ unparsed-text($assets-dir||'css/entities.structured.css') }</style>
                        <!-- <link rel="stylesheet" href="../../css/entities.structured.css" /> -->
                    </head>
                    
                    <body>
                        
                    { 
                        local:transform( $node/node() ) 
                    }
                    </body>
                </html>
            
            case element(entityxml:meta) return 
                <div class="collection-meta">
                    { local:transform( $node/node() ) }
                </div>
                
            case element(entityxml:data) return 
                (
                    <div class="toc">
                        <h2>Table of contents</h2>
                        { local:toc( $node/node() ) }
                    </div>,
                    local:transform( $node/node() )
                )
            
            case element(entityxml:list) return 
                <div class="entity-list">
                {
                    if ($node[entityxml:title]) then (
                        <h2>{ $node/entityxml:title => data() }</h2>
                    ) else (),
                    if ($node[entityxml:abstract]) then (
                        <div class="abstract">{ $node/entityxml:abstract => data() }</div>
                    ) else ()
                }
                {   
                    for $entity in $node/element()[ not(name() = ("title", "abstract")) ]
                    return local:entity( $entity )
                }
                </div>
                
            (:case element(entityxml:person) return 
                local:entity( $node )
               
            case element(entityxml:corporateBody) return 
                local:entity( $node )
            
            case element(entityxml:place) return 
                local:entity( $node ):)
                
            default return 
                local:default-html( $node )
};

declare function local:default-html( $nodes as node()* ){
    local:transform( $nodes/node() )
};


declare function local:toc( $entityxml ){
    for $node in $entityxml
    return 
        typeswitch($node)
            case text() return $node
            case comment() return $node
            case element(entityxml:collection) return 
                local:toc( $node/node() )
            
            case element(entityxml:data) return 
                <div class="toc">
                {
                    local:toc( $node/node() )
                }
                </div>
            
            case element(entityxml:list) return 
                let $nr := count($node/preceding::entityxml:list)+1
                let $entities := $node/element()[ not(name() = ("title", "abstract")) ]
                let $title := if ( $node[entityxml:title] ) then ( $node/entityxml:title => data() ) else ( "Liste"||" #"||$nr )
                return 
                    <div class="list">
                        <h3>{ $title||" ("||count($entities)||")"}</h3>
                        <div>
                        {   
                            (:for $entity in $entities
                            let $type := $entity/name()
                            return 
                                <span class="toc-item {$type}"><a href="#{generate-id($entity)}">{entities:entity-title($entity)}</a></span>:)
                            
                            let $groups := distinct-values($entities/name())
                            for $name in $groups
                            let $entities-by-name := $entities[name() = $name]
                            return 
                                <div class="group">
                                    <h4>{ $name } ({count($entities-by-name)})</h4>
                                    {
                                        for $entity in $entities-by-name
                                        return 
                                           <span class="toc-item"><a href="#{generate-id($entity)}">{entities:entity-title($entity)}</a></span>
                                             
                                    }
                                </div>
                                
                            
                        }
                        </div>
                    </div>
                    
            default return local:toc($node/node())
};


declare function local:parse-url( $string as xs:string* ){
    let $s := $string
    return 
        if ( matches($s, "^(http://|https://)(.+)$") )
        then ( <a href="{$s}" target="_blank">{$s}</a>) 
        else ( $string )
};


declare function local:entity( $entity as node()* ){
    let $delivery := ()
    let $lobid-query := entities:lobid-query($entity)
    let $gndo-uri := 
        if ($entity[@gndo:uri]) 
        then (data($entity/@gndo:uri)) 
        else if ($entity/gndo:gndIdenitifer[@gndo:id]) 
        then ("https://d-nb.info/gnd/"||data($entity/gndo:gndIdenitifer/@gndo:id)) 
        else ("false")
    return 
        <div class="entity {$entity/local-name()}" data-id="{$entity/@xml:id}" id="{generate-id($entity)}" data-gndo-uri="{$gndo-uri}">
            <div class="entity-header">
                <h4>{entities:entity-title($entity)}</h4>
            </div>
            <div class="entity-body">
                <div class="meta-section">
                    <ul>
                        
                        <li class="entity-type"><span class="term">Typ</span>{entities:entity-type($entity)}</li>
                    {
                        for $elem in $entity/@*
                        order by $elem/name()
                        return 
                            <li>
                                <span class="term">{$elem/name()}</span>
                                {
                                    if ( $elem/name() = "gndo:uri" ) 
                                    then (<a href="{data($elem)}" target="_blank">{data($elem)}</a>) 
                                    else (data($elem))
                                }
                            </li>
                    }
                    {
                        <li>
                            <span class="term">lobid search</span>
                            <form action="https://lobid.org/gnd/search" method="GET" id="resources-form" target="_blank">
                                <div class="input-group" id="search-simple">
                                    <input 
                                        autofocus="" type="text" name="q" id="gnd-query" value="{$lobid-query}" class="form-control" autocomplete="off" 
                                        placeholder='Suchoptionen: AND, OR, AND NOT, ""-Phrasensuche, *-Trunkierung' title="Suchanfrage" />
                                    <button class="btn btn-default" type="submit" title="Suchen">
                                        Search in lobid
                                    </button>
                                    
                                </div>
                            </form>
                        </li>
                    }
                    </ul>
                </div>
                <div class="revision-section"><!--TODO--></div>
                <div class="markup-section">{ local:markup-section( $entity/node() ) }</div>
                <div class="data-section">
                    <div class="data" data-type="xml" style="display:none"><textarea>{ $entity }</textarea></div>
                    <div class="data" data-type="json" style="display:none"><!-- TODO --></div>
                    <div class="data" data-type="html">{ local:structured-data( $entity/node() ) }</div>
                </div>
            </div>
        </div>
};


(: ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ :)
(: * MARKUP Section HTML Serialisation * :)

declare function local:markup-section( $nodes as node()* ){
    for $node in $nodes
    return 
        typeswitch($node)
            case text() return $node
            case comment() return $node
            case processing-instruction() return $node
            case element(entityxml:revision) return ()
            default return
                <span class="property" data-name="{ $node/name() }" title="{ $node/name() }">
                {
                    local:markup-section( $node/node() )
                }
                </span>
};


(: ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ :)
(: * STRUCTURED DATA Section HTML Serialisation * :)

declare function local:structured-data( $nodes as node()* ){
    for $node in $nodes
    return 
        typeswitch($node)
            case text() return ()
            case comment() return ()
            case processing-instruction() return ()
            case element(entityxml:revision) return ()
            
            default return local:properties( $node, () )
};

declare function local:properties( $properties as element()*, $parent-name as xs:string* ){
    let $distinct-properties := if ( $mode = "extended" ) then ( $properties ) else ( $properties[name() = $declared-names] )
    
    for $property in $distinct-properties
    let $property-child-elements := $property/element()
    let $property-attributes := $property/@*
    let $property-type := if( count(($property-child-elements, $property-attributes))> 0 ) then ('complex') else('simple')
    let $name := if ( $parent-name ) then ($parent-name||"/"||$property/name()) else ($property/name())
    return 
        if ( $property-type = 'complex' )
        then ( 
            <span class="property complex">
            {
                
                if ( $property-child-elements ) 
                then ( 
                    local:attributes( $property/@*, $property/name() ),
                    local:properties( $property/element(), $property/name() ) ) 
                else ( 
                    <span class="property-label">{$name}</span>,
                    <span class="property-content">
                    {   
                        local:magic( $property )
                    }
                    </span>,
                    local:attributes( $property/@*, $property/name() ) 
                ) 
            }
            </span>
        )
        else (
            let $magic := local:magic( $property )
            return 
                local:simple-property-template( $property, $name, $magic )
        )
};

declare function local:attributes( $attrs as node()*, $parent-name as xs:string* ){
    let $distinct-attrs := if ( $mode = "extended" ) then ( $attrs ) else ( $attrs[name() = $declared-names] )
    
    for $attr in $attrs
    let $name := if ( $parent-name ) then ($parent-name||"/@"||$attr/name()) else ("@"||$attr/name())
    let $value := data($attr)
    return 
        <span class="attribute" data-name="{$name}">
            <span class="attribute-label">{$name}</span>
            <span class="attribute-value">{ local:parse-url($value) }</span>
        </span>
};

declare function local:simple-property-template( $property as node()*, $name as xs:string*, $value as item()* ){
    <span class="property simple" data-name="{$property/name()}">
        <span class="property-label">
        {
            if ($name) then ($name) else ( $property/name() )
        }
        </span>
        <span class="property-content">
        {   
            if ($value) then ($value) else ( local:parse-url($property/text()) ) 
        }
        </span>
    </span>
};

declare function local:magic( $nodes as node()* ){
    for $node in $nodes
    let $name := $node/name()
    return 
        if ( $name = ("gndo:gndIdentifier") ) then (
            <a href="https://d-nb.info/gnd/{$node/text()}" target="_blank">{$node/text()}</a>
        ) 
        
        else if ( $node[@gndo:id] ) then (
            <a href="https://d-nb.info/gnd/{data($node/@gndo:id)}" target="_blank">{$node/text()}</a>
            
        ) 
        
        else if ( $node[@gndo:uri] ) then (
            <a href="{data($node/@gndo:uri)}" target="_blank">{$node/text()}</a>
            
        ) else (
            local:parse-url($node/text())
        )
};

local:transform(./entityxml:entityXML)
