xquery version "3.1";

module namespace stats = "https://sub.uni-goettingen.de/met/apps/entity-xml/statistics";

declare namespace entity = "https://sub.uni-goettingen.de/met/standards/entity-xml#";
declare namespace foaf = "http://xmlns.com/foaf/0.1/";
declare namespace gndo = "https://d-nb.info/standards/elementset/gnd#";
declare namespace owl = "http://www.w3.org/2002/07/owl#";
declare namespace skos = "http://www.w3.org/2004/02/skos/core#";


declare function stats:person-level( $person ){
    let $level := 
        <level>
            <measure type="names" total="3">
            {
                if ( $person[gndo:preferredName] or $person[gndo:alternativeName] ) 
                then (
                    if ( $person/gndo:preferredName[gndo:forename] and $person/gndo:preferredName[gndo:surname]) 
                    then ( <check level="3"></check> ) 
                    else if( $person/gndo:preferredName[gndo:personalName] )
                    then ( <check level="3"></check> )
                    else if ( $person/gndo:preferredName[gndo:forename] or $person/gndo:preferredName[gndo:surname]) 
                    then ( <check level="2"></check> ) 
                    else ( <check level="1"></check> )
                ) 
                
                else ( <check level="0"></check> )
            }
            </measure>
            <measure type="dates" total="3">
                {
                    if ( $person[gndo:dateOfBirth] and $person[gndo:dateOfDeath] ) 
                    then ( <check level="3"></check> )
                    else if ( $person[gndo:dateOfBirth] or $person[gndo:dateOfDeath] ) 
                    then ( <check level="2"></check> )
                    else if ( $person[gndo:periodOfActivity]) 
                    then ( <check level="1"></check> )
                    else ( <check level="0"></check> )
                }
            </measure>
            <measure type="page" total="3">
                {
                    if ( $person[foaf:page] ) 
                    then ( <check level="3"></check> )
                    else ( <check level="0"></check> )
                }
            </measure>
        </level>
    let $calc := 
        <calc>
        {
            for $measure in $level/measure
            let $total := xs:integer($measure/@total => data())
            let $check := xs:integer($measure/check/@level => data())
            return <elem name="{$measure/@type => data()}">{($check div $total) * 100}</elem>
        }
        </calc>
        
         
    return 
        <eval stat="{ round(sum(for $i in $calc/elem return xs:float($i/text()) ) div count($calc/elem))}">
            { $calc }
            { $level }
        </eval>
};
