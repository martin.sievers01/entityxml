xquery version "3.1";

import module namespace entities="http://sub.uni-goettingen.de/met/apps/entitystore/entities" at "modules/entities.xqm";

declare namespace entityxml="https://sub.uni-goettingen.de/met/standards/entity-xml#";
declare namespace gndo="https://d-nb.info/standards/elementset/gnd#" ;
declare namespace foaf="http://xmlns.com/foaf/0.1/";
declare namespace output = "http://www.w3.org/2010/xslt-xquery-serialization"; 

declare option output:method "html"; 



declare function local:transform( $nodes as node()* ){
    for $node in $nodes
    return 
        typeswitch($node)
            case text() return $node
            case comment() return $node
            case processing-instruction() return $node
            
            
                
            default return 
                local:default( $node )
};

declare function local:default( $node as node()* ){
    element {$node/name()}{
        $node/@*,
        local:transform( $node/node() )
    }
};

local:transform(./*:grammar)

