<?xml version="1.0" encoding="UTF-8"?>
<entityXML xmlns="https://sub.uni-goettingen.de/met/standards/entity-xml#" 
    xmlns:gndo="https://d-nb.info/standards/elementset/gnd#"
    xmlns:owl="http://www.w3.org/2002/07/owl#" 
    xmlns:skos="http://www.w3.org/2004/02/skos/core#" 
    xmlns:dc="http://purl.org/dc/elements/1.1/"
    xmlns:foaf="http://xmlns.com/foaf/0.1/" 
    xmlns:dnb="https://www.dnb.de" 
    xmlns:geo="http://www.opengis.net/ont/geosparql#"
    xmlns:ex="https://gitlab.gwdg.de/usikora/entityxml/tests" 
    xmlns:es="https://sub.uni-goettingen.de/met/standards/entity-xml-staging#">
    <collection>
        <metadata>
            <title>entityXML Beispiele</title>
            <abstract>Beispiele für die Präsentation beim GND-Forum, 26.01.2023</abstract>
            <provider id="text-plus-test">
                <title>Text+ GND Testdaten Provider</title>
                <abstract>Datenlieferant für sehr verlässliche Beispieldaten, die sich für Präsentationen eignen.</abstract>
                <respStmt id="MM">
                    <resp>Bearbeiter</resp>
                    <name>Musterfrau, Maxima</name>
                    <contact>
                        <mail>muster@entityxml.de</mail>
                        <address>Am Datenstrom 5, 1100101 Modulstadt</address>
                        <phone>0551-1100101</phone>
                        <web>www.entityxml-beispiele.de</web>
                    </contact>
                </respStmt>
            </provider>
            <agency isil="DE-7">
                <title>Text+ GND-Agentur</title>
                <abstract>Die GND-Agentur aus Text+ an der SUB Göttingen</abstract>
                <respStmt id="US">
                    <resp>Text+ GND-Agentur &amp; entityXML Entwicklung</resp>
                    <name>Uwe Sikora</name>
                    <contact>
                        <mail>sikora@sub.uni-goettingen.de</mail>
                    </contact>
                </respStmt>
            </agency>
            <revision status="opened">
                <change when="2023-01-20" who="MM">Lieber Uwe, hier eine kleine Datensammlung, die du als Beispiel am Donnerstag im GND Forum zeigen
                    kannst. Viel Spaß damit!</change>
                <change when="2023-01-22" who="US">Super, vielen Dank! Ich schicke dir die entityXML nochmal zurück, weil ich dich bitten wollte, ob
                    du noch Irgendwen hinzufügen könntest? Das wäre super!</change>
                <change when="2023-01-23" who="MM">"Irgendwer" ist nun als Werkseintrag in der Sammlung angelegt. Schau es dir doch gerne mal
                    an!</change>
            </revision>
        </metadata>
        <data>
            <list>
                <title>Personendatensätze</title>
                
                <person id="uwe_sikora">
                    <gndo:preferredName>
                        <gndo:forename>Uwe</gndo:forename>
                        <gndo:surname>Sikora</gndo:surname>
                    </gndo:preferredName> ist ein <gndo:professionOrOccupation gndo:ref="https://d-nb.info/gnd/4161681-9"
                        >Informationswissenschaftler</gndo:professionOrOccupation> an der <gndo:affiliation gndo:ref="https://d-nb.info/gnd/2020450-4"
                        >SUB Göttingen</gndo:affiliation>. Er wurde <gndo:dateOfBirth iso-date="1985">1985</gndo:dateOfBirth> in <gndo:placeOfBirth
                        gndo:ref="https://d-nb.info/gnd/4021477-1">Göttingen</gndo:placeOfBirth> geboren. <gndo:biographicalOrHistoricalInformation
                        xml:lang="de">Er ist ein passionierter Kletterer, egal ob am Fels oder in der Halle</gndo:biographicalOrHistoricalInformation>
                    und Autor des <gndo:publication ref="#entityxml">entityXML Handbuch</gndo:publication>s. Die
                    <gndo:fieldOfStudy gndo:ref="https://d-nb.info/gnd/1271364182">Modellierung von Daten</gndo:fieldOfStudy> ist eines seiner wissenschaftlichen
                    Interessengebiete.</person>
                
                <work id="entityxml" agency="create">
                    <gndo:preferredName>entityXML Handbuch</gndo:preferredName>
                    <gndo:variantName>entityXML Dokumentation</gndo:variantName>
                    <gndo:variantName>DER eXML Leitfaden</gndo:variantName>
                    <skos:note>Das offizielle Handbuch zum entityXML Format inkl. Dokumentation</skos:note>
                    <gndo:firstAuthor ref="#uwe_sikora"> Uwe Sikora</gndo:firstAuthor>
                    <!-- Alternative Auszeichung der Beziehung mittels `gndo:relatesTo` -->
                    <gndo:relatesTo gndo:type="https://d-nb.info/gnd/4003982-1" ref="#uwe_sikora">Autor: Uwe Sikora</gndo:relatesTo>
                </work>

                <person id="ernest_chevalier" agency="create">
                    <gndo:preferredName>
                        <gndo:surname>Chevalier</gndo:surname>, <gndo:forename>Ernest</gndo:forename>
                    </gndo:preferredName>
                    <gndo:dateOfBirth iso-date="1820-08-14">14.08.1820</gndo:dateOfBirth>
                    <gndo:dateOfDeath iso-date="1887-12-05">05.12.1887</gndo:dateOfDeath>
                    <gndo:placeOfBirth>Villers-en-Vexin</gndo:placeOfBirth>
                    <gndo:placeOfDeath gndo:ref="https://d-nb.info/gnd/4044660-8">Paris</gndo:placeOfDeath>
                    <gndo:gender gndo:term="https://d-nb.info/standards/vocab/gnd/gender#male">männlich</gndo:gender>
                    <gndo:geographicAreaCode gndo:term="https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-FR"
                        >Frankreich</gndo:geographicAreaCode>
                    <gndo:professionOrOccupation gndo:ref="https://d-nb.info/gnd/4046517-2" gndo:type="significant"
                        >Politiker</gndo:professionOrOccupation>
                    <gndo:publication dnb:catalogue="https://d-nb.info/968701086" gndo:ref="https://d-nb.info/gnd/118641166X">Vergessene Zusammenhänge
                        (S. 139)</gndo:publication>
                    <foaf:page>https://fr.wikipedia.org/wiki/Ernest_Chevalier</foaf:page>
                    <source>https://fr.wikipedia.org/wiki/Ernest_Chevalier</source>
                    <revision status="opened">
                        <change when="2023-01-21" who="MM">Bitte in die GND aufnehmen</change>
                    </revision>
                </person>

                <person id="nguyen-kim-mai-thi" gndo:uri="https://d-nb.info/gnd/1135801363"
                    xmlns:social="http://social-media.statistics.de/kennzahlen" enrich="false">
                    <gndo:preferredName>
                        <gndo:surname>Nguyen-Kim</gndo:surname>, <gndo:forename>Mai Thi</gndo:forename>
                    </gndo:preferredName>
                    <gndo:variantName>
                        <gndo:forename>Mai-Thi</gndo:forename>
                        <gndo:surname>Leiendecker</gndo:surname>
                    </gndo:variantName>
                    <social:twitter when="2023-01-11" follower="450602" joined="2013-01-01">maithi_nk</social:twitter>
                    <social:youtube when="2023-01-11" follower="1480000" joined="2016-09-08">maiLab</social:youtube>
                    <owl:sameAs>https://orcid.org/0000-0003-4787-0508</owl:sameAs>
                    <owl:sameAs>https://viaf.org/viaf/3094149844949202960003</owl:sameAs>
                    <owl:sameAs>https://www.wikidata.org/wiki/Q29169693</owl:sameAs>
                    <owl:sameAs>https://id.loc.gov/authorities/names/n2019011012</owl:sameAs>
                </person>

                <entity id="lido-example" gndo:type="https://d-nb.info/standards/elementset/gnd#Work">
                    <lido:lido xmlns="http://www.lido-schema.org" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                        xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:lido="http://www.lido-schema.org">
                        <lido:lidoRecID lido:source="Deutsches Dokumentationszentrum für Kunstgeschichte - Bildarchiv Foto Marburg"
                            lido:type="http://terminology.lido-schema.org/lido00100">DE-Mb112/lido-obj00154983</lido:lidoRecID>
                        <lido:objectPublishedID lido:type="http://terminology.lido-schema.org/lido00099"
                            lido:pref="http://terminology.lido-schema.org/lido00170"
                            >https://www.uffizi.it/opere/botticelli-primavera</lido:objectPublishedID>
                        <lido:objectPublishedID lido:type="http://terminology.lido-schema.org/lido00099"
                            lido:pref="http://terminology.lido-schema.org/lido00170">https://d-nb.info/gnd/4069615-7</lido:objectPublishedID>
                        <lido:category>
                            <skos:Concept rdf:about="http://terminology.lido-schema.org/lido00096">
                                <skos:note>...</skos:note>
                                <skos:prefLabel xml:lang="en">Human-made object</skos:prefLabel>
                                <skos:exactMatch>http://vocab.getty.edu/aat/300386957</skos:exactMatch>
                                <skos:exactMatch>http://erlangen-crm.org/current/E22_Human-Made_Object</skos:exactMatch>
                            </skos:Concept>
                            <lido:term lido:addedSearchTerm="yes">man-made object</lido:term>
                        </lido:category>
                        <!-- ... -->
                    </lido:lido>
                </entity>
            </list>
            <list>
                <title>Andere Datensätze</title>
                
                <place xml:id="lummerland" agency="create">
                    <gndo:preferredName>Lummerland</gndo:preferredName>
                    <gndo:variantName>Eine Insel mit zwei Bergen</gndo:variantName>
                    <gndo:biographicalOrHistoricalInformation xml:lang="de">Eine fiktive Insel aus den Geschichten von Michael
                        Ende.</gndo:biographicalOrHistoricalInformation>
                    <source url="https://de.wikipedia.org/wiki/Jim_Knopf_und_Lukas_der_Lokomotivf%C3%BChrer">Jim Knopf und Lukas der
                        Lokomotivführer</source>
                    <geo:hasGeometry geo:source="https://sws.geonames.org/2918632">
                        <lat xmlns="http://www.w3.org/2003/01/geo/wgs84_pos#">51.534166</lat>
                        <long xmlns="http://www.w3.org/2003/01/geo/wgs84_pos#">09.932222</long>
                    </geo:hasGeometry>
                </place>
                
                <place id="goslar_rathaus" gndo:uri="http://d-nb.info/gnd/4231845-2">
                    <gndo:preferredName>Rathaus Goslar</gndo:preferredName>
                    <gndo:geographicAreaCode gndo:term="https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DE-NI">Niedersachsen</gndo:geographicAreaCode>
                    <gndo:homepage iso-date="2023-02-02">https://www.goslar.de/</gndo:homepage>
                    <owl:sameAs>http://viaf.org/viaf/237785155</owl:sameAs>
                    <gndo:place gndo:ref="https://d-nb.info/gnd/4021643-3">Goslar</gndo:place>
                </place>
                
                <work id="irgendwer" agency="create" xmlns:imdb="https://www.imdb.com">
                    <gndo:preferredName>Irgendwer</gndo:preferredName>
                    <es:type gndo:term="https://d-nb.info/gnd/4166233-7">Kurzfilm</es:type>
                    <es:regie gndo:ref="https://d-nb.info/gnd/13935476X">Marco Gadge</es:regie>
                    <gndo:relatesTo gndo:type="https://d-nb.info/gnd/4049050-6" gndo:ref="https://d-nb.info/gnd/13935476X">Regisseur: Marco
                        Gadge</gndo:relatesTo>
                    <es:music gndo:ref="https://d-nb.info/gnd/1061635473">Konstantin Kemnitz</es:music>
                    <es:source url="https://de.wikipedia.org/wiki/Irgendwer" es:bewertung="Die Quelle ist nicht wirklich aussagekräftig"
                        >Wikipedia</es:source>
                    <es:uwes-bewertung>Ich kann dazu nichts sagen ... ich habe den Film nicht gesehen, aber die Bewertung bei IMDB sieht ganz gut
                        aus!</es:uwes-bewertung>
                    <gndo:firstAuthor gndo:ref="https://d-nb.info/gnd/118602802">Rosenzweig, Franz</gndo:firstAuthor>
                    <imdb:score ref="https://www.imdb.com/title/tt6700834/">6.7</imdb:score>
                    <revision status="opened">
                        <change when="2023-01-22" who="MM">Bitte in die GND eintragen</change>
                        <change when="2023-01-25" who="US">Bisher nur rudimentäre Informationen vorhanden. Einspielung in die GND so nicht
                            möglich.</change>
                    </revision>
                </work>
                
                <!-- Diesen Eintrag kann man mit `scripts/xquery/enrich-records.lobid.xquery` automatisch anreichern. -->
                <person agency="ignore" id="marco_gadge" gndo:uri="https://d-nb.info/gnd/13935476X" enrich="true" />
                
                
            </list>
        </data>
    </collection>
</entityXML>
