
<a name="entity-notes"></a>
# Anmerkungen

Für jeden Eintrag können eine oder mehrere optionale, **allgemeine Anmerkungen** mittels [``skos:note``](specs-elems.md#elem.skos.note) dokumentiert werden, die zusätzliche Informationen oder Hinweise zu der beschriebenen Entität enthalten. Die Anmerkung sollte dabei in einer Form gehalten sein, die angebracht für eine spätere Veröffentlichung ist.

Die Sprache, in der eine Anmerkung verfasst ist, lässt sich via `@xml:lang` explizit spezifizieren. Wenn keine Sprache explizit angegeben wird, gilt **Deutsch** als default.

Zusätzlich kann über `@gndo:type` angegeben werden, ob es sich um eine im späteren Normdatensatz sichtbare, d.h. öffentliche Anmerkung oder um eine interne Anmerkung handelt, die nicht in den Normdateneintrag übernommen wird.

```xml
<entity xml:id="max_muster">
   <dc:title>Mustermann, Max</dc:title>
   <skos:note>Eine fiktive Person, die als Platzhalter fungiert</skos:note>
   <skos:note gndo:type="internal">Dieser Eintrag hat als Beispiel 
      mittlerweise so einen "langen Bart". Ich denke er hat ausgedient.<skos:note>
</entity>
```

**Zusätzliche Ressourcen**:


- Das MARC 21 Feld 680 (Scope Note) im "[Cataloger's
                        Reference Shelf MARC 21 Format for Classification Data](https://www.itsmarc.com/crs/mergedprojects/conciscl/conciscl/idh_680_clas.htm)"
- Das Feld 680 (Benutzerhinweis) im [GND-Erfassungsleitfaden](https://wiki.dnb.de/download/attachments/50759357/680.pdf)
