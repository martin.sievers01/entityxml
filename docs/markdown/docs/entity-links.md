
<a name="entity-links"></a>
# Sonstige Webressourcen

Links auf sonstige Onlineressourcen, die zusätzliche Informationen über die beschriebene Entität enthalten, können mit Hilfe von [``ref``](specs-elems.md#elem.ref) dokumentiert werden. Hierbei stehen zwei Auszeichnungsmöglichkeit zur Auswahl:

**(1)** Der Link zur entsprechenden Ressource wird direkt als Text innhalb von `ref` dokumentiert.

**(2)** Der Link zur entsprechenden Ressource wird im `@target` Attribut von `ref` dokumentiert und innerhalb von `ref` wird ein Stellvertretertext angegeben.

```xml
 <!-- Ein Beispiel mit 'ref/@target' und Stellvertretertext -->
 
 <person xml:id="Malachlabra" gndo:type="https://d-nb.info/standards/elementset/gnd#LiteraryOrLegendaryCharacter">
   <gndo:preferredName>
      <gndo:personalName>Malachlabra</gndo:personalName>
   </gndo:preferredName>
   <skos:note>Malachlabra ist ein Charakter aus den D&amp;D: Forgotten Realms.</skos:note>
   <foaf:page>https://forgottenrealms.fandom.com/wiki/Malachlabra</foaf:page>
   <ref target="https://www.reddit.com/r/DnD/comments/806uow/does_anyone_know_anything_about_malachlabra/">
      Reddit: Does anyone know anything about Malachlabra?
   </ref>
</person>
```

```xml
<!-- Das gleiche Beispiel ohne @target -->

<ref>https://www.reddit.com/r/DnD/comments/806uow/does_anyone_know_anything_about_malachlabra/</ref>
```

Es stellt sich vielleicht die Frage, warum man neben [``owl:sameAs``](specs-elems.md#elem.owl.sameAs), [``source``](specs-elems.md#elem.source) (und [``gndo:homepage``](specs-elems.md#elem.gndo.homepage) bzw. [``foaf:page``](specs-elems.md#elem.foaf.page), die an späterer Stelle in diesem Handbuch beschrieben werden), noch ein weiteres Element benötigt, um zusätzliche Links zu dokumentieren? Zum Einen, weil die eben anzitierten Elemente recht spezifische Sachverhalte abbilden, die wesentlich konkreter sind als "Eine Ressource, die zusätzliche Informationen über die beschriebene Entität enthält". Zum Anderen – und das ist nicht ganz ernst gemeint – freut sich (fast) Jede*r über ein "Sonstiges" Feld.

Ernsthaft: Bei `ref` handelt es sich einfach um eine Möglichkeit, Referenzen auf Online-Ressourcen zu dokumentieren, die nicht so richtig unter die Definitionen von `owl:sameAs`, `source`, `gndo:homepage` oder `foaf:page` passen wollen – nicht mehr und nicht weniger.


> **Keine Entsprechung in der GND**  
> Für diese Information gibt es keine Entsprechung in der GND! Daher wird sie bei der Erstellung eines neuen Normdateneintrags oder beim Update eines Bestehenden ignoriert.


