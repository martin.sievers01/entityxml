
<a name="specs-attrs"></a>
# Attributspezifikationen

Derzeit umfasst das entityXML Schema **43** spezifizierte Attribute.

Die technische Spezifikation für Attribute umfasst folgende Eigenschaften: 

- **Name**: Der Attributsname bzw. XML-name, der das Attribut bezeichnet.
- **Datatype**: Angabe zum spezifizierten Datentyp des Attributs.
- **Label**: Ein oder mehrere Labels in Deutsch (und Englisch).
- **Description**: Eine oder mehrere Beschreibungen, die die Semantik des Attributs beschreiben.
- **Contained by**: Führt die Elemente auf, in denen das entsprechende Attrbut verwendet wird bzw. werden kann.
- **Predefined Values**: Falls spezifiziert, werden hier vordefinierte Werte angegeben.
- **Validation**: Falls spezifiziert, werden hier Validierungsregeln (z.B. Schematron Pattern) für das entsprechende Element angegeben.



<a name="d17e1960"></a>
### ANY ATTRIBUTE
|     |     |
| --- | --- |
| **Name** | *Any name.*  |
| **Datatype** | string |
| **Label** (de) | ANY ATTRIBUTE |
| **Label** (en) | ANY ATTRIBUTE |
| **Description** (de) | Ein Attribut mit frei wählbarem **QName**. |
| **Description** (en) | - |
| **Contained by** | [`<anyElement.broad>`](specs-elems.md#any-all)  [`<anyElement.narrow>`](specs-elems.md#any-restricted) |

---

<p style="margin-bottom:60px"></p>

<a name="attr.record.agency"></a>
### Agenturanfrage
|     |     |
| --- | --- |
| **Name** | `@agency`  |
| **Datatype** | string |
| **Label** (de) | Agenturanfrage |
| **Label** (en) | agency contact |
| **Description** (de) | Anfrage an die Agentur, welche Aktion in Hinblick auf den Eintrag durchgeführt werden soll. |
| **Description** (en) | - |
| **Contained by** | [`corporateBody`](specs-elems.md#corporate-body-record)  [`entity`](specs-elems.md#entity-record)  [`event`](specs-elems.md#d17e243)  [`expression`](specs-elems.md#d17e1235)  [`manifestation`](specs-elems.md#d17e1304)  [`person`](specs-elems.md#person-record)  [`place`](specs-elems.md#d17e1061)  [`work`](specs-elems.md#d17e1378) |

***Predefined Values***  (multiple choice)

 - **create**: (Eintrag in der GND neu anlegen) Der Eintrag soll als Normdatensatz neu in der GND angelegt werden.
 - **update**: (Normdatensatz mit Daten aus dem Eintrag erweitern) Der Normdatensatz soll durch neue Informationen angereichert werden.
 - **merge**: (Normdatensätze zusammenführen) Dubletten, die in dem Eintrag dokumentiert werden, sollen mit dem Normdatensatz, der durch den GND-URI des Eintrags (`@gndo:uri`) identifiziert ist, zusammengeführt werden.
 - **ignore**: (Eintrag ingonieren) Der Eintrag wird von der Agentur nicht bearbeitet.

---

<p style="margin-bottom:60px"></p>

<a name="d17e5568"></a>
### Aktion (Agentur)
|     |     |
| --- | --- |
| **Name** | `@agency`  |
| **Datatype** | string |
| **Label** (de) | Aktion (Agentur) |
| **Label** (en) | Task (Agentur) |
| **Description** (de) | Anfrage an die Agentur, welche Aktion in Hinblick auf eine Property durchgeführt werden soll. |
| **Description** (en) | - |
| **Contained by** | [`bf:instanceOf`](specs-elems.md#instance-of)  [`dublicateGndIdentifier`](specs-elems.md#dublicates)  [`embodimentOf`](specs-elems.md#d17e1339)  [`foaf:page`](specs-elems.md#elem.foaf.page)  [`gndo:abbreviatedName`](specs-elems.md#d17e2856)  [`gndo:academicDegree`](specs-elems.md#d17e2902)  [`gndo:affiliation`](specs-elems.md#d17e2929)  [`gndo:author`](specs-elems.md#d17e2952)  [`gndo:biographicalOrHistoricalInformation`](specs-elems.md#d17e2985)  [`gndo:counting`](specs-elems.md#d17e3024)  [`gndo:dateOfBirth`](specs-elems.md#person-record-dateOfBirth)  [`gndo:dateOfDeath`](specs-elems.md#person-record-dateOfDeath)  [`gndo:dateOfEstablishment`](specs-elems.md#d17e3105)  [`gndo:dateOfEstablishmentAndTermination`](specs-elems.md#d17e3133)  [`gndo:dateOfPublication`](specs-elems.md#d17e3174)  [`gndo:dateOfTermination`](specs-elems.md#d17e3201)  [`gndo:editor`](specs-elems.md#d17e3277)  [`gndo:fieldOfStudy`](specs-elems.md#d17e3363)  [`gndo:firstAuthor`](specs-elems.md#d17e3403)  [`gndo:formOfWorkAndExpression`](specs-elems.md#d17e3332)  [`gndo:functionOrRole`](specs-elems.md#d17e3460)  [`gndo:gender`](specs-elems.md#d17e3491)  [`gndo:geographicAreaCode`](specs-elems.md#elem.gndo.geographicAreaCode)  [`gndo:gndIdentifier`](specs-elems.md#d17e3593)  [`gndo:gndSubjectCategory`](specs-elems.md#d17e3634)  [`gndo:hierarchicalSuperiorOfPlaceOrGeographicName`](specs-elems.md#d17e3673)  [`gndo:homepage`](specs-elems.md#elem.gndo.homepage)  [`gndo:languageCode`](specs-elems.md#d17e3736)  [`gndo:literarySource`](specs-elems.md#d17e3787)  [`gndo:periodOfActivity`](specs-elems.md#prop-periodOfActivity)  [`gndo:place`](specs-elems.md#geographic-entity-place)  [`gndo:placeOfActivity`](specs-elems.md#person-record-placeOfActivity)  [`gndo:placeOfBirth`](specs-elems.md#person-record-placeOfBirth)  [`gndo:placeOfBusiness`](specs-elems.md#d17e3967)  [`gndo:placeOfDeath`](specs-elems.md#person-record-placeOfDeath)  [`gndo:precedingCorporateBody`](specs-elems.md#d17e4133)  [`gndo:precedingPlaceOrGeographicName`](specs-elems.md#d17e4101)  [`gndo:preferredName`](specs-elems.md#person-record-preferredName)  [`gndo:preferredName`](specs-elems.md#d17e4196)  [`gndo:professionOrOccupation`](specs-elems.md#d17e4262)  [`gndo:pseudonym`](specs-elems.md#d17e4321)  [`gndo:publication`](specs-elems.md#d17e4368)  [`gndo:relatedWork`](specs-elems.md#d17e4565)  [`gndo:relatesTo`](specs-elems.md#d17e4511)  [`gndo:succeedingCorporateBody`](specs-elems.md#d17e4596)  [`gndo:succeedingPlaceOrGeographicName`](specs-elems.md#d17e4626)  [`gndo:temporaryName`](specs-elems.md#d17e4681)  [`gndo:titleOfNobility`](specs-elems.md#d17e4726)  [`gndo:variantName`](specs-elems.md#person-record-variantName)  [`gndo:variantName`](specs-elems.md#elem.gndo.variantName.standard)  [`owl:sameAs`](specs-elems.md#elem.owl.sameAs)  [`realizationOf`](specs-elems.md#d17e1265)  [`skos:note`](specs-elems.md#elem.skos.note)  [`source`](specs-elems.md#elem.source)  [`wgs84:lat`](specs-elems.md#d17e2806)  [`wgs84:long`](specs-elems.md#d17e2819) |

***Predefined Values***  (multiple choice)

 - **add**: (Eigenschaft anlegen) Die Eigenschaft soll dem GND-Normdatensatz des Records hinzugefügt werden.
 - **ignore**: (Eigenschaft ingonieren) Die Eigenschaft wird von der Agentur nicht bearbeitet.
 - **remove**: (Eigenschaft entfernen) Die Eigenschaft soll aus dem GND-Normdatensatz entfernt werden.

---

<p style="margin-bottom:60px"></p>

<a name="attr.iso-from"></a>
### Anfangsdatum
|     |     |
| --- | --- |
| **Name** | `@iso-from`  |
| **Datatype** | date gYearMonth gYear  |
| **Label** (de) | Anfangsdatum |
| **Label** (en) | Startdate |
| **Description** (de) | Ein Anfangsdatum im ISO Format JJJJ-MM-TT, JJJJ-MM oder JJJJ |
| **Description** (en) | A startdate in ISO format YYYY-MM-DD, YYYY-MM or YYYY |
| **Contained by** | [`gndo:dateOfEstablishmentAndTermination`](specs-elems.md#d17e3133)  [`gndo:periodOfActivity`](specs-elems.md#prop-periodOfActivity) |

---

<p style="margin-bottom:60px"></p>

<a name="d17e5620"></a>
### Angereicherte Eigenschaft
|     |     |
| --- | --- |
| **Name** | `@enriched`  |
| **Datatype** | anyURI |
| **Label** (de) | Angereicherte Eigenschaft |
| **Label** (en) | Enriched Property |
| **Description** (de) | Diese Eigenschaft ist automatisch mithilfe des angegebenen externen Datendienstes angereichert wurden! |
| **Description** (en) | This Property was automatically added to the record using the documented Dataservice! |
| **Contained by** | [`bf:instanceOf`](specs-elems.md#instance-of)  [`dublicateGndIdentifier`](specs-elems.md#dublicates)  [`embodimentOf`](specs-elems.md#d17e1339)  [`foaf:page`](specs-elems.md#elem.foaf.page)  [`gndo:abbreviatedName`](specs-elems.md#d17e2856)  [`gndo:academicDegree`](specs-elems.md#d17e2902)  [`gndo:affiliation`](specs-elems.md#d17e2929)  [`gndo:author`](specs-elems.md#d17e2952)  [`gndo:biographicalOrHistoricalInformation`](specs-elems.md#d17e2985)  [`gndo:counting`](specs-elems.md#d17e3024)  [`gndo:dateOfBirth`](specs-elems.md#person-record-dateOfBirth)  [`gndo:dateOfDeath`](specs-elems.md#person-record-dateOfDeath)  [`gndo:dateOfEstablishment`](specs-elems.md#d17e3105)  [`gndo:dateOfEstablishmentAndTermination`](specs-elems.md#d17e3133)  [`gndo:dateOfPublication`](specs-elems.md#d17e3174)  [`gndo:dateOfTermination`](specs-elems.md#d17e3201)  [`gndo:editor`](specs-elems.md#d17e3277)  [`gndo:fieldOfStudy`](specs-elems.md#d17e3363)  [`gndo:firstAuthor`](specs-elems.md#d17e3403)  [`gndo:formOfWorkAndExpression`](specs-elems.md#d17e3332)  [`gndo:functionOrRole`](specs-elems.md#d17e3460)  [`gndo:gender`](specs-elems.md#d17e3491)  [`gndo:geographicAreaCode`](specs-elems.md#elem.gndo.geographicAreaCode)  [`gndo:gndIdentifier`](specs-elems.md#d17e3593)  [`gndo:gndSubjectCategory`](specs-elems.md#d17e3634)  [`gndo:hierarchicalSuperiorOfPlaceOrGeographicName`](specs-elems.md#d17e3673)  [`gndo:homepage`](specs-elems.md#elem.gndo.homepage)  [`gndo:languageCode`](specs-elems.md#d17e3736)  [`gndo:literarySource`](specs-elems.md#d17e3787)  [`gndo:periodOfActivity`](specs-elems.md#prop-periodOfActivity)  [`gndo:place`](specs-elems.md#geographic-entity-place)  [`gndo:placeOfActivity`](specs-elems.md#person-record-placeOfActivity)  [`gndo:placeOfBirth`](specs-elems.md#person-record-placeOfBirth)  [`gndo:placeOfBusiness`](specs-elems.md#d17e3967)  [`gndo:placeOfDeath`](specs-elems.md#person-record-placeOfDeath)  [`gndo:precedingCorporateBody`](specs-elems.md#d17e4133)  [`gndo:precedingPlaceOrGeographicName`](specs-elems.md#d17e4101)  [`gndo:preferredName`](specs-elems.md#person-record-preferredName)  [`gndo:preferredName`](specs-elems.md#d17e4196)  [`gndo:professionOrOccupation`](specs-elems.md#d17e4262)  [`gndo:pseudonym`](specs-elems.md#d17e4321)  [`gndo:publication`](specs-elems.md#d17e4368)  [`gndo:relatedWork`](specs-elems.md#d17e4565)  [`gndo:relatesTo`](specs-elems.md#d17e4511)  [`gndo:succeedingCorporateBody`](specs-elems.md#d17e4596)  [`gndo:succeedingPlaceOrGeographicName`](specs-elems.md#d17e4626)  [`gndo:temporaryName`](specs-elems.md#d17e4681)  [`gndo:titleOfNobility`](specs-elems.md#d17e4726)  [`gndo:variantName`](specs-elems.md#person-record-variantName)  [`gndo:variantName`](specs-elems.md#elem.gndo.variantName.standard)  [`owl:sameAs`](specs-elems.md#elem.owl.sameAs)  [`realizationOf`](specs-elems.md#d17e1265)  [`skos:note`](specs-elems.md#elem.skos.note)  [`source`](specs-elems.md#elem.source)  [`wgs84:lat`](specs-elems.md#d17e2806)  [`wgs84:long`](specs-elems.md#d17e2819) |

---

<p style="margin-bottom:60px"></p>

<a name="d17e5171"></a>
### Anmerkungsart
|     |     |
| --- | --- |
| **Name** | `@gndo:type`  |
| **Datatype** | string |
| **Label** (de) | Anmerkungsart |
| **Description** (de) | Angabe zur Art der Anmerkung, sprich, ob es sich um eine interne Anmerkung handelt. |
| **Contained by** | [`skos:note`](specs-elems.md#elem.skos.note) |

***Predefined Values***  

 - **internal**: (Interne Anmerkung)Eine Anmerkung, die ausschließlich für interne Zwecke dokumentiert wird.

---

<p style="margin-bottom:60px"></p>

<a name="d17e5700"></a>
### Anreicherung
|     |     |
| --- | --- |
| **Name** | `@enrich`  |
| **Datatype** | boolean |
| **Label** (de) | Anreicherung |
| **Label** (en) | enrichment |
| **Description** (de) | Der Record soll mit Daten eines externen Datendienstes angereichert werden. Hierfür wird ein zusätzliches Konversionsscript benötigt, dass die Konversionsroutinen zur verfügung stellt, um den Record anzureichern! |
| **Description** (en) | The Record is marked to be enriched by a separate enrichment routine provided by an additional conversion script! |
| **Contained by** | [`corporateBody`](specs-elems.md#corporate-body-record)  [`entity`](specs-elems.md#entity-record)  [`event`](specs-elems.md#d17e243)  [`expression`](specs-elems.md#d17e1235)  [`manifestation`](specs-elems.md#d17e1304)  [`person`](specs-elems.md#person-record)  [`place`](specs-elems.md#d17e1061)  [`work`](specs-elems.md#d17e1378) |

---

<p style="margin-bottom:60px"></p>

<a name="rev-status"></a>
### Bearbeitungsphase
|     |     |
| --- | --- |
| **Name** | `@status`  |
| **Datatype** | string |
| **Label** (de) | Bearbeitungsphase |
| **Label** (en) | Phase |
| **Description** (de) | Die Phase der Bearbeitung, in der sich die Datensammlung oder der Eintrag momentan befindet. |
| **Description** (en) | The phase of a records editing-lifecycle. |
| **Contained by** | [`revision`](specs-elems.md#revision) |

***Predefined Values***  

 - **opened**: (Geöffnet, *default*) Die Informationsressource wurde angelegt und befindet sich derzeit in der Bearbeitung durch den Datenlieferanten.
 - **staged**: (Auf dem Prüfstand) Die Informationsressource befindet sich derzeit in der Kontrolle durch eine GND-Agentur: Es wird geprüft, ob die geltenden Qualitätskriterien erfüllt sind. Mögliche Rückfragen und Vorschläge zur Anpassungen werden durch die GND-Agentur dokumentiert und den Datenlieferanten mitgeteilt.
 - **closed**: (Abgeschlossen) Alle Arbeiten an der Informationsressource wurden durchgeführt. Sie gilt als abgeschlossen.

---

<p style="margin-bottom:60px"></p>

<a name="change-status"></a>
### Bearbeitungsstadium
|     |     |
| --- | --- |
| **Name** | `@status`  |
| **Datatype** | string |
| **Label** (de) | Bearbeitungsstadium |
| **Description** (de) | Der momentane Bearbeitungsstatus der Informationsressource. |
| **Contained by** | [`change`](specs-elems.md#d17e2229) |

***Predefined Values***  

 - **draft**: (Draft, *default*) Die Informationsressource befindet sich in der Bearbeitung.
 - **candidate**: (Kandidat) Die Bearbeitung ist soweit abgeschlossen. Die Informationsressource gilt nun als potenzieller Kandidat zur Übertragung an die GND und ist somit bereit zur Prüfung durch eine GND-Agentur.
 - **embargoed**: (Gesperrt) Die Informationsressource ist derzeit gesperrt. Sie erfüllt entweder nicht die erforderlichen Qualitätskriterien oder ist aus einem anderen Grund, der in dem Änderungseintrag vermerkt ist, gesperrt. Sie muss entsprechend überarbeitet werden.
 - **withdrawn**: (Zurückgezogen) Die Informationsressource wurde zurückgezogen. Sie wird in Hinblick auf eine Übertragung an die GND ignoriert.
 - **cleared**: (Überarbeitet) Die Informationsressource wurde in Hinblick auf die geltenden Qualitätskriterien überarbeitet und gilt nun als überarbeiteter Kandidat, der erneut geprüft wird.
 - **approved**: (Angenommen) Die Prüfung der Informationsressource ist abgeschlossen. Sie erfüllt alle erforderlichen Qualitätsstandards und ist bereit zur Übertragung an die GND. Dieser Bearbeitungsschritt ist Mitarbeiter\*innen einer GND-Agentur vorbehalten.
 - **submitted**: (An GND geliefert) Die Daten der Informationsressource wurden an die GND zum Übertragen geliefert. Dieser Bearbeitungsschritt ist Mitarbeiter\*innen einer GND-Agentur vorbehalten.
 - **published**: (Veröffentlicht) Die Daten der Informationsressource wurden in der GND veröffentlicht. Dieser Bearbeitungsschritt ist Mitarbeiter\*innen einer GND-Agentur vorbehalten.

---

<p style="margin-bottom:60px"></p>

<a name="d17e4534"></a>
### Beziehungstyp
|     |     |
| --- | --- |
| **Name** | `@gndo:type`  |
| **Datatype** | anyURI (*RNG Pattern Matching*, *RNG Pattern Matching*, *RNG Pattern Matching*) |
| **Label** (de) | Beziehungstyp |
| **Label** (en) | Type of Relation |
| **Description** (de) | Spezifiziert die Art der ausgewiesenen Beziehung entweder über einen **GND-URI**, einen **Term-URI aus einem GND-Vokabular** oder einen **Term-URI aus der GNDO**. |
| **Description** (en) | Spezifies the type of the encoded relation either using a **GND-URI**, a **GND-vocabulary descriptor URI** or **GNDO descriptor URI**. |
| **Contained by** | [`gndo:relatesTo`](specs-elems.md#d17e4511) |

***Validation***  

```xml 
<param xmlns="http://relaxng.org/ns/structure/1.0" name="pattern">(https|http)://d-nb.info/standards/elementset/gnd#(.+)</param>
 
```

```xml 
<param xmlns="http://relaxng.org/ns/structure/1.0" name="pattern">(http|https)://d-nb.info/standards/vocab/gnd/(.+)</param>
 
```

```xml 
<param xmlns="http://relaxng.org/ns/structure/1.0" name="pattern">(http|https)://d-nb.info/gnd/(.+)</param>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e5339"></a>
### Certainty
|     |     |
| --- | --- |
| **Name** | `@cert`  |
| **Datatype** | string |
| **Label** (de) | Certainty |
| **Description** (de) | Angabe zum Status der getroffenen Aussage. `@cert` hat einen vorgegebenen, exklusiven Wertebereich ("low", "middle", "high"). |
| **Contained by** | [`dublicateGndIdentifier`](specs-elems.md#dublicates)  [`gndo:gndIdentifier`](specs-elems.md#d17e3593) |

***Predefined Values***  

 - **low**: (unsicher) Die Aussage ist unsicher
 - **middle**: (wahrscheinlich) Die Aussage ist wahrscheinlich
 - **high**: (belegt) Die Aussage ist belegt

---

<p style="margin-bottom:60px"></p>

<a name="d17e4402"></a>
### DNB Katalog
|     |     |
| --- | --- |
| **Name** | `@dnb:catalogue`  |
| **Datatype** | anyURI (*RNG Pattern Matching*) |
| **Label** (de) | DNB Katalog |
| **Description** (de) | Verknüpfung einer `gndo:publication` mit einem URL eines Eintrags im [Katalog der Deutschen Nationalbibliothek](https://portal.dnb.de/opac.htm). |
| **Contained by** | [`gndo:publication`](specs-elems.md#d17e4368) |

***Validation***  

```xml 
<param xmlns="http://relaxng.org/ns/structure/1.0" name="pattern">(http|https)://d-nb.info/(.+)</param>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e5776"></a>
### Datum
|     |     |
| --- | --- |
| **Name** | `@when`  |
| **Datatype** | date |
| **Label** (de) | Datum |
| **Description** (de) | Angabe des Datums der Änderung. |
| **Contained by** | [`change`](specs-elems.md#d17e2229) |

---

<p style="margin-bottom:60px"></p>

<a name="attr.iso-to"></a>
### Enddatum
|     |     |
| --- | --- |
| **Name** | `@iso-to`  |
| **Datatype** | date gYearMonth gYear  |
| **Label** (de) | Enddatum |
| **Label** (en) | Enddate |
| **Description** (de) | Ein Enddatum im ISO Format JJJJ-MM-TT, JJJJ-MM oder JJJJ |
| **Description** (en) | An enddate in ISO format YYYY-MM-DD, YYYY-MM or YYYY |
| **Contained by** | [`gndo:dateOfEstablishmentAndTermination`](specs-elems.md#d17e3133)  [`gndo:periodOfActivity`](specs-elems.md#prop-periodOfActivity) |

---

<p style="margin-bottom:60px"></p>

<a name="d17e3513"></a>
### GND Gender Descriptor URI
|     |     |
| --- | --- |
| **Name** | `@gndo:term`  |
| **Datatype** | string |
| **Label** (de) | GND Gender Descriptor URI |
| **Description** (de) | Ein oder mehrere URIs von Termen aus dem GND Vokabular "Gender" (https://d-nb.info/standards/vocab/gnd/gender#). |
| **Contained by** | [`gndo:gender`](specs-elems.md#d17e3491) |

***Predefined Values***  (multiple choice)

 - **https://d-nb.info/standards/vocab/gnd/gender#female**: *no description available*
 - **https://d-nb.info/standards/vocab/gnd/gender#male**: *no description available*
 - **https://d-nb.info/standards/vocab/gnd/gender#notKnown**: *no description available*

---

<p style="margin-bottom:60px"></p>

<a name="d17e5403"></a>
### GND Geographic Area Code Term
|     |     |
| --- | --- |
| **Name** | `@gndo:term`  |
| **Datatype** | string |
| **Label** (de) | GND Geographic Area Code Term |
| **Description** (de) | URI eines Terms aus dem GND Vokabular *[Geographic Area Code](https://d-nb.info/standards/vocab/gnd/geographic-area-code)* |
| **Contained by** | [`gndo:geographicAreaCode`](specs-elems.md#elem.gndo.geographicAreaCode) |

***Predefined Values***  

 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#NTHH**: (Neutral Zone (-1993))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA**: (Europe)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-AAAT**: (Austria (-12.11.1918))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-AD**: (Andorra)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-AL**: (Albania)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-AT**: (Austria)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-AT-1**: (Burgenland)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-AT-2**: (Carinthia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-AT-3**: (Lower Austria)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-AT-4**: (Upper Austria)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-AT-5**: (Salzburg)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-AT-6**: (Styria)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-AT-7**: (Tyrol)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-AT-8**: (Vorarlberg)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-AT-9**: (Vienna)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-AX**: (Ǻland Island)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-BA**: (Bosnia and Hercegovina)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-BE**: (Belgium)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-BG**: (Bulgaria)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-BY**: (Belarus)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH**: (Switzerland)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-AG**: (Aargau)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-AI**: (Appenzell Innerrhoden)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-AR**: (Appenzell Ausserrhoden)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-BE**: (Bern)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-BL**: (Basel-Landschaft)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-BS**: (Basel-Stadt)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-FR**: (Fribourg)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-GE**: (Geneva)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-GL**: (Glarus)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-GR**: (Grisons)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-JU**: (Jura)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-LU**: (Luzern)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-NE**: (Neuchâtel)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-NW**: (Nidwalden)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-OW**: (Obwalden)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-SG**: (St. Gallen)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-SH**: (Schaffhausen)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-SO**: (Solothurn)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-SZ**: (Schwyz)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-TG**: (Thurgau)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-TI**: (Ticino)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-UR**: (Uri)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-VD**: (Vaud)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-VS**: (Valais)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-ZG**: (Zug)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-ZH**: (Zürich)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CSHH**: (Czechoslovakia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CSXX**: (Serbia and Montenegro)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CY**: (Cyprus)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CZ**: (Czech Republic)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DDDE**: (Germany East)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DE**: (Germany)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DE-BB**: (Brandenburg)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DE-BE**: (Berlin)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DE-BW**: (Baden-Württemberg)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DE-BY**: (Bavaria)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DE-HB**: (Bremen)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DE-HE**: (Hesse)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DE-HH**: (Hamburg)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DE-MV**: (Mecklenburg-Vorpommern)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DE-NI**: (Lower Saxony)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DE-NW**: (North Rhine-Westphalia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DE-RP**: (Rhineland-Palatinate)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DE-SH**: (Schleswig-Holstein)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DE-SL**: (Saarland)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DE-SN**: (Saxony)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DE-ST**: (Saxony-Anhalt)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DE-TH**: (Thuringia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DK**: (Denmark)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DXDE**: (Germany (-1949))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-EE**: (Estonia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-ES**: (Spain)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-FI**: (Finland)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-FR**: (France)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-GB**: (Great Britain)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-GG**: (Guernsey)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-GI**: (Gibraltar)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-GR**: (Greece)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-HR**: (Croatia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-HU**: (Hungary)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-IE**: (Ireland)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-IM**: (Isle of Man)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-IS**: (Iceland)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-IT**: (Italy)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-IT-32**: (Trentino-Alto Adige Italy)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-JE**: (Jersey)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-LI**: (Liechtenstein)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-LT**: (Lithuania)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-LU**: (Luxembourg)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-LV**: (Latvia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-MC**: (Monaco)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-MD**: (Moldova)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-ME**: (Montenegro)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-MK**: (North Macedonia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-MT**: (Malta)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-NL**: (Netherlands)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-NO**: (Norway)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-PL**: (Poland)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-PT**: (Portugal)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-QV**: (Kosovo)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-RO**: (Romania)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-RS**: (Serbia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-RU**: (Russia (Federation))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-SE**: (Sweden)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-SI**: (Slovenia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-SK**: (Slovakia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-SM**: (San Marino)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-SUHH**: (Soviet Union)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-UA**: (Ukraine)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-VA**: (Vatican City)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-YUCS**: (Yugoslavia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB**: (Asia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-AE**: (United Arab Emirates)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-AF**: (Afghanistan)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-AM**: (Armenia (Republic))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-AZ**: (Azerbaijan)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-BD**: (Bangladesh)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-BH**: (Bahrain)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-BN**: (Brunei)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-BT**: (Bhutan)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-BUMM**: (Burma)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-CN**: (China)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-CN-54**: (Tibet (China))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-GE**: (Georgia (Republic))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-HK**: (Hong Kong)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-ID**: (Indonesia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-IL**: (Israel)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-IN**: (India)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-IQ**: (Iraq)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-IR**: (Iran)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-JO**: (Jordan)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-JP**: (Japan)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-KG**: (Kyrgyzstan)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-KH**: (Cambodia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-KP**: (Korea (North))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-KR**: (Korea (South))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-KW**: (Kuwait)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-KZ**: (Kazakhstan)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-LA**: (Laos)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-LB**: (Lebanon)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-LK**: (Sri Lanka)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-MM**: (Burma)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-MN**: (Mongolia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-MO**: (Macau (China : Special Administrative Region))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-MV**: (Maldives)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-MY**: (Malaysia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-NP**: (Nepal)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-OM**: (Oman)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-PH**: (Philippines)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-PK**: (Pakistan)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-QA**: (Qatar)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-SA**: (Saudi Arabia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-SG**: (Singapore)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-SKIN**: (Sikkim)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-SY**: (Syria)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-TH**: (Thailand)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-TJ**: (Tajikistan)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-TL**: (East Timor)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-TM**: (Turkmenistan)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-TPTL**: (East Timor)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-TR**: (Turkey)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-TW**: (Taiwan)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-UZ**: (Uzbekistan)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-VDVN**: (Vietnam (South))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-VN**: (Vietnam)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-YDYE**: (Yemen (Republic))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-YE**: (Yemen)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC**: (Africa)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-AIDJ**: (French Afars and Issas)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-AO**: (Angola)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-BF**: (Burkina Faso)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-BI**: (Burundi)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-BJ**: (Benin)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-BW**: (Botswana)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-CD**: (Congo (Democratic Republic))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-CF**: (Central African Republic)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-CG**: (Congo (Brazzaville))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-CI**: (Côte d'Ivoire)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-CM**: (Cameroon)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-CV**: (Cape Verde)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-DJ**: (Djibouti)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-DYBJ**: (Dahomey)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-DZ**: (Algeria)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-EG**: (Egypt)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-EH**: (Western Sahara)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-ER**: (Eritrea)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-ET**: (Ethiopia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-GA**: (Gabon)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-GH**: (Ghana)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-GM**: (Gambia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-GN**: (Guinea)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-GQ**: (Equatorial Guinea)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-GW**: (Guinea-Bissau)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-HVBF**: (Upper Volta)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-KE**: (Kenya)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-KM**: (Comoros)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-LR**: (Liberia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-LS**: (Lesotho)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-LY**: (Libya)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-MA**: (Morocco)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-MG**: (Madagascar)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-ML**: (Mali)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-MR**: (Mauritania)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-MU**: (Mauritius)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-MW**: (Malawi)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-MZ**: (Mozambique)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-NA**: (Namibia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-NE**: (Niger)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-NG**: (Nigeria)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-RHZW**: (Southern Rhodesia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-RW**: (Rwanda)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-SC**: (Seychelles)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-SD**: (Sudan)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-SL**: (Sierra Leone)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-SN**: (Senegal)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-SO**: (Somalia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-SS**: (South Sudan)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-ST**: (Sao Tome and Principe)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-SZ**: (Swaziland)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-TD**: (Chad)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-TG**: (Togo)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-TN**: (Tunisia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-TZ**: (Tanzania)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-UG**: (Uganda)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-YT**: (Mayotte)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-ZA**: (South Africa)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-ZM**: (Zambia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-ZRCD**: (Zaire)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-ZW**: (Zimbabwe)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD**: (America)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-AG**: (Antigua and Barbuda)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-AI**: (Anguilla)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-ANHH**: (Netherlands Antilles)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-AR**: (Argentina)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-AS**: (American Samoa)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-AW**: (Aruba)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-BB**: (Barbados)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-BL**: (Saint Barthélemy)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-BM**: (Bermuda Islands)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-BO**: (Bolivia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-BR**: (Brazil)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-BQ**: (Bonaire, Sint Eustatius and Saba)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-BS**: (Bahamas)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-BZ**: (Belize)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-CA**: (Canada)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-CL**: (Chile)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-CO**: (Colombia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-CR**: (Costa Rica)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-CU**: (Cuba)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-CW**: (Curaçao)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-DM**: (Dominica)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-DO**: (Dominican Republic)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-EC**: (Ecuador)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-GD**: (Grenada)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-GF**: (French Guiana)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-GP**: (Guadeloupe)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-GT**: (Guatemala)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-GY**: (Guyana)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-HN**: (Honduras)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-HT**: (Haiti)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-JM**: (Jamaica)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-KN**: (Saint Kitts-Nevis)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-KY**: (Cayman Islands)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-LC**: (Saint Lucia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-MF**: (Saint Martin, Northern)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-MQ**: (Martinique)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-MS**: (Montserrat)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-MX**: (Mexico)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-NI**: (Nicaragua)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-PA**: (Panama)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-PE**: (Peru)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-PM**: (Saint Pierre and Miquelon)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-PR**: (Puerto Rico)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-PY**: (Paraguay)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-PZPA**: (Panama Canal Zone)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-SR**: (Suriname)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-SV**: (El Salvador)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-SX**: (Sint Maarten (Dutch part))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-TC**: (Turks and Caicos Islands)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-TT**: (Trinidad and Tobago)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-US**: (United States)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-UY**: (Uruguay)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-VC**: (Saint Vincent and the Grenadines)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-VE**: (Venezuela)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-VG**: (British Virgin Islands)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-VI**: (Virgin Islands of the United States)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE**: (Australia, Oceania)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-AU**: (Australia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-CC**: (Cocos (Keeling) Islands)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-CK**: (Cook Islands)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-CTKI**: (Canton and Enderbury)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-CX**: (Christmas Island (Indian Ocean))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-FJ**: (Fiji)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-FM**: (Micronesia (Federated States))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-GEHH**: (Gilbert Islands)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-GU**: (Guam)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-KI**: (Kiribati)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-MH**: (Marshall Islands)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-MP**: (Northern Mariana Islands)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-NC**: (New Caledonia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-NF**: (Norfolk Island)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-NHVU**: (New Hebrides)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-NR**: (Nauru)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-NZ**: (New Zealand)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-PCHH**: (Pacific Islands (Trust Territory))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-PF**: (French Polynesia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-PG**: (Papua New Guinea)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-PW**: (Palau)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-SB**: (Solomon Islands)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-TK**: (Tokelau)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-TO**: (Tonga)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-TV**: (Tuvalu)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-VU**: (Vanuatu)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-WF**: (Wallis and Futuna)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-WS**: (Samoa)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XH**: (Arctic)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XI**: (Antarctic Ocean/Antarctica)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XI-AQ**: (Antarctica)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XI-BQAQ**: (British Antarctic Territory)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XI-FQHH**: (Terres australes et antarctiques françaises)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XI-HM**: (Heard and McDonald Islands)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XI-NQAQ**: (Queen Maud Land)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XK**: (Atlantic Ocean)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XK-BV**: (Bouvet Island)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XK-FK**: (Falkland Islands)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XK-FO**: (Faroe Islands)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XK-GL**: (Greenland)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XK-GS**: (South Georgia and the South Sandwich Islands)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XK-SH**: (Saint Helena)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XK-SJ**: (Svalbard and Jan Mayen)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XL**: (Indian Ocean)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XL-IO**: (British Indian Ocean Territory)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XL-RE**: (Réunion)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XL-TF**: (French Southern Territories (the))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XM**: (Pacific Ocean)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XM-JTUM**: (Johnston Atoll)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XM-MIUM**: (Midway Islands)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XM-NU**: (Niue)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XM-PN**: (Pitcairn Island)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XM-PUUM**: (American Territory in the Pacific (-1986))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XM-UM**: (United States Misc. Pacific Islands)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XM-WKUM**: (Wake Island)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XN**: (Outer Space)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XP**: (International Organizations)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XQ**: (World)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XR**: (Orient)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XS**: (Ancient Greece)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XT**: (Rome)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XU**: (Byzantine Empire)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XV**: (Ottoman Empire)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XW**: (Palestinian Arabs)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XX**: (Arab Countries)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XY**: (Jews)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XZ**: (Imaginary places)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#ZZ**: (Country unknown)

---

<p style="margin-bottom:60px"></p>

<a name="d17e5459"></a>
### GND Term URI
|     |     |
| --- | --- |
| **Name** | `@gndo:term`  |
| **Datatype** | anyURI (*RNG Pattern Matching*) |
| **Label** (de) | GND Term URI |
| **Description** (de) | URI eines Terms entweder aus der **GND** (https://d-nb.info/gnd/) oder einem **GND Vokabular** (https://d-nb.info/standards/vocab/) |
| **Contained by** | [`gndo:functionOrRole`](specs-elems.md#d17e3460)  [`gndo:gndSubjectCategory`](specs-elems.md#d17e3634)  [`gndo:professionOrOccupation`](specs-elems.md#d17e4262) |

***Validation***  

```xml 
<param xmlns="http://relaxng.org/ns/structure/1.0" name="pattern">(http|https)://d-nb.info/standards/vocab/gnd/(.+)</param>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e5439"></a>
### GND-Referenz
|     |     |
| --- | --- |
| **Name** | `@gndo:ref`  |
| **Datatype** | anyURI (*RNG Pattern Matching*) |
| **Label** (de) | GND-Referenz |
| **Label** (en) | GND-Reference |
| **Description** (de) | Referenz auf eine Entität in der GND, identifiziert durch einen GND-URI. |
| **Description** (en) | Reference to an entity in the GND identified by a GND-URI. |
| **Contained by** | [`bf:instanceOf`](specs-elems.md#instance-of)  [`embodimentOf`](specs-elems.md#d17e1339)  [`gndo:affiliation`](specs-elems.md#d17e2929)  [`gndo:author`](specs-elems.md#d17e2952)  [`gndo:editor`](specs-elems.md#d17e3277)  [`gndo:fieldOfStudy`](specs-elems.md#d17e3363)  [`gndo:firstAuthor`](specs-elems.md#d17e3403)  [`gndo:formOfWorkAndExpression`](specs-elems.md#d17e3332)  [`gndo:literarySource`](specs-elems.md#d17e3787)  [`gndo:place`](specs-elems.md#geographic-entity-place)  [`gndo:placeOfActivity`](specs-elems.md#person-record-placeOfActivity)  [`gndo:placeOfBirth`](specs-elems.md#person-record-placeOfBirth)  [`gndo:placeOfBusiness`](specs-elems.md#d17e3967)  [`gndo:placeOfDeath`](specs-elems.md#person-record-placeOfDeath)  [`gndo:precedingCorporateBody`](specs-elems.md#d17e4133)  [`gndo:precedingPlaceOrGeographicName`](specs-elems.md#d17e4101)  [`gndo:professionOrOccupation`](specs-elems.md#d17e4262)  [`gndo:pseudonym`](specs-elems.md#d17e4321)  [`gndo:publication`](specs-elems.md#d17e4368)  [`gndo:relatedWork`](specs-elems.md#d17e4565)  [`gndo:relatesTo`](specs-elems.md#d17e4511)  [`gndo:succeedingCorporateBody`](specs-elems.md#d17e4596)  [`gndo:succeedingPlaceOrGeographicName`](specs-elems.md#d17e4626)  [`gndo:temporaryName`](specs-elems.md#d17e4681)  [`gndo:titleOfNobility`](specs-elems.md#d17e4726)  [`realizationOf`](specs-elems.md#d17e1265) |

***Validation***  

```xml 
<param xmlns="http://relaxng.org/ns/structure/1.0" name="pattern">(http|https)://d-nb.info/gnd/(.+)</param>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e5417"></a>
### GND-URI
|     |     |
| --- | --- |
| **Name** | `@gndo:uri`  |
| **Datatype** | anyURI (*RNG Pattern Matching*) |
| **Label** (de) | GND-URI |
| **Label** (en) | GND-URI |
| **Description** (de) | Angabe des GND-HTTP URIs der beschriebenen Entität in der GND, sofern vorhanden. |
| **Description** (en) | - |
| **Contained by** | [`corporateBody`](specs-elems.md#corporate-body-record)  [`entity`](specs-elems.md#entity-record)  [`event`](specs-elems.md#d17e243)  [`expression`](specs-elems.md#d17e1235)  [`manifestation`](specs-elems.md#d17e1304)  [`person`](specs-elems.md#person-record)  [`place`](specs-elems.md#d17e1061)  [`work`](specs-elems.md#d17e1378) |

***Validation***  

```xml 
<param xmlns="http://relaxng.org/ns/structure/1.0" name="pattern">(http|https)://d-nb.info/gnd/(.+)</param>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e191"></a>
### GNDO-Typ der beschriebenen Entität
|     |     |
| --- | --- |
| **Name** | `@gndo:type`  |
| **Datatype** | string |
| **Label** (de) | GNDO-Typ der beschriebenen Entität |
| **Description** (de) | URI eines Entitätstyps (Class) aus der [GNDO](https://d-nb.info/standards/elementset/gnd#). |
| **Contained by** | [`entity`](specs-elems.md#entity-record) |

***Predefined Values***  

 - **https://d-nb.info/standards/elementset/gnd#CorporateBody**: (Körperschaft) [GNDO](https://d-nb.info/standards/elementset/gnd#id-39ad8031221791a9a0a5adafd88b8ff9)
 - **https://d-nb.info/standards/elementset/gnd#Company**: (Firma) [GNDO](https://d-nb.info/standards/elementset/gnd#id-1d31818b589bae5f506efcd36f8e4071)
 - **https://d-nb.info/standards/elementset/gnd#FictiveCorporateBody**: (Fiktive Körperschaft) [GNDO](https://d-nb.info/standards/elementset/gnd#id-233492de7966e18b7fe36583464ada24)
 - **https://d-nb.info/standards/elementset/gnd#MusicalCorporateBody**: (Musikalische Körperschaft) [GNDO](https://d-nb.info/standards/elementset/gnd#id-227522d76d555c9211e00f612ea79763)
 - **https://d-nb.info/standards/elementset/gnd#OrganOfCorporateBody**: (Organ einer Körperschaft) [GNDO](https://d-nb.info/standards/elementset/gnd#id-908a2b3cce6f09b3795666879fa1b13f)
 - **https://d-nb.info/standards/elementset/gnd#ProjectOrProgram**: (Projekt oder Programm) [GNDO](https://d-nb.info/standards/elementset/gnd#id-4dc1ef59dd262d466aeaf68091c1afdd)
 - **https://d-nb.info/standards/elementset/gnd#ReligiousAdministrativeUnit**: (Religiöse Verwaltungseinheit) [GNDO](https://d-nb.info/standards/elementset/gnd#id-4862583256b7553a04901cb9d78d2d8f)
 - **https://d-nb.info/standards/elementset/gnd#ReligiousCorporateBody**: (Religiöse Körperschaft) [GNDO](https://d-nb.info/standards/elementset/gnd#id-f5be3238b49f943d1458d99173b33295)
 - **https://d-nb.info/standards/elementset/gnd#ConferenceOrEvent**: (Konferenz oder Veranstaltung) [GNDO](https://d-nb.info/standards/elementset/gnd#id-50ee5a233a390c91e26b0d72b0a9a437)
 - **https://d-nb.info/standards/elementset/gnd#DifferentiatedPerson**: (Individualisierte Person) [GNDO](https://d-nb.info/standards/elementset/gnd#id-07e558e11b6629c85c4cf0312a8d9acc)
 - **https://d-nb.info/standards/elementset/gnd#CollectivePseudonym**: (Sammelpseudonym) [GNDO](https://d-nb.info/standards/elementset/gnd#id-e1a0c1b1bb1f0c6d862c101153127efb)
 - **https://d-nb.info/standards/elementset/gnd#Gods**: (Götter) [GNDO](https://d-nb.info/standards/elementset/gnd#id-4c3eb377f41e2e358d95fd66e85531f6)
 - **https://d-nb.info/standards/elementset/gnd#LiteraryOrLegendaryCharacter**: (Literarische oder Sagengestalt) [GNDO](https://d-nb.info/standards/elementset/gnd#id-561f9122a59eb9f11ca9ac9fbbb41015)
 - **https://d-nb.info/standards/elementset/gnd#Pseudonym**: (Pseudonym) [GNDO](https://d-nb.info/standards/elementset/gnd#id-4a1b77ecd57d7fe7074050aa7fddef3e)
 - **https://d-nb.info/standards/elementset/gnd#RoyalOrMemberOfARoyalHouse**: (Regierender Fürst oder Mitglied eines regierenden Fürstenhauses) [GNDO](https://d-nb.info/standards/elementset/gnd#id-48fef5513dcda9669ac3250908b3fc0e)
 - **https://d-nb.info/standards/elementset/gnd#Spirits**: (Geister) [GNDO](https://d-nb.info/standards/elementset/gnd#id-a53c2a79950d8502724d1e1a31ab19cb)
 - **https://d-nb.info/standards/elementset/gnd#PlaceOrGeographicName**: (Geografikum) [GNDO](https://d-nb.info/standards/elementset/gnd#id-a708231e5f676cb2d2aed717ac50d3c0)
 - **https://d-nb.info/standards/elementset/gnd#AdministrativeUnit**: (Verwaltungseinheit) [GNDO](https://d-nb.info/standards/elementset/gnd#id-1f0359b1603bcf7dfd0e822f7f4ffce7)
 - **https://d-nb.info/standards/elementset/gnd#BuildingOrMemorial**: (Bauwerk oder Denkmal) [GNDO](https://d-nb.info/standards/elementset/gnd#id-6873abeb35c905e0a6bd4279db13338e)
 - **https://d-nb.info/standards/elementset/gnd#Country**: (Land oder Staat) [GNDO](https://d-nb.info/standards/elementset/gnd#id-28d79d328de51fb38a288f6386717122)
 - **https://d-nb.info/standards/elementset/gnd#ExtraterrestrialTerritory**: (Extraterrestrikum) [GNDO](https://d-nb.info/standards/elementset/gnd#id-4a1b77ecd57d7fe7074050aa7fddef3e)
 - **https://d-nb.info/standards/elementset/gnd#FictivePlace**: (Fiktiver Ort) [GNDO](https://d-nb.info/standards/elementset/gnd#id-03854be83ea75e449883470169192ac1)
 - **https://d-nb.info/standards/elementset/gnd#MemberState**: (Gliedstaat) [GNDO](https://d-nb.info/standards/elementset/gnd#id-b9a80ea4ee95ebb62d0b78e7bc1baeb2)
 - **https://d-nb.info/standards/elementset/gnd#NameOfSmallGeographicUnitLyingWithinAnotherGeographicUnit**: (Kleinräumiges Geografikum innerhalb eines Ortes) [GNDO](https://d-nb.info/standards/elementset/gnd#id-d804f233455af5455ceea054be998d1f)
 - **https://d-nb.info/standards/elementset/gnd#NaturalGeographicUnit**: (Natürlich geografische Einheit) [GNDO](https://d-nb.info/standards/elementset/gnd#id-cdeb648dbe6d599c7f6f3ed98e7f3cdd)
 - **https://d-nb.info/standards/elementset/gnd#ReligiousTerritory**: (Religiöses Territorium) [GNDO](https://d-nb.info/standards/elementset/gnd#id-580efe99e43d92a022fe381e592afecf)
 - **https://d-nb.info/standards/elementset/gnd#TerritorialCorporateBodyOrAdministrativeUnit**: (Gebietskörperschaft oder Verwaltungseinheit) [GNDO](https://d-nb.info/standards/elementset/gnd#id-5752c1088a8f6f21005a54f1b3ac1edf)
 - **https://d-nb.info/standards/elementset/gnd#WayBorderOrLine**: (Weg, Grenze oder Linie) [GNDO](https://d-nb.info/standards/elementset/gnd#id-508b5841a6ebe341e738e53fbf1c7455)
 - **https://d-nb.info/standards/elementset/gnd#Work**: (Werk) [GNDO](https://d-nb.info/standards/elementset/gnd#id-c1e6e78875a240757c51a48091e75f13)
 - **https://d-nb.info/standards/elementset/gnd#Collection**: (Sammlung) [GNDO](https://d-nb.info/standards/elementset/gnd#id-cefc48c804ce704690b446462cb31aec)
 - **https://d-nb.info/standards/elementset/gnd#CollectiveManuscript**: (Sammelhandschrift) [GNDO](https://d-nb.info/standards/elementset/gnd#id-6a1601c81cac71d7324e45d4dcdc0771)
 - **https://d-nb.info/standards/elementset/gnd#Expression**: (Expression) [GNDO](https://d-nb.info/standards/elementset/gnd#id-3bd92dac383bb4fb6b9a636cf9bf9910)
 - **https://d-nb.info/standards/elementset/gnd#Manuscript**: (Schriftdenkmal) [GNDO](https://d-nb.info/standards/elementset/gnd#id-80ff91c42283532e5e51e45b11d3783a)
 - **https://d-nb.info/standards/elementset/gnd#MusicalWork**: (Werk der Musik) [GNDO](https://d-nb.info/standards/elementset/gnd#id-9696269e5379c76bd51afb23a34d3c10)
 - **https://d-nb.info/standards/elementset/gnd#ProvenanceCharacteristic**: (Provenienzmerkmal) [GNDO](https://d-nb.info/standards/elementset/gnd#id-f0a8b51b89f16454cda6c0e332225bfc)
 - **https://d-nb.info/standards/elementset/gnd#VersionOfAMusicalWork**: (Fassung eines Werks der Musik) [GNDO](https://d-nb.info/standards/elementset/gnd#id-931aecb58a23c5f3ef7148cd891c38ee)

---

<p style="margin-bottom:60px"></p>

<a name="d17e5475"></a>
### ID
|     |     |
| --- | --- |
| **Name** | `@id`  |
| **Datatype** | ID |
| **Label** (de) | ID |
| **Description** (de) | Eine interne ID zur Identifikation des entsprechenden Elements. |
| **Contained by** | [`list`](specs-elems.md#data-list)  [`respStmt`](specs-elems.md#respStmt) |

---

<p style="margin-bottom:60px"></p>

<a name="d17e5379"></a>
### ISIL/Bibliothekssiegel
|     |     |
| --- | --- |
| **Name** | `@isil`  |
| **Datatype** | string (*RNG Pattern Matching*) |
| **Label** (de) | ISIL/Bibliothekssiegel |
| **Label** (de) | ISIL/Acronym for libraries |
| **Description** (de) | Eindeutiger Identifikator der Organisation als ISIL (International Standard Identifier for Libraries and Related Organisations). |
| **Description** (de) | ISIL of an Organisation (International Standard Identifier for Libraries and Related Organisations). |
| **Contained by** | [`agency`](specs-elems.md#agency-stmt) |

***Validation***  

```xml 
<param xmlns="http://relaxng.org/ns/structure/1.0" name="pattern">[A-Z]{1,4}-[a-zA-Z0-9\-/:]{1,11}</param>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="attr.iso-date"></a>
### ISO-Datum
|     |     |
| --- | --- |
| **Name** | `@iso-date`  |
| **Datatype** | date gYearMonth gYear  |
| **Label** (de) | ISO-Datum |
| **Label** (en) | ISO-Date |
| **Description** (de) | Ein ISO Datum im Format JJJJ-MM-TT, JJJJ-MM oder JJJJ |
| **Description** (en) | An ISO date in the Format YYYY-MM-DD, YYYY-MM or YYYY |
| **Contained by** | [`foaf:page`](specs-elems.md#elem.foaf.page)  [`gndo:dateOfBirth`](specs-elems.md#person-record-dateOfBirth)  [`gndo:dateOfDeath`](specs-elems.md#person-record-dateOfDeath)  [`gndo:dateOfEstablishment`](specs-elems.md#d17e3105)  [`gndo:dateOfPublication`](specs-elems.md#d17e3174)  [`gndo:dateOfTermination`](specs-elems.md#d17e3201)  [`gndo:homepage`](specs-elems.md#elem.gndo.homepage) |

---

<p style="margin-bottom:60px"></p>

<a name="d17e586"></a>
### Körperschaftstyp
|     |     |
| --- | --- |
| **Name** | `@gndo:type`  |
| **Datatype** | string |
| **Label** (de) | Körperschaftstyp |
| **Label** (en) | Type of a Corporate Body |
| **Description** (de) | Typisierung der dokumentierten Körperschaft (z.B. als fiktive Körperschaft, Firma usw). Wenn kein `@gndo:type` vergeben wird, gilt die Körperschaft als [gndo:CorporateBody](https://d-nb.info/standards/elementset/gnd#id-39ad8031221791a9a0a5adafd88b8ff9). |
| **Description** (en) | Spezifies the Corporate Body encoded as eg. fictional corporate body, company etc.). If no `@gndo:type` is provided the default is [gndo:CorporateBody](https://d-nb.info/standards/elementset/gnd#id-39ad8031221791a9a0a5adafd88b8ff9). |
| **Contained by** | [`corporateBody`](specs-elems.md#corporate-body-record) |

***Predefined Values***  

 - **https://d-nb.info/standards/elementset/gnd#Company**: (Firma) [GNDO](https://d-nb.info/standards/elementset/gnd#id-1d31818b589bae5f506efcd36f8e4071)
 - **https://d-nb.info/standards/elementset/gnd#FictiveCorporateBody**: (Fiktive Körperschaft) [GNDO](https://d-nb.info/standards/elementset/gnd#id-233492de7966e18b7fe36583464ada24)
 - **https://d-nb.info/standards/elementset/gnd#MusicalCorporateBody**: (Musikalische Körperschaft) [GNDO](https://d-nb.info/standards/elementset/gnd#id-227522d76d555c9211e00f612ea79763)
 - **https://d-nb.info/standards/elementset/gnd#OrganOfCorporateBody**: (Organ einer Körperschaft) [GNDO](https://d-nb.info/standards/elementset/gnd#id-908a2b3cce6f09b3795666879fa1b13f)
 - **https://d-nb.info/standards/elementset/gnd#ProjectOrProgram**: (Projekt oder Programm) [GNDO](https://d-nb.info/standards/elementset/gnd#id-4dc1ef59dd262d466aeaf68091c1afdd)
 - **https://d-nb.info/standards/elementset/gnd#ReligiousAdministrativeUnit**: (Religiöse Verwaltungseinheit) [GNDO](https://d-nb.info/standards/elementset/gnd#id-4862583256b7553a04901cb9d78d2d8f)
 - **https://d-nb.info/standards/elementset/gnd#ReligiousCorporateBody**: (Religiöse Körperschaft) [GNDO](https://d-nb.info/standards/elementset/gnd#id-f5be3238b49f943d1458d99173b33295)

---

<p style="margin-bottom:60px"></p>

<a name="d17e3757"></a>
### Language Code URI
|     |     |
| --- | --- |
| **Name** | `@gndo:term`  |
| **Datatype** | anyURI (*Schematron Pattern*) |
| **Label** (de) | Language Code URI |
| **Description** (de) | Ein URI, bevorzugt aus dem [LOC ISO 693-2 Language Vocabulary](http://id.loc.gov/vocabulary/iso639-2/) |
| **Contained by** | [`gndo:languageCode`](specs-elems.md#d17e3736) |

***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="gndo:languageCode/@gndo:term" role="warning">
      <sch:assert test="matches(., '(http|https)://id.loc.gov/vocabulary/iso639-2/(.+)')">(Missing <sch:name/>): It is recommendet to use an URI Descriptor 
                            from the LOC ISO 693-2 Language Vocabulary (http://id.loc.gov/vocabulary/iso639-2/).</sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e1167"></a>
### Ortstyp
|     |     |
| --- | --- |
| **Name** | `@gndo:type`  |
| **Datatype** | string |
| **Label** (de) | Ortstyp |
| **Label** (en) | Type of a Place |
| **Description** (de) | Typisierung des dokumentierten Orts (z.B. als fiktiver Ort, Gebäude oder Land). Wenn kein `@gndo:type` vergeben wird, gilt der Ort als [gndo:PlaceOrGeographicName](https://d-nb.info/standards/elementset/gnd#id-a708231e5f676cb2d2aed717ac50d3c0). |
| **Description** (en) | Spezifies the place encoded as eg. fictional Place, Building or Country). If no `@gndo:type` is provided the default is [gndo:PlaceOrGeographicName](https://d-nb.info/standards/elementset/gnd#id-a708231e5f676cb2d2aed717ac50d3c0). |
| **Contained by** | [`place`](specs-elems.md#d17e1061) |

***Predefined Values***  

 - **https://d-nb.info/standards/elementset/gnd#AdministrativeUnit**: (Verwaltungseinheit) [GNDO](https://d-nb.info/standards/elementset/gnd#id-1f0359b1603bcf7dfd0e822f7f4ffce7)
 - **https://d-nb.info/standards/elementset/gnd#BuildingOrMemorial**: (Bauwerk oder Denkmal) [GNDO](https://d-nb.info/standards/elementset/gnd#id-6873abeb35c905e0a6bd4279db13338e)
 - **https://d-nb.info/standards/elementset/gnd#Country**: (Land oder Staat) [GNDO](https://d-nb.info/standards/elementset/gnd#id-28d79d328de51fb38a288f6386717122)
 - **https://d-nb.info/standards/elementset/gnd#ExtraterrestrialTerritory**: (Extraterrestrikum) [GNDO](https://d-nb.info/standards/elementset/gnd#id-4a1b77ecd57d7fe7074050aa7fddef3e)
 - **https://d-nb.info/standards/elementset/gnd#FictivePlace**: (Fiktiver Ort) [GNDO](https://d-nb.info/standards/elementset/gnd#id-03854be83ea75e449883470169192ac1)
 - **https://d-nb.info/standards/elementset/gnd#MemberState**: (Gliedstaat) [GNDO](https://d-nb.info/standards/elementset/gnd#id-b9a80ea4ee95ebb62d0b78e7bc1baeb2)
 - **https://d-nb.info/standards/elementset/gnd#NameOfSmallGeographicUnitLyingWithinAnotherGeographicUnit**: (Kleinräumiges Geografikum innerhalb eines Ortes) [GNDO](https://d-nb.info/standards/elementset/gnd#id-d804f233455af5455ceea054be998d1f)
 - **https://d-nb.info/standards/elementset/gnd#NaturalGeographicUnit**: (Natürlich geografische Einheit) [GNDO](https://d-nb.info/standards/elementset/gnd#id-cdeb648dbe6d599c7f6f3ed98e7f3cdd)
 - **https://d-nb.info/standards/elementset/gnd#ReligiousTerritory**: (Religiöses Territorium) [GNDO](https://d-nb.info/standards/elementset/gnd#id-580efe99e43d92a022fe381e592afecf)
 - **https://d-nb.info/standards/elementset/gnd#TerritorialCorporateBodyOrAdministrativeUnit**: (Gebietskörperschaft oder Verwaltungseinheit) [GNDO](https://d-nb.info/standards/elementset/gnd#id-5752c1088a8f6f21005a54f1b3ac1edf)
 - **https://d-nb.info/standards/elementset/gnd#WayBorderOrLine**: (Weg, Grenze oder Linie) [GNDO](https://d-nb.info/standards/elementset/gnd#id-508b5841a6ebe341e738e53fbf1c7455)

---

<p style="margin-bottom:60px"></p>

<a name="person-types"></a>
### Personentyp
|     |     |
| --- | --- |
| **Name** | `@gndo:type`  |
| **Datatype** | string |
| **Label** (de) | Personentyp |
| **Label** (en) | Type of a Person |
| **Description** (de) | Typisierung der dokumentierten Person (z.B. als fiktive Person, Gott oder royale Person). Wenn kein `@gndo:type` vergeben wird, gilt die Person als [gndo:DifferentiatedPerson](https://d-nb.info/standards/elementset/gnd#id-07e558e11b6629c85c4cf0312a8d9acc). |
| **Description** (en) | Spezifies the person encoded as eg. fictional character, god oder royal). If no `@gndo:type` is provided the default is [gndo:DifferentiatedPerson](https://d-nb.info/standards/elementset/gnd#id-07e558e11b6629c85c4cf0312a8d9acc). |
| **Contained by** | [`person`](specs-elems.md#person-record) |

***Predefined Values***  

 - **https://d-nb.info/standards/elementset/gnd#CollectivePseudonym**: (Sammelpseudonym) [GNDO](https://d-nb.info/standards/elementset/gnd#id-e1a0c1b1bb1f0c6d862c101153127efb)
 - **https://d-nb.info/standards/elementset/gnd#Gods**: (Götter) [GNDO](https://d-nb.info/standards/elementset/gnd#id-4c3eb377f41e2e358d95fd66e85531f6)
 - **https://d-nb.info/standards/elementset/gnd#LiteraryOrLegendaryCharacter**: (Literarische oder Sagengestalt) [GNDO](https://d-nb.info/standards/elementset/gnd#id-561f9122a59eb9f11ca9ac9fbbb41015)
 - **https://d-nb.info/standards/elementset/gnd#Pseudonym**: (Pseudonym) [GNDO](https://d-nb.info/standards/elementset/gnd#id-4a1b77ecd57d7fe7074050aa7fddef3e)
 - **https://d-nb.info/standards/elementset/gnd#RoyalOrMemberOfARoyalHouse**: (Regierender Fürst oder Mitglied eines regierenden Fürstenhauses) [GNDO](https://d-nb.info/standards/elementset/gnd#id-48fef5513dcda9669ac3250908b3fc0e)
 - **https://d-nb.info/standards/elementset/gnd#Spirits**: (Geister) [GNDO](https://d-nb.info/standards/elementset/gnd#id-a53c2a79950d8502724d1e1a31ab19cb)

---

<p style="margin-bottom:60px"></p>

<a name="d17e5260"></a>
### Projekt ID
|     |     |
| --- | --- |
| **Name** | `@project`  |
| **Datatype** | string |
| **Label** (de) | Projekt ID |
| **Description** (de) | Identifiers einer Projekts, mit dem der Datensatz in einem entityXML Store assoziiert ist. |
| **Contained by** | [`store:store`](specs-elems.md#d17e5232) |

---

<p style="margin-bottom:60px"></p>

<a name="d17e4983"></a>
### Provider-ID
|     |     |
| --- | --- |
| **Name** | `@id`  |
| **Datatype** | ID |
| **Label** (de) | Provider-ID |
| **Description** (de) | Der GND-Agentur Identifier des Datenproviders. Dieser Identifier wird in der Regel direkt von der GND-Agentur, bei der der Datenlieferant registriert ist, vergeben. |
| **Contained by** | [`provider`](specs-elems.md#data-provider) |

---

<p style="margin-bottom:60px"></p>

<a name="d17e2788"></a>
### Quelle der Koordinaten
|     |     |
| --- | --- |
| **Name** | `@geo:source`  |
| **Datatype** | anyURI |
| **Label** (de) | Quelle der Koordinaten |
| **Label** (en) | Source of Coordinates |
| **Description** (de) | Ein URL zur Quelle der documentierten Koordinaten. |
| **Description** (en) | An URL pointing to the source of the coordinates encoded. |
| **Contained by** | [`geo:hasGeometry`](specs-elems.md#d17e2764) |

---

<p style="margin-bottom:60px"></p>

<a name="d17e5720"></a>
### Record ID
|     |     |
| --- | --- |
| **Name** | `@xml:id`  |
| **Datatype** | ID |
| **Label** (de) | Record ID |
| **Description** (de) | Angabe eines Identifiers zur Identifikation eines Entitäteneintrags innerhalb der Datensammlung. Die Angabe einer ID via `@xml:id` ist verpflichtend! |
| **Contained by** | [`corporateBody`](specs-elems.md#corporate-body-record)  [`entity`](specs-elems.md#entity-record)  [`event`](specs-elems.md#d17e243)  [`expression`](specs-elems.md#d17e1235)  [`manifestation`](specs-elems.md#d17e1304)  [`person`](specs-elems.md#person-record)  [`place`](specs-elems.md#d17e1061)  [`work`](specs-elems.md#d17e1378) |

---

<p style="margin-bottom:60px"></p>

<a name="d17e5735"></a>
### Referenz
|     |     |
| --- | --- |
| **Name** | `@ref`  |
| **Datatype** | anyURI (*Schematron Pattern*) |
| **Label** (de) | Referenz |
| **Description** (de) | Verweis auf die ID (`@xml:id`) eines Records in der selben entityXML Ressource oder auf einen HTTP-URI. |
| **Contained by** | [`bf:instanceOf`](specs-elems.md#instance-of)  [`embodimentOf`](specs-elems.md#d17e1339)  [`gndo:affiliation`](specs-elems.md#d17e2929)  [`gndo:author`](specs-elems.md#d17e2952)  [`gndo:editor`](specs-elems.md#d17e3277)  [`gndo:fieldOfStudy`](specs-elems.md#d17e3363)  [`gndo:firstAuthor`](specs-elems.md#d17e3403)  [`gndo:formOfWorkAndExpression`](specs-elems.md#d17e3332)  [`gndo:literarySource`](specs-elems.md#d17e3787)  [`gndo:place`](specs-elems.md#geographic-entity-place)  [`gndo:placeOfActivity`](specs-elems.md#person-record-placeOfActivity)  [`gndo:placeOfBirth`](specs-elems.md#person-record-placeOfBirth)  [`gndo:placeOfBusiness`](specs-elems.md#d17e3967)  [`gndo:placeOfDeath`](specs-elems.md#person-record-placeOfDeath)  [`gndo:precedingCorporateBody`](specs-elems.md#d17e4133)  [`gndo:precedingPlaceOrGeographicName`](specs-elems.md#d17e4101)  [`gndo:pseudonym`](specs-elems.md#d17e4321)  [`gndo:publication`](specs-elems.md#d17e4368)  [`gndo:relatedWork`](specs-elems.md#d17e4565)  [`gndo:relatesTo`](specs-elems.md#d17e4511)  [`gndo:succeedingCorporateBody`](specs-elems.md#d17e4596)  [`gndo:succeedingPlaceOrGeographicName`](specs-elems.md#d17e4626)  [`gndo:temporaryName`](specs-elems.md#d17e4681)  [`gndo:titleOfNobility`](specs-elems.md#d17e4726)  [`realizationOf`](specs-elems.md#d17e1265) |

***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="@ref[starts-with(., '#')]" role="error">
      <sch:let name="id" value="substring-after(data(.), '#')"/>
      <sch:assert test="//element()[@xml:id = $id]">(Wrong ID): There is no ID "<sch:value-of select="$id"/>" in this resource! Your reference is most likely wrong!</sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e4416"></a>
### Rolle der Entität (Publikation)
|     |     |
| --- | --- |
| **Name** | `@role`  |
| **Datatype** | string |
| **Label** (de) | Rolle der Entität (Publikation) |
| **Description** (de) | Hinweis darauf, welche Rolle die beschreibene Entität in Zusammenhang mit der Publikation spielt. |
| **Contained by** | [`gndo:publication`](specs-elems.md#d17e4368) |

***Predefined Values***  

 - **author**: (Autor:in *default*) Die beschriebene Entität (Person) ist der/die Autor:in der Publikation.
 - **editor**: (Herausgeber:in) Die beschriebene Entität (Person) ist der/die Herausgeber:in der Publikation.
 - **about**: (About) Die Publikation handelt von der beschriebenen Entität.

---

<p style="margin-bottom:60px"></p>

<a name="d17e4294"></a>
### Signifikanz
|     |     |
| --- | --- |
| **Name** | `@gndo:type`  |
| **Datatype** | string |
| **Label** (de) | Signifikanz |
| **Description** (de) | Angabe zur Signifikanz der Beschäftigung einer Person, d.h. ob es sich bei der angegebenen Beschäftigung um ihre hauptsächliche bzw. charakteristische Beschäfigung handelt. |
| **Contained by** | [`gndo:professionOrOccupation`](specs-elems.md#d17e4262) |

***Predefined Values***  

 - **significant**: (charakteristisch) Der Beruf oder der Tätigkeitsbereich ist für die Person charakteristisch.

---

<p style="margin-bottom:60px"></p>

<a name="d17e5550"></a>
### Sprache
|     |     |
| --- | --- |
| **Name** | `@xml:lang`  |
| **Datatype** | string |
| **Label** (de) | Sprache |
| **Label** (en) | Language |
| **Description** (de) | Die Sprache, in der die Information angegeben ist. |
| **Description** (en) | The language, the information is given in. |
| **Contained by** | [`dc:title`](specs-elems.md#d17e2645)  [`gndo:abbreviatedName`](specs-elems.md#d17e2856)  [`gndo:biographicalOrHistoricalInformation`](specs-elems.md#d17e2985)  [`gndo:preferredName`](specs-elems.md#person-record-preferredName)  [`gndo:preferredName`](specs-elems.md#d17e4196)  [`gndo:temporaryName`](specs-elems.md#d17e4681)  [`gndo:titleOfNobility`](specs-elems.md#d17e4726)  [`gndo:variantName`](specs-elems.md#person-record-variantName)  [`gndo:variantName`](specs-elems.md#elem.gndo.variantName.standard)  [`skos:note`](specs-elems.md#elem.skos.note) |

---

<p style="margin-bottom:60px"></p>

<a name="d17e5764"></a>
### Term
|     |     |
| --- | --- |
| **Name** | `@term`  |
| **Datatype** | string |
| **Label** (de) | Term |
| **Description** (de) | - |
| **Contained by** |  |

---

<p style="margin-bottom:60px"></p>

<a name="d17e5216"></a>
### URL
|     |     |
| --- | --- |
| **Name** | `@url`  |
| **Datatype** | string |
| **Label** (de) | URL |
| **Description** (de) | URL zu einer Quelle, falls vorhanden. |
| **Contained by** | [`source`](specs-elems.md#elem.source) |

---

<p style="margin-bottom:60px"></p>

<a name="d17e2245"></a>
### Verantwortliche Person
|     |     |
| --- | --- |
| **Name** | `@who`  |
| **Datatype** | IDREF |
| **Label** (de) | Verantwortliche Person |
| **Description** (de) | Die ID einer Person, die für die Änderung verantwortlich zeichnet. |
| **Contained by** | [`change`](specs-elems.md#d17e2229) |

---

<p style="margin-bottom:60px"></p>

<a name="d17e1467"></a>
### Werkstyp
|     |     |
| --- | --- |
| **Name** | `@gndo:type`  |
| **Datatype** | string |
| **Label** (de) | Werkstyp |
| **Label** (en) | Type of a Work |
| **Description** (de) | Typisierung des dokumentierten Werks (z.B. als Sammlung, Werk der Musik etc.). Wenn kein `@gndo:type` vergeben wird, gilt der Ort als [gndo:Work](https://d-nb.info/standards/elementset/gnd#id-c1e6e78875a240757c51a48091e75f13). |
| **Description** (en) | Spezifies the work encoded as eg. collection, musical work etc. If no `@gndo:type` is provided the default is [gndo:Work](https://d-nb.info/standards/elementset/gnd#id-c1e6e78875a240757c51a48091e75f13). |
| **Contained by** | [`work`](specs-elems.md#d17e1378) |

***Predefined Values***  

 - **https://d-nb.info/standards/elementset/gnd#Collection**: (Sammlung) [GNDO](https://d-nb.info/standards/elementset/gnd#id-cefc48c804ce704690b446462cb31aec)
 - **https://d-nb.info/standards/elementset/gnd#CollectiveManuscript**: (Sammelhandschrift) [GNDO](https://d-nb.info/standards/elementset/gnd#id-6a1601c81cac71d7324e45d4dcdc0771)
 - **https://d-nb.info/standards/elementset/gnd#Expression**: (Expression) [GNDO](https://d-nb.info/standards/elementset/gnd#id-3bd92dac383bb4fb6b9a636cf9bf9910)
 - **https://d-nb.info/standards/elementset/gnd#Manuscript**: (Schriftdenkmal) [GNDO](https://d-nb.info/standards/elementset/gnd#id-80ff91c42283532e5e51e45b11d3783a)
 - **https://d-nb.info/standards/elementset/gnd#MusicalWork**: (Werk der Musik) [GNDO](https://d-nb.info/standards/elementset/gnd#id-9696269e5379c76bd51afb23a34d3c10)
 - **https://d-nb.info/standards/elementset/gnd#ProvenanceCharacteristic**: (Provenienzmerkmal) [GNDO](https://d-nb.info/standards/elementset/gnd#id-f0a8b51b89f16454cda6c0e332225bfc)
 - **https://d-nb.info/standards/elementset/gnd#VersionOfAMusicalWork**: (Fassung eines Werks der Musik) [GNDO](https://d-nb.info/standards/elementset/gnd#id-931aecb58a23c5f3ef7148cd891c38ee)

---

<p style="margin-bottom:60px"></p>

<a name="d17e5021"></a>
### Zieladresse (Hyperlink)
|     |     |
| --- | --- |
| **Name** | `@target`  |
| **Datatype** | anyURI (*RNG Pattern Matching*) |
| **Label** (de) | Zieladresse (Hyperlink) |
| **Description** (de) | Zieladresse eines Hyperlinks, repräsentiert durch einen URL nach HTTP oder HTTPS Schema. |
| **Contained by** | [`ref`](specs-elems.md#elem.ref) |

***Validation***  

```xml 
<param xmlns="http://relaxng.org/ns/structure/1.0" name="pattern">(https|http)://(\S+)</param>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e5248"></a>
### entityXML Store ID
|     |     |
| --- | --- |
| **Name** | `@id`  |
| **Datatype** | string |
| **Label** (de) | entityXML Store ID |
| **Description** (de) | Identifier des Datensatzes in einem entityXML Store. |
| **Contained by** | [`store:store`](specs-elems.md#d17e5232) |

---

<p style="margin-bottom:60px"></p>

<a name="d17e5302"></a>
### name
|     |     |
| --- | --- |
| **Name** | `@name`  |
| **Datatype** | string |
| **Contained by** | [`store:step`](specs-elems.md#d17e5288) |

---

<p style="margin-bottom:60px"></p>

<a name="d17e5304"></a>
### timestamp
|     |     |
| --- | --- |
| **Name** | `@timestamp`  |
| **Datatype** | string |
| **Contained by** | [`store:step`](specs-elems.md#d17e5288) |

---

<p style="margin-bottom:60px"></p>

