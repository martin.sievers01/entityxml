
<a name="cb-record"></a>
# Körperschaften

[TODO]

```xml
<content>
   <attribute name="gndo:type"/>
   <attribute name="xml:id" required="true"/>
   <attribute name="gndo:uri"/>
   <attribute name="agency"/>
   <attribute name="enrich"/>
   <anyOrder>
      <element name="foaf:page" repeatable="true"/>
      <element name="gndo:abbreviatedName" repeatable="true"/>
      <element name="gndo:affiliation" repeatable="true"/>
      <element name="gndo:dateOfEstablishment"/>
      <element name="gndo:dateOfEstablishmentAndTermination"/>
      <element name="gndo:placeOfBusiness"/>
      <element name="gndo:precedingCorporateBody"/>
      <element name="gndo:preferredName"/>
      <element name="gndo:publication" repeatable="true"/>
      <element name="gndo:succeedingCorporateBody"/>
      <element name="gndo:temporaryName" repeatable="true"/>
      <element name="gndo:variantName" repeatable="true"/>
      <element name="dc:title"/>
      <element name="gndo:biographicalOrHistoricalInformation" repeatable="true"/>
      <element name="dublicateGndIdentifier" repeatable="true"/>
      <element name="gndo:geographicAreaCode" repeatable="true"/>
      <element name="gndo:gndIdentifier" repeatable="true"/>
      <element name="gndo:gndSubjectCategory" repeatable="true"/>
      <element name="gndo:languageCode" repeatable="true"/>
      <element name="gndo:relatesTo" repeatable="true"/>
      <element name="ref" repeatable="true"/>
      <element name="owl:sameAs" repeatable="true"/>
      <element name="skos:note" repeatable="true"/>
      <element name="source" repeatable="true"/>
      <anyElement type="narrow" repeatable="true"/>
      <text/>
   </anyOrder>
   <element name="revision"/>
</content>
```


**[`@gndo:type`](specs-attrs.md#d17e586)**  
(*Körperschaftstyp*): Typisierung der dokumentierten Körperschaft (z.B. als fiktive Körperschaft, Firma usw). Wenn kein `@gndo:type` vergeben wird, gilt die Körperschaft als [gndo:CorporateBody](https://d-nb.info/standards/elementset/gnd#id-39ad8031221791a9a0a5adafd88b8ff9).
  **Möglicher Wertebereich**:  


 - '*https://d-nb.info/standards/elementset/gnd#Company*' : (Firma) [GNDO](https://d-nb.info/standards/elementset/gnd#id-1d31818b589bae5f506efcd36f8e4071)
 - '*https://d-nb.info/standards/elementset/gnd#FictiveCorporateBody*' : (Fiktive Körperschaft) [GNDO](https://d-nb.info/standards/elementset/gnd#id-233492de7966e18b7fe36583464ada24)
 - '*https://d-nb.info/standards/elementset/gnd#MusicalCorporateBody*' : (Musikalische Körperschaft) [GNDO](https://d-nb.info/standards/elementset/gnd#id-227522d76d555c9211e00f612ea79763)
 - '*https://d-nb.info/standards/elementset/gnd#OrganOfCorporateBody*' : (Organ einer Körperschaft) [GNDO](https://d-nb.info/standards/elementset/gnd#id-908a2b3cce6f09b3795666879fa1b13f)
 - '*https://d-nb.info/standards/elementset/gnd#ProjectOrProgram*' : (Projekt oder Programm) [GNDO](https://d-nb.info/standards/elementset/gnd#id-4dc1ef59dd262d466aeaf68091c1afdd)
 - '*https://d-nb.info/standards/elementset/gnd#ReligiousAdministrativeUnit*' : (Religiöse Verwaltungseinheit) [GNDO](https://d-nb.info/standards/elementset/gnd#id-4862583256b7553a04901cb9d78d2d8f)
 - '*https://d-nb.info/standards/elementset/gnd#ReligiousCorporateBody*' : (Religiöse Körperschaft) [GNDO](https://d-nb.info/standards/elementset/gnd#id-f5be3238b49f943d1458d99173b33295)



**[`@xml:id`](specs-attrs.md#d17e5720)**  ***ID***  
(*Record ID*): Angabe eines Identifiers zur Identifikation eines Entitäteneintrags innerhalb der Datensammlung. Die Angabe einer ID via `@xml:id` ist verpflichtend!

**[`@gndo:uri`](specs-attrs.md#d17e5417)**  ***anyURI***  
(*GND-URI*): Angabe des GND-HTTP URIs der beschriebenen Entität in der GND, sofern vorhanden.(http|https)://d-nb.info/gnd/(.+)

**[`@agency`](specs-attrs.md#attr.record.agency)**  
(*Agenturanfrage*): Anfrage an die Agentur, welche Aktion in Hinblick auf den Eintrag durchgeführt werden soll.
  **Möglicher Wertebereich**:  


 - '*create*' : (Eintrag in der GND neu anlegen) Der Eintrag soll als Normdatensatz neu in der GND angelegt werden.
 - '*update*' : (Normdatensatz mit Daten aus dem Eintrag erweitern) Der Normdatensatz soll durch neue Informationen angereichert werden.
 - '*merge*' : (Normdatensätze zusammenführen) Dubletten, die in dem Eintrag dokumentiert werden, sollen mit dem Normdatensatz, der durch den GND-URI des Eintrags (`@gndo:uri`) identifiziert ist, zusammengeführt werden.
 - '*ignore*' : (Eintrag ingonieren) Der Eintrag wird von der Agentur nicht bearbeitet.



**[`@enrich`](specs-attrs.md#d17e5700)**  ***boolean***  
(*Anreicherung*): Der Record soll mit Daten eines externen Datendienstes angereichert werden. Hierfür wird ein zusätzliches Konversionsscript benötigt, dass die Konversionsroutinen zur verfügung stellt, um den Record anzureichern!

**[`foaf:page`](specs-elems.md#elem.foaf.page)**  
(*Webseite*): URL eines Dokuments mit näheren Informationen über die beschriebene Entität (z.B. Homepage, Wikipedia Seite etc.).

**[`gndo:abbreviatedName`](specs-elems.md#d17e2856)**  
(*Abgekürzter Name*): Eine abgekürzte Namensform der dokumentierten Entität.

**[`gndo:affiliation`](specs-elems.md#d17e2929)**  
(*Zugehörigkeit*): Die Person oder Körperschaft ist zugehörig zu einer Körperschaft, oder ist mit einem Ort oder einem Event verbunden.

**[`gndo:dateOfEstablishment`](specs-elems.md#d17e3105)**  
(*Gründungsdatum*): Das Datum, an dem die beschriebene Entität entstanden ist bzw. gegründet wurde.

**[`gndo:dateOfEstablishmentAndTermination`](specs-elems.md#d17e3133)**  
(*Gründungs- und Auflösungsdatum*): Zeitraum, innerhalb dessen die beschriebene Entität bestand.

**[`gndo:placeOfBusiness`](specs-elems.md#d17e3967)**  
(*Sitz (Körperschaft)*): Der (Haupt)sitz der Körperschaft.

**[`gndo:precedingCorporateBody`](specs-elems.md#d17e4133)**  
(*Vorherige Körperschaft (Körperschaft)*): Eine Vorgängerinstitution der Körperschaft.

**[`gndo:preferredName`](specs-elems.md#d17e4196)**  
(*Vorzugsbenennung (Standard)*): Eine bevorzugte Namensform, um die beschriebenen Entität zu bezeichnen.

**[`gndo:publication`](specs-elems.md#d17e4368)**  
(*Publikation*): Eine Publikation, die mit der Entität in Zusammenhang steht. Es kann sich hierbei (1) um eigene Titel der Entität (Person), (2) um Titel, die von der Entität (Person) herausgegeben worden sind und auch (3) um Titel über die Entität handeln.

**[`gndo:succeedingCorporateBody`](specs-elems.md#d17e4596)**  
(*Nachfolgende Körperschaft (Körperschaft)*): Eine Nachfolgeinstitution der Körperschaft.

**[`gndo:temporaryName`](specs-elems.md#d17e4681)**  
(*Zeitweiser Name*): Eine Namensform, die zeitweise zur Bezeichnung der dokumentierten Entität benutzt wurde.

**[`gndo:variantName`](specs-elems.md#elem.gndo.variantName.standard)**  
(*Variante Namensform (Standard)*): Eine alternative Namensform, um die beschriebenen Entität zu bezeichnen.

**[`dc:title`](specs-elems.md#d17e2645)**  
(*Titel (Record)*): Ein Titel für die Ressource. `dc:title` wird dann zur Bezeichnung einer Entität verwendet, wenn keine Vorzugsbenennung dokumentiert wird bzw. werden soll.

**[`gndo:biographicalOrHistoricalInformation`](specs-elems.md#d17e2985)**  
(*Biographische Angaben*): Biographische oder historische Angaben über die Entität.

**[`dublicateGndIdentifier`](specs-elems.md#dublicates)**  
(*GND-Identifier einer Dublette*): Der GND-Identifier einer möglichen Dublette.

**[`gndo:geographicAreaCode`](specs-elems.md#elem.gndo.geographicAreaCode)**  
(*Geographischer Schwerpunkt*): Ländercode(s) der Staat(en), denen die Entität zugeordnet werden kann (z.B. Lebensmittelpunkt bzw. Schwerpunkt ihres Wirkens bei Personen). Um die Datenqualität zu erhöhen und eine automatisierte Verarbeitung zu erleichtern, muss via `@gndo:term` ein Deskriptor aus dem GND Area Code Vokabular (https://d-nb.info/standards/vocab/gnd/geographic-area-code) angegeben werden!

**[`gndo:gndIdentifier`](specs-elems.md#d17e3593)**  
(*GND-Identifier*): Der GND-Identifier eines in der GND eingetragenen Normdatensatzes sofern vorhanden

**[`gndo:gndSubjectCategory`](specs-elems.md#d17e3634)**  
(*GND-Sachgruppe*): (GND-Sachgruppe)

**[`gndo:languageCode`](specs-elems.md#d17e3736)**  
(*Sprachraum*): Code(s) des Sprachraumes, dem die Entität zugeordnet werden kann. Um die Datenqualität zu erhöhen und eine automatisierte Verarbeitung zu erleichtern, kann via @gndo:term zsätzlich ein Deskriptor aus dem LOC ISO 693-2 Language Vokabular (http://id.loc.gov/vocabulary/iso639-2/) angegeben werden!

**[`gndo:relatesTo`](specs-elems.md#d17e4511)**  
(*Beziehung*): Beziehung der beschriebenen Entität zu einer anderen Entität, entweder in der GND oder in der gleichen entityXML Ressource. Diese Property hat keine direkte Entsprechung in der GNDO!

**[`ref`](specs-elems.md#elem.ref)**  
(*Hyperlink*): Ein Link zu einer Webressource, die weitere Informationen über die im Eintrag beschriebene Entität enthält. Der entsprechende URL wird entweder direkt als Text in `ref` oder via `@target` dokumentiert.

**[`owl:sameAs`](specs-elems.md#elem.owl.sameAs)**  
(*Gleiche Entität*): HTTP-URI der selben Entität in einem anderen Normdatendienst.

**[`skos:note`](specs-elems.md#elem.skos.note)**  
(*Anmerkung*): Eine Anmerkung zum Eintrag.

**[`source`](specs-elems.md#elem.source)**  
(*Quellenangabe*): Angaben zu einer Quelle, die verwendet wurde, um die in diesem Eintrag gegebenen Informationen zu recherchieren.

**[`anyElement.narrow`](specs-elems.md#any-restricted)**  
(*ANY ELEMENT (eng gefasst)*): Ein Element, dessen **QName** einem *custom namespace* angehört und nicht im vorliegenden Schema explizit spezifiziert ist.

**[`revision`](specs-elems.md#revision)**  
(*Revisionsbeschreibung*): Statusinformationen zu einer Ressource (Datensammlung oder einzelner Entitätseintrag), die einzelne Bearbeitungsstände, Fragen oder anderweitige Anmerkungen zur Verarbeitung der entsprechenden Ressource dokumentiert. Die einzelnen Einträge (via `change`) werden ausgehend vom Datum von jung nach alt absteigend sortiert, sprich: Der jüngste Eintrag steht als Erstes in der Revisionsbeschreibung!

---

