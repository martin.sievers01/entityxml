
<a name="about"></a>
# Über dieses Handbuch

Dieses Handbuch ist als Benutzungsleitfaden für entityXML gedacht, der zugleich die technische Dokumentation enthält. Es soll in erster Linie als Einführung und Nachschlagewerk in die Verwendung das Formats dienen, enthält aber auch technische Hinweise zur Verwendung hilfreicher Komponenten, die ebenfalls Teil von entityXML sind.

Das Handbuch wird parallel zur Implementierung des entityXML Schemas (die technische Formatspezifikation) gepflegt, d.h. die Dokumentation der technischen Spezifikationen in <span class="emph">Anhang 1</span> entspricht immer dem Schema.

Wie das Format selbst, befindet sich auch dieses Handbuch im Aufbau! Daher können Fehler zum Einen nicht ausgeschlossen werden. Zum Anderen sind viele Bereich bisher nur unvollständig oder garnicht dokumentiert. Das wird sich in der nächsten Zeit ändern.

Für Fragen, Anregungen etc. zu entityXML stehe ich gerne auch persönlich zur Verfügung. Wenn ihr also in diesem Handbuch nicht fündig werdet, dann schreibt mir doch einfach eine Mail an sikora[at]sub.uni-goettingen.de.
