
<a name="serialisation"></a>
# Serialisierungen

Daten in entityXML lassen sich in verschiedene XML Stile aber auch andere Datenformate serialisieren:

<a name="docs_d14e2668"></a>
## XML


- **entityXML → entityXML (strict)**: [`entityxml.strict.xsl`](https://gitlab.gwdg.de/entities/entityxml/-/blob/master/scripts/xslt/entityxml.strict.xsl): Konversion, die alle *custom namespaces* aus einer entityXML Ressource entfernt. Sie ist fester Bestandteil des Ingestworkflows von Daten in die GND durch die Text+ GND Agentur.
- **entityXML → MARC-XML**: [`entityxml2marcxml.xsl`](https://gitlab.gwdg.de/entities/entityxml/-/blob/master/scripts/xslt/marcxml/entityxml2marcxml.xsl): Konversion, die entityXML Einträge in MARC-XML konvertiert. Sie ist fester Bestandteil des Ingestworkflows von Daten in die GND durch die Text+ GND Agentur.

<a name="docs_d14e2691"></a>
## JSON


- **entityXML → JSON**: [`entityxml2json.xsl`](https://gitlab.gwdg.de/entities/entityxml/-/blob/master/scripts/xslt/entityxml2json.xsl): JSON Serialisierung einer entityXML Ressource.

<a name="docs_d14e2705"></a>
## Markdown


- **entityXML → Markdown**: [`entityxml2md.xsl`](https://gitlab.gwdg.de/entities/entityxml/-/blob/master/scripts/xslt/entityxml2md.xsl): Markdown Liste mit einzelnen Einträgen aus einer entityXML Ressource

<a name="docs_d14e2720"></a>
## CSV


- **entityXML → CSV**: [`entityxml2csv.xsl`](https://gitlab.gwdg.de/entities/entityxml/-/blob/master/scripts/xslt/entityxml2csv.xsl) (Proof of Concept): CSV Liste aller Einträge einer entityXML Ressource. *nota bene*: Diese Konversion steckt noch in den Kinderschuhen.
