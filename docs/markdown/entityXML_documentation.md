# entityXML (v.0.5.1 ALPHA): Handbuch

## Inhalt

- [entityXML: Ein Austausch- und Speicherformat](#introduction)
	- [Als Austauschformat](#docs_d14e88)
	- [Als Speicherformat](#docs_d14e130)
- [Über dieses Handbuch](#about)
- [Setup und Technisches](#docs_d14e195)
	- [Setup](#setup)
		- [Einfache Verwendung: entityXML for takeaway](#docs_d14e220)
		- [Download](#entityxml-offline)
		- [Verwendung als oXygen XML Framework](#docs_d14e304)
			- [Option 1: Feste Integration](#docs_d14e331)
			- [Option 2 (Bevorzugt): Lockere Integration als zusätzliches Frameworks](#docs_d14e349)
	- [Versionen](#entityxml-versions)
	- [Ressourcen](#resources)
		- [Schema](#entityxml-schema-intro)
			- [Erweiterungen](#docs_d14e509)
		- [Skripte](#entityxml-scripts-intro)
		- [Assets](#docs_d14e598)
	- [Arbeiten in oXygen XML](#docs_d14e616)
		- [oXygen Author Mode](#oxygen-author-mode)
			- [Views](#docs_d14e640)
			- [Filter](#docs_d14e676)
			- [Verwendung des Author Mode bei installiertem Framework](#author-mode-framework)
			- [Verwendung des Author Mode ohne Framework Installation](#docs_d14e716)
- [entityXML Document Model](#docs_d14e800)
	- [Allgemeines](#entityxml-dm-intro)
		- [Datensammlung](#docs_d14e889)
	- [Metadaten einer Datensammlung](#metadata-section)
		- [Titel einer Sammlung](#docs_d14e932)
		- [Kurzbeschreibung](#docs_d14e945)
		- [Providerbeschreibung](#provider-stmt)
		- [Beschreibung von Verantwortlichkeiten](#resp-stmt)
	- [entityXML Lifecycle](#lifecycle)
		- [Revisionsbeschreibung](#revision-desc)
			- [Anmerkung zur Ausführlichkeit von Revisionsbeschreibungen](#docs_d14e1184)
	- [Datenbereich](#data-collection)
	- [Entitätseintrag im Allgemeinen](#docs_d14e1248)
		- [Einstieg ins Anlegen von Entitätseinträgen](#records-intro)
		- [Verwendung fremder Namensräume](#custom-namespaces)
			- [Von der einfachen Anwendung ...](#docs_d14e1386)
			- [... hin zum Mittelweg ...](#docs_d14e1419)
			- [... und auf die Spitze getrieben!](#lido)
			- [Arbeiten mit schemafremden Elementen im Author Mode](#docs_d14e1474)
			- [Verfahrensweise mit Daten aus fremden Namensräumen bei der Text+ GND-Agentur](#docs_d14e1487)
		- [Verarbeitungshinweis für die Agentur](#agency-mode-record)
		- [Anmerkungen](#entity-notes)
		- [Biographische und/oder historische Angaben zur Entität](#entity-biogr-histogr)
		- [Dokumentation von Mehrfacheinträgen](#dublicates-encoding)
		- [Geographischer Schwerpunkt einer Entität](#entity-geographic-area)
		- [Namens- bzw. Titelangaben](#entity-title)
			- [Titel als Fallback](#docs_d14e1773)
		- [Quellenangabe](#entity-source)
		- [Sachgruppen](#entity-subject-categories)
		- [Gleiche Entität in anderer Normdatei oder anderem Datendienst](#entity-same-as)
		- [Sonstige Webressourcen](#entity-links)
	- [Spezifische Entitätsklassen](#docs_d14e1928)
		- [Personen](#person-record)
			- [Unterscheidung von Personentypen](#docs_d14e1976)
			- [Erschließung von Vorzugsbenennung und Namensvarianten](#person-name)
			- [Geographische Zuordnung](#person-geographical-area-code)
			- [Lebens- und Wirkungsdaten](#person-dates)
			- [Lebens- und Wirkungsorte](#person-places)
			- [Angaben zum Geschlecht](#person-gender)
			- [Berufe und Tätigkeiten](#person-profession)
			- [Titelangaben/Veröffentlichungen](#person-publications)
			- [Beispiel 1: Maxima Musterfrau](#docs_d14e2245)
			- [Beispiel 2: Ernest Chevalier](#docs_d14e2255)
			- [Modell: Personeneintrag](#docs_d14e2265)
		- [Körperschaften](#cb-record)
		- [Orte](#place-record)
		- [Werke](#work-record)
- [Tutorials](#docs_d14e2315)
	- [HOW TO: Person](#docs_d14e2321)
- [Technische Spezifikation](#doc-specs)
- [Serialisierungen](#serialisation)
	- [XML](#docs_d14e2668)
	- [JSON](#docs_d14e2691)
	- [Markdown](#docs_d14e2705)
	- [CSV](#docs_d14e2720)
- [Revisionen](#revision-history)


---

<a name="introduction"></a>
## entityXML: Ein Austausch- und Speicherformat

entityXML ist (bisher) eine Konzeptstudie in der Version *0.5.1* (ALPHA), die darauf abzielt, ein einheitliches XML basiertes Datenformat für die Text+ GND Agentur zu modellieren.

Das Format soll dabei vorallem drei Aspekte abdecken: Die Bereitstellung eines einheitlichen (1) **Austausch**- und (2) **Speicherformats** zur Beschreibung von Entitäten, das direkt auf die GND gemappt werden kann, und der Text+ GND Agentur zudem als (3) **Workflow-Steuerungsinstrument** dient.

<a name="docs_d14e88"></a>
### Als Austauschformat

Als <span class="emph">Austauschformat</span> für die Text+ GND-Agentur soll entityXML als transparentes und dokumentiertes Format dienen, um Daten zwischen Datenlieferanten (etwa digitale Editions- oder Erschließungsprojekte) und der Text+ GND Agentur systematisch (d.h. gemäß eines wohl definierten Ablaufs) austauschen zu können. Aus diesem Grund baut das entityXML Schema grundlegend auf der [GND-Ontologie](https://d-nb.info/standards/elementset/gnd) als Beschreibungsmodell von Entitäten auf. 

Daten, die in entityXML angelegt und ausgetauscht werden, können mit Hilfe des [entityXML
                     Schemas](#entityxml-schema-intro) sowohl auf Nutzer- als auch auf Agenturseite validiert werden, um eine erste und regelmäßige Qualitätssicherung zu gewährleisten. Das Datenmodell ermöglicht in der Folge die Implementierung [generischer
                     Transformationsszenarien](#entityxml-scripts-intro), die in Zukunft unteranderem den Einspeisevorgang in die GND unterstützen sollen. 

Desweiteren untertützt entityXML die Kommunikation zwischen Datenlieferanten und der Text+ GND Agentur, indem Fragen, Anmerkungen und anderweitige Informationen entweder für eine ganze Lieferung oder für einzelne Einträge direkt mit erfasst und ausgewertet werden können.

Das Wichtigste zum Aspekt "<span class="emph">Austauschformat</span>" in Kürze: 

- entityXML baut auf den Klassen und Properties der GND Ontologie auf
- Qualitätssicherung in Hinblick auf die Spezifikation der GND durch Validierung mit dem entityXML Schema
- Automatisierte Transformationsszenarien zum Export von entityXML Daten (z.B. zur Einspeisung in die GND)
- Workflowsteuerung der GND-Agentur
- Fragen an eine GND Agentur können direkt an den Daten dokumentiert werden


<a name="docs_d14e130"></a>
### Als Speicherformat

Als <span class="emph">Speicherformat</span> für Projekte bietet entityXML zahlreiche Möglichkeiten der Verwendung: Einerseits hat man hiermit eine niedrigschwellige Möglichkeit an der Hand, Entitäten wartungsarm und ohne komplexe Speicherlösung zu dokumentieren und zu speichern. Die erschlossenen Daten bleiben somit zum Einen als Erschließungsleistung der entsprechenden Projekte transparent (z.B. zum Nachweis von Arbeitsleistungen für Förderinstitutionen). Zum Zweiten können sie einfach in bestehende Forschungsdatenmanagementkonzepte (z.B. spezifische Versionierungsworkflows, Langzeitarchivierung etc.) eingebunden werden. Zum Dritten (und hier liegt ein echter Vorteil zu Excel und Co.) können die (XML) Daten, die auf diese Weise entstehen, direkt in projektspezifischen Softwarelösungen genutzt werden (z.B. Einspeisung in Webportale, Datenbanken, Erstellung von Registern, etc.).

entityXML basiert zwar auf der Idee, die GND-Ontologie zur Beschreibung von Entitäten zu verwenden. Allerdings ist das Schema <span class="emph">offen</span> gehalten, sprich: Sofern an die Daten, die dokumentiert werden sollen, Anforderungen gestellt werden, die sich über die GND-Ontologie nicht beschreiben lassen, dann bietet entityXML die Möglichkeit, [schemafremde
                     Elemente und Attribute aus eigenen Namensräumen](#custom-namespaces) mit zu erschließen. Diese Informationen werden zwar bei der Einspeisung in die GND vernachlässigt, nichtsdestoweniger bleiben sie den Projekten als integrale Bestandteile der Forschungsdaten erhalten und können selbstverständlich weiter genutzt werden.

Darüberhinaus bietet entityXML die Möglichkeit sowohl auf Datensammlungs- als auch auf Eintragsebene detailierte Revisionsbeschreibungen vorzunehmen, um Recherchestände zu dokumentieren - und kann dementsprechend dabei helfen projektinterne Arbeitsabläufe zu unterstützen.

Das Wichtigste zum Aspekt "<span class="emph">Speicherformat</span>" in Kürze: 

- Projekte können ihre Daten unabhängig von einer komplexen Speicherlösung speichern
- Recherchearbeit bleibt transparent und nachvollziehbar
- Daten bleiben als Forschungsdaten des Projektes transparent 
- Daten können versioniert werden
- Bearbeitungsstand kann beschrieben und so dokumentiert werden
- Enkoppelung von Projekten und GND in Hinblick auf Zeitpläne, da Datenlieferanten nicht auf die Eintragung in die GND warten müssen, sondern ihre Daten direkt nutzen können.
- Möglichkeit der Verwendung von Elementen in *custom namespaces*
- Markup, also etwas mit dem vorallem Editionsprojekte Erfahrung haben


<a name="about"></a>
## Über dieses Handbuch

Dieses Handbuch ist als Benutzungsleitfaden für entityXML gedacht, der zugleich die technische Dokumentation enthält. Es soll in erster Linie als Einführung und Nachschlagewerk in die Verwendung das Formats dienen, enthält aber auch technische Hinweise zur Verwendung hilfreicher Komponenten, die ebenfalls Teil von entityXML sind.

Das Handbuch wird parallel zur Implementierung des entityXML Schemas (die technische Formatspezifikation) gepflegt, d.h. die Dokumentation der technischen Spezifikationen in <span class="emph">Anhang 1</span> entspricht immer dem Schema.

Wie das Format selbst, befindet sich auch dieses Handbuch im Aufbau! Daher können Fehler zum Einen nicht ausgeschlossen werden. Zum Anderen sind viele Bereich bisher nur unvollständig oder garnicht dokumentiert. Das wird sich in der nächsten Zeit ändern.

Für Fragen, Anregungen etc. zu entityXML stehe ich gerne auch persönlich zur Verfügung. Wenn ihr also in diesem Handbuch nicht fündig werdet, dann schreibt mir doch einfach eine Mail an sikora[at]sub.uni-goettingen.de.

<a name="docs_d14e195"></a>
## Setup und Technisches

<a name="setup"></a>
### Setup

entityXML ist nicht bloß als XML-Format, sondern vorallem als <span class="emph">Werkzeugkasten</span> gedacht, der einerseits das entityXML Schema und andererseits zahlreiche Tools (Konversionen, Stylesheets etc.) umfasst. 

Zusätzlich liefert entityXML ein <span class="emph">Document Type Association Framework</span> für den [oXygen XML Editor](https://www.oxygenxml.com/) aus (im Folgenden "Framework"), kann aber natürlich auch ohne oXygen verwendet werden.

Die folgenden Schritte lohnen sich also, wenn man oXygen oder einen anderen XML Editor verwendet, der "schema-bewusst" arbeitet, d.h. die Daten direkt in Abhängigkeit des angegebenen Validierungsschema überprüft.

<a name="docs_d14e220"></a>
#### Einfache Verwendung: entityXML for takeaway

Um entityXML auf die einfachste Art zu nutzen, kopiert man einfach die folgenden 4 Zeilen Code (hierbei handelt es sich um sog. *Processing Instructions*) in eine leere XML Datei und ist fertig:

```xml
<?xml-model href="https://gitlab.gwdg.de/entities/entityxml/-/raw/master/schema/entityXML.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="https://gitlab.gwdg.de/entities/entityxml/-/raw/master/schema/entityXML.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<?xml-stylesheet type="text/css" href="https://gitlab.gwdg.de/entities/entityxml/-/raw/master/assets/css/author/entities.author.css" title="entityxml" alternate="no"?>
<?xml-stylesheet type="text/css" href="https://gitlab.gwdg.de/entities/entityxml/-/raw/master/assets/css/author/entities.author.structured.css" title="entityxml" alternate="yes"?>
```

Diese *Processing Instructions* verweisen auf das entityXML RNG Schema, die Schematron Validierungsroutinen und die Author Mode CSS im GitLab. Das ist alles, um zu starten! Wenn man auf diese Art mit entityXML arbeitet, arbeitet man immer mit der aktuellen Schemaversion. Allerdings benötigt man eine aktive Internetverbindung.

Hier eine ausführlicheres Rumpftemplate zum einfachen Copy-Paste:

```xml
<?xml-model href="https://gitlab.gwdg.de/entities/entityxml/-/raw/master/schema/entityXML.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="https://gitlab.gwdg.de/entities/entityxml/-/raw/master/schema/entityXML.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<?xml-stylesheet type="text/css" href="https://gitlab.gwdg.de/entities/entityxml/-/raw/master/assets/css/author/entities.author.css" title="entityxml" alternate="no"?>
<?xml-stylesheet type="text/css" href="https://gitlab.gwdg.de/entities/entityxml/-/raw/master/assets/css/author/entities.author.structured.css" title="entityxml" alternate="yes"?>
<entityXML xmlns="https://sub.uni-goettingen.de/met/standards/entity-xml#" 
    xmlns:gndo="https://d-nb.info/standards/elementset/gnd#" 
    xmlns:owl="http://www.w3.org/2002/07/owl#" 
    xmlns:skos="http://www.w3.org/2004/02/skos/core#" 
    xmlns:dc="http://purl.org/dc/elements/1.1/" 
    xmlns:foaf="http://xmlns.com/foaf/0.1/" 
    xmlns:dnb="https://www.dnb.de" 
    xmlns:geo="http://www.opengis.net/ont/geosparql#"> 
    <collection>
        <metadata>
            <title></title>
            <abstract></abstract>
            <provider id=""></provider>
            <revision status=""></revision>
        </metadata>
        <data>
            <list>
                
            </list>
        </data>
    </collection>
</entityXML>
```

<a name="entityxml-offline"></a>
#### Download

Wer sich nicht von einer aktiven Internetverbindung abhängig machen möchte, der kann sich das [entityXML GitLab Repository](https://gitlab.gwdg.de/entities/entityxml) einfach runterladen. Hier hat man zwei Möglichkeiten:

**(1)** Download des Repositories über das Download Icon im GitLab. Hier kann man zwischen 4 Kompressionsformaten wählen (*.zip, *.tar.gz, *.tar.bz2, *.tar) und bekommt dann ein entsprechend komprimiertes Archiv, aus dem dann das entityXML Verzeichnis entpackt und dann an einem beliebigen Ort abgelegt werden kann.

**(2)** Klon des GitLab Repositories mittels Git:


- Via *HTTP*: (`git clone https://gitlab.gwdg.de/entities/entityxml.git`)
- Via *SSH*: (`git clone git@gitlab.gwdg.de:entities/entityxml.git`)

Auf diese Weise hat man die Möglichkeit die lokal gespeicherte Version von entityXML mit Hilfe von Git immer up-to-date zu den Entwicklungsständen im GitLab zu halten.

Damit stehen alle Ressourcen zur Verfügung, um offline zu arbeiten. Was sich nun ändert, sind die Pfade, die in den *Processing Instructions* verwendet werden, um die entsprechenden Dateien zu lokalisieren.

**Hier ein Beispiel**: Mal angenommen, wir laden das entityXML Repository auf einen lokalen Computer runter, und zwar - sagen wir - in das Verzeichnis `Schreibtisch/Arbeit/Entitätenerschließung`. Dieser Ordner hat ein Unterverzeichnis `data`, in dem wir unsere entityXML Dateien anlegen und pflegen möchten. Zum Zwecke dieses Beispiels legen wir eine entityXML Datei an, die wir `entitäten_personen.xml` nennen. Die Ordnerstruktur sieht dann wie folgt aus:

```text
Schreibtisch/
 └── Arbeit/
      └── Entitätenerschließung/
           └── data/		
                └── entitäten_personen.xml
           └── entityXML/	
                └── docs
                └── schema/
                     └── entityXML.rng
                └── assets/
                     └── css/
                          └── author/
                               └── entities.author.css
                └── ...
```

Vor diesem Hintgrund müssten wir die oben angeführten Pfade in unserer Beispieldatei `entitäten_personen.xml` entsprechend unserer Verzeichnisstruktur anpassen:

```xml
<?xml-model href="../entityXML/schema/entityXML.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="../entityXML/schema/entityXML.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<?xml-stylesheet type="text/css" href="../entityXML/assets/css/author/entities.author.css" title="entityxml" alternate="no"?>
<?xml-stylesheet type="text/css" href="../entityXML/assets/css/author/entities.author.structured.css" title="entityxml" alternate="yes"?>

```

<a name="docs_d14e304"></a>
#### Verwendung als oXygen XML Framework

entityXML wird als <span class="emph">oXygen XML Framework</span> ausgeliefert. Somit kann das Wissen um die Validierung und Verarbeitung von entityXML Daten in oXygen XML fest integriert werden: Sofern eine entityXML Ressource in oXygen geöffnet wird, wird sie automatisch validiert. Fest definierte Transformationsszenarien stehen gleichsam direkt zur Verfügung. Kurz um: Wer regelmäßig mit entityXML arbeitet und die volle Bandbreite des entityXML Werkzeugkastens nutzen möchte, erleichtert sich die Arbeit durch die Frameworkintegration von entityXML in oXygen XML enorm. 

Wählt man diesen Weg, weden außerdem die *Processing Instructions* zu Beginn einer entityXML Ressource überflüssig.

Um entityXML als Framework in oXygen XML zu integrieren, gibt es **zwei Optionen**. Beide setzen voraus, dass das entityXML GitLab Repository als Kopie auf den entsprechenden Rechner lokal vorhanden ist, entweder als [Download](#entityxml-offline) oder als [geklontes Git Repository](https://gitlab.gwdg.de/entities/entityxml):

<a name="docs_d14e331"></a>
##### Option 1: Feste Integration



- Gehe zum `frameworks` Verzeichnis der oXygen XML Anwendung.
- ... Erstelle ein Verzeichnis `entityXML`.
- ... Kopiere den Inhalt des entityXML Repositories in das erstellte Verzeichnis.


<a name="docs_d14e349"></a>
##### Option 2 (Bevorzugt): Lockere Integration als zusätzliches Frameworks

Hierbei handelt es sich m.E. um die beste Option, um entityXML als Framework in oXygen einzubinden, denn mit der Hilfe von [Git](https://git-scm.com/) kann man so das lokale Verzeichnis von entityXML mit dem offiziellen GitLab Repository immer up-to-date halten (wenn man möchte):



- Wähle `Optionen` > `Einstellungen` im Menü aus.
- ... Wähle `Dokumenttypen-Zuordnung` von der linken Liste aus und klicke auf den Untereintrag `Orte`.
- ... Füge den Pfad zum Elternverzeichnis des entityXML Repositories auf deinem Computer zu `Zusätzliche Framework-Verzeichnisse` hinzu.


Auf diese Weise lässt sich entityXML auch in das oXygen Plugin des [TextGrid Laboratorys](https://textgrid.de/download) integrieren:



- Wähle `Fenster` > `Benutzervorgaben` im Menü aus.
- ... Wähle `Oxygen XML Editor` > `Document Type Association` von der linken Liste aus und klicke auf den Untereintrag `Locations`.
- ... Füge den Pfad zum Elternverzeichnis des entityXML Repositories auf deinem Computer zu `Additonal framework directories` hinzu.


<a name="entityxml-versions"></a>
### Versionen

Der <span class="emph">master branch</span> des [entityXML GitLab](https://gitlab.gwdg.de/entities/entityxml) entspricht dem sog. *Nightly Snapshot*, also dem zwischenzeitlichen, (mehr oder weniger) tagesaktuellen und unter Umständen ungetesteten Entwicklungsstand von entityXML.

Offizielle Releases werden mittels Tags im GitLab vorbereitet und veröffentlich. Das <span class="emph">Latest Release</span> ist **[Version 0.5.0](https://gitlab.gwdg.de/entities/entityxml/-/releases/v0.5.0)** unter dem Tag [`v0.5.0`](https://gitlab.gwdg.de/entities/entityxml/-/tree/v0.5.0).

Einen Überblick über alle Tags gibt's [hier](https://gitlab.gwdg.de/entities/entityxml/-/tags), über alle Releases [hier](https://gitlab.gwdg.de/entities/entityxml/-/releases) und die Revisionsbeschreibung [hier](#revision-history).

Dateien aus Tags können unter Verwendung des entsprechenden Tagnamens aufgerufen bzw. eingebunden werden, z.B. das Schema und die Autor Modus CSS der Version 0.5.0 mit dem Tag `v0.5.0`:

```xml
<?xml-model href="https://gitlab.gwdg.de/entities/entityxml/-/raw/v0.5.0/schema/entityXML.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="https://gitlab.gwdg.de/entities/entityxml/-/raw/v0.5.0/schema/entityXML.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<?xml-stylesheet type="text/css" href="https://gitlab.gwdg.de/entities/entityxml/-/raw/v0.5.0/assets/css/author/entities.author.css" title="entityxml" alternate="no"?>
<?xml-stylesheet type="text/css" href="https://gitlab.gwdg.de/entities/entityxml/-/raw/v0.5.0/assets/css/author/entities.author.structured.css" title="entityxml" alternate="yes"?>
```

Um entityXML Ressourcen einer bestimmten Version auf eine aktuellere Version (oder die aktuelle Nightly) zu updaten, können die Scripte im Verzeichnis [`/scripts/xslt/update`](https://gitlab.gwdg.de/entities/entityxml/-/tree/master/scripts/xslt/update) verwendet werden.

<a name="resources"></a>
### Ressourcen

entityXML umfasst als Werkzeugkasten zahlreiche Komponenten und Ressourcen, die sich im offiziellen [GitLab Repository](https://gitlab.gwdg.de/entities/entityxml) befinden:

- `/schema`: Hier liegen die <span class="emph">Validierungsroutinen</span> von entityXML, sprich ein RNG-Schema inkl. Schematron Regeln
- `/scripts`: Hier liegen <span class="emph">XSLT</span> und <span class="emph">XQuery Skripte</span> zur Verarbeitung von entityXML bereit
- `/assets`: Zusätzliche Dateien und <span class="emph">CSS Stylesheets</span>, die unter anderem für den oXygen Author Modus verwendet werden
- `/samples`: Hier liegen entityXML Beispiele zum Nachvollziehen und Ausprobieren.
- `/templates`: Copy & Paste Templates zu einzelnen Entitätseinträgen.


<a name="entityxml-schema-intro"></a>
#### Schema

Im Verzeichnis `schema/` befindet sich das Herzstück von entityXML: Das RNG-Validierungsschema [**`entityXML.rng`**](https://gitlab.gwdg.de/entities/entityxml/-/blob/master/schema/entityXML.rng). 
> **Version 0.3.2 als 'Untermieter'**  
>  Offiziell aus Gründen der Abwärtskompatibilität (inoffiziell aus dem einfachen Grund, dass sich entityXML noch in der ALPHA-Phase befindet) "schlummert" hier noch die "ältere Schwester" (v.0.3.2) der aktuellen Version. Nach Abschluss der ALPHA Phase wird sie ausziehen. 



Das Validierungsscheme ist in der XML Syntax von **[RELAX NG](https://relaxng.org/)** (kurz: <span class="emph">RNG</span>) implementiert. RNG ist eine XML Validerungssprache, mit deren Hilfe XML-Modelle, also ein definiertes Set von **Elementen, Attributen zzgl. ihrer Inhaltsmodelle**, erstellt werden können. RNG sorgt also vorallem dafür, dass die Struktur einer entityXML Ressource klar definiert ist: Welches Element darf welche anderen Elemente enthalten? Welche Attribute können in welchen Elementen gesetzt werden? Welche Werte können in einem Attribut benutzte werden? Alles Fragen, die im Rahmen des RNG-Schemas beantwortet werden.

Das RNG-Schema enthält zusätzlich **[Schematron](https://www.schematron.com/)**, eine weitere Validierungssprache für XML Dateien, die im Rahmen von entityXML der **erweiterten Qualitätssicherung** dient: sog. "Business Rules", also kleine in Schematron definierte Regeln, sorgen für eine feingliedrigere Überprüfung von entityXML Daten als es allein mit RNG möglich wäre. Als Beispiel für eine konkrete Schematron Regel in entityXML wäre hier die Überprüfung der obligatorischen Vorzugsbenennung für Entitäten zu nennen, sofern diese nicht bereits durch einen GND-URI (`@gndo:uri`) mit einem Normdateneintrag in der GND identifiziert wurden. Die hinterliegende "Businessrule" wäre etwa: "Wie in der GND, ist jede Entität durch eine bevorzugte Namensform benannt." 

<a name="docs_d14e509"></a>
##### Erweiterungen

RNG ermöglicht es, verhältnismäßig einfach <span class="emph">Erweiterungen</span> zu einem existierenden Schema zu implementieren. Im Verzeichnis `schema/extensions` befinden sich bereits zwei Erweiterungen zum entityXML Schema, die spezifische Anwendungsfälle abbilden.

[`entityXML.bibframe.rng`](https://gitlab.gwdg.de/entities/entityxml/-/blob/master/schema/extensions/entityXML.bibframe.rng) implementiert einen `bf:Instance` Eintrag, wobei es sich um eine an [Bibframe](https://www.loc.gov/bibframe/) orientierte Klasse handelt, mit der Instanziierungen von Werken beschrieben werden können, und die nun stellvertretend für `manifestation` und `expression` Einträge verwendet werden kann.

[`entityXML.project.rng`](https://gitlab.gwdg.de/entities/entityxml/-/blob/master/schema/extensions/entityXML.project.rng) hingegen implementiert ein `project` Element, das im Rahmen des Workflows der Text+ GND Agentur benutzt wird, um Metadaten zu Projekten zu erfassen, und das, unabhängig von gelieferten Datensammlungen.

<a name="entityxml-scripts-intro"></a>
#### Skripte

Im Verzeichnis `scripts/` befinden sich verschiedene Verarbeitungsskripte für entityXML Daten. Die Masse ist in <span class="emph">XSL</span> oder <span class="emph">XQuery</span> geschrieben. Dabei soll es aber nicht bleiben.

Die Spezifikation einer XML-Grammatik wie entityXML ist eine Sache. Die Verarbeitung von Daten, die dieser Grammatik folgen, eine Andere. Und genau dieser zweite Aspekt, also die Verarbeitung von entityXML Daten und die Erzeugung von Informationen auf Grundlage dieser Daten durch (automatisierte) Informationsprozesse ist der Ausgangspunkt für die Skripte in diesem Verzeichnis.

**Nehmen wir beispielsweise folgendes Szenario**: Projekt X erschließt Entitäten mit entityXML. Die projektspezifische Datenbank ist allerdings keine XML Datenbank, sondern eine JSON-Datenbank. Was tun? entityXML ist zwar nicht entity*JSON*, aber lässt sich über [`entityxml2json.xsl`](https://gitlab.gwdg.de/entities/entityxml/-/blob/master/scripts/xslt/entityxml2json.xsl) einfach in <span class="emph">JSON</span> konvertieren. 

**Betrachten wir ein zweites Szenario**: Forschergruppe Y arbeitet an einer spezifischen Personendatenbank mit projektspezifischen Daten, möchte aber Informationen aus der GND in ihre eigenen Bestände nachladen (und nicht per copy-paste in ihre Einträge händisch kopieren). [`enrich-records.lobid.xquery`](https://gitlab.gwdg.de/entities/entityxml/-/blob/master/scripts/xquery/enrich-records.lobid.xquery) reichert alle Einträge mit Daten aus [LOBID](https://lobid.org/gnd) an, die mit `@enrich="true"` markiert sind und GND-URIs verzeichnen.

**Ein abschließendes drittes Szenario**: Ein langfristiges Ziel der Text+ GND Agentur ist die (halb)automatisierte Einspeisung von entityXML Daten in die GND. Die GND (konkreter meine ich hier das Informationssystem der GND) kennt allerdings kein entityXML, genausowenig wie Microsoft Excel oder Microsoft Word (oder andere Formate, die gerne ins Land gefürt werden, um Daten an die GND zwecks Austausch zu senden). Aber was im Rahmen des GND Workflows für den Austausch von Daten eine Rolle spielt, ist [MARC](https://www.loc.gov/marc/) (siehe auch hier bei der [DNB](https://www.dnb.de/DE/Professionell/Metadatendienste/Exportformate/MARC21/marc21_node.html)). MARC kennen nun allerdings die wenigsten Nutzer und Nutzerinnen der GND … ein Dilemma zeichnet sich ab. *Long story short*: Für dieses Problem gibt es [`entityxml2marcxml.xsl`](https://gitlab.gwdg.de/entities/entityxml/-/blob/master/scripts/xslt/marcxml/entityxml2marcxml.xsl), um aus einer entityXML Ressource ein Format - nämlich [MARC-XML](https://www.loc.gov/standards/marcxml/) - zu erzeugen, dass sich in die GND einspielen lässt.

Im Kapitel "[Serialisierungen](#serialisation)" findet sich eine kleine Auflistung bereits existierender Konversionsroutinen.

<a name="docs_d14e598"></a>
#### Assets

Im Verzeichnis `assets/` sind zusätzliche Dateien untergebracht, die bei der Verarbeitung von entityXML verwendet werden (können).

Die wohl prominentesten Dateien in diesem Verzeichnis, genauer in `assets/author` sind die <span class="emph">CSS Dateien</span>, die das Rendering von entityXML Dateien im Rahmen des [Author Modes von oXygen XML](#oxygen-author-mode) definieren.

<a name="docs_d14e616"></a>
### Arbeiten in oXygen XML

Unabhängig davon, ob entityXML nun als Framework in oXygen installiert ist, oder nicht: Die einzelnen Ressourcen, die entityXML als *Werkzeugkasten* ausmachen, stehen auch unabhängig von einer festen Integration als oXygen Framework zur [Verfügung](#resources). 

Die feste Integration von entityXML als Framework ist, wie bereits schon erwähnt, vorallem für Nutzer*innen interessant, die häufig mit dem Format arbeiten, denn es erleichtert die Anwendung von Konversionen oder die Benutzung des Author Modes wesentlich.

<a name="oxygen-author-mode"></a>
#### oXygen Author Mode

Um die Arbeit mit entityXML in oXygen intuitiver zu gestalten, werden mehrere <span class="emph">CSS Dateien</span> bereitgestellt, um die Autoren Ansicht zu strukturieren und mit einem benutzerfreundlicheren Design als der XML Ansicht auszustatten.

<a name="docs_d14e640"></a>
##### Views

Views definieren die generelle Struktur des entityXML Author Modes.


- **Basis View** (`/assets/css/author/entities.author.css`): Die Haupt-CSS des entityXML Frameworks, obligatorisch für den oXygen Author Mode. Dieser View rendert Einträge in prosaischer Form.
- **Strukturierte Ansicht** (`/assets/css/author/entities.author.structured.css`): Eine zusätzliche CSS zum Basis View, um Einträge in *halbstrukturierter Form* darzustellen. Eine bessere Übersichtlichkeit der Daten ist m.E. garantiert! 
	- **Vollstrukturierte Ansicht** (`/assets/css/author/entities.author.structured.full.css`): Erweiterung des Strukturierten Views, der alle Elemente eines Eintrags hierachisch abbildet.
- **Interaktive Ansicht** (`/assets/css/author/entities.author.interactive.css`): Zusätzliche CSS, die die Eingabe im Author Mode interaktiver durch Schaltflächen unterstützt. So können z.B. Revisionsbeschreibungen intuitiver angelegt werden oder IDs für Einträge automatisch erzeugt werden.

<a name="docs_d14e676"></a>
##### Filter

Filter steuern die Anzeige von Einträgen nach gewissen Kriterien.


- **Filter "Nur offene Einträge"** (`/assets/css/author/agency.open-records.css`): Filter Modus, der nur alle Einträge anzeigt, die noch nicht als abgeschlossen markiert sind.
- **Filter "Nur Einträge ohne GND-URI"** (`/assets/css/author/agency.no-gnd-uri.css`): Filter Modus, der nur alle Einträge anzeigt, die keinen GND-URI verzeichnen.

<a name="author-mode-framework"></a>
##### Verwendung des Author Mode bei installiertem Framework

Wenn entityXML bereits als Framework installiert ist, müssen keine weiteren Schritte unternommen werden. Wechselt man nun im oXygen XML Editor in den <span class="emph">Author Mode</span> (Unten links am Bildschirmrand unter **`Autor`**, siehe auch die offizielle [oXygen Dokomentation](https://www.oxygenxml.com/doc/versions/25.0/ug-editor/topics/editing-xml-documents-author.html)) können unter dem **`Stile`** Dropdownreiter (Oberer Menüleiste) die verschiedenen, im Framework vorkonfigurierten Stile bzw. Views zu- oder abgeschaltet werden, je nach persönlicher Vorliebe.

<a name="docs_d14e716"></a>
##### Verwendung des Author Mode ohne Framework Installation

Falls entityXML nicht als Framework installiert ist, lassen sich die CSS Stylesheets für den <span class="emph">Author Mode</span>, wie im [Setup Kapitel](#setup) einleitend beschrieben, auch manuell ohne Aufwand in die jeweilige entityXML Ressource einbinden und entsprechend verwenden. Dazu muss folgende *Processing Instruction* hinter die XML-Deklaration der entityXML Datei, die bearbeitet werden soll, gesetzt werden, um den **Basis View** des entityXML Author Modes zu verwenden:

```xml
<?xml-stylesheet type="text/css" href="https://gitlab.gwdg.de/entities/entityxml/-/raw/master/assets/css/author/entities.author.css" title="entityxml" alternate="no"?>
```

Um zusätzlich die **Strukturierte Ansicht** nutzen zu können, muss eine weitere *Processing Instruction* hinter die Erste gesetzt werden:

```xml
<?xml-stylesheet type="text/css" href="https://gitlab.gwdg.de/entities/entityxml/-/raw/master/assets/css/author/entities.author.structured.css" title="entityxml" alternate="yes"?>
```

**Wichtig (!)** ist hierbei, dass die **Titel** (der `title` Parameter in der *Processing Instruction*) der beiden verknüpften CSS Ressourcen identisch sind, damit oXygen beide Dateien zusammenführt und daraus einen View rendert. Als Konvention verwenden wir hierfür **`entityxml`**. Das Ergebnis in der XML Datei sollte nun etwa so aussehen:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!-- Hier die Autor Mode CSS -->
<?xml-stylesheet type="text/css" href="https://gitlab.gwdg.de/entities/entityxml/-/raw/master/assets/css/author/entities.author.css" title="entityxml" alternate="no"?>
<?xml-stylesheet type="text/css" href="https://gitlab.gwdg.de/entities/entityxml/-/raw/master/assets/css/author/entities.author.structured.css" title="entityxml" alternate="yes"?>
<!-- Weitere Processing Instructions, z.B. zur Schemaanbindung -->
<entityXML xmlns="https://sub.uni-goettingen.de/met/standards/entity-xml#" 
    xmlns:dc="http://purl.org/dc/elements/1.1/"
    xmlns:foaf="http://xmlns.com/foaf/0.1/" 
    xmlns:gndo="https://d-nb.info/standards/elementset/gnd#" 
    xmlns:owl="http://www.w3.org/2002/07/owl#"
    xmlns:skos="http://www.w3.org/2004/02/skos/core#" 
    xmlns:geo="http://www.opengis.net/ont/geosparql#" 
    xmlns:dnb="https://www.dnb.de"
    xmlns:wgs84="http://www.w3.org/2003/01/geo/wgs84_pos#">
    <!-- ... -->
</entityXML>
```

Nun kann der Author Mode, wie bei einer [festen Installation des entityXML Frameworks](#author-mode-framework) (siehe oben) genutzt werden.

Diese Form der Author Mode CSS Integration bringt den (verschmerzbaren) Nachteil mit sich, dass zwischen einzelnen Views nicht kompfortabel über den **`Stile`** Reiter hin und her gewechselt werden kann. Um die optionale **Strukturierte Ansicht** zu deaktivieren oder zu aktivieren, muss die entsprechende *Processing Instruction* entweder auskommentiert bzw. gelöscht werden (Strukturierte Ansicht ist aus) oder gesetzt sein (Strukturierte Ansicht ist an). 

Dieser Ansatz setzt eine bestehende Internetverbindung voraus. Wenn man sicher gehen will, dass das ganze Setup auch <span class="emph">offline</span> funktioniert, [kann man sich die Dateien lokal speichern und die Pfade in
                           den Processing Instructions entprechend anpassen](#entityxml-offline).

<a name="docs_d14e800"></a>
## entityXML Document Model

<a name="entityxml-dm-intro"></a>
### Allgemeines

Das entityXML <span class="emph">Dokument Modell</span> verwendet folgenden Standard Namensraum:

**`https://sub.uni-goettingen.de/met/standards/entity-xml#`**

Zusätzliche Namespaces, die in entityXML Schema verwendet werden, sind: 

- **bf**: [http://id.loc.gov/ontologies/bibframe/](http://id.loc.gov/ontologies/bibframe/)
- **dc**: [http://purl.org/dc/elements/1.1/](http://purl.org/dc/elements/1.1/)
- **dnb**: [https://www.dnb.de](https://www.dnb.de)
- **foaf**: [http://xmlns.com/foaf/0.1/](http://xmlns.com/foaf/0.1/)
- **geo**: [http://www.opengis.net/ont/geosparql#](http://www.opengis.net/ont/geosparql#)
- **gndo**: [https://d-nb.info/standards/elementset/gnd#](https://d-nb.info/standards/elementset/gnd#)
- **owl**: [http://www.w3.org/2002/07/owl#](http://www.w3.org/2002/07/owl#)
- **skos**: [http://www.w3.org/2004/02/skos/core#](http://www.w3.org/2004/02/skos/core#)
- **wgs84**: [http://www.w3.org/2003/01/geo/wgs84_pos#](http://www.w3.org/2003/01/geo/wgs84_pos#)


entityXML Dokumente bestehen aus einem `entityXML` Wurzelelement, das eine oder mehrere Sammlungen enthalten kann:

```xml
<entityXML xmlns="https://sub.uni-goettingen.de/met/standards/entity-xml#" 
   xmlns:dc="http://purl.org/dc/elements/1.1/"
   xmlns:foaf="http://xmlns.com/foaf/0.1/" 
   xmlns:gndo="https://d-nb.info/standards/elementset/gnd#" 
   xmlns:owl="http://www.w3.org/2002/07/owl#"
   xmlns:skos="http://www.w3.org/2004/02/skos/core#" 
   xmlns:geo="http://www.opengis.net/ont/geosparql#" 
   xmlns:dnb="https://www.dnb.de"
   xmlns:wgs84="http://www.w3.org/2003/01/geo/wgs84_pos#">
   <collection>
      <metadata>
         <!-- Metadata Section -->
      </metadata>
      <data>
         <!-- Data Section -->
      </data>
   </collection>
</entityXML>
```

<a name="docs_d14e889"></a>
#### Datensammlung

Ein entityXML Datensammlung wird durch das Element `collection` repräsentiert, das sich aus einem Metadaten- (`metadata`) und einem Datenbereich (`data`) zusammensetzt. 

 Der Metadatenbereich stellt den Rahmen bereit, in dem Metadaten mit Blick auf den Erschließungskontext einer Datensammlung dokumentiert werden können. 

Im Datenbereich werden die eigentlichen Entitäteneinträge erschlossen bzw. dokumentiert.

<a name="metadata-section"></a>
### Metadaten einer Datensammlung

Der Metadatenbereich einer Datensammlung wird durch das Element `metadata` repräsentiert und stellt die Möglichkeit zur Verfügung, administrative und deskriptive Metadaten für eine Datensammlung zu dokumentieren.

In `metadata` können z.B. der Titel einer Datensammlung, Bearbeiter oder andere Verantwortlichkeiten, sowie Informationen hinsichtlich des Projekts bzw. des Kontexts, aus dem heraus die Entitäteneinträge der Datensammlung erschlossen wurden, angegeben werden.

Folgende Elemente können in diesem Kontext verwendet werden: 

- **Titel** [`title`](#d17e5317) (*verpflichtend*)  
  "Ein Titel für das Informationsobjekt, das in diesem Kontext beschrieben wird."
- **Abstract (Metadaten)** [`abstract`](#d17e4841) (*verpflichtend*)  
  "Eine kurze Beschreibung."
- **Verantwortlichkeit (Metadaten)** [`respStmt`](#respStmt) (*optional und wiederholbar*)  
  "Angabe zur Rolle einer Person oder Institution in Hinblick auf die Bearbeitung einer Datensammlung."
- **Datenprovider** [`provider`](#data-provider) (*verpflichtend*)  
  "Informationen zum Datenprovider der Datensammlung und der in diesem Rahmen dokumentierten Entitäten."
- **GND-Agentur** [`agency`](#agency-stmt) (*optional*)  
  "Informationen zur GND-Agentur, die die Datensammlung bearbeitet."
- **Revisionsbeschreibung** [`revision`](#revision) (*verpflichtend*)  
  "Statusinformationen zu einer Ressource (Datensammlung oder einzelner Entitätseintrag), die einzelne Bearbeitungsstände, Fragen oder anderweitige Anmerkungen zur Verarbeitung der entsprechenden Ressource dokumentiert. Die einzelnen Einträge (via `change`) werden ausgehend vom Datum von jung nach alt absteigend sortiert, sprich: Der jüngste Eintrag steht als Erstes in der Revisionsbeschreibung!"



```xml
<metadata>
   <title>[Resource Title]</title>
   <provider id="[Provider-ID]">
      <title>[Name des Lieferanten bzw. Erschließungskontexts der Datensammlung]</title>
      <respStmt id="[Person-ID]">
         <resp>[Rolle]</resp>
         <name>[Eigenname der Person]</name>
         <contact>
            <mail>[Email Adresse der Person]</mail>
         </contact>
      </respStmt>
      <abstract>[Kurzbeschreibung über den Lieferanten bzw. Erschließungskontext]</abstract>
   </provider>
   <respStmt id="[Person-ID]">
      <resp>[Rolle]</resp>
      <name>[Eigenname der Person]</name>
      <contact>
         <mail>[Email Adresse der Person]</mail>
      </contact>
   </respStmt>
   <abstract>[Kurzbeschreibung zur Datenlieferung]</abstract>
   <revision status="opened">[Dokumentation der Bearbeitungsstände der vorliegenden Ressource]</revision>
</metadata>
```

<a name="docs_d14e932"></a>
#### Titel einer Sammlung

Der Titel einer Sammlung wird via `title` dokumentiert. Hierbei handelt es sich für gewöhnlich um die Vorzugsbenennung einer Sammlung.

```xml
<title>Editionsprojekt XY: Entitäten</title>
```

<a name="docs_d14e945"></a>
#### Kurzbeschreibung

Die Angabe von `abstract` ist für jede Datensammlung obligatorisch. `abstract` umfasst eine Kurzbeschreibung, die den Entstehungskontext der Datensammlung näher beschreibt.

```xml
<abstract>Sammlung von Entitäten, die im Kontext des digitalen Editionsprojekts XY erschlossen wurden.</abstract>
```

<a name="provider-stmt"></a>
#### Providerbeschreibung

Das Element `provider` umfasst deskriptive und administrative Metadaten zu dem Datenlieferanten bzw. dem Kontext, aus dem die in der entsprechenden entityXML Ressource dokumentierten Entitätseinträge heraus erschlossen wurden.

Die Providerbeschreibung ist **optional** aber empfohlen und kann Informationen wie *Name des Providers*, *personelle Verantwortlichkeiten* und eine *Kurzbeschreibung* enthalten: 

- **Provider-ID** [`@id`](#d17e4983) (*verpflichtend*)  
  "Der GND-Agentur Identifier des Datenproviders. Dieser Identifier wird in der Regel direkt von der GND-Agentur, bei der der
                    Datenlieferant registriert ist, vergeben."
- **Titel** [`title`](#d17e5317) (*verpflichtend*)  
  "Ein Titel für das Informationsobjekt, das in diesem Kontext beschrieben wird."
- **Abstract (Metadaten)** [`abstract`](#d17e4841) (*verpflichtend*)  
  "Eine kurze Beschreibung."
- **Verantwortlichkeit (Metadaten)** [`respStmt`](#respStmt) (*optional und wiederholbar*)  
  "Angabe zur Rolle einer Person oder Institution in Hinblick auf die Bearbeitung einer Datensammlung."



Folgendes Template verdeutlicht die Struktur des Providerstatments:

```xml
<provider id="[Projekt-ID]">
   <name>[Projekttitel]</name>
   <respStmt id="[Person-ID]">
     <resp>[Rolle]</resp>
     <name>[Eigenname der Person]</name>
     <contact>
         <mail>[Email Adresse der Person]</mail>
         <address>[Adresse der Person]</address>
         <phone>[Telefonnummer]</phone>
         <web>[URL einer Homepage]</web>
     </contact>
   </respStmt>
   <abstract>[Kurzbeschreibung des Providers bzw Erschließungskontexts]</abstract>
</provider>
```

<a name="resp-stmt"></a>
#### Beschreibung von Verantwortlichkeiten

Personen, die im Rahmen eines [Providerstatements](#provider-stmt) oder eine [Datensammlung](#data-collection) als Verantwortliche (z.B. "Bearbeiter", "Koordinator" etc.) dokumentiert werden sollen, können via `respStmt` dokumentiert werden. Folgende Elemente können in diesem Rahmen dokumentiert werden: 

- **ID** [`@id`](#d17e5475) (*verpflichtend*)  
  "Eine interne ID zur Identifikation des entsprechenden Elements."
- **Rolle (Verantwortlichkeit)** [`resp`](#d17e4889) (*verpflichtend*)  
  "Eine Rolle, die der Person oder Institution im Rahmen der Bearbeitung einer Datensammlung zugeschrieben wird."
- **Name (Verantworlichkeit)** [`name`](#d17e4906) (*verpflichtend*)  
  "Name der verantwortlichen Person oder Institution."
- **Kontaktinformationen** [`contact`](#contact-metadata) (*optional*)  
  "Informationen, die zur Kontaktaufnahme genutzt werden können."



Zusätzliche Informationen zur Kontaktaufnahme können via `contact` erfolgen. Erforderlich ist hier die Angabe einer Mailadresse (`mail`); alle weiteren Informationen sind optional: 

- **Mailadresse** [`mail`](#d17e2536) (*verpflichtend und wiederholbar*)  
  "Email Adresse zur Kontaktaufnahme."
- **Telefon** [`phone`](#d17e2556) (*optional*)  
  "Telefonnummer zur Kontaktaufnahme"
- **Adresse** [`address`](#d17e2576) (*optional*)  
  "Postalische Anschrift."
- **Homepage (Kontakt)** [`web`](#d17e2596) (*optional*)  
  "Ein URL zu einer offiziellen oder persönlichen Homepage."



```xml
<respStmt id="MM">
   <resp>Bearbeiter</resp>
   <name>Mustermann, Max</name>
   <contact>
      <mail>muster@entitxml.de</mail>
      <address>Am Datenstrom 5, 1100101 Modulstadt</address>
      <phone>0551-1100101</phone>
      <web>www.entityxml-beispiele.de</web>
   </contact>
</respStmt>
```

<a name="lifecycle"></a>
### entityXML Lifecycle

Bevor wir weiter über die möglichen Metadaten einer entityXML Datensammlung reden, sollten wir einen Schritt zurück gehen und uns zunächst das <span class="emph">Lifecyclekonzept</span> von entityXML und das hiermit in Zusammenhang stehende Workflowmodell aus Sicht der Text+ GND-Agentur näher ansehen:

![markdown/docs/img/revision-workflow.png](docs/img/revision-workflow.png)

Der prototypische entityXML Workflow orientiert sich an drei grundlegenden Phasen, die sowohl von einer Datensammlung als auch von jedem Eintrag einzeln durchlaufen werden können. Im Folgenden spreche ich daher von <span class="emph">Informationsressource</span> und meine damit sowohl eine entityXML `collection` als auch die einzelnen definierten Typen von Eintitätseinträgen (näheres hierzu weiter unten):

**(1) Eröffnung und Bearbeitung durch den potenziellen Datenlieferanten**: Hierbei handelt es sich um die Anfangsphase einer Informationsressource, die damit als <span class="emph">eröffnet</span> ("opened") gilt: Ein Projekt, eine Forschergruppe oder einfach Irgendwer, der gerne Entitäten dokumentiert, legt in diesem Stadium eine entityXML Datei an und beginnt eine Sammlung mit Einträgen zu füllen. Diese Einträge – und damit auch die Sammlung – befindet sich somit in der ständigen Bearbeitung, d.h. Hintergründe und Informationen werden recherchiert und in den entsprechenden Einträgen dokumentiert. Im besten Falle werden die entityXML Daten, die so entstehen, von dem Projekt oder der Person schon in einem eigenen Informationssystem verarbeitet und individuell genutzt.

> **'Bedenke die Möglichkeiten'**  
> An dieser Stelle möchte ich erwähnen, dass der Workflow einzig und allein davon abhängt, was man mit den Daten am Ende machen möchte! Man kann entityXML auch benutzen, ohne überhaupt jemals einen Austausch mit der GND in Erwägung zu ziehen. Genausogut kann eine entityXML auch maschienell als <span class="emph">Nebenprodukt</span> erzeugt werden, sprich: Die Daten werden primär in einem ganz anderen System erschlossen und entityXML dient lediglich als Exportformat, das dann wiederum an eine GND-Agentur geht. Im Prinzip sind der Phantasie hier keine Grenzen gesetzt. Zumindest aus Sicht der Text+ GND-Agentur stellt sich der Workflow so dar, wie oben gezeigt!


**(2) Überprüfung und Bearbeitung durch die Text+ GND-Agentur**: In dieser zweiten Phase befindet sich eine Informationsressource auf dem Prüfstand ("staged") bei der Text+ GND-Agentur, sprich: Die Informationsressource wird daraufhin geprüft, ob die geltenden Qualitätskriterien erfüllt sind. Sollte diese Prüfung negativ ausfallen, wird die Agentur von Fall zu Fall entscheiden, (1) ob die Daten durch Transformationen mit angemessenem Aufwand entsprechend angepasst werden können oder, (2) ob eine erneute Bearbeitung durch den verantwortlichen Datenlieferanten erforderlich ist. Festgestellte Probleme in und Rückfragen zu den Daten werden in jedem Fall durch die Agentur direkt an den Daten dokumentiert und an den Datenlieferanten zurückgemeldet. In dem Moment, in dem die Daten die Qualitätskriterien erfüllen, tritt die Informationsressource in das dritte Stadium ein.

**(3) Abschluss der Bearbeitung durch die Text+ GND-Agentur**: Diese dritte und letzte Phase markiert das Ende der Bearbeitung einer Informationsressource, die nun die entsprechenden Qualitätskriterien erfüllt und somit abschließend durch die Text+ GND-Agentur in die GND eingespielt, d.h. veröffentlicht werden kann: GND-URIs für neue Einträge werden erstellt und an den entsprechenden Stellen in der entityXML Datei dokumentiert, mögliche Änderungen wurden an Normdateneinträgen vorgenommen etc. Als letzte Handlung wird der Datenlieferant über den Abschluss benachrichtigt, indem er die aktualisierte und nun als <span class="emph">abgeschlossen</span> ("closed") geltende entityXML Ressource, angereichert mit den entsprechenden Information, zurück erhält.

> Die zweite und dritte Phase ist strukturell vergleichbar mit einem Prozesses, den man in der Informationsverarbeitung auch als [ETL](https://de.wikipedia.org/wiki/ETL-Prozess) ("Extract, Transform, Load") kennt: In ETL Prozessen werden heterogene Quelldaten zuerst extrahiert und für anschließende Transformationsschritte aufbereitet. In der Folge werden die Daten durch unterschiedliche Transformationen in ein definiertes Zielformat konvertiert, dass dann letztlich in ein definiertes Zielsystem eingespielt ("geladen") wird. 


Innerhalb dieser drei Phasen kann eine Sammlung bzw. ein Eintrag acht unterschiedliche <span class="emph">Bearbeitungsstadien</span> durchlaufen. Diese lassen sich als einzelne Entwicklungsschritte einer Ressource in ihrem Lebenszyklus beschreiben – von dem Moment der Erstellung an bis zur Publikation:

![markdown/docs/img/revision-steps.png](docs/img/revision-steps.png)

Eine Erklärung zu den hier abgebildeten acht Stadien bleibe ich noch für einen kurzen Moment schuldig; sie folgt in wenigen Absätzen. Bis hierher ging es vorallem darum, einen Eindruck davon zu gewinnen, wie der Lebenszyklus einer entityXML Ressource aufgebaut ist (bzw. sein kann) und wie sich die Text+ GND-Agentur hierauf aufbauend einen Verarbeitungsworkflow definiert hat.

<a name="revision-desc"></a>
#### Revisionsbeschreibung

Bleibt die Frage, wie der geschilderte Lebenszyklus in den Daten dokumentiert wird? Dafür dient die <span class="emph">Revisionsbeschreibung</span>, die durch das Element `revision` repräsentiert wird.

Revisionsbeschreibungen sind für Sammlungen (`collection`) als auch für Entitätseinträge (`entity`, `person`, `place` usw.) implementiert. Für Sammlungen ist `revision` obligatorisch! Für Einträge ist `revision` solange optional, bis ein Eintrag explizit für die Bearbeitung durch eine GND-Agentur vorgesehen wird (durch die Verwendung von `@agency` angezeigt, aber mehr dazu [später](#agency-mode-record)). Als <span class="emph">Best Practice</span> gilt jedenfalls, dass jeder Eintrag von Anfang an eine konsistente Revisionsbeschreibung nachweisen kann. 

Eine Revisionsbeschreibung besteht im Kern aus zwei Kerninformationen: Der momentanen <span class="emph">Bearbeitungsphase</span> (siehe oben), in dem sich die Sammlung oder der Entitätseintrag befindet, sowie mindestens einer, aber in der Regel mehrere <span class="emph">Änderungseinträge</span>, die einzelne, vorgenommene Änderungen oder Arbeitsschritte dokumentieren.

Die Bearbeitungsphase, also ob sich die Sammlung oder der Eintrag in der anfänglichen Bearbeitungsphase ("**opened**") oder schon auf dem Prüfstand einer GND-Agentur ("**staged**") befindet oder bereits fertig bearbeitet und abgeschlossen ("**closed**") ist, wird in `@status` angegeben.

Änderungseinträge werden durch `change` Elemente innerhalb von `revision` angelegt und dienen der knappen und präzisen textlichen Dokumentation von Änderungen, Erweiterungen oder anderer Arbeiten, die an einer Sammlung oder einem Eintrag vorgenommen wurden. Zusätzlich verzeichnet ein Änderungseintrag das Datum, an dem er angelegt wurde via `@when` als ISO 8601 sowie die ID der verantworlich zeichnenden Person via `@who`:

```xml
<revision status="opened">
   <change when="2022-12-19" who="MM">
      Sammlung angelegt und erste Liste mit Personen begonnen.
   </change>
</revision>
```

> **Who?**  
> Die verantwortlich zeichnende Person muss selbstverständlich vorher bei den bearbeitenden Personen in der [Providerbeschreibung](#provider-stmt) definiert werden.


**Achtung**: Änderungseinträge werden immer von oben nach unten sortiert, ausgehend von dem jüngsten Eintrag, sprich: Der Eintrag mit dem aktuellsten Datum befindet sich immer an erster Stelle in der Revisionsbeschreibung!

Ein Änderungseintrag hat wie schon das übergeordnete `revision` Element ein `@status` Attribut. Hier kommen nun die oben bereits angesprochenen <span class="emph">acht Bearbeitungsstadien</span> ins Spiel, die auf diese Weise in der Revisionsbeschreibung angegeben werden <span class="emph">können</span>. "Können" heißt: Nicht jeder Änderungseintrag muss ein Bearbeitungsstadium ausweisen, aber wenn z. B. besondere Meilsensteine anstehen (etwa der Tag, an dem die entityXML an die Text+ GND-Agentur geschickt wird), sollte zumindest der aktuellste Eintrag ein explizites Bearbeitungsstadium dokumentieren:

```xml
<revision status="opened">
   <change when="2023-01-05" who="MM" status="candidate">
      Bearbeitung abgeschlossen und an die Text+ GND-Agentur geschickt.
   </change>
   <change when="2022-12-19" who="MM">
      Sammlung angelegt und erste Liste mit Personen begonnen
   </change>
</revision>
```

Und hier nun die versprochene Erklärung zu den einzelnen Stadien:



- **draft**: (Draft, *default*) Die Informationsressource befindet sich in der Bearbeitung.
- **candidate**: (Kandidat) Die Bearbeitung ist soweit abgeschlossen. Die Informationsressource gilt nun als potenzieller Kandidat zur Übertragung an die GND und ist somit bereit zur Prüfung durch eine GND-Agentur.
- **embargoed**: (Gesperrt) Die Informationsressource ist derzeit gesperrt. Sie erfüllt entweder nicht die erforderlichen Qualitätskriterien oder ist aus einem anderen Grund, der in dem Änderungseintrag vermerkt ist, gesperrt. Sie muss entsprechend überarbeitet werden.
- **withdrawn**: (Zurückgezogen) Die Informationsressource wurde zurückgezogen. Sie wird in Hinblick auf eine Übertragung an die GND ignoriert.
- **cleared**: (Überarbeitet) Die Informationsressource wurde in Hinblick auf die geltenden Qualitätskriterien überarbeitet und gilt nun als überarbeiteter Kandidat, der erneut geprüft wird.
- **approved**: (Angenommen) Die Prüfung der Informationsressource ist abgeschlossen. Sie erfüllt alle erforderlichen Qualitätsstandards und ist bereit zur Übertragung an die GND. Dieser Bearbeitungsschritt ist Mitarbeiter\*innen einer GND-Agentur vorbehalten.
- **submitted**: (An GND geliefert) Die Daten der Informationsressource wurden an die GND zum Übertragen geliefert. Dieser Bearbeitungsschritt ist Mitarbeiter\*innen einer GND-Agentur vorbehalten.
- **published**: (Veröffentlicht) Die Daten der Informationsressource wurden in der GND veröffentlicht. Dieser Bearbeitungsschritt ist Mitarbeiter\*innen einer GND-Agentur vorbehalten.



Wenn also kein Stadium explizit gemacht wird, gilt eine Sammlung oder ein Eintrag als "**draft**". Die folgende Abbildung verdeutlicht den Lebenszyklus nochmal anhand der acht Stadien in einer auf das Wesentliche reduzierten Form:

![markdown/docs/img/revision-steps.condensed.png](docs/img/revision-steps.condensed.png)

<a name="docs_d14e1184"></a>
##### Anmerkung zur Ausführlichkeit von Revisionsbeschreibungen

Wie immer bei textlicher Dokumentation stellt sich auch bei der Revisionsbeschreibung die Frage, was auf welche weise und vorallem wie ausführlich dokumentiert werden soll? Nun, sowohl der Inhalt als auch die Ausführlichkeit der Beschreibung liegt in der Verantwortung der Bearbeiter\*innen der jeweiligen entityXML Ressource, denn diese wissen wahrscheinlich am Besten, was hilfreiche Informationen sind, die mit Blick auf die Arbeit an ihren Daten festgehalten werden sollten.

Dennoch empfielt es sich, in diesem Bereich nicht nur für sich, sondern auch *für Dritte nachvollziehbar zu dokumentieren*. Die Revisionsbeschreibung dient zwar in erster Linie dazu, Bearbeitungsstände zu dokumentieren, kann und <span class="emph">soll</span> aber auch zur Unterstützung der Kommunikation mit einer GND-Agentur beitragen. Dementsprechend bietet es sich an, an dieser Stelle hilfreiche Informationen nicht nur für die primären Bearbeiter\*innen sondern auch für Agenturmitarbeiter\*innen zu hinterlegen, die in Hinblick auf den Umgang mit der entsprechenden Sammlung oder dem entsprechenden Eintrag von Relevanz sind bzw. sein können. Dazu gehören z.B. Erwartungen oder auch Wünsche, die an die Agentur gestellt werden, genauso wie hilfreiche Hinweise, die die Arbeit erleichtern können:

```xml
<!-- Beispiel einer Revisionsbeschreibung eines Eintrags, der angelegt werden soll -->
<revision status="opened">
   <change when="2022-12-09" who="MFZ" status="candidate">Nicht in GND enthalten. Bitte eintragen.</change>
</revision>
```

```xml
<revision status="opened">
   <change when="2023-01-05" who="MM" status="candidate">
      Bearbeitung abgeschlossen und an die Text+ GND-Agentur geschickt. 
      Die Sammlung besteht aus drei Listen: (1) Personen, (2) Orte, (3) Körperschaften. 
      Bei den Körperschaften bestehen zu einzelnen Einträgen noch
      wichtige Fragen an die GND-Agentur. Diese sind in den 
      entsprechenden Revisionsbeschreibungen hinterlegt. Die Einträge
      sind als "embargoed" gekennzeichnet.
   </change>
   <change when="2022-12-19" who="MM">
      Sammlung angelegt und erste Liste mit Personen begonnen
   </change>
</revision>
```

Das letzte Beispiel zeigt, dass auch Hinweise bzw. Anmerkungen zu einer Sammlung oder einem Eintrag durchaus hilfreich sein können. In gleicher Weise wird auch die Agentur an dieser Stelle mögliche Rückmeldungen oder Rückfragen dokumentieren, so wie z.B. für den Eintrag mit der ID `ulrich_renner` (GND: [https://d-nb.info/gnd/141596643](https://d-nb.info/gnd/141596643)) in der Beispieldatei [KMG-GND.20230203.0.5.1.xml](https://gitlab.gwdg.de/entities/entityxml/-/blob/master/samples/KMG-GND.20230203.0.5.1.xml):

```xml
<revision status="staged">
   <change when="2023-01-19" who="US" status="approved">
      Die Daten hinsichtlich der verknüpften Publikationen stammen nicht aus 
      dem GND Eintrag (https://d-nb.info/gnd/141596643) sondern aus dem DNB Katalog: 
      Die Katalogseinträge zu den Ausgaben sind mit dem falschen GND Eintrag 
      verknüpft. Die Anfrage wird an die DNB zur Änderung im Katalog weitergeleitet.
   </change>
   <change when="2022-12-08" who="MFZ" status="candidate">Fehlerhafter Eintrag; nicht-zugehörige Literatur gefunden</change>
</revision>
```

<a name="data-collection"></a>
### Datenbereich

Der Datenbereich einer `entityXML` Ressource wird durch das Element`data` repräsentiert, das eine oder mehrere Datenlisten (`list`) enthalten kann.

Jede Liste kann durch einen Titel (`title`) und ein Abstract (`abstract`) näher beschrieben werden:

```xml
<data>
   <list>
      <title><!-- Titel der Liste --></title>
      <abstract><!-- Kurzbeschreibung der Liste --></abstract>
      <!-- Einer oder mehrere Entitäteneinträge folgen hier -->
   </list>
</data>
```

Jede Liste bildet schließlich den Kontext für die zu dokumentierenden Entitätseinträge.

<a name="docs_d14e1248"></a>
### Entitätseintrag im Allgemeinen

<a name="records-intro"></a>
#### Einstieg ins Anlegen von Entitätseinträgen

Ein Entitätseintrag bzw. ein Record repräsentiert die Beschreibung einer konkreten Entität einer bestimmten Klasse. entityXML unterstützt derzeit folgende Entitätsklassen:


- `entity` Allgemeine Entität (eine Art Oberklasse aller anderen Entitätstypen)
- `person` Person
- `corporateBody` Körperschaft
- `place` Ort
- `work` Werk

Es gibt noch zwei weitere Recordklassen, die bisher pro forma im Schema enthalten sind und noch nicht wirklich ausspezifiziert sind, weil hierfür noch keine Anwendungsfälle bestehen:


- `expression` Ausdrucksform eines Werks (Expression)
- `manifestation` Manifestation einers Werks (Manifestation)

entityXML ist in erster Linie ein <span class="emph">Markup Format</span>! Das bedeuted, dass Wissen über eine Eintität zunächst ersteinmal in Form von (menschenlesbarem) Fließtext repräsentiert werden kann. Bei dieser Form der Repräsentation von Daten als Text spricht man auch gerne von <span class="emph">unstrukturierten Daten</span>:

```xml
<entity xml:id="erika_muster">
   In meiner neuen Geldbörse habe ich ein Papierkärtchen entdeckt, dass 
   eine "Mustermann, Erika" verzeichnet. Auf Wikipedia fand ich heraus, 
   dass es sich um eine fiktive Person handelt, die als Platzhalter
   für eine beliebige (reale) Frau steht.
</entity>
```

Dieser Fließtext wird dann in der Folge durch entsprechendes Markup angereichert, um das im Text enthaltene Wissen über eine Entität explizit und <span class="emph">maschienenlesbar</span> (d.h. die Form der Daten ist so angelegt, dass sie einen direkten systematischen Zugriff auf die Informationsinhalte durch eine Maschiene erlaubt) zu machen. Diese durch Markup angereicherte Form von (textlichen) Daten bezeichnet man auch als <span class="emph">semistrukturierte Daten</span>: 

```xml
<entity xml:id="erika_muster">
   In meiner neuen Geldbörse habe ich ein Papierkärtchen entdeckt, dass 
   eine "<dc:title>Mustermann, Erika</dc:title>" verzeichnet. 
   <ref target="https://de.wikipedia.org/wiki/Mustermann">Wikipedia</ref> 
   schreibt, <note>es handle sich um eine fiktive Person, die als Platzhalter
   für eine beliebige (reale) Frau steht</note>.
</entity>
```

Dementsprechen kann man Entitätsbeschreibungen bzw. Entitätseinträge auch generell in einer eher strukturierten Form anlegen, die weniger "textlich" gehalten ist: 

```xml
<entity xml:id="max_muster">
   <skos:note>Eine fiktive Person, die als Platzhalter fungiert</skos:note>
   <source>Eine kleine Karte in meiner neuen Geldbörse</source>
   <dc:title>Mustermann, Max</dc:title>
   <ref target="https://de.wikipedia.org/wiki/Mustermann">Wikipedia</ref>
</entity>
```

> **Records in entityXML sind also strukturierte Daten?**  
> Nein, es handelt sich nachwievor um semistrukturierte Daten! Die Informationsinhalte sind zwar strukturierter <span class="emph">dargestellt</span>, aber es ist (und bleibt) Text mit Markup. Nichtsdestoweniger folgen entityXML Daten durch die Anbindung an ein Schema einer definierten Struktur und können somit als "strukturierter" gelten, als andere XML Daten, die nicht nach einem spezifizierten Schema erschlossen wurden.


Auch wenn die in entityXML spezifizierten Entitätsklassen je unterschiedliche Informationen enthalten können, teilen sie sich neben der <span class="emph">[Revisionsbeschreibung](#revision-desc)</span>, die bereits eingehend erläutert wurde, folgende Elemente und Attribute: 

- **Record ID** [`@xml:id`](#d17e5720) (*verpflichtend*)  
  "Angabe eines Identifiers zur Identifikation eines Entitäteneintrags innerhalb der Datensammlung. Die Angabe einer ID via `@xml:id` ist verpflichtend!"
- **GND-URI** [`@gndo:uri`](#d17e5417) (*optional*)  
  "Angabe des GND-HTTP URIs der beschriebenen Entität in der GND, sofern vorhanden."
- **Agenturanfrage** [`@agency`](#attr.record.agency) (*optional*)  
  "Anfrage an die Agentur, welche Aktion in Hinblick auf den Eintrag durchgeführt werden soll."
- **Anreicherung** [`@enrich`](#d17e5700) (*optional*)  
  "Der Record soll mit Daten eines externen Datendienstes angereichert werden. Hierfür wird ein zusätzliches Konversionsscript benötigt, dass die Konversionsroutinen zur verfügung stellt, um den Record anzureichern!"



- **Titel (Record)** [`dc:title`](#d17e2645) (*optional*)  
  "Ein Titel für die Ressource. `dc:title` wird dann zur Bezeichnung einer Entität verwendet, wenn keine Vorzugsbenennung dokumentiert wird bzw. werden soll."
- **Biographische Angaben** [`gndo:biographicalOrHistoricalInformation`](#d17e2985) (*optional und wiederholbar*)  
  "Biographische oder historische Angaben über die Entität."
- **GND-Identifier einer Dublette** [`dublicateGndIdentifier`](#dublicates) (*optional und wiederholbar*)  
  "Der GND-Identifier einer möglichen Dublette."
- **Geographischer Schwerpunkt** [`gndo:geographicAreaCode`](#elem.gndo.geographicAreaCode) (*optional und wiederholbar*)  
  "Ländercode(s) der Staat(en), denen die Entität zugeordnet werden kann (z.B. Lebensmittelpunkt bzw. Schwerpunkt ihres Wirkens bei Personen). Um die Datenqualität zu erhöhen und eine automatisierte Verarbeitung zu erleichtern, muss via `@gndo:term` ein Deskriptor aus dem GND Area Code Vokabular (https://d-nb.info/standards/vocab/gnd/geographic-area-code) angegeben werden!"
- **GND-Identifier** [`gndo:gndIdentifier`](#d17e3593) (*optional und wiederholbar*)  
  "Der GND-Identifier eines in der GND eingetragenen Normdatensatzes sofern vorhanden"
- **GND-Sachgruppe** [`gndo:gndSubjectCategory`](#d17e3634) (*optional und wiederholbar*)  
  "(GND-Sachgruppe)"
- **Sprachraum** [`gndo:languageCode`](#d17e3736) (*optional und wiederholbar*)  
  "Code(s) des Sprachraumes, dem die Entität zugeordnet werden kann. Um die Datenqualität zu erhöhen und eine automatisierte Verarbeitung zu erleichtern, kann via @gndo:term zsätzlich ein Deskriptor aus dem LOC ISO 693-2 Language Vokabular (http://id.loc.gov/vocabulary/iso639-2/) angegeben werden!"
- **Beziehung** [`gndo:relatesTo`](#d17e4511) (*optional und wiederholbar*)  
  "Beziehung der beschriebenen Entität zu einer anderen Entität, entweder in der GND oder in der gleichen entityXML Ressource. Diese Property hat keine direkte Entsprechung in der GNDO!"
- **Hyperlink** [`ref`](#elem.ref) (*optional und wiederholbar*)  
  "Ein Link zu einer Webressource, die weitere Informationen über die im Eintrag beschriebene Entität enthält. Der entsprechende URL wird entweder direkt als Text in `ref` oder via `@target` dokumentiert."
- **Gleiche Entität** [`owl:sameAs`](#elem.owl.sameAs) (*optional und wiederholbar*)  
  "HTTP-URI der selben Entität in einem anderen Normdatendienst."
- **Anmerkung** [`skos:note`](#elem.skos.note) (*optional und wiederholbar*)  
  "Eine Anmerkung zum Eintrag."
- **Quellenangabe** [`source`](#elem.source) (*optional und wiederholbar*)  
  "Angaben zu einer Quelle, die verwendet wurde, um die in diesem Eintrag gegebenen Informationen zu recherchieren."
- **ANY ELEMENT (eng gefasst)** [`anyElement.narrow`](#any-restricted) (*optional und wiederholbar*)  
  "Ein Element, dessen **QName** einem *custom namespace* angehört und nicht im vorliegenden Schema explizit spezifiziert ist."



<a name="custom-namespaces"></a>
#### Verwendung fremder Namensräume

Bevor wir das allgemeine Element- und Attributsset eines Entitätseintrags in entityXML durchgehen, widmen wir uns kurz der Frage "Was, wenn ich Informationen dokumentieren möchte, die nicht im Schema spezifiziert sind?".

entityXML verhält sich als Format in Hinblick auf die Auszeichnung von Informationen innerhalb aller Entitätseinträge (`entity` und alle seine Unterklassen wie z.B. `person` oder `place`) <span class="emph">offen</span>: Die Spezifikation macht zwar gewisse Vorgaben in Hinblick auf Elemente aus der GNDO und anderen Formaten, sowie diesbezügliche Validierungsregeln; nichtdestoweniger erlaubt es ausdrücklich Elemente und Attribute aus anderen Namensräumen (sog. *custom elements* bzw. *attributes*), [die nicht im
                        Rahmen der entityXML Spezifikation definiert sind](#entityxml-dm-intro) und somit als <span class="emph">spezifikationsfremde Namensräume</span> gelten.

Dementsprechend sind folgende Elemente von der Verwendung als Custom Elements innerhalb von Entitätseinträgen ausgeschlossen, da sie fester Bestandteil der entityXML Spezifikation sind:

**Elemente:**



- http://id.loc.gov/ontologies/bibframe/**instanceOf**
- http://xmlns.com/foaf/0.1/**page**
- http://purl.org/dc/elements/1.1/**title**
- http://www.w3.org/2004/02/skos/core#**note**
- http://www.w3.org/2002/07/owl#**sameAs**
- http://www.opengis.net/ont/geosparql#**hasGeometry**
- http://www.w3.org/2003/01/geo/wgs84_pos#**lat**
- http://www.w3.org/2003/01/geo/wgs84_pos#**long**

**Generell alle Elemente aus folgenden Namensräumen:**



- https://sub.uni-goettingen.de/met/standards/entity-xml#
- https://sub.uni-goettingen.de/met/standards/entity-store#
- https://d-nb.info/standards/elementset/gnd#

<a name="docs_d14e1386"></a>
##### Von der einfachen Anwendung ...

Die Implementierung von entityXML ist so angelegt, dass es als Speicherformat von (Forschungs- )projekten verwendet werden kann, um Entitätsbeschreibungen als Forschungsdaten so frei wie möglich und so restriktiv wie nötig (aus Perspektive der späteren Datenverarbeitung durch eine GND Agentur) zu dokumentieren und zu nutzen.

Um *custom elements* oder *attributes* in einem entityXML Entitätseintrag zu verwenden, muss zunächst ein entsprechender beliebiger Namensraum definiert werden, der nicht durch die vorliegende Spezifikation ausgeschlossen wird (siehe oben). Für gewöhnlich bietet sich der Wurzelknoten `entityXML` dafür an:

```xml
<entityXML 
   xmlns="https://sub.uni-goettingen.de/met/standards/entity-xml#" 
   xmlns:example="http://example.com/irgendein-custom-namespace">
   <!-- ... -->
</entityXML>
```

Nichtsdestoweniger kann der bzw. die Namespaces auch **direkt am jeweiligen Entitätselement** definiert werden:

```xml
<entity xml:id="max_muster" xmlns:example="http://example.com/mein-namespace">
   <skos:note>Eine fiktive Person, die wahrescheinlich mal einen Finger verloren hat.</skos:note>
   <source>Eine kleine Karte in meiner neuen Geldbörse</source>
   <dc:title>Mustermann, Max</dc:title>
   <ref target="https://de.wikipedia.org/wiki/Mustermann">Wikipedia</ref>
   <example:finger-anzahl>9</example:finger-anzahl>
</entity>
```

So ist es möglich, für jede Entität jegliche Art von Datum zu dokumentieren - je nach den eigenen Anforderungen, die an die Daten gestellt werden.

<a name="docs_d14e1419"></a>
##### ... hin zum Mittelweg ...

In der Regel dienen *custom elements* dazu, zusätzliche Daten in Einträgen zu dokumentieren, **die in der GND als Normdatei keinen Platz finden** und dementsprechen auch nicht über die GNDO beschrieben werden können.

Diese Verwendungsweise von *custom elements* als "Erweiterungen" der GNDO Beschreibung um (projektspezifische) zusätzliche Daten verstehen wir als "Goldenen Weg" bzw. den optimalen Mittelweg:

```xml
<person xml:id="nguyen-kim-mai-thi" gndo:uri="https://d-nb.info/gnd/1135801363" 
   xmlns:social="http://social-media.statistics.de/kennzahlen">
   <gndo:preferredName>
     <gndo:surname>Nguyen-Kim</gndo:surname>, 
     <gndo:forename>Mai Thi</gndo:forename>
   </gndo:preferredName>
   <social:twitter when="2023-01-11" 
      follower="450602" 
      joined="2013-01-01">maithi_nk</social:twitter>
   <social:youtube when="2023-01-11" 
      follower="1480000" 
      joined="2016-09-08">maiLab</social:youtube>
</person>
```

Dieses Beispiel erweitert einen Personeneintrag um Angaben zu Social-Media Accounts und diesbezügliche Kennzahlen wie Anzahl der Follower, Beitrittsdatum und Account-ID. Diese Daten können dann z.B. als Grundlage eigener statistischer Analysen dienen.

<a name="lido"></a>
##### ... und auf die Spitze getrieben!

Im Prinzip kann man dieses Konzept auf die Spitze treiben und z.B. einen kompletten <span class="emph">LIDO</span> Datensatz in einem `entity` Eintrag (und nur hier) unterbringen:

```xml
<entity xml:id="lido00099" gndo:type="https://d-nb.info/standards/elementset/gnd#Work">
     <lido:lido xmlns="http://www.lido-schema.org"
         xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
         xmlns:skos="http://www.w3.org/2004/02/skos/core#"
         xmlns:lido="http://www.lido-schema.org">
         <lido:lidoRecID lido:source="Deutsches Dokumentationszentrum für Kunstgeschichte - Bildarchiv Foto Marburg" lido:type="http://terminology.lido-schema.org/lido00100">DE-Mb112/lido-obj00154983</lido:lidoRecID>
         <lido:objectPublishedID lido:type="http://terminology.lido-schema.org/lido00099" lido:pref="http://terminology.lido-schema.org/lido00170">https://www.uffizi.it/opere/botticelli-primavera</lido:objectPublishedID>
         <lido:objectPublishedID lido:type="http://terminology.lido-schema.org/lido00099" lido:pref="http://terminology.lido-schema.org/lido00170">https://d-nb.info/gnd/4069615-7</lido:objectPublishedID>
         <lido:category>
             <skos:Concept rdf:about="http://terminology.lido-schema.org/lido00096">
                 <skos:prefLabel xml:lang="en">Human-made object</skos:prefLabel>
                 <skos:exactMatch>http://vocab.getty.edu/aat/300386957</skos:exactMatch>
                 <skos:exactMatch>http://erlangen-crm.org/current/E22_Human-Made_Object</skos:exactMatch>
             </skos:Concept>
             <lido:term lido:addedSearchTerm="yes">man-made object</lido:term>
         </lido:category>
         <!-- ... -->
     </lido:lido>
 </entity>
```

Das wird durch die besondere Implementierung des `entity` Elements ermöglicht, das zwei mögliche Optionen der Beschreibung von Entitäten zulässt: (1) Die Beschreibung einer Entität gemäß des im entityXML Schema spezifizierten Standardsets von Elementen und Attributen oder (2) die Beschreibung einer Entität durch ein beliebiges Set an Elementen und Attributen, die nicht den Namensräumen <https://sub.uni-goettingen.de/met/standards/entity-xml#>, <https://sub.uni-goettingen.de/met/standards/entity-store#> und <https://d-nb.info/standards/elementset/gnd#> angehören.

> **Warum ist bei Option 2 der GNDO Namespace ausgeschlossen?**  
> Berechtigte Frage! Weil für Daten, die mithilfe der GNDO Terme beschrieben werden, die in diesem Schema implementierten Entitätsklassen gedacht sind. Für diese ist spezifisch geregelt, welche Daten mit welchen GNDO-Beschreibungskategorien erschlossen werden können bzw. sollen. Von diesem Standpunkt aus betrachtet, ergibt es keinen Sinn, dass in `entity` andere GNDO Elemente verwendet werden (können), als die, die bereits im Schema spezifiziert werden. Wenn die Beschreibung einer Entität also gemäß der GNDO erfolgen soll, dann kann man die entsprechenden Eintragselemente verwenden und sollte nicht auf `entity` ausweichen.


Im abgebildeten Beipiel wird also der extremste Weg eingeschlagen, der in entityXML möglich ist: Die **vollständige Integration eines fremden XML-Formats** (sofern es nicht den Namespace der GND oder die beiden entityXML Namespaces integriert).

`@gndo:type` dient der Identifikation des Entitätstyps, der durch das fremde Format beschrieben wird. In unserem Beispiel handelt es sich um ein Gemälde, was sich grob als `gndo:Work` fassen lässt.

<a name="docs_d14e1474"></a>
##### Arbeiten mit schemafremden Elementen im Author Mode

oXygen XML arbeitet im Author Mode mit einem hilfreichen Werkzeug, dass sich ["Content Completion Assistant"](https://www.oxygenxml.com/doc/versions/25.0/ug-editor/topics/content-completion-author-mode.html#content-completion-author-mode) nennt. Hierbei handelt sich um ein Eingabeinterface für Elemente, das in seiner Standardeinstellung nur die Eingabe von schema-bekannten Elementen erlaubt.

Um die Eingabe schemafremder Elemente zu erlauben geht man in die Einstellungen `Editor > Bearbeitungsmodus > Autor > Schemabewusst` und entfernt den Haken bei "Nur das Einfügen gültiger Elemente und Attribute erlauben".

<a name="docs_d14e1487"></a>
##### Verfahrensweise mit Daten aus fremden Namensräumen bei der Text+ GND-Agentur

Daten, die mit Hilfe von *custom elements* erschlossen werden, werden in Hinblick auf die Anlage neuer Einträge bzw. die Anreicherung bereits bestehender Einträge in der GND ignoriert. Bei einer Verarbeitung von entityXML Ressourcen durch die Text+ GND Agentur werden ausschließlich die Elemente betrachtet, die fester Bestandteil der Spezifikation sind. 

Nichtsdestoweniger bleiben die Daten in *custom elements* in den Einträgen erhalten. Sie sind weiterhin Bestandteile des projektinternen Forschungsdatenlebenszyklus, können in diesem Rahmen versioniert und in andere Informationsprozesse eingebracht werden.

<a name="agency-mode-record"></a>
#### Verarbeitungshinweis für die Agentur

Als Austauschformat sollte entityXML vorallem dem <span class="emph">Datenaustausch</span> (genauer: den Prozesse des Austauschens von Daten) zwischen Datengebern (z.B. Projekte, die für ihre eigenen Zwecke Entitäten beschreiben) auf der einen und einer GND-Agentur auf der anderen Seite eine definierte Struktur geben - so die Idee. Zu dieser Struktur gehören auch Informationen darüber, was mit einem Eintrag aus Sicht der Datengeber eigentlich geschehen soll: Handelt es sich bei einem Eintrag um eine Entität, die noch nicht in der GND enthalten ist und daher angelegt werden soll? Oder sollen die in einem Eintrag enthaltenen Information einem bereits bestehenden Normdatensatz hinzugefügt werden?

Der **Verarbeitungshinweis** soll diese Fragen addressierbar machen: Für jeden Eintrag lässt sich mit Hilfe von ``@agency`` ein Hinweis vergeben, wie der Eintrag von einer GND Agentur verarbeitet werden soll. Diesbezüglich stehen folgende Deskriptoren zur Verfügung:


- *create*: Der Eintrag soll als Normdatensatz neu in der GND angelegt werden.
- *update*: Ein bestehender GND Normdatensatz, der durch den GND-URI des Eintrags (`@gndo:uri`) identifiziert ist, soll durch neue Informationen aus diesem Eintrag angereichert werden.
- *merge*: Dubletten, die in dem Eintrag dokumentiert werden, sollen mit dem GND Normdatensatz, der durch den GND-URI des Eintrags (`@gndo:uri`) identifiziert ist, zusammengeführt werden.
- *ignore*: Der Eintrag wird von der Agentur nicht bearbeitet.

```xml
<place xml:id="lummerland" agency="create">
   <gndo:preferredName>Lummerland</gndo:preferredName>
   <gndo:biographicalOrHistoricalInformation>
      Eine fiktive Insel aus den Geschichten von Michael Ende.
   </gndo:biographicalOrHistoricalInformation>
   <!-- ... -->
</place>
```


> **Experimentelles Feature**  
> Der Verarbeitungshinweis ist zwar ein essentieller Bestandteil, befindet sich momentan aber noch in der Erprobung. Wie produktiv sich hiermit die Bearbeitung von Einträgen strukturieren lässt, wird sich erst noch in der Praxis zeigen müssen.



<a name="entity-notes"></a>
#### Anmerkungen

Für jeden Eintrag können eine oder mehrere optionale, **allgemeine Anmerkungen** mittels ``skos:note`` dokumentiert werden, die zusätzliche Informationen oder Hinweise zu der beschriebenen Entität enthalten. Die Anmerkung sollte dabei in einer Form gehalten sein, die angebracht für eine spätere Veröffentlichung ist.

Die Sprache, in der eine Anmerkung verfasst ist, lässt sich via `@xml:lang` explizit spezifizieren. Wenn keine Sprache explizit angegeben wird, gilt **Deutsch** als default.

Zusätzlich kann über `@gndo:type` angegeben werden, ob es sich um eine im späteren Normdatensatz sichtbare, d.h. öffentliche Anmerkung oder um eine interne Anmerkung handelt, die nicht in den Normdateneintrag übernommen wird.

```xml
<entity xml:id="max_muster">
   <dc:title>Mustermann, Max</dc:title>
   <skos:note>Eine fiktive Person, die als Platzhalter fungiert</skos:note>
   <skos:note gndo:type="internal">Dieser Eintrag hat als Beispiel 
      mittlerweise so einen "langen Bart". Ich denke er hat ausgedient.<skos:note>
</entity>
```

**Zusätzliche Ressourcen**:


- Das MARC 21 Feld 680 (Scope Note) im "[Cataloger's
                        Reference Shelf MARC 21 Format for Classification Data](https://www.itsmarc.com/crs/mergedprojects/conciscl/conciscl/idh_680_clas.htm)"
- Das Feld 680 (Benutzerhinweis) im [GND-Erfassungsleitfaden](https://wiki.dnb.de/download/attachments/50759357/680.pdf)

<a name="entity-biogr-histogr"></a>
#### Biographische und/oder historische Angaben zur Entität

Um ergänzende biographische, historische oder andere Angaben zu einer Entität in textlicher Form zu dokumentieren, dient das Element `gndo:biographicalOrHistoricalInformation`. Es ist *optional* und *wiederholbar*, sprich: Für eine Entität können beliebig viele dieser Angaben erstellt werden.

Zusätzliche lässt sich die verwendete Sprache der Angabe via ``@xml:lang`` eindeutig spezifizieren. Als default (sprich, falls kein `@xml:lang` gesetzt ist) gilt die Sprache der Angabe als **Deutsch**.

*Eine Anmerkung zur Form der Angabe*: Der Erfassungsleitfaden zum äquivalenten PICA3/MARC 21 Feld **[678](https://wiki.dnb.de/download/attachments/50759357/678.pdf)** (Biografische, historische und andere Angaben) empfielt einen "möglichst prägnant[en] und kurz[en]" Text, wobei die verwendete Sprache auf Deutsch festgelegt ist.

```xml
<place xml:id="goslar" gndo:uri="http://d-nb.info/gnd/4021643-3">
   <gndo:preferredName>Goslar</gndo:preferredName>
   <gndo:biographicalOrHistoricalInformation>Kreisstadt des Landkreises 
      Goslar, Niedersachsen</gndo:biographicalOrHistoricalInformation>
   <gndo:biographicalOrHistoricalInformation xml:lang="en">Historic town in 
      Lower Saxony, Germany and the administrative centre 
      of the district of Goslar.</gndo:biographicalOrHistoricalInformation>
</place>
```

<a name="dublicates-encoding"></a>
#### Dokumentation von Mehrfacheinträgen

Als entscheidendes Kriterium für einen Normdatendienst gilt die ein-eindeutige Identifizierbarkeit von Entitäten durch Identifier. Datendienste wie die GND arbeiten daher mit hohem Ressourceneinsatz daran, ihre Daten frei von Dubletten zu halten. Nichtsdestoweniger kann es vorkommen, dass im Rahmen der GND Mehrfacheinträge zu einer Entität vorhanden sind, die bisher noch nicht dedubliziert wurden.

entityXML ermöglicht die Dokumentation solcher **Mehrfacheinträgen** via `dublicateGndIdentifier`, hier am Beispiel eines Personeneintrags:

```xml
<person xml:id="edwald_albee" gndo:uri="https://d-nb.info/gnd/118501380">
   <dc:title>Albee, Edwald</dc:title>
   <dublicateGndIdentifier>185806961</dublicateGndIdentifier>
</person>
```

Der Normdatensatz, dessen HTTP-URI in `@gndo:uri` erhoben wird, gilt immer als **Haupteintrag** und die über `dubcliateGndIdentifier` erschlossenen GND-IDs beschreiben *dublikate Einträge*, die mit dem Haupteintrag zusammengeführt werden sollten. 

Um eine gewünschte Zusammenführung für den Eintrag anzugeben, wird der entsprechende Eintrag via `@agency` als `merge` gekennzeichnet:

```xml
<person xml:id="edwald_albee" gndo:uri="https://d-nb.info/gnd/118501380" agency="merge">
   <dc:title>Albee, Edwald</dc:title>
   <dublicateGndIdentifier>185806961</dublicateGndIdentifier>
</person>
```

In der Regel werden Dubletten, wenn sie im Rahmen der Dedublizierung erkannt wurden, innerhalb der GND zusammengeführt und auf einen entsprechenden Haupteintrag umgeleitet. Es kann dennoch vorkommen, dass die URL-Weiterleitung für einzelne Einträge nicht vorgenommen wurde und der entsprechende Eintrag einen Hinweis: "Status: Datensatz ist nicht mehr Bestandteil der Gemeinsamen Normdatei (GND)" verzeichnet.

<a name="entity-geographic-area"></a>
#### Geographischer Schwerpunkt einer Entität

Entitäten lassen sich geographisch einordnen bzw. bestimmen: Bei Personen kann man örtlich gebundene Lebensmittelpunkte oder Wirkungsorte beschreiben. Körperschaften und Bauwerke lassen sich mit Ländern und Ortschaften assozieren, genauso wie Werke der Literatur, Kunst und Wissenschaft usw.

Zur Dokumentation eines geographischen Schwerpunkts dient ``gndo:geographicAreaCode``. Um eine <span class="emph">maschienelle Verarbeitung</span> dieser Information zu gewährleisten, muss ein Ländercode nach ISO 3166 via `@gndo:term` angegeben werden. Die GND stellt hierfür das Vokabular "[Geographic Area Codes](https://d-nb.info/standards/vocab/gnd/geographic-area-code)" zur Verfügung:

```xml
<place xml:id="goslar_rathaus" gndo:uri="http://d-nb.info/gnd/4231845-2">
   <gndo:preferredName>Rathaus Goslar</gndo:preferredName>
   <gndo:geographicAreaCode gndo:term="https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DE-NI">
       Niedersachsen
   </gndo:geographicAreaCode>
   <!-- ... -->
</place>
```

Eine geographische Einordnung entspricht in der Regel realen Orten, Gebieten etc. Für Fällen, bei denen eine Entität geographisch einem fingierten Ort zugeordnet werden soll, wird der Deskriptor `https://d-nb.info/standards/vocab/gnd/geographic-area-code#XZ` verwendet.

```xml
<person xml:id="malachlabra" gndo:type="https://d-nb.info/standards/elementset/gnd#LiteraryOrLegendaryCharacter">
   <gndo:preferredName><gndo:personalName>Malachlabra</gndo:personalName></gndo:preferredName>
   <foaf:page>https://forgottenrealms.fandom.com/wiki/Malachlabra</foaf:page>
   <skos:note>Malachlabra ist ein fiktiver Charakter aus den D&amp;D: Forgotten Realms.</skos:note>
   <gndo:geographicAreaCode gndo:term="https://d-nb.info/standards/vocab/gnd/geographic-area-code#XZ">
      Avernus, Neun Höllen (Fiktiver Ort)
   </gndo:geographicAreaCode>
</person>
```

Sofern der geographische Schwerpunkt einer Entität unbekannt ist, dann kann dies über den Deskriptor `https://d-nb.info/standards/vocab/gnd/geographic-area-code#ZZ` verzeichnet werden.

**Zusätzliche Ressourcen**:


- Das Feld 043 (Ländercode) im [GND-Erfassungsleitfaden](https://wiki.dnb.de/download/attachments/50759357/043.pdf)

<a name="entity-title"></a>
#### Namens- bzw. Titelangaben

Eine Entität hat in der Regel einen Namen, durch den dann Bezug auf sie genommen werden kann. In entityXML werden drei Oberkategorien von Namen unterschieden: (1) Vorzugsbezeichnung, (2) Alternative Namensform bzw. Namensvariante und (3) (Ressourcen)titel.

Eine **Vorzugsbenennung** definiert den hauptsächlich gebräuchlichen, also den <span class="emph">bevorzugten Namen,</span> mit dem auf eine Entität bezug genommen wird. Hierfür wird das Element ``gndo:preferedName`` verwendet, wobei sich optional die verwendete Sprache mit `@xml:lang` spezifizieren lässt; default ist hier Deutsch:

```xml
<place xml:id="santiago-de-chile">
   <gndo:preferredName xml:lang="es">Santiago de Chile</gndo:preferredName>
</place>
```

**Namensvarianten** bzw. Alternative Namensformen werden via ``gndo:variantName`` erfasst. Hierbei handelt es sich um <span class="emph">zusätzliche Namensformen</span>, unter denen die Entität ebenfalls bekannt ist, sofern vorhanden. Das können Spitz- oder Kosenamen sein, genauso wie Rufnamen. Wie bei `gndo:preferedName` kann die verwendete Sprache einer Namensvariante durch `@xml:lang` spezifiziert werden:

```xml
<place xml:id="santiago-de-chile">
   <!-- ... -->
   <gndo:variantName>Santiago</gndo:variantName>
   <gndo:variantName>Sanhatten</gndo:variantName>
   <skos:note>"Sanhatten", weil Santiago de Chile vom Cerro San Cristóbal wie Manhatten aussieht.</skos:note>
</place>
```

Vorzugsbenennungen und Namensvarianten werden bei jedem Entitätstyp leicht unterschiedlich gehandhabt, sowohl, was mögliche Inhalte als auch die Validierung betrifft. Dementsprechend lohnt sich ein Blick an die entsprechenden Stellen in diesem Handbuch:


- [Erschließung von Namen bei Personen](#person-name)

<a name="docs_d14e1773"></a>
##### Titel als Fallback

In entityXML gibt es noch die Möglichkeit einen **Fallback Titel** für eine Entität mittels `dc:title` zu vergeben, falls aus irgendwelchen Gründen keine Vorzugsbenennung oder Namensvariante dokumentiert werden soll. Mögliche Fälle wären z.B. Einträge, die bereits in der GND vorhanden sind, und für die man in seinen eigenen Einträgen keine eigene Namensformen anführen möchte:

```xml
<place xml:id="goslar_rathaus" gndo:uri="http://d-nb.info/gnd/4231845-2">
   <dc:title>Rathaus Goslar</dc:title>
</place>
```

<a name="entity-source"></a>
#### Quellenangabe

Das Wissen um eine Entität ist das Ergebniss von Recherche und diese beruht wiederum auf der Interpretation von Quellen. Angaben zu Quellen, aus denen die Informationsinhalte eines Entitätseintrages (z.B. eine bevorzugte Namensform oder biographische bzw. historische Informationen) zusammengestellt werden, werden via `source` dokumentiert. Hierbei handelt es sich bei bibliographischen Quellen für gewöhnlich um ein Kurzzitat der Quelle oder, falls es sich um eine nicht-bibliographische Quelle oder eine Onlineressource handelt, um einen prägnanten Begriff zur Bestimmung der Art der Quelle.

Onlineressourcen werden mit ihrem URL - optimaler Weise ein Permalink - via `@url` dokumentiert:

```xml
<person xml:id="nguyen-kim-mai-thi" gndo:uri="https://d-nb.info/gnd/1135801363">
   <gndo:variantName>
      <gndo:forename>Mai-Thi</gndo:forename>
      <gndo:surname>Leiendecker</gndo:surname>
   </gndo:variantName>
   <source url="https://de.wikipedia.org/wiki/Mai_Thi_Nguyen-Kim">Wikipedia</source>
</person>
```

```xml
<person xml:id="a_v_jungenfeld">
   <gndo:preferredName>
      <gndo:surname>Gedult von Jungenfeld</gndo:surname>
      <gndo:forename>Arnold Ferdinand</gndo:forename>
   </gndo:preferredName>
   <source url="http://http://scans.hebis.de/22/23/04/22230490_exl-1.jpg">Provenienzmerkmal</source>
   <source url="https://de.wikipedia.org/wiki/Arnold_von_Jungenfeld">Wikipedia</source>
</person>
```

Wenn es sich bei einer Quelle um eine Monographie handelt, dann sollte das Kurzzitat mindestens aus dem Verfassernamen mit Vornamen (auch abgekürzt), Titel und Jahr bestehen.

Bei unselbständigen Veröffentlichungen ist der Mindeststandard die Angabe von Verfassername mit Vorname (auch abgekürzt), Titel, Jahr, Titel der Zeitschrift/Zeitung mit Jahrgang - und/oder Bandzählung sowie Seitenzahl.

**Zusätzliche Ressourcen**:


- Das Feld 670 (Quellenangabe) im [GND-Erfassungsleitfaden](https://wiki.dnb.de/download/attachments/50759357/670.pdf)

<a name="entity-subject-categories"></a>
#### Sachgruppen

[TODO]

<a name="entity-same-as"></a>
#### Gleiche Entität in anderer Normdatei oder anderem Datendienst

Entitäten können bereits in anderen (Norm)datendiensten verzeichnet sein (z.B. [Library of Congress Name Authority
                     File](https://id.loc.gov/authorities/names.html), [VIAF](https://viaf.org/), [Deutschen Digitalen Bibliothek](https://www.deutsche-digitale-bibliothek.de/), [Wikidata](https://www.wikidata.org/) etc.) 

Einträge, die in entityXML angelegt werden, lassen sich mit solchen "externen" Einträgen anderer Datendienste mittels ``owl:sameAs`` in Verbindung bringen. Dies dient grundsätzlich der eindeutigen Identifizierung der beschriebenen Entität:

```xml
<person xml:id="nguyen-kim-mai-thi" gndo:uri="https://d-nb.info/gnd/1135801363">
   <gndo:preferredName>
      <gndo:surname>Nguyen-Kim</gndo:surname>, <gndo:forename>Mai Thi</gndo:forename>
   </gndo:preferredName>
   <owl:sameAs>https://orcid.org/0000-0003-4787-0508</owl:sameAs>
   <owl:sameAs>https://viaf.org/viaf/3094149844949202960003</owl:sameAs>
   <owl:sameAs>https://www.wikidata.org/wiki/Q29169693</owl:sameAs>
   <owl:sameAs>https://id.loc.gov/authorities/names/n2019011012</owl:sameAs>
</person>
```

Darüberhinaus lassen sich so auch andere Informationsprozesse anschließen, um bspw. auf Informationen, die durch die entsprechenden Datendienste zur Verfügung gestellt werden, zuzugreifen.

<a name="entity-links"></a>
#### Sonstige Webressourcen

Links auf sonstige Onlineressourcen, die zusätzliche Informationen über die beschriebene Entität enthalten, können mit Hilfe von ``ref`` dokumentiert werden. Hierbei stehen zwei Auszeichnungsmöglichkeit zur Auswahl:

**(1)** Der Link zur entsprechenden Ressource wird direkt als Text innhalb von `ref` dokumentiert.

**(2)** Der Link zur entsprechenden Ressource wird im `@target` Attribut von `ref` dokumentiert und innerhalb von `ref` wird ein Stellvertretertext angegeben.

```xml
 <!-- Ein Beispiel mit 'ref/@target' und Stellvertretertext -->
 
 <person xml:id="Malachlabra" gndo:type="https://d-nb.info/standards/elementset/gnd#LiteraryOrLegendaryCharacter">
   <gndo:preferredName>
      <gndo:personalName>Malachlabra</gndo:personalName>
   </gndo:preferredName>
   <skos:note>Malachlabra ist ein Charakter aus den D&amp;D: Forgotten Realms.</skos:note>
   <foaf:page>https://forgottenrealms.fandom.com/wiki/Malachlabra</foaf:page>
   <ref target="https://www.reddit.com/r/DnD/comments/806uow/does_anyone_know_anything_about_malachlabra/">
      Reddit: Does anyone know anything about Malachlabra?
   </ref>
</person>
```

```xml
<!-- Das gleiche Beispiel ohne @target -->

<ref>https://www.reddit.com/r/DnD/comments/806uow/does_anyone_know_anything_about_malachlabra/</ref>
```

Es stellt sich vielleicht die Frage, warum man neben ``owl:sameAs``, ``source`` (und ``gndo:homepage`` bzw. ``foaf:page``, die an späterer Stelle in diesem Handbuch beschrieben werden), noch ein weiteres Element benötigt, um zusätzliche Links zu dokumentieren? Zum Einen, weil die eben anzitierten Elemente recht spezifische Sachverhalte abbilden, die wesentlich konkreter sind als "Eine Ressource, die zusätzliche Informationen über die beschriebene Entität enthält". Zum Anderen – und das ist nicht ganz ernst gemeint – freut sich (fast) Jede*r über ein "Sonstiges" Feld.

Ernsthaft: Bei `ref` handelt es sich einfach um eine Möglichkeit, Referenzen auf Online-Ressourcen zu dokumentieren, die nicht so richtig unter die Definitionen von `owl:sameAs`, `source`, `gndo:homepage` oder `foaf:page` passen wollen – nicht mehr und nicht weniger.


> **Keine Entsprechung in der GND**  
> Für diese Information gibt es keine Entsprechung in der GND! Daher wird sie bei der Erstellung eines neuen Normdateneintrags oder beim Update eines Bestehenden ignoriert.



<a name="docs_d14e1928"></a>
### Spezifische Entitätsklassen

<a name="person-record"></a>
#### Personen

Ein **Personeneintrag** (Personrecord) enthält Informationen zu *realen* oder *fiktiven Personen*. entityXML definiert ein festes Set an Eigenschaften aus dem GNDO-Namensraum, um Informationen zu dokumentieren, die von der GND unterstützt werden. Zusätzlich ist es möglich weitere Eigenschaften in einem <span class="emph">eigenen Namensraum</span> (Elemente und Attribute in sog. [custom namespaces](#custom-namespaces)) in einem Personeneintrag zu dokumentieren.

Ein Personeneintrag wird durch das Element `person` repräsentiert und benötigt eine **`@xml:id`**.

Sofern für die Person bereits ein Normdatensatz in der GND vorhanden ist, wird der entsprechende GND HTTP-URI in `@gndo:uri` dokumentiert. Falls nicht, ist die Angabe einer <span class="emph">Vorzugsbenennung</span> via `gndo:preferredName` oder einer <span class="emph">Namensvariante</span> via `gndo:variantName` **verpflichtend**.

```xml
<person xml:id="mmuster">
    <gndo:preferredName><!-- ... --></gndo:preferredName>
</person>
```

<a name="docs_d14e1976"></a>
##### Unterscheidung von Personentypen

Entitäten, die in `person` erschlossen werden, gelten per default als [gndo:DifferentiatedPerson](https://d-nb.info/standards/elementset/gnd#id-07e558e11b6629c85c4cf0312a8d9acc), und können via ``@gndo:type`` als Unterklasse einer gndo:DifferentiatedPerson weiter spezifiziert werden. Sofern es sich z.B. nicht um eine reale Person, sondern um eine fingierte Person bzw. eine Figur aus einem fiktionalen Werk handelt, dann würde der Eintrag entsprechend über `@gndo:type` mit '[https://d-nb.info/standards/elementset/gnd#LiteraryOrLegendaryCharacter](https://d-nb.info/standards/elementset/gnd#LiteraryOrLegendaryCharacter)' spezifiziert werden:

```xml
<person xml:id="malachlabra" gndo:type="https://d-nb.info/standards/elementset/gnd#LiteraryOrLegendaryCharacter">
   <gndo:preferredName><gndo:personalName>Malachlabra</gndo:personalName></gndo:preferredName>
   <gndo:biographicalOrHistoricalInformation xml:lang="en">Malachlabra was an archfiend and the daughter of Dispater in the late 14th century DR. She was ambushed by the Simbul in 1372 DR.</gndo:biographicalOrHistoricalInformation>
   <skos:note>Malachlabra ist ein Charakter aus den D&amp;D: Forgotten Realms.</skos:note>
</person>
```

<a name="person-name"></a>
##### Erschließung von Vorzugsbenennung und Namensvarianten

Zur Erschließung von Namensformen für Personen dienen folgende Elemente:

- [`gndo:preferredName`](person-record-preferredName), **Vorzugsbenennung (Person)**: Die bevorzugte Namensform zur Denomination einer Person.
- [`gndo:variantName`](person-record-variantName), **Variante Namensform (Person)**: Eine alternative Namensform, unter der eine Person ebenfalls bekannt ist.


Für Personeinträge ist die Angabe einer <span class="emph">Vorzugsbenennung</span>**obligatorisch**, sofern die Person nicht bereits über einen GND-URI identifiziert wurde. Die Angabe von (beliebig vielen) <span class="emph">Namensvarianten</span> ist optional.

Eine Vorzugs- bzw. Alternativbenennung kann auf *zwei alternative Arten* angegeben werden:

(1) unter Angabe **verschiedener Namensbestandteile**

- **Vorname** [`gndo:forename`](#d17e3434) (*verpflichtend*)  
  "Ein oder mehrere Vornamen einer Person."
- **Namenspräfix** [`gndo:prefix`](#d17e4237) (*optional*)  
  "Namensteil, der dem Namen vorangestellt sind (z.B. Adelskennzeichnungen, "von", "zu" etc.)."
- **Nachname** [`gndo:surname`](#d17e4656) (*verpflichtend*)  
  "Der Nach- oder Familienname, unter dem eine Person bekannt ist."
- **Zählung** [`gndo:counting`](#d17e3024) (*optional*)  
  "Eine Zählung als Namensbestandteil (z.B. in Ramses **II** oder Sethos **I**)."
- **Namenszusatz** [`gndo:nameAddition`](#d17e3818) (*optional und wiederholbar*)  
  "Zusätzliches Element des Namens, unter dem eine Person bekannt ist, z.B. "Graf von Wallmoden"."
- **Beiname, Gattungsname, Titulatur, Territorium** [`gndo:epithetGenericNameTitleOrTerritory`](#d17e3307) (*optional und wiederholbar*)  
  "Beiname einer Person, bei dem es sich um ein Epitheton, Titel oder eine Ortsassoziation handelt."



(2) unter Angabe eines **persönlichen Namens**

- **Persönlicher Name** [`gndo:personalName`](#d17e4075) (*verpflichtend*)  
  "Ein Name, unter dem die Person gemeinhin bekannt ist. Ein persönlicher Name wird dann angegeben, wenn die Person nicht durch einen Vor- oder Nachname benannt wird, z.B. "Aristoteles""



```xml
<!-- Beispiel Vorzugsbenennung mit Vor- und Nachnamen-->
<gndo:preferredName>
    <gndo:forename>Ludolph Christian</gndo:forename>
    <gndo:surname> Meier</gndo:surname>
</gndo:preferredName>

<!-- Beispiel Vorzugsbenennung mit persönlichem Namen-->
<gndo:variantName>
    <gndo:personalName>Aristoteles</gndo:personalName>
</gndo:variantName>

<!-- Beispiel Namensvariante mit präfigiertem "von"-->
<gndo:variantName>
    <gndo:forename>Georg</gndo:forename>
    <gndo:prefix>von</gndo:prefix>
    <gndo:surname>Mejern</gndo:surname>
</gndo:variantName>
```

<a name="person-geographical-area-code"></a>
##### Geographische Zuordnung

Die geographische Zuordnung von Personen zu einem Ort ist in der GND verpflichtend, so auch in entityXML! Wie [weiter oben](#entity-geographic-area) beschrieben wird hierfür `gndo:geographicAreaCode` verwendet, inkl. der Angabe eines Terms via `@gndo:term` aus dem Vokabular "[Geographic Area Codes](https://d-nb.info/standards/vocab/gnd/geographic-area-code)": 

<a name="person-dates"></a>
##### Lebens- und Wirkungsdaten

Lebens- und Wirkungsdaten sind wichtige Identifikationsmerkmale einer Person. Sie lassen sich mittels folgender Elemente dokumentieren: 

- [`gndo:dateOfBirth`](person-record-dateOfBirth), **Geburtsdatum (Person)**: Das Geburtsdatum der Person.
- [`gndo:dateOfDeath`](person-record-dateOfDeath), **Sterbedatum (Person)**: Das Sterbedatum der Person.
- [`gndo:periodOfActivity`](prop-periodOfActivity), **Wirkungsdaten**: Zeitraum, innerhalb dessen die beschriebene Entität aktiv war. Wird nur dann erschlossen, wenn die Lebensdaten unbekannt sind.


Die Elemente `gndo:dateOfBirth` und `gndo:dateOfDeath` markieren einen <span class="emph">Zeitpunkt</span>, dessen Datum als [ISO 8601](https://www.iso.org/iso-8601-date-and-time-format.html), also in der Form **YYYY-MM-DD** via ``@iso-date`` dokumentiert wird.

`gndo:periodeOfActivity` beschreibt hingegen einen <span class="emph">Zeitraum</span> und verwendet dementsprechend je ein Attribute zur Beschreibung eines Anfangs- (``@iso-from``) und eines Enddatums (``@iso-to``), die ebenfalls dem Format ISO 8601 entsprechen müssen. Die Angabe von midestens einem Start- oder einem Enddatum ist hier verpflichtend. Ein Wirkungszeitraum wird nur dann erschlossen, wenn kein Geburts- und Sterbedatum für die beschriebene Person bekannt ist.

Zusätzlich zu den eher maschienenlesbaren Datumsangaben in den Attributen, kann eine menschenlesbare Repräsentation des Datums als Text innerhalb des jeweiligen Datums- oder Zeitraumelements angegeben werden.

```xml
<gndo:dateOfBirth iso-date="1980-01-11">11 Januar 1985</gndo:dateOfBirth>
<gndo:dateOfDeath iso-date="2020-01-11">11.01.2020</gndo:dateOfDeath>
```

```xml
<gndo:periodOfActivity iso-from="1940" iso-to="2000">1940-2000</gndo:periodOfActivity>
```

<a name="person-places"></a>
##### Lebens- und Wirkungsorte

Lebens- und Wirkungsorte einer Person werden durch folgende Elemente erschlossen: 

- [`gndo:placeOfBirth`](person-record-placeOfBirth), **Geburtsort (Person)**: Der Geburtsort der Person. Über `@gndo:ref` kann ein Deskriptor (GND-URI) aus der GND erfasst werden. Steht kein GND-URI zur Verfügung erfolgt die Erfassung des Ortes als Freitext im Element.
- [`gndo:placeOfDeath`](person-record-placeOfDeath), **Sterbeort (Person)**: Der Sterbeort der Person. Über `@gndo:ref` kann ein Deskriptor (GND-URI) aus der GND erfasst werden. Steht kein GND-URI zur Verfügung erfolgt die Erfassung des Ortes als Freitext im Element.


Der Ort wird als Text innerhalb des jeweiligen Ortelements dokumentiert. Es wird empfohlen zusätzlich den entsprechenden GND-URI des Orts via `@gndo:ref` anzugeben.

```xml
<gndo:placeOfBirth gndo:ref="https://d-nb.info/gnd/4007666-0">Bonn</gndo:placeOfBirth>
<gndo:placeOfDeath gndo:ref="https://d-nb.info/gnd/4023118-5">Hamburg</gndo:placeOfDeath>
```

<a name="person-gender"></a>
##### Angaben zum Geschlecht

Angaben zum Geschlecht einer Person können via `gndo:gender` dokumentiert werden. Mittels `@gndo:type` wird ein Term oder mehrere Terme aus dem GND Vokabular "Gender" [https://d-nb.info/standards/vocab/gnd/gender#](https://d-nb.info/standards/vocab/gnd/gender#) dokumentiert. Eine zusätzliche Beschreibung kann zusätzlich als Text innerhalb des Elements verzeichnet werden. 

```xml
<gndo:gender gndo:term="https://d-nb.info/standards/vocab/gnd/gender#female">weiblich</gndo:gender>
```

```xml
<gndo:gender gndo:term="https://d-nb.info/standards/vocab/gnd/gender#female https://d-nb.info/standards/vocab/gnd/gender#male">nichtbinär</gndo:gender>
```

<a name="person-profession"></a>
##### Berufe und Tätigkeiten

Berufe und/oder Tätigkeitsfelder einer Person lassen sich mittels `gndo:professionOrOccupation` dokumentieren. Verpflichtend ist die zusätzliche Angabe eines Sachbegriffs aus der GND über `@gndo:ref`, und zwar als URI:

```xml
<gndo:professionOrOccupation gndo:ref="https://d-nb.info/gnd/4025098-2">Historiker</gndo:professionOrOccupation>
```

Berufe und Tätigkeiten lassen sich optional via `@type` als Hauptbeschäftigung, sprich als 'significant' spezifizieren:

```xml
<gndo:professionOrOccupation gndo:ref="https://d-nb.info/gnd/4025098-2">
   Historiker
</gndo:professionOrOccupation>
<gndo:professionOrOccupation gndo:type="significant" gndo:ref="https://d-nb.info/gnd/4161681-9">
   Informationswissenschaftler
</gndo:professionOrOccupation>
```

<a name="person-publications"></a>
##### Titelangaben/Veröffentlichungen

Über `gndo:publication` lassen sich Veröffentlichungen dokumentieren, die mit der beschriebenen Person in Zusammenhang stehen. Das kingt unspezifisch und genau so ist es auch gedacht: Veröffentlichungen bzw. "Titelangaben" gelten in der GND vorallem als weiteres <span class="emph">Distinguierungssmerkmal</span>, um die Person von Anderen zu unterscheiden, sprich: Die auf diese Weise verzeichneten Titel belegen einerseits die Existenz der Person als Entität und enthalten andererseits weitere Informationen (z.B. Vorzugsbenennungen, Kontextinformationen etc.), um im Zweifelsfall homonyme Personen von einander unterscheiden zu können.

In `gndo:publication` können dementsprechend Veröffentlichungen dokumentiert werden, die …


- … von der Person selber stammen (<span class="emph">autorschaftlicher Berzug</span>)
- … von der Person herausgegeben wurden (<span class="emph">herausgeberschaftlicher Bezug</span>)
- … von der Person handeln (<span class="emph">thematischer Bezug</span>)

Diese drei unterschiedlichen Bezüge werden im optionalen `@role` Attribut in `gndo:publication` erfasst, das folgenden Wertebereich vordefiniert: "**auhor**" (autorschaftlicher Bezug), "**editor**" (herausgeberschaftlicher Bezug), "**about**" (thematischer Bezug). Sofern `@role` nicht explizit ausgewiesen wird, gilt der Bezug als autorschaftlich, sprich: "author" gilt als *default*.

Innerhalb von `gndo:publication` gibt es ein Basisset von Elementen, die verwendet werden können, um ein grundständiges bibliographisches Zitat zu strukturieren:


- **Autor, erste Verfasserschaft** [`gndo:firstAuthor`](#d17e3403) (*optional und wiederholbar*)  
  "Die Person oder Körperschaft, die verantwortlich für die primäre Verfasserschaft an einem Werk oder einer Veröffentlichung zeichnet."
- **Titel** [`title`](#d17e5317) (*verpflichtend*)  
  "Ein Titel für das Informationsobjekt, das in diesem Kontext beschrieben wird."
- **Titelzusatz** [`add`](#d17e2202) (*optional*)  
  "Zusätze zum Titel im Rahmen der bibliographischen Angabe."
- **Datum** [`date`](#d17e2619) (*verpflichtend*)  
  "Ein Zeitpunkt oder Zeitraum, der mit einem Ereignis im Lebenszyklus der beschriebenen Ressource in Zusammenhang steht."


> **Warum nur ein 'Basisset'?**  
> entityXML implementiert (bisher) nur das Nötigste an Elementen, um eine bibliographsche Angabe zu beschreiben, die sich später in einen MARC-Datensatz konvertieren lässt. An dieser Stelle soll nicht die Ausführlichkeit eines Bibliothekskatalogs abgebildet werden, sondern lediglich die Informationseinheiten, die als eigenständige Felder in den MARC Feldern 670 und 672 benötigt werden. 


```xml
<person xml:id="uwe_sikora" gndo:uri="https://d-nb.info/gnd/1155094360">
   <gndo:preferredName>
      <gndo:forename>Uwe</gndo:forename>
      <gndo:surname>Sikora</gndo:surname>
   </gndo:preferredName>
   <gndo:publication>
      <title>entityXML Handbuch</title>, 
      <date>2023</date>
   </gndo:publication>
</person>
```

Sofern Titelangaben verzeichnet werden, die von der beschreibenenen Person handeln, wird die Angabe eines <span class="emph">Autor\*innennamens</span> zur Pflicht:

```xml
<person xml:id="wilhelm_meister" gndo:type="https://d-nb.info/standards/elementset/gnd#LiteraryOrLegendaryCharacter">
   <gndo:preferredName>
      <gndo:surname>Meister</gndo:surname>
      <gndo:forename>Wilhelm</gndo:forename>
   </gndo:preferredName>
   <gndo:publication role="about" gndo:ref="https://d-nb.info/gnd/4123299-9">
      <gndo:firstAuthor>Goethe, Johann Wolfgang von</gndo:firstAuthor>
      <title>Wilhelm-Meister-Romane</title>
      <date>1776</date>
   </gndo:publication>
</person>
```

<a name="docs_d14e2245"></a>
##### Beispiel 1: Maxima Musterfrau

Wie alle Einträge kann ein Personeneintrag in eher prosaischer Form verfasst werden:

```xml
<person xml:id="maxima_musterfrau" agency="create">
    <gndo:preferredName><gndo:surname>Musterfrau</gndo:surname>,<gndo:forename>Maxima</gndo:forename>  
    </gndo:preferredName>, auch bekannt als <gndo:variantName><gndo:personalName>Maxima</gndo:personalName>
    </gndo:variantName>, geboren <gndo:dateOfBirth iso-date="1990-05-25">25.05.1990</gndo:dateOfBirth> in 
    <gndo:placeOfBirth gndo:ref="https://d-nb.info/gnd/4592817-4">Wuhlheide (Berlin)</gndo:placeOfBirth> ist 
    die Tochter von Max Mustermann. Ein Eintrag in der GND könnte <gndo:gndIdentifier cert="low">118500775</gndo:gndIdentifier> 
    sein. Man kann aufjedenfall sagen, dass sie in
    <gndo:placeOfActivity>Berlin</gndo:placeOfActivity> aktiv ist und zwar als bekannte
    <gndo:professionOrOccupation>Aktivistin</gndo:professionOrOccupation> der Initiative "Normdaten für alle aber 
    ordentlich!". Sie arbeitet als freiberufliche 
    <gndo:professionOrOccupation>Bibliothekarin</gndo:professionOrOccupation> in ihrer frei
    zugänglichen Privatbibliothek. Ihre Homepage <foaf:page>http://rettet-normdaten-ev.org/</foaf:page> bietet 
    zahlreiche Informationen zu Normdaten. <skos:note>Dieser Eintrag ist ein Test, unterschiedliche Aspekte der 
        Validierung und der Inhaltsmodelle abzuprüfen.</skos:note>
    <revision status="opened">
        <change when="2022-05-10" who="US">Berlin URI bei placeOfActivity raussuchen und nachtragen.</change>
        <change when="2022-04-06" who="US">Die GND-ID 118500775 identifiziert nicht Maxima Musterfrau sondern Theodor W. Adorno.</change>
        <change when="2022-03-31" who="MM">Eintrag angelegt. Bitte in die GND überführen.</change>
    </revision>
</person>
```

<a name="docs_d14e2255"></a>
##### Beispiel 2: Ernest Chevalier

Hier ein Beispiel, dass im Gegensatz zur prosaischen Form eine eher strukturierte Form eines Eintrages verdeutlicht:

```xml
<person xml:id="ernest_chevalier" agency="create">
   <gndo:preferredName>
      <gndo:surname>Chevalier</gndo:surname>
      <gndo:forename>Ernest</gndo:forename>
   </gndo:preferredName>
   <gndo:dateOfBirth iso-date="1820-08-14">14.08.1820</gndo:dateOfBirth>
   <gndo:dateOfDeath iso-date="1887-12-05">05.12.1887</gndo:dateOfDeath>
   <gndo:placeOfBirth>Villers-en-Vexin</gndo:placeOfBirth>
   <gndo:placeOfDeath gndo:ref="https://d-nb.info/gnd/4044660-8">Paris</gndo:placeOfDeath>
   <gndo:gender gndo:term="https://d-nb.info/standards/vocab/gnd/gender#male">männlich</gndo:gender>
   <gndo:geographicAreaCode gndo:term="https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-FR">Frankreich</gndo:geographicAreaCode>
   <gndo:professionOrOccupation gndo:ref="https://d-nb.info/gnd/4046517-2" gndo:type="significant">Politiker</gndo:professionOrOccupation>
   <foaf:page>https://fr.wikipedia.org/wiki/Ernest_Chevalier</foaf:page>
   <source>https://fr.wikipedia.org/wiki/Ernest_Chevalier</source>
   <revision status="opened">
      <change when="2023-01-21" who="MM">Bitte in die GND aufnehmen</change>
   </revision>
</person>
```

<a name="docs_d14e2265"></a>
##### Modell: Personeneintrag

Das **Modell** für einen Personeneintrag sieht wie folgt aus:

```xml
<content>
   <attribute name="gndo:type"/>
   <attribute name="xml:id" required="true"/>
   <attribute name="gndo:uri"/>
   <attribute name="agency"/>
   <attribute name="enrich"/>
   <anyOrder>
      <element name="foaf:page" repeatable="true"/>
      <element name="gndo:academicDegree"/>
      <element name="gndo:affiliation" repeatable="true"/>
      <choice>
         <group>
            <element name="gndo:dateOfBirth"/>
            <element name="gndo:dateOfDeath"/>
         </group>
         <element name="gndo:periodOfActivity"/>
      </choice>
      <element name="gndo:fieldOfStudy" repeatable="true"/>
      <element name="gndo:functionOrRole" repeatable="true"/>
      <element name="gndo:gender"/>
      <element name="gndo:placeOfActivity" repeatable="true"/>
      <element name="gndo:placeOfBirth"/>
      <element name="gndo:placeOfDeath"/>
      <element name="gndo:preferredName"/>
      <element name="gndo:professionOrOccupation" repeatable="true"/>
      <element name="gndo:publication" repeatable="true"/>
      <element name="gndo:pseudonym" repeatable="true"/>
      <element name="gndo:titleOfNobility" repeatable="true"/>
      <element name="gndo:variantName" repeatable="true"/>
      <element name="dc:title"/>
      <element name="gndo:biographicalOrHistoricalInformation" repeatable="true"/>
      <element name="dublicateGndIdentifier" repeatable="true"/>
      <element name="gndo:geographicAreaCode" repeatable="true"/>
      <element name="gndo:gndIdentifier" repeatable="true"/>
      <element name="gndo:gndSubjectCategory" repeatable="true"/>
      <element name="gndo:languageCode" repeatable="true"/>
      <element name="gndo:relatesTo" repeatable="true"/>
      <element name="ref" repeatable="true"/>
      <element name="owl:sameAs" repeatable="true"/>
      <element name="skos:note" repeatable="true"/>
      <element name="source" repeatable="true"/>
      <anyElement type="narrow" repeatable="true"/>
      <text/>
   </anyOrder>
   <element name="revision"/>
</content>
```


**[`@gndo:type`](#person-types)**  
(*Personentyp*): Typisierung der dokumentierten Person (z.B. als fiktive Person, Gott oder royale Person). Wenn kein `@gndo:type` vergeben wird, gilt die Person als [gndo:DifferentiatedPerson](https://d-nb.info/standards/elementset/gnd#id-07e558e11b6629c85c4cf0312a8d9acc).
  **Möglicher Wertebereich**:  


 - '*https://d-nb.info/standards/elementset/gnd#CollectivePseudonym*' : (Sammelpseudonym) [GNDO](https://d-nb.info/standards/elementset/gnd#id-e1a0c1b1bb1f0c6d862c101153127efb)
 - '*https://d-nb.info/standards/elementset/gnd#Gods*' : (Götter) [GNDO](https://d-nb.info/standards/elementset/gnd#id-4c3eb377f41e2e358d95fd66e85531f6)
 - '*https://d-nb.info/standards/elementset/gnd#LiteraryOrLegendaryCharacter*' : (Literarische oder Sagengestalt) [GNDO](https://d-nb.info/standards/elementset/gnd#id-561f9122a59eb9f11ca9ac9fbbb41015)
 - '*https://d-nb.info/standards/elementset/gnd#Pseudonym*' : (Pseudonym) [GNDO](https://d-nb.info/standards/elementset/gnd#id-4a1b77ecd57d7fe7074050aa7fddef3e)
 - '*https://d-nb.info/standards/elementset/gnd#RoyalOrMemberOfARoyalHouse*' : (Regierender Fürst oder Mitglied eines regierenden Fürstenhauses) [GNDO](https://d-nb.info/standards/elementset/gnd#id-48fef5513dcda9669ac3250908b3fc0e)
 - '*https://d-nb.info/standards/elementset/gnd#Spirits*' : (Geister) [GNDO](https://d-nb.info/standards/elementset/gnd#id-a53c2a79950d8502724d1e1a31ab19cb)



**[`@xml:id`](#d17e5720)**  ***ID***  
(*Record ID*): Angabe eines Identifiers zur Identifikation eines Entitäteneintrags innerhalb der Datensammlung. Die Angabe einer ID via `@xml:id` ist verpflichtend!

**[`@gndo:uri`](#d17e5417)**  ***anyURI***  
(*GND-URI*): Angabe des GND-HTTP URIs der beschriebenen Entität in der GND, sofern vorhanden.(http|https)://d-nb.info/gnd/(.+)

**[`@agency`](#attr.record.agency)**  
(*Agenturanfrage*): Anfrage an die Agentur, welche Aktion in Hinblick auf den Eintrag durchgeführt werden soll.
  **Möglicher Wertebereich**:  


 - '*create*' : (Eintrag in der GND neu anlegen) Der Eintrag soll als Normdatensatz neu in der GND angelegt werden.
 - '*update*' : (Normdatensatz mit Daten aus dem Eintrag erweitern) Der Normdatensatz soll durch neue Informationen angereichert werden.
 - '*merge*' : (Normdatensätze zusammenführen) Dubletten, die in dem Eintrag dokumentiert werden, sollen mit dem Normdatensatz, der durch den GND-URI des Eintrags (`@gndo:uri`) identifiziert ist, zusammengeführt werden.
 - '*ignore*' : (Eintrag ingonieren) Der Eintrag wird von der Agentur nicht bearbeitet.



**[`@enrich`](#d17e5700)**  ***boolean***  
(*Anreicherung*): Der Record soll mit Daten eines externen Datendienstes angereichert werden. Hierfür wird ein zusätzliches Konversionsscript benötigt, dass die Konversionsroutinen zur verfügung stellt, um den Record anzureichern!

**[`foaf:page`](#elem.foaf.page)**  
(*Webseite*): URL eines Dokuments mit näheren Informationen über die beschriebene Entität (z.B. Homepage, Wikipedia Seite etc.).

**[`gndo:academicDegree`](#d17e2902)**  
(*Akademischer Grad*): Angaben zum akademischen Grad der Person, z.B. "Dr. jur.".

**[`gndo:affiliation`](#d17e2929)**  
(*Zugehörigkeit*): Die Person oder Körperschaft ist zugehörig zu einer Körperschaft, oder ist mit einem Ort oder einem Event verbunden.

**[`gndo:dateOfBirth`](#person-record-dateOfBirth)**  
(*Geburtsdatum (Person)*): Das Geburtsdatum der Person.

**[`gndo:dateOfDeath`](#person-record-dateOfDeath)**  
(*Sterbedatum (Person)*): Das Sterbedatum der Person.

**[`gndo:periodOfActivity`](#prop-periodOfActivity)**  
(*Wirkungsdaten*): Zeitraum, innerhalb dessen die beschriebene Entität aktiv war. Wird nur dann erschlossen, wenn die Lebensdaten unbekannt sind.

**[`gndo:fieldOfStudy`](#d17e3363)**  
(*Studienfach*): Das Studienfach bzw. -gebiet einer Person. Über `@gndo:ref` kann ein entsprechendes Schlagwort aus der GND mit erfasst werden.

**[`gndo:functionOrRole`](#d17e3460)**  
(*Funktion oder RolleFunction or role*): 

**[`gndo:gender`](#d17e3491)**  
(*Geschlecht*): Angaben zum Geschlecht einer Person.

**[`gndo:placeOfActivity`](#person-record-placeOfActivity)**  
(*Wirkungsort (Person)*): Ein Wirkungsort, an dem die Person gewirkt oder gelebt (o.Ä.) hat. Über `@gndo:ref` kann ein Deskriptor (GND-URI) aus der GND erfasst werden. Steht kein GND-URI zur Verfügung erfolgt die Erfassung des Ortes als Freitext im Element.

**[`gndo:placeOfBirth`](#person-record-placeOfBirth)**  
(*Geburtsort (Person)*): Der Geburtsort der Person. Über `@gndo:ref` kann ein Deskriptor (GND-URI) aus der GND erfasst werden. Steht kein GND-URI zur Verfügung erfolgt die Erfassung des Ortes als Freitext im Element.

**[`gndo:placeOfDeath`](#person-record-placeOfDeath)**  
(*Sterbeort (Person)*): Der Sterbeort der Person. Über `@gndo:ref` kann ein Deskriptor (GND-URI) aus der GND erfasst werden. Steht kein GND-URI zur Verfügung erfolgt die Erfassung des Ortes als Freitext im Element.

**[`gndo:preferredName`](#person-record-preferredName)**  
(*Vorzugsbenennung (Person)*): Die bevorzugte Namensform zur Denomination einer Person.

**[`gndo:professionOrOccupation`](#d17e4262)**  
(*Beruf oder Beschäftigung*): Angabe zum Beruf, Tätigkeitsbereich o.Ä., der dokumentierten Person. Zulässige Deskriptoren stammen aus der GND-Systematik und entprechen GND-URIs von Sachbegriffen, z.B. https://d-nb.info/gnd/4168391-2 ("Lyriker").

**[`gndo:publication`](#d17e4368)**  
(*Publikation*): Eine Publikation, die mit der Entität in Zusammenhang steht. Es kann sich hierbei (1) um eigene Titel der Entität (Person), (2) um Titel, die von der Entität (Person) herausgegeben worden sind und auch (3) um Titel über die Entität handeln.

**[`gndo:pseudonym`](#d17e4321)**  
(*Pseudonym*): Ein Pseudonym, unter dem die dokumentierte Person ebenfalls bekannt ist.

**[`gndo:titleOfNobility`](#d17e4726)**  
(*Adelstitel*): Ein Adelsname der dokumentierten Person oder Personengruppe.

**[`gndo:variantName`](#person-record-variantName)**  
(*Variante Namensform (Person)*): Eine alternative Namensform, unter der eine Person ebenfalls bekannt ist.

**[`dc:title`](#d17e2645)**  
(*Titel (Record)*): Ein Titel für die Ressource. `dc:title` wird dann zur Bezeichnung einer Entität verwendet, wenn keine Vorzugsbenennung dokumentiert wird bzw. werden soll.

**[`gndo:biographicalOrHistoricalInformation`](#d17e2985)**  
(*Biographische Angaben*): Biographische oder historische Angaben über die Entität.

**[`dublicateGndIdentifier`](#dublicates)**  
(*GND-Identifier einer Dublette*): Der GND-Identifier einer möglichen Dublette.

**[`gndo:geographicAreaCode`](#elem.gndo.geographicAreaCode)**  
(*Geographischer Schwerpunkt*): Ländercode(s) der Staat(en), denen die Entität zugeordnet werden kann (z.B. Lebensmittelpunkt bzw. Schwerpunkt ihres Wirkens bei Personen). Um die Datenqualität zu erhöhen und eine automatisierte Verarbeitung zu erleichtern, muss via `@gndo:term` ein Deskriptor aus dem GND Area Code Vokabular (https://d-nb.info/standards/vocab/gnd/geographic-area-code) angegeben werden!

**[`gndo:gndIdentifier`](#d17e3593)**  
(*GND-Identifier*): Der GND-Identifier eines in der GND eingetragenen Normdatensatzes sofern vorhanden

**[`gndo:gndSubjectCategory`](#d17e3634)**  
(*GND-Sachgruppe*): (GND-Sachgruppe)

**[`gndo:languageCode`](#d17e3736)**  
(*Sprachraum*): Code(s) des Sprachraumes, dem die Entität zugeordnet werden kann. Um die Datenqualität zu erhöhen und eine automatisierte Verarbeitung zu erleichtern, kann via @gndo:term zsätzlich ein Deskriptor aus dem LOC ISO 693-2 Language Vokabular (http://id.loc.gov/vocabulary/iso639-2/) angegeben werden!

**[`gndo:relatesTo`](#d17e4511)**  
(*Beziehung*): Beziehung der beschriebenen Entität zu einer anderen Entität, entweder in der GND oder in der gleichen entityXML Ressource. Diese Property hat keine direkte Entsprechung in der GNDO!

**[`ref`](#elem.ref)**  
(*Hyperlink*): Ein Link zu einer Webressource, die weitere Informationen über die im Eintrag beschriebene Entität enthält. Der entsprechende URL wird entweder direkt als Text in `ref` oder via `@target` dokumentiert.

**[`owl:sameAs`](#elem.owl.sameAs)**  
(*Gleiche Entität*): HTTP-URI der selben Entität in einem anderen Normdatendienst.

**[`skos:note`](#elem.skos.note)**  
(*Anmerkung*): Eine Anmerkung zum Eintrag.

**[`source`](#elem.source)**  
(*Quellenangabe*): Angaben zu einer Quelle, die verwendet wurde, um die in diesem Eintrag gegebenen Informationen zu recherchieren.

**[`anyElement.narrow`](#any-restricted)**  
(*ANY ELEMENT (eng gefasst)*): Ein Element, dessen **QName** einem *custom namespace* angehört und nicht im vorliegenden Schema explizit spezifiziert ist.

**[`revision`](#revision)**  
(*Revisionsbeschreibung*): Statusinformationen zu einer Ressource (Datensammlung oder einzelner Entitätseintrag), die einzelne Bearbeitungsstände, Fragen oder anderweitige Anmerkungen zur Verarbeitung der entsprechenden Ressource dokumentiert. Die einzelnen Einträge (via `change`) werden ausgehend vom Datum von jung nach alt absteigend sortiert, sprich: Der jüngste Eintrag steht als Erstes in der Revisionsbeschreibung!

---


<a name="cb-record"></a>
#### Körperschaften

[TODO]

```xml
<content>
   <attribute name="gndo:type"/>
   <attribute name="xml:id" required="true"/>
   <attribute name="gndo:uri"/>
   <attribute name="agency"/>
   <attribute name="enrich"/>
   <anyOrder>
      <element name="foaf:page" repeatable="true"/>
      <element name="gndo:abbreviatedName" repeatable="true"/>
      <element name="gndo:affiliation" repeatable="true"/>
      <element name="gndo:dateOfEstablishment"/>
      <element name="gndo:dateOfEstablishmentAndTermination"/>
      <element name="gndo:placeOfBusiness"/>
      <element name="gndo:precedingCorporateBody"/>
      <element name="gndo:preferredName"/>
      <element name="gndo:publication" repeatable="true"/>
      <element name="gndo:succeedingCorporateBody"/>
      <element name="gndo:temporaryName" repeatable="true"/>
      <element name="gndo:variantName" repeatable="true"/>
      <element name="dc:title"/>
      <element name="gndo:biographicalOrHistoricalInformation" repeatable="true"/>
      <element name="dublicateGndIdentifier" repeatable="true"/>
      <element name="gndo:geographicAreaCode" repeatable="true"/>
      <element name="gndo:gndIdentifier" repeatable="true"/>
      <element name="gndo:gndSubjectCategory" repeatable="true"/>
      <element name="gndo:languageCode" repeatable="true"/>
      <element name="gndo:relatesTo" repeatable="true"/>
      <element name="ref" repeatable="true"/>
      <element name="owl:sameAs" repeatable="true"/>
      <element name="skos:note" repeatable="true"/>
      <element name="source" repeatable="true"/>
      <anyElement type="narrow" repeatable="true"/>
      <text/>
   </anyOrder>
   <element name="revision"/>
</content>
```


**[`@gndo:type`](#d17e586)**  
(*Körperschaftstyp*): Typisierung der dokumentierten Körperschaft (z.B. als fiktive Körperschaft, Firma usw). Wenn kein `@gndo:type` vergeben wird, gilt die Körperschaft als [gndo:CorporateBody](https://d-nb.info/standards/elementset/gnd#id-39ad8031221791a9a0a5adafd88b8ff9).
  **Möglicher Wertebereich**:  


 - '*https://d-nb.info/standards/elementset/gnd#Company*' : (Firma) [GNDO](https://d-nb.info/standards/elementset/gnd#id-1d31818b589bae5f506efcd36f8e4071)
 - '*https://d-nb.info/standards/elementset/gnd#FictiveCorporateBody*' : (Fiktive Körperschaft) [GNDO](https://d-nb.info/standards/elementset/gnd#id-233492de7966e18b7fe36583464ada24)
 - '*https://d-nb.info/standards/elementset/gnd#MusicalCorporateBody*' : (Musikalische Körperschaft) [GNDO](https://d-nb.info/standards/elementset/gnd#id-227522d76d555c9211e00f612ea79763)
 - '*https://d-nb.info/standards/elementset/gnd#OrganOfCorporateBody*' : (Organ einer Körperschaft) [GNDO](https://d-nb.info/standards/elementset/gnd#id-908a2b3cce6f09b3795666879fa1b13f)
 - '*https://d-nb.info/standards/elementset/gnd#ProjectOrProgram*' : (Projekt oder Programm) [GNDO](https://d-nb.info/standards/elementset/gnd#id-4dc1ef59dd262d466aeaf68091c1afdd)
 - '*https://d-nb.info/standards/elementset/gnd#ReligiousAdministrativeUnit*' : (Religiöse Verwaltungseinheit) [GNDO](https://d-nb.info/standards/elementset/gnd#id-4862583256b7553a04901cb9d78d2d8f)
 - '*https://d-nb.info/standards/elementset/gnd#ReligiousCorporateBody*' : (Religiöse Körperschaft) [GNDO](https://d-nb.info/standards/elementset/gnd#id-f5be3238b49f943d1458d99173b33295)



**[`@xml:id`](#d17e5720)**  ***ID***  
(*Record ID*): Angabe eines Identifiers zur Identifikation eines Entitäteneintrags innerhalb der Datensammlung. Die Angabe einer ID via `@xml:id` ist verpflichtend!

**[`@gndo:uri`](#d17e5417)**  ***anyURI***  
(*GND-URI*): Angabe des GND-HTTP URIs der beschriebenen Entität in der GND, sofern vorhanden.(http|https)://d-nb.info/gnd/(.+)

**[`@agency`](#attr.record.agency)**  
(*Agenturanfrage*): Anfrage an die Agentur, welche Aktion in Hinblick auf den Eintrag durchgeführt werden soll.
  **Möglicher Wertebereich**:  


 - '*create*' : (Eintrag in der GND neu anlegen) Der Eintrag soll als Normdatensatz neu in der GND angelegt werden.
 - '*update*' : (Normdatensatz mit Daten aus dem Eintrag erweitern) Der Normdatensatz soll durch neue Informationen angereichert werden.
 - '*merge*' : (Normdatensätze zusammenführen) Dubletten, die in dem Eintrag dokumentiert werden, sollen mit dem Normdatensatz, der durch den GND-URI des Eintrags (`@gndo:uri`) identifiziert ist, zusammengeführt werden.
 - '*ignore*' : (Eintrag ingonieren) Der Eintrag wird von der Agentur nicht bearbeitet.



**[`@enrich`](#d17e5700)**  ***boolean***  
(*Anreicherung*): Der Record soll mit Daten eines externen Datendienstes angereichert werden. Hierfür wird ein zusätzliches Konversionsscript benötigt, dass die Konversionsroutinen zur verfügung stellt, um den Record anzureichern!

**[`foaf:page`](#elem.foaf.page)**  
(*Webseite*): URL eines Dokuments mit näheren Informationen über die beschriebene Entität (z.B. Homepage, Wikipedia Seite etc.).

**[`gndo:abbreviatedName`](#d17e2856)**  
(*Abgekürzter Name*): Eine abgekürzte Namensform der dokumentierten Entität.

**[`gndo:affiliation`](#d17e2929)**  
(*Zugehörigkeit*): Die Person oder Körperschaft ist zugehörig zu einer Körperschaft, oder ist mit einem Ort oder einem Event verbunden.

**[`gndo:dateOfEstablishment`](#d17e3105)**  
(*Gründungsdatum*): Das Datum, an dem die beschriebene Entität entstanden ist bzw. gegründet wurde.

**[`gndo:dateOfEstablishmentAndTermination`](#d17e3133)**  
(*Gründungs- und Auflösungsdatum*): Zeitraum, innerhalb dessen die beschriebene Entität bestand.

**[`gndo:placeOfBusiness`](#d17e3967)**  
(*Sitz (Körperschaft)*): Der (Haupt)sitz der Körperschaft.

**[`gndo:precedingCorporateBody`](#d17e4133)**  
(*Vorherige Körperschaft (Körperschaft)*): Eine Vorgängerinstitution der Körperschaft.

**[`gndo:preferredName`](#d17e4196)**  
(*Vorzugsbenennung (Standard)*): Eine bevorzugte Namensform, um die beschriebenen Entität zu bezeichnen.

**[`gndo:publication`](#d17e4368)**  
(*Publikation*): Eine Publikation, die mit der Entität in Zusammenhang steht. Es kann sich hierbei (1) um eigene Titel der Entität (Person), (2) um Titel, die von der Entität (Person) herausgegeben worden sind und auch (3) um Titel über die Entität handeln.

**[`gndo:succeedingCorporateBody`](#d17e4596)**  
(*Nachfolgende Körperschaft (Körperschaft)*): Eine Nachfolgeinstitution der Körperschaft.

**[`gndo:temporaryName`](#d17e4681)**  
(*Zeitweiser Name*): Eine Namensform, die zeitweise zur Bezeichnung der dokumentierten Entität benutzt wurde.

**[`gndo:variantName`](#elem.gndo.variantName.standard)**  
(*Variante Namensform (Standard)*): Eine alternative Namensform, um die beschriebenen Entität zu bezeichnen.

**[`dc:title`](#d17e2645)**  
(*Titel (Record)*): Ein Titel für die Ressource. `dc:title` wird dann zur Bezeichnung einer Entität verwendet, wenn keine Vorzugsbenennung dokumentiert wird bzw. werden soll.

**[`gndo:biographicalOrHistoricalInformation`](#d17e2985)**  
(*Biographische Angaben*): Biographische oder historische Angaben über die Entität.

**[`dublicateGndIdentifier`](#dublicates)**  
(*GND-Identifier einer Dublette*): Der GND-Identifier einer möglichen Dublette.

**[`gndo:geographicAreaCode`](#elem.gndo.geographicAreaCode)**  
(*Geographischer Schwerpunkt*): Ländercode(s) der Staat(en), denen die Entität zugeordnet werden kann (z.B. Lebensmittelpunkt bzw. Schwerpunkt ihres Wirkens bei Personen). Um die Datenqualität zu erhöhen und eine automatisierte Verarbeitung zu erleichtern, muss via `@gndo:term` ein Deskriptor aus dem GND Area Code Vokabular (https://d-nb.info/standards/vocab/gnd/geographic-area-code) angegeben werden!

**[`gndo:gndIdentifier`](#d17e3593)**  
(*GND-Identifier*): Der GND-Identifier eines in der GND eingetragenen Normdatensatzes sofern vorhanden

**[`gndo:gndSubjectCategory`](#d17e3634)**  
(*GND-Sachgruppe*): (GND-Sachgruppe)

**[`gndo:languageCode`](#d17e3736)**  
(*Sprachraum*): Code(s) des Sprachraumes, dem die Entität zugeordnet werden kann. Um die Datenqualität zu erhöhen und eine automatisierte Verarbeitung zu erleichtern, kann via @gndo:term zsätzlich ein Deskriptor aus dem LOC ISO 693-2 Language Vokabular (http://id.loc.gov/vocabulary/iso639-2/) angegeben werden!

**[`gndo:relatesTo`](#d17e4511)**  
(*Beziehung*): Beziehung der beschriebenen Entität zu einer anderen Entität, entweder in der GND oder in der gleichen entityXML Ressource. Diese Property hat keine direkte Entsprechung in der GNDO!

**[`ref`](#elem.ref)**  
(*Hyperlink*): Ein Link zu einer Webressource, die weitere Informationen über die im Eintrag beschriebene Entität enthält. Der entsprechende URL wird entweder direkt als Text in `ref` oder via `@target` dokumentiert.

**[`owl:sameAs`](#elem.owl.sameAs)**  
(*Gleiche Entität*): HTTP-URI der selben Entität in einem anderen Normdatendienst.

**[`skos:note`](#elem.skos.note)**  
(*Anmerkung*): Eine Anmerkung zum Eintrag.

**[`source`](#elem.source)**  
(*Quellenangabe*): Angaben zu einer Quelle, die verwendet wurde, um die in diesem Eintrag gegebenen Informationen zu recherchieren.

**[`anyElement.narrow`](#any-restricted)**  
(*ANY ELEMENT (eng gefasst)*): Ein Element, dessen **QName** einem *custom namespace* angehört und nicht im vorliegenden Schema explizit spezifiziert ist.

**[`revision`](#revision)**  
(*Revisionsbeschreibung*): Statusinformationen zu einer Ressource (Datensammlung oder einzelner Entitätseintrag), die einzelne Bearbeitungsstände, Fragen oder anderweitige Anmerkungen zur Verarbeitung der entsprechenden Ressource dokumentiert. Die einzelnen Einträge (via `change`) werden ausgehend vom Datum von jung nach alt absteigend sortiert, sprich: Der jüngste Eintrag steht als Erstes in der Revisionsbeschreibung!

---


<a name="place-record"></a>
#### Orte

[TODO]

---


<a name="work-record"></a>
#### Werke

[TODO]

---


<a name="docs_d14e2315"></a>
## Tutorials

<a name="docs_d14e2321"></a>
### HOW TO: Person

Dieses Tutorial dient der Verdeutlichung, wie man in entityXML eine Person dokumentiert.

Als Beispiel werden wir **[Ernest Chevalier](https://fr.wikipedia.org/wiki/Ernest_Chevalier)** (Warum ausgerechnet Ernest Chevalier, werden wir im Laufe des Tutorials sehen) beschreiben, über den unsere Recherche folgende Informationen ergeben hat: 
```text
Chevalier, Ernest (männlich)
Französischer Politiker
Erwähnt in "Mollenhauer, Klaus: Vergessene Zusammenhänge, 2003, S. 139". 
Wikipedia Seite: https://fr.wikipedia.org/wiki/Ernest_Chevalier
Geboren: 14.08.1820, Villers-en-Vexin
Gestorben: 05.12.1887, Paris
```


Wir starten mit dem `person` Element, dass wir im Datenbereich (`data/list`) unserer entityXML Ressource bereits angelegt haben. Da jede Entität bzw. jeder Eintrag in entityXML durch eine ID eindeutig identifiziert werden <span class="emph">muss</span>, fügen wir `@xml:id` hinzu. Im Grunde ist die Form der ID frei wählbar, solange es sich um einen [NCName](https://www.w3.org/TR/xml-names11/#NT-NCName) handelt, vereinfacht gesagt: Eine Zeichenkette, die mit einem Buchstaben beginnt und kein ":" enthält. Für unser Beispiel bleiben wir der Einfachheit halber bei `ernest_chevalier`. 

Ausserdem wollen wir einer potenziellen GND Agentur, die diese Daten später verarbeiten soll, mitteilen, dass es sich hierbei um einen Eintrag handelt, der in der GND angelegt werden soll. Das machen wir mit Hilfe von `@agency`, dem wir den Wert "create" geben.

```xml
<data>
   <list>
      
      <!-- Einträge davor -->
      
      <person xml:id="ernest_chevalier" agency="create"></person>
      
      <!-- Einträge danach -->
   </list>
</data>
```

Jetzt ist der Eintrag initial angelegt und identifiziert. Nun können wir unseren Personeneintrag nach und nach mit den recherchierten Daten anreichern. Am Einfachsten wäre es, wenn wir unser Rechercheergebnis erstmal in den leeren Eintrag kopieren. So haben wir schoneinmal alles zusammen:

```xml
<person xml:id="ernest_chevalier" agency="create">
   Chevalier, Ernest (männlich)
   Französischer Politiker
   Erwähnt in "Mollenhauer, Klaus: Vergessene Zusammenhänge, 2003, S. 139". 
   Wikipedia Seite: https://fr.wikipedia.org/wiki/Ernest_Chevalier
   Geboren: 14.08.1820, Villers-en-Vexin
   Gestorben: 05.12.1887, Paris
</person>

```

Bevor wir uns nun dran machen, Daten in diesem Eintrag zu erschließen, legen wir noch eine Revisionsbeschreibung via `revision` an und dokumentieren die Bearbeitungsphase und den Zeitpunkt, an dem wir mit diesem Eintrag begonnen haben. Der Eintrag ist ganz frisch angelegt und befindet sich somit in der Anfangsphase; dementsprechend verzeichnen wir "opened" im `@status` Attribut von `revision` und legen einen ersten Änderungseintrag mit dem aktuellen Datum an:

```xml
<person xml:id="ernest_chevalier" agency="create">
   Chevalier, Ernest (männlich)
   Französischer Politiker
   Erwähnt in "Mollenhauer, Klaus: Vergessene Zusammenhänge, 2003, S. 139". 
   Wikipedia Seite: https://fr.wikipedia.org/wiki/Ernest_Chevalier
   Geboren: 14.08.1820, Villers-en-Vexin
   Gestorben: 05.12.1887, Paris
   <revision status="opened">
      <change when="2023-02-10" who="US">Eintrag angelegt.</change>
   </revision>
</person>

```

Bei Ernest Chevalier handelt es sich um eine Person aus Fleisch und Blut und nicht etwa um eine fiktive Person oder gar einen Gott. Dementsprechend gilt er als 'gndo:DifferentiatedPerson' und so benötigen wir keine weitere Spezifizierung via `@gndo:type`.

Da entityXML als **Markupformat** konzipiert ist, können die (derzeit noch) in unstrukturierter, rein menschenlesbarer Form vorliegendenden Daten sukzessive ausgezeichnet werden. Damit ein Personeneintrag (ohne GND-URI) valide ist, d.h. dem Schema von entityXML entspricht, müssen wir mindestend eine **Vorzugsbenennung** angeben, also den Namen, unter dem Ernest Chevalier hauptsächlich bekannt ist. Dafür verwenden wir `gndo:preferredName`:

```xml
<person xml:id="ernest_chevalier" agency="create">
   <gndo:preferredName>Chevalier, Ernest</gndo:preferredName> (männlich)
   Französischer Politiker
   Erwähnt in "Mollenhauer, Klaus: Vergessene Zusammenhänge, 2003, S. 139". 
   Wikipedia Seite: https://fr.wikipedia.org/wiki/Ernest_Chevalier
   Geboren: 14.08.1820, Villers-en-Vexin
   Gestorben: 05.12.1887, Paris
   <revision status="opened">
      <change when="2023-02-10" who="US">Eintrag angelegt.</change>
   </revision>
</person>
```

Das entityXML Schema sieht bei einem Personennamen vor, dass er entweder aus einem *persönlichen Namen* (`gndo:personalName`) oder aus einem *Vor*- (`gndo:forename`) und einem *Nachnamen* (`gndo:surname`) besteht:

```xml
<person xml:id="ernest_chevalier" agency="create">
   <gndo:preferredName>
      <gndo:surname>Chevalier</gndo:surname>, 
      <gndo:forename>Ernest</gndo:forename>
   </gndo:preferredName> (männlich)
   Französischer Politiker
   Erwähnt in "Mollenhauer, Klaus: Vergessene Zusammenhänge, 2003, S. 139". 
   Wikipedia Seite: https://fr.wikipedia.org/wiki/Ernest_Chevalier
   Geboren: 14.08.1820, Villers-en-Vexin
   Gestorben: 05.12.1887, Paris
   <revision status="opened">
      <change when="2023-02-10" who="US">Eintrag angelegt.</change>
   </revision>
</person>
```

Sofern wir in *oXygen XML* oder einem anderen Editor, der "schema-aware" ist, also eine Schemadatei zur automatischen Validierung verwenden kann und dementsprechend Rückmeldung gibt, sehen wir nun ein valides Stück XML: Unser Personeneintrag zu Ernest Chevalier ist eindeutig in der XML identifiziert und durch eine Vorzugsbenennung benannt und erfüllt so die Mindestanforderungen. Nun kümmern wir uns um die weiteren Daten.

Da wir das **biologische Geschlecht** von Herrn Chevalier recherchiert haben und uns diese Information maschienenlesbar zur Verfügung stehen soll, verwenden wir `gndo:gender` zur Auszeichnung der Information im Text:

```xml
<person xml:id="ernest_chevalier" agency="create">
   <gndo:preferredName>
      <gndo:surname>Chevalier</gndo:surname>, 
      <gndo:forename>Ernest</gndo:forename>
   </gndo:preferredName> (<gndo:gender>männlich</gndo:gender>)
   Französischer Politiker
   Erwähnt in "Mollenhauer, Klaus: Vergessene Zusammenhänge, 2003, S. 139". 
   Wikipedia Seite: https://fr.wikipedia.org/wiki/Ernest_Chevalier
   Geboren: 14.08.1820, Villers-en-Vexin
   Gestorben: 05.12.1887, Paris
   <revision status="opened">
      <change when="2023-02-10" who="US">Eintrag angelegt.</change>
   </revision>
</person>
```

Nun meldet sich die Validierung abermals zu Wort, denn das entityXML Schema sieht vor, dass `gndo:gender` das Geschlecht explizit über einen Bezeichner aus dem [GND Vokabular "Gender"](https://d-nb.info/standards/vocab/gnd/gender#) ausweist. Dafür verwenden wir `@gndo:term`; die zulässigen Werte sind bereits vordefiniert:

```xml
<person xml:id="ernest_chevalier" agency="create">
   <gndo:preferredName>
      <gndo:surname>Chevalier</gndo:surname>, 
      <gndo:forename>Ernest</gndo:forename>
   </gndo:preferredName> (<gndo:gender 
      gndo:term="https://d-nb.info/standards/vocab/gnd/gender#male">männlich</gndo:gender>)
   Französischer Politiker
   Erwähnt in "Mollenhauer, Klaus: Vergessene Zusammenhänge, 2003, S. 139". 
   Wikipedia Seite: https://fr.wikipedia.org/wiki/Ernest_Chevalier
   Geboren: 14.08.1820, Villers-en-Vexin
   Gestorben: 05.12.1887, Paris
   <revision status="opened">
      <change when="2023-02-10" who="US">Eintrag angelegt.</change>
   </revision>
</person>
```

Bisher habe ich noch garnicht gesagt, warum wir Ernest Chevalier überhaupt anlegen. Wie für viele Andere gibt es auch für ihn (noch) keinen Eintrag in der GND. Interessant ist er für unser kleines Tutorial allerdings aus einem anderen Grund, denn die Beschreibung von Ernest Chevalier ist Teil eines <span class="emph">konkreten Anwendungsfalles</span>: Ein Editionsprojekt (im vorliegenden Falle die [Mollenhauer
                     Gesamtausgabe](https://www.uni-goettingen.de/de/klaus+mollenhauer+gesamtausgabe+%28kmg%29/584741.html)) identifiziert alle in der digitalen Edition namentlich aufgeführten Personen über GND-URIs. Für die Fälle, für die kein GND-URI, also kein Eintrag in der GND existiert, sollen die Personen trotzdem eindeutig identifiziert und mit weiteren Daten dokumentiert werden, um hiermit weitere Informationsprozesse zu ermöglichen bzw. zu steuern (z.B. automatisierte Registererstellung, Anzeige zusätzlicher Informationen auf dem Projektportal zu einer Person, Austausch der dokumentierten Daten mit der GND etc.).

Und genau aus diesem Anwendungsszenario stammt die Information "*Vergessene Zusammenhänge (S. 139)*", denn genau hier erwähnt [Klaus Mollenhauer](https://d-nb.info/gnd/118583352) den von uns hier in der Erschließung befindlichen Ernest Chevalier. Entsprechend geben wir diese Referenz als `gndo:publication` mit dem Verweis auf die Ausgabe von 2003 im **Katalog der Deutschen Nationalbibliothek** via `@dnb:catalogue` an. In der GND existiert zudem ein Werknormdatensatz für die "Vergessenen Zusammenhänge", den wir ebenfalls dokumentieren wollen und mittels `@gndo:ref` angeben:

```xml
<person xml:id="ernest_chevalier" agency="create">
   <gndo:preferredName>
      <gndo:surname>Chevalier</gndo:surname>, 
      <gndo:forename>Ernest</gndo:forename>
   </gndo:preferredName> (<gndo:gender 
      gndo:term="https://d-nb.info/standards/vocab/gnd/gender#male">männlich</gndo:gender>)
   Französischer Politiker
   Erwähnt in "<gndo:publication role="about" dnb:catalogue="https://d-nb.info/968701086"
      gndo:ref="https://d-nb.info/gnd/118641166X"><gndo:firstAuthor>Mollenhauer, Klaus</gndo:firstAuthor>: <title>Vergessene Zusammenhänge</title>, <date>2003</date>, S. 139</gndo:publication>". 
   Wikipedia Seite: https://fr.wikipedia.org/wiki/Ernest_Chevalier
   Geboren: 14.08.1820, Villers-en-Vexin
   Gestorben: 05.12.1887, Paris
   <revision status="opened">
      <change when="2023-02-10" who="US">Eintrag angelegt.</change>
   </revision>
</person>
```

Bevor wir weiter machen und die restlichen Daten auszeichnen, sollten wir zunächst explizit machen, **wo** wir diese Informationen eigentlich her haben. Wir stützen uns auf die Informationen, die in der französischen Wikipedia aufgeführt sind und verwenden `source`, um den *URL der entsprechenden Seite* explizit als **Quelle** auszuzeichnen:

```xml
<person xml:id="ernest_chevalier" agency="create">
   <gndo:preferredName>
      <gndo:surname>Chevalier</gndo:surname>, 
      <gndo:forename>Ernest</gndo:forename>
   </gndo:preferredName> (<gndo:gender 
      gndo:term="https://d-nb.info/standards/vocab/gnd/gender#male">männlich</gndo:gender>)
   Französischer Politiker
   Erwähnt in "<gndo:publication role="about" dnb:catalogue="https://d-nb.info/968701086"
      gndo:ref="https://d-nb.info/gnd/118641166X"><gndo:firstAuthor>Mollenhauer, Klaus</gndo:firstAuthor>: <title>Vergessene Zusammenhänge</title>, <date>2003</date>, S. 139</gndo:publication>". 
   Wikipedia Seite: <source>https://fr.wikipedia.org/wiki/Ernest_Chevalier</source>
   Geboren: 14.08.1820, Villers-en-Vexin
   Gestorben: 05.12.1887, Paris
   <revision status="opened">
      <change when="2023-02-10" who="US">Eintrag angelegt.</change>
   </revision>
</person>
```

Hierbei handelt es sich auch gleichsam um ein **Dokument für zusätzliche Informationen** zu Ernest Chevalier und da wir nicht alle Einzelheiten in unserem Datensatz aufführen wollen, machen wir durch `foaf:page` deutlich, dass man auf der Wikiseite näheres zu der hier beschriebenen Person finden kann. Dafür dublizieren wir den URL der französischen Wikipedia Seite und zeichnen ihn entsprechend aus:

```xml
<person xml:id="ernest_chevalier" agency="create">
   <gndo:preferredName>
      <gndo:surname>Chevalier</gndo:surname>, 
      <gndo:forename>Ernest</gndo:forename>
   </gndo:preferredName> (<gndo:gender 
      gndo:term="https://d-nb.info/standards/vocab/gnd/gender#male">männlich</gndo:gender>)
   Französischer Politiker
   Erwähnt in "<gndo:publication role="about" dnb:catalogue="https://d-nb.info/968701086"
      gndo:ref="https://d-nb.info/gnd/118641166X"><gndo:firstAuthor>Mollenhauer, Klaus</gndo:firstAuthor>: <title>Vergessene Zusammenhänge</title>, <date>2003</date>, S. 139</gndo:publication>". 
   Wikipedia Seite: <source>https://fr.wikipedia.org/wiki/Ernest_Chevalier</source>
   Zusätzliche Informationen: <foaf:page>https://fr.wikipedia.org/wiki/Ernest_Chevalier</foaf:page>
   Geboren: 14.08.1820, Villers-en-Vexin
   Gestorben: 05.12.1887, Paris
   <revision status="opened">
      <change when="2023-02-10" who="US">Eintrag angelegt.</change>
   </revision>
</person>
```

So, nun haben wir es fast geschafft. Bleiben noch die Lebendaten und der Beruf, die bisher "nur" in menschenlesbarer Form im Eintrag existieren.

Um **Geburts- und Sterbedatum** maschienenlesbar auszuzeichnen, verwenden wir `gndo:dateOfBirth` und `gndo:dateOfDeath`. Da es sich um Datumsangaben handelt, weisen wir zusätzlich zur menschenlesbaren Form des Datums ein obligatorisches ISO-Datum via `@iso-date` aus:

```xml
<person xml:id="ernest_chevalier" agency="create">
   <gndo:preferredName>
      <gndo:surname>Chevalier</gndo:surname>, 
      <gndo:forename>Ernest</gndo:forename>
   </gndo:preferredName> (<gndo:gender 
      gndo:term="https://d-nb.info/standards/vocab/gnd/gender#male">männlich</gndo:gender>)
   Französischer Politiker
   Erwähnt in "<gndo:publication role="about" dnb:catalogue="https://d-nb.info/968701086"
      gndo:ref="https://d-nb.info/gnd/118641166X"><gndo:firstAuthor>Mollenhauer, Klaus</gndo:firstAuthor>: <title>Vergessene Zusammenhänge</title>, <date>2003</date>, S. 139</gndo:publication>". 
   Wikipedia Seite: <source>https://fr.wikipedia.org/wiki/Ernest_Chevalier</source>
   Zusätzliche Informationen: <foaf:page>https://fr.wikipedia.org/wiki/Ernest_Chevalier</foaf:page>
   Geboren: <gndo:dateOfBirth iso-date="1820-08-14">14.08.1820</gndo:dateOfBirth>, Villers-en-Vexin
   Gestorben: <gndo:dateOfDeath iso-date="1887-12-05">05.12.1887</gndo:dateOfDeath>, Paris
   <revision status="opened">
      <change when="2023-02-10" who="US">Eintrag angelegt.</change>
   </revision>
</person>
```

**Geburts- und Sterbeorte** zeichnen wir mittels `gndo:placeOfBirth` und `gndo:placeOfDeath` aus. Auch hier recherchieren wir noch zusätzlich die entsprechenden *Normdateneinträge für Orte in der GND* und weisen diese unter Verwendung der entsprechenden GND-URIs via `@gndo:ref` mit aus, zumindest, sofern wir hierzu fündig werden; ich hatte bei "Villers-en-Vexin" keinen Erfolg und konnte den Ort über die GND nicht identifizieren. Nun sollten wir alle Lebensdaten erschlossen haben:

```xml
<person xml:id="ernest_chevalier" agency="create">
   <gndo:preferredName>
      <gndo:surname>Chevalier</gndo:surname>, 
      <gndo:forename>Ernest</gndo:forename>
   </gndo:preferredName> (<gndo:gender 
      gndo:term="https://d-nb.info/standards/vocab/gnd/gender#male">männlich</gndo:gender>)
   Französischer Politiker
   Erwähnt in "<gndo:publication role="about" dnb:catalogue="https://d-nb.info/968701086"
      gndo:ref="https://d-nb.info/gnd/118641166X"><gndo:firstAuthor>Mollenhauer, Klaus</gndo:firstAuthor>: <title>Vergessene Zusammenhänge</title>, <date>2003</date>, S. 139</gndo:publication>". 
   Wikipedia Seite: <source>https://fr.wikipedia.org/wiki/Ernest_Chevalier</source>
   Zusätzliche Informationen: <foaf:page>https://fr.wikipedia.org/wiki/Ernest_Chevalier</foaf:page>
   Geboren: <gndo:dateOfBirth iso-date="1820-08-14">14.08.1820</gndo:dateOfBirth>, <gndo:placeOfBirth>Villers-en-Vexin</gndo:placeOfBirth>
   Gestorben: <gndo:dateOfDeath iso-date="1887-12-05">05.12.1887</gndo:dateOfDeath>, <gndo:placeOfDeath 
      gndo:ref="https://d-nb.info/gnd/4044660-8">Paris</gndo:placeOfDeath>
   <revision status="opened">
      <change when="2023-02-10" who="US">Eintrag angelegt.</change>
   </revision>
</person>
```

Als letzte kümmern wir uns noch um die Auszeichnung des Berufs. Ernest Chevalier war Politiker und das hauptberuflich. Wir verwenden `gndo:professionOrOccupation` und fügen das Attribut `@gndo:type` mit dem Wert "**significant**" hinzu, um auch diese Information menschen- und maschienenlesbar zu dokumentieren; `@gndo:type` dient nämlich in diesem Kontext zu Klassifikation einer Beschäftigung als **Hauptbeschäftigung**. Für den Beruf "Politiker" gibt es einen Deskriptor in den GND Sachbegriffen und zwar "https://d-nb.info/gnd/4046517-2". Diesen geben wir wieder unter Verwendung von `@gndo:ref` mit an:

```xml
<person xml:id="ernest_chevalier" agency="create">
   <gndo:preferredName>
      <gndo:surname>Chevalier</gndo:surname>, 
      <gndo:forename>Ernest</gndo:forename>
   </gndo:preferredName> (<gndo:gender 
      gndo:term="https://d-nb.info/standards/vocab/gnd/gender#male">männlich</gndo:gender>)
   Französischer <gndo:professionOrOccupation gndo:ref="https://d-nb.info/gnd/4046517-2" 
      gndo:type="significant">Politiker</gndo:professionOrOccupation>
   Erwähnt in "<gndo:publication role="about" dnb:catalogue="https://d-nb.info/968701086"
      gndo:ref="https://d-nb.info/gnd/118641166X"><gndo:firstAuthor>Mollenhauer, Klaus</gndo:firstAuthor>: <title>Vergessene Zusammenhänge</title>, <date>2003</date>, S. 139</gndo:publication>". 
   Wikipedia Seite: <source>https://fr.wikipedia.org/wiki/Ernest_Chevalier</source>
   Zusätzliche Informationen: <foaf:page>https://fr.wikipedia.org/wiki/Ernest_Chevalier</foaf:page>
   Geboren: <gndo:dateOfBirth iso-date="1820-08-14">14.08.1820</gndo:dateOfBirth>, <gndo:placeOfBirth>Villers-en-Vexin</gndo:placeOfBirth>
   Gestorben: <gndo:dateOfDeath iso-date="1887-12-05">05.12.1887</gndo:dateOfDeath>, <gndo:placeOfDeath 
      gndo:ref="https://d-nb.info/gnd/4044660-8">Paris</gndo:placeOfDeath>
   <revision status="opened">
      <change when="2023-02-10" who="US">Eintrag angelegt.</change>
   </revision>
</person>
```

<span class="emph">Geschafft</span>! Unser Eintrag zu Ernest Chevalier ist abgeschlossen und (zumindest für die Belange dieses Tutorials) mit genügend Daten angereichert. Den Abschluss dokumentieren wir noch in der Revisionsbeschreibung mittels `change` und markieren den Eintrag mit Hilfe des `@status` Attributs als Kandidaten für die Übertragung in die GND:

```xml
<person xml:id="ernest_chevalier" agency="create">
   <gndo:preferredName>
      <gndo:surname>Chevalier</gndo:surname>, 
      <gndo:forename>Ernest</gndo:forename>
   </gndo:preferredName> 
   <!-- Weitere Inhalte der Übersichtlichkeit halber ausgeblendet -->
   <revision status="opened">
      <change when="2023-02-10" who="US" status="candidate">Bearbeitung abgeschlossen; bereit für die Übernahme in die GND.</change>
      <change when="2023-02-10" who="US">Eintrag angelegt.</change>
   </revision>
</person>
```

Man muss die Daten nicht - wie in diesem Tutorial geschehen - als *Text* strukturieren. Man kann sich auch für eine eher *strukturiertere Form* entscheiden. Dafür nehmen wir einfach unser Beispiel und löschen alle textlichen Informationen, die in einer strukturierten Form eher nicht enthalten sein sollten: 

```xml
<person xml:id="ernest_chevalier" agency="create">
   <gndo:preferredName>
      <gndo:surname>Chevalier</gndo:surname> 
      <gndo:forename>Ernest</gndo:forename>
   </gndo:preferredName>
   <gndo:dateOfBirth iso-date="1820-08-14">14.08.1820</gndo:dateOfBirth> 
   <gndo:dateOfDeath iso-date="1887-12-05">05.12.1887</gndo:dateOfDeath>
   <gndo:placeOfBirth>Villers-en-Vexin</gndo:placeOfBirth>
   <gndo:placeOfDeath gndo:ref="https://d-nb.info/gnd/4044660-8">Paris</gndo:placeOfDeath>
   <gndo:gender gndo:term="https://d-nb.info/standards/vocab/gnd/gender#male">männlich</gndo:gender>
   <gndo:geographicAreaCode gndo:term="https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-FR">Frankreich</gndo:geographicAreaCode>
   <gndo:professionOrOccupation gndo:ref="https://d-nb.info/gnd/4046517-2" gndo:type="significant">Politiker</gndo:professionOrOccupation>
   <gndo:publication role="about" dnb:catalogue="https://d-nb.info/968701086" gndo:ref="https://d-nb.info/gnd/118641166X">
      <gndo:firstAuthor>Mollenhauer, Klaus</gndo:firstAuthor>
      <title>Vergessene Zusammenhänge</title>
      <date>2003</date>, S. 139
   </gndo:publication> 
   <foaf:page>https://fr.wikipedia.org/wiki/Ernest_Chevalier</foaf:page>
   <source>https://fr.wikipedia.org/wiki/Ernest_Chevalier</source>
   <revision status="opened">
      <change when="2023-02-10" who="US" status="candidate">Bearbeitung abgeschlossen; bereit für die Übernahme in die GND.</change>
      <change when="2023-02-10" who="US">Eintrag angelegt.</change>
   </revision>
</person>
```

Wir haben uns nun bei diesem Schritt noch dazu entschieden, einen **geographischen Bezug** für Ernest Chevalier mit aufzunehmen, und zwar *Frankreich*. Dazu verwenden wir `gndo:geographicAreaCode` und weisen einen entsprechenden Deskriptor aus dem [GND Vokabular
                     "Geographic Area Code"](https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-FR) via `@gndo:term` mit aus.

Und damit sind wir am Ende dieses Tutorial angekommen. Der originale Eintrag zu Chevalier befindet sich übrigens in [`samples/KMG-GND.20230203.0.5.1.xml`](https://gitlab.gwdg.de/entities/entityxml/-/blob/master/samples/KMG-GND.20230203.0.5.1.xml) mit der ID `ernest_chevalier`. 

Viel Spaß beim eigenen Anlegen von Personeneinträgen!

<a name="doc-specs"></a>
## Technische Spezifikation

<a name="specs-elems"></a>
### Elementspezifikationen

Derzeit umfasst das entityXML Schema **100** spezifizierte Elemente.

Die technische Spezifikation für Elemente umfasst folgende Eigenschaften: 

- **Name**: Der Elementname bzw. XML-Tag, der das Element bezeichnet.
- **Equivalents**: Externe Definitionen equivalenter Terme (z.B. in der GNDO).
- **Label**: Ein oder mehrere Labels in Deutsch (und Englisch).
- **Description**: Eine oder mehrere Beschreibungen, die die Semantik des Elements beschreiben.
- **Attributes**: Sofern ein Element Attribute enthalten kann, werden diese hier aufgeführt.
- **Contained by**: Führt die Elemente auf, in denen das entsprechende Element verwendet wird bzw. werden kann.
- **May Contain**: Führt Elemente auf, die innerhalb des entsprechenden Elements verwendet werden bzw. werden können.
- **Content Model**: Abstrahiertes Inhaltsmodell des Elements.
- **Validation**: Falls spezifiziert, werden hier Validierungsregeln (z.B. Schematron Pattern) für das entsprechende Element angegeben.



<a name="any-restricted"></a>
#### ANY ELEMENT (eng gefasst)
|     |     |
| --- | --- |
| **Name** | *Any name except those listed below or associated with a namespace listed below.*  |
| **Label** (de) | ANY ELEMENT (eng gefasst) |
| **Label** (en) | ANY ELEMENT (narrow) |
| **Description** (de) | Ein Element, dessen **QName** einem *custom namespace* angehört und nicht im vorliegenden Schema explizit spezifiziert ist. |
| **Description** (en) | - |
| **Attributes** | [`@<anyAttribute>`](#d17e1960)   |
| **Contained by** | [`<anyElement.narrow>`](#any-restricted)  [`corporateBody`](#corporate-body-record)  [`entity`](#entity-record)  [`event`](#d17e243)  [`expression`](#d17e1235)  [`manifestation`](#d17e1304)  [`person`](#person-record)  [`place`](#d17e1061)  [`work`](#d17e1378) |
| **May contain** | [`<anyElement.narrow>`](#any-restricted) |

***Content Model***  
```xml
<content>
   <anyAttribute repeatable="true"/>
   <anyOrder>
      <anyElement type="narrow" repeatable="true"/>
      <text/>
   </anyOrder>
</content>
```


***Permited Namespaces & Names***  

- **ns**: https://sub.uni-goettingen.de/met/standards/entity-xml#
- **ns**: https://sub.uni-goettingen.de/met/standards/entity-store#
- **ns**: https://d-nb.info/standards/elementset/gnd#
- **name**: http://id.loc.gov/ontologies/bibframe/**instanceOf**
- **name**: http://xmlns.com/foaf/0.1/**page**
- **name**: http://purl.org/dc/elements/1.1/**title**
- **name**: http://www.w3.org/2004/02/skos/core#**note**
- **name**: http://www.w3.org/2002/07/owl#**sameAs**
- **name**: http://www.opengis.net/ont/geosparql#**hasGeometry**
- **name**: http://www.w3.org/2003/01/geo/wgs84_pos#**lat**
- **name**: http://www.w3.org/2003/01/geo/wgs84_pos#**long**

---

<p style="margin-bottom:60px"></p>

<a name="any-all"></a>
#### ANY ELEMENT (weit gefasst)
|     |     |
| --- | --- |
| **Name** | *Any name except those listed below or associated with a namespace listed below.*  |
| **Label** (de) | ANY ELEMENT (weit gefasst) |
| **Label** (en) | ANY ELEMENT (broad) |
| **Description** (de) | Ein Element, dessen **QName** einem *custom namespace* angehört. Dieses Element kann im extremsten Fall einen vollständigen Datensatz eines anderen XML-Formats repräsentieren. |
| **Description** (en) | - |
| **Attributes** | [`@<anyAttribute>`](#d17e1960)   |
| **Contained by** | [`<anyElement.broad>`](#any-all)  [`entity`](#entity-record) |
| **May contain** | [`<anyElement.broad>`](#any-all) |

***Content Model***  
```xml
<content>
   <anyAttribute repeatable="true"/>
   <anyOrder>
      <anyElement type="broad" repeatable="true"/>
      <text/>
   </anyOrder>
</content>
```


***Permited Namespaces & Names***  

- **ns**: https://sub.uni-goettingen.de/met/standards/entity-xml#
- **ns**: https://sub.uni-goettingen.de/met/standards/entity-store#
- **ns**: https://d-nb.info/standards/elementset/gnd#

---

<p style="margin-bottom:60px"></p>

<a name="d17e2856"></a>
#### Abgekürzter Name
|     |     |
| --- | --- |
| **Name** | `gndo:abbreviatedName`  |
| **See also** | [gndo:abbreviatedName](https://d-nb.info/standards/elementset/gnd#id-1472608b807eccfbe05c8861cdd266a3), [gndo:abbreviatedNameForTheConferenceOrEvent](https://d-nb.info/standards/elementset/gnd#id-4c24157d47aca283599607d794e4fd48), [gndo:abbreviatedNameForTheCorporateBody](https://d-nb.info/standards/elementset/gnd#id-1ba0febdc4cd55c389cbd0eebf2ab57e), [gndo:abbreviatedNameForThePlaceOrGeographicName](https://d-nb.info/standards/elementset/gnd#id-9dd18ba8df90b33fa913e4548854bc19), [gndo:abbreviatedNameForTheWork](https://d-nb.info/standards/elementset/gnd#id-64427ae559606b8cc06dbf544e6785a4) |
| **Label** (de) | Abgekürzter Name |
| **Label** (en) | Abbreviated name |
| **Description** (de) | Eine abgekürzte Namensform der dokumentierten Entität. |
| **Description** (en) | An abbreviated Name for the entity encoded. |
| **Attributes** | [`@agency`](#d17e5568)  [`@enriched`](#d17e5620)  [`@xml:lang`](#d17e5550) |
| **Contained by** | [`corporateBody`](#corporate-body-record)  [`event`](#d17e243)  [`place`](#d17e1061)  [`work`](#d17e1378) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="xml:lang"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e4841"></a>
#### Abstract (Metadaten)
|     |     |
| --- | --- |
| **Name** | `abstract`  |
| **Label** (de) | Abstract (Metadaten) |
| **Label** (en) | Abstract (Metadata) |
| **Description** (de) | Eine kurze Beschreibung. |
| **Description** (en) | A short description. |
| **Contained by** | [`agency`](#agency-stmt)  [`metadata`](#collection-metadata)  [`provider`](#data-provider) |

***Content Model***  
```xml
<content>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e4726"></a>
#### Adelstitel
|     |     |
| --- | --- |
| **Name** | `gndo:titleOfNobility`  |
| **See also** | [gndo:titleOfNobility](https://d-nb.info/standards/elementset/gnd?#id-ad626b6a82fe798456e7a7031fbb5909) |
| **Label** (de) | Adelstitel |
| **Label** (en) | Title of nobility |
| **Description** (de) | Ein Adelsname der dokumentierten Person oder Personengruppe. |
| **Description** (en) | A title of nobility held by a person or family. |
| **Attributes** | [`@agency`](#d17e5568)  [`@enriched`](#d17e5620)  [`@gndo:ref`](#d17e5439)  [`@ref`](#d17e5735)  [`@xml:lang`](#d17e5550) |
| **Contained by** | [`person`](#person-record) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <attribute name="xml:lang"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e2576"></a>
#### Adresse
|     |     |
| --- | --- |
| **Name** | `address`  |
| **Label** (de) | Adresse |
| **Label** (en) | address |
| **Description** (de) | Postalische Anschrift. |
| **Description** (en) | - |
| **Contained by** | [`contact`](#contact-metadata) |

***Content Model***  
```xml
<content>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e2902"></a>
#### Akademischer Grad
|     |     |
| --- | --- |
| **Name** | `gndo:academicDegree`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#id-872d14a53d24d2ad72134e6c23794234](https://d-nb.info/standards/elementset/gnd#id-872d14a53d24d2ad72134e6c23794234) |
| **Label** (de) | Akademischer Grad |
| **Label** (en) | Academic degree |
| **Description** (de) | Angaben zum akademischen Grad der Person, z.B. "Dr. jur.". |
| **Description** (en) | Description with regard to the academic degree of a person |
| **Attributes** | [`@agency`](#d17e5568)  [`@enriched`](#d17e5620) |
| **Contained by** | [`person`](#person-record) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="elem.skos.note"></a>
#### Anmerkung
|     |     |
| --- | --- |
| **Name** | `skos:note`  |
| **See also** | [skos:note](https://www.w3.org/2009/08/skos-reference/skos.html#note) |
| **Label** (de) | Anmerkung |
| **Label** (en) | Note |
| **Description** (de) | Eine Anmerkung zum Eintrag. |
| **Description** (en) | - |
| **Attributes** | [`@agency`](#d17e5568)  [`@enriched`](#d17e5620)  [`@gndo:type`](#d17e5171)[`@xml:lang`](#d17e5550)   |
| **Contained by** | [`corporateBody`](#corporate-body-record)  [`entity`](#entity-record)  [`event`](#d17e243)  [`expression`](#d17e1235)  [`manifestation`](#d17e1304)  [`person`](#person-record)  [`place`](#d17e1061)  [`work`](#d17e1378) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="xml:lang"/>
   <attribute name="gndo:type"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e3201"></a>
#### Auflösungsdatum
|     |     |
| --- | --- |
| **Name** | `gndo:dateOfTermination`  |
| **See also** | [gndo:dateOfTermination](https://d-nb.info/standards/elementset/gnd#id-c54ae8844a832f843469b17e941ec98b) |
| **Label** (de) | Auflösungsdatum |
| **Label** (en) | Date of termination |
| **Description** (de) | Das Datum, an dem die beschriebene Entität aufgelöst wurde. |
| **Description** (en) | - |
| **Attributes** | [`@agency`](#d17e5568)  [`@enriched`](#d17e5620)  [`@iso-date`](#attr.iso-date) |
| **Contained by** |  |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="iso-date" required="true"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e1235"></a>
#### Ausdrucksform
|     |     |
| --- | --- |
| **Name** | `expression`  |
| **Label** (de) | Ausdrucksform |
| **Label** (en) | Expression |
| **Description** (de) | Ausdrucksform eines Werks nach FRBR. |
| **Description** (en) | Expression of a Work (FRBR). |
| **Attributes** | [`@agency`](#attr.record.agency)  [`@enrich`](#d17e5700)  [`@gndo:uri`](#d17e5417)  [`@xml:id`](#d17e5720)   |
| **Contained by** | [`list`](#data-list) |
| **May contain** | [`<anyElement.narrow>`](#any-restricted)  [`dc:title`](#d17e2645)  [`dublicateGndIdentifier`](#dublicates)  [`gndo:biographicalOrHistoricalInformation`](#d17e2985)  [`gndo:geographicAreaCode`](#elem.gndo.geographicAreaCode)  [`gndo:gndIdentifier`](#d17e3593)  [`gndo:gndSubjectCategory`](#d17e3634)  [`gndo:languageCode`](#d17e3736)  [`gndo:relatesTo`](#d17e4511)  [`owl:sameAs`](#elem.owl.sameAs)  [`realizationOf`](#d17e1265)  [`ref`](#elem.ref)  [`revision`](#revision)[`skos:note`](#elem.skos.note)  [`source`](#elem.source)   |

***Content Model***  
```xml
<content>
   <attribute name="xml:id" required="true"/>
   <attribute name="gndo:uri"/>
   <attribute name="agency"/>
   <attribute name="enrich"/>
   <anyOrder>
      <element name="realizationOf"/>
      <element name="dc:title"/>
      <element name="gndo:biographicalOrHistoricalInformation" repeatable="true"/>
      <element name="dublicateGndIdentifier" repeatable="true"/>
      <element name="gndo:geographicAreaCode" repeatable="true"/>
      <element name="gndo:gndIdentifier" repeatable="true"/>
      <element name="gndo:gndSubjectCategory" repeatable="true"/>
      <element name="gndo:languageCode" repeatable="true"/>
      <element name="gndo:relatesTo" repeatable="true"/>
      <element name="ref" repeatable="true"/>
      <element name="owl:sameAs" repeatable="true"/>
      <element name="skos:note" repeatable="true"/>
      <element name="source" repeatable="true"/>
      <anyElement type="narrow" repeatable="true"/>
      <text/>
   </anyOrder>
   <element name="revision"/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e3403"></a>
#### Autor, erste Verfasserschaft
|     |     |
| --- | --- |
| **Name** | `gndo:firstAuthor`  |
| **See also** | [gndo:firstAuthor](https://d-nb.info/standards/elementset/gnd#id-2b82bfc7d406c8da5c093ca823a7a35a) |
| **Label** (de) | Autor, erste Verfasserschaft |
| **Label** (en) | First author |
| **Description** (de) | Die Person oder Körperschaft, die verantwortlich für die primäre Verfasserschaft an einem Werk oder einer Veröffentlichung zeichnet. |
| **Description** (en) | A person or organization that takes primary responsibility for a particular activity or endeavor. May be combined with another relator term or code to show the greater importance this person or organization has regarding that particular role. If more than one relator is assigned to a heading, use the Lead relator only if it applies to all the relators. |
| **Attributes** | [`@agency`](#d17e5568)  [`@enriched`](#d17e5620)  [`@gndo:ref`](#d17e5439)  [`@ref`](#d17e5735) |
| **Contained by** | [`gndo:publication`](#d17e4368)  [`work`](#d17e1378) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e3307"></a>
#### Beiname, Gattungsname, Titulatur, Territorium
|     |     |
| --- | --- |
| **Name** | `gndo:epithetGenericNameTitleOrTerritory`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#id-910102daa117483460a8315c39985b69](https://d-nb.info/standards/elementset/gnd#id-910102daa117483460a8315c39985b69) |
| **Label** (de) | Beiname, Gattungsname, Titulatur, Territorium |
| **Label** (en) | Epithet, generic name, title or territory |
| **Description** (de) | Beiname einer Person, bei dem es sich um ein Epitheton, Titel oder eine Ortsassoziation handelt. |
| **Description** (en) | - |
| **Contained by** | [`gndo:preferredName`](#person-record-preferredName)  [`gndo:variantName`](#person-record-variantName) |

***Content Model***  
```xml
<content>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e4262"></a>
#### Beruf oder Beschäftigung
|     |     |
| --- | --- |
| **Name** | `gndo:professionOrOccupation`  |
| **See also** | [gndo:professionOrOccupation](https://d-nb.info/standards/elementset/gnd#id-f1989b1c523444e9b4e15338ce373ab5) |
| **Label** (de) | Beruf oder Beschäftigung |
| **Label** (en) | Profession or occupation |
| **Description** (de) | Angabe zum Beruf, Tätigkeitsbereich o.Ä., der dokumentierten Person. Zulässige Deskriptoren stammen aus der GND-Systematik und entprechen GND-URIs von Sachbegriffen, z.B. https://d-nb.info/gnd/4168391-2 ("Lyriker"). |
| **Description** (en) | A profession or occupation practiced by a person or family |
| **Attributes** | [`@agency`](#d17e5568)  [`@enriched`](#d17e5620)  [`@gndo:ref`](#d17e5439)  [`@gndo:term`](#d17e5459)  [`@gndo:type`](#d17e4294) |
| **Contained by** | [`person`](#person-record) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref" required="true"/>
   <attribute name="gndo:term"/>
   <attribute name="gndo:type"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e4511"></a>
#### Beziehung
|     |     |
| --- | --- |
| **Name** | `gndo:relatesTo`  |
| **Label** (de) | Beziehung |
| **Label** (en) | Relates to |
| **Description** (de) | Beziehung der beschriebenen Entität zu einer anderen Entität, entweder in der GND oder in der gleichen entityXML Ressource. Diese Property hat keine direkte Entsprechung in der GNDO! |
| **Description** (en) | Relation to another Entity in the GND or in the current entityXML resource. This property doesn't have a direct equivalent in the GNDO! |
| **Attributes** | [`@agency`](#d17e5568)  [`@enriched`](#d17e5620)  [`@gndo:ref`](#d17e5439)  [`@gndo:type`](#d17e4534)[`@ref`](#d17e5735)   |
| **Contained by** | [`corporateBody`](#corporate-body-record)  [`entity`](#entity-record)  [`event`](#d17e243)  [`expression`](#d17e1235)  [`manifestation`](#d17e1304)  [`person`](#person-record)  [`place`](#d17e1061)  [`work`](#d17e1378) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <choice>
      <attribute name="gndo:ref" required="true"/>
      <attribute name="ref" required="true"/>
   </choice>
   <attribute name="gndo:type" required="true"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e2985"></a>
#### Biographische Angaben
|     |     |
| --- | --- |
| **Name** | `gndo:biographicalOrHistoricalInformation`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#id-feb84240ebb4724e71ceb35c74b63b13](https://d-nb.info/standards/elementset/gnd#id-feb84240ebb4724e71ceb35c74b63b13) |
| **Label** (de) | Biographische Angaben |
| **Label** (en) | bibliographic or historical Information |
| **Description** (de) | Biographische oder historische Angaben über die Entität. |
| **Description** (en) | - |
| **Attributes** | [`@agency`](#d17e5568)  [`@enriched`](#d17e5620)  [`@xml:lang`](#d17e5550) |
| **Contained by** | [`corporateBody`](#corporate-body-record)  [`entity`](#entity-record)  [`event`](#d17e243)  [`expression`](#d17e1235)  [`manifestation`](#d17e1304)  [`person`](#person-record)  [`place`](#d17e1061)  [`work`](#d17e1378) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="xml:lang"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="data-section"></a>
#### Daten Bereich
|     |     |
| --- | --- |
| **Name** | `data`  |
| **Label** (de) | Daten Bereich |
| **Label** (en) | data area |
| **Description** (de) | - |
| **Description** (en) | - |
| **Contained by** | [`collection`](#collection) |
| **May contain** | [`list`](#data-list) |

***Content Model***  
```xml
<content>
   <element name="list" required="true" repeatable="true"/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="data-provider"></a>
#### Datenprovider
|     |     |
| --- | --- |
| **Name** | `provider`  |
| **Label** (de) | Datenprovider |
| **Label** (en) | Dataprovider |
| **Description** (de) | Informationen zum Datenprovider der Datensammlung und der in diesem Rahmen dokumentierten Entitäten. |
| **Description** (en) | Information about the dataprovider of the datacollection and the contained entities. |
| **Attributes** | [`@id`](#d17e4983)   |
| **Contained by** | [`metadata`](#collection-metadata) |
| **May contain** | [`abstract`](#d17e4841)  [`respStmt`](#respStmt)[`title`](#d17e5317)   |

***Content Model***  
```xml
<content>
   <attribute name="id" required="true"/>
   <anyOrder>
      <element name="title" required="true"/>
      <element name="abstract" required="true"/>
      <element name="respStmt" repeatable="true"/>
   </anyOrder>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="collection"></a>
#### Datensammlung
|     |     |
| --- | --- |
| **Name** | `collection`  |
| **Label** (de) | Datensammlung |
| **Label** (en) | Data Collection |
| **Description** (de) | Eine Datensammlung, die Entity Records enthält. Eine Datensammlung kann in einer oder mehrerer Listen organisisert sein, um die Sammlung weiter zu strukturieren. |
| **Description** (en) | A Datacollection containing Entity records. A datacollection can be organised using one or many lists to provide a more detailed structure to the collection. |
| **Contained by** | [`entityXML`](#entity-xml) |
| **May contain** | [`data`](#data-section)[`metadata`](#collection-metadata)   |

***Content Model***  
```xml
<content>
   <element name="metadata" required="true"/>
   <element name="data" required="true"/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e2619"></a>
#### Datum
|     |     |
| --- | --- |
| **Name** | `date`  |
| **See also** | [dc:date](http://purl.org/dc/elements/1.1/date) |
| **Label** (de) | Datum |
| **Label** (en) | Date |
| **Description** (de) | Ein Zeitpunkt oder Zeitraum, der mit einem Ereignis im Lebenszyklus der beschriebenen Ressource in Zusammenhang steht. |
| **Description** (en) | A point or period of time associated with an event in the lifecycle of the resource. |
| **Contained by** | [`gndo:publication`](#d17e4368) |

***Content Model***  
```xml
<content>
   <choice/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="entity-xml"></a>
#### EntityXML Ressource
|     |     |
| --- | --- |
| **Name** | `entityXML`  |
| **Label** (de) | EntityXML Ressource |
| **Label** (en) | EntityXML Resource |
| **Description** (de) | Eine entityXML Ressource, die eine Datensammlung oder eine Projektbeschreibung enthält. |
| **Description** (en) | - |
| **Contained by** |  |
| **May contain** | [`collection`](#collection)  [`store:store`](#d17e5232) |

***Content Model***  
```xml
<content>
   <choice>
      <element name="collection" required="true"/>
   </choice>
   <element name="store:store"/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="entity-record"></a>
#### Entität (Record)
|     |     |
| --- | --- |
| **Name** | `entity`  |
| **Label** (de) | Entität (Record) |
| **Label** (en) | Entity (Record) |
| **Description** (de) | Ein Entitätsdatensatz zur potenziellen Ansetzung in der Gemeinsamen Normdatei (GND). `entity` ermöglicht es zudem, ganze Datensätze eines fremden Namespaces zu integrieren. |
| **Description** (en) | - |
| **Attributes** | [`@agency`](#attr.record.agency)  [`@enrich`](#d17e5700)  [`@gndo:type`](#d17e191)  [`@gndo:uri`](#d17e5417)  [`@xml:id`](#d17e5720)   |
| **Contained by** | [`list`](#data-list) |
| **May contain** | [`<anyElement.broad>`](#any-all)[`<anyElement.narrow>`](#any-restricted)  [`dc:title`](#d17e2645)  [`dublicateGndIdentifier`](#dublicates)  [`gndo:biographicalOrHistoricalInformation`](#d17e2985)  [`gndo:geographicAreaCode`](#elem.gndo.geographicAreaCode)  [`gndo:gndIdentifier`](#d17e3593)  [`gndo:gndSubjectCategory`](#d17e3634)  [`gndo:homepage`](#elem.gndo.homepage)  [`gndo:languageCode`](#d17e3736)  [`gndo:preferredName`](#d17e4196)  [`gndo:relatesTo`](#d17e4511)  [`gndo:variantName`](#elem.gndo.variantName.standard)  [`owl:sameAs`](#elem.owl.sameAs)  [`ref`](#elem.ref)  [`revision`](#revision)  [`skos:note`](#elem.skos.note)  [`source`](#elem.source)   |

***Content Model***  
```xml
<content>
   <attribute name="xml:id" required="true"/>
   <attribute name="gndo:uri"/>
   <attribute name="agency"/>
   <attribute name="enrich"/>
   <attribute name="gndo:type" required="true"/>
   <choice>
      <group>
         <anyOrder>
            <element name="gndo:homepage" repeatable="true"/>
            <element name="gndo:preferredName"/>
            <element name="gndo:variantName" repeatable="true"/>
            <element name="dc:title"/>
            <element name="gndo:biographicalOrHistoricalInformation" repeatable="true"/>
            <element name="dublicateGndIdentifier" repeatable="true"/>
            <element name="gndo:geographicAreaCode" repeatable="true"/>
            <element name="gndo:gndIdentifier" repeatable="true"/>
            <element name="gndo:gndSubjectCategory" repeatable="true"/>
            <element name="gndo:languageCode" repeatable="true"/>
            <element name="gndo:relatesTo" repeatable="true"/>
            <element name="ref" repeatable="true"/>
            <element name="owl:sameAs" repeatable="true"/>
            <element name="skos:note" repeatable="true"/>
            <element name="source" repeatable="true"/>
            <anyElement type="narrow" repeatable="true"/>
            <text/>
         </anyOrder>
         <element name="revision"/>
      </group>
      <anyElement type="broad" required="true"/>
   </choice>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e3174"></a>
#### Erscheinungszeit
|     |     |
| --- | --- |
| **Name** | `gndo:dateOfPublication`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#id-d8c4d7ac6cada2ffd5ccf1cfcc93ab9a](https://d-nb.info/standards/elementset/gnd#id-d8c4d7ac6cada2ffd5ccf1cfcc93ab9a) |
| **Label** (de) | Erscheinungszeit |
| **Label** (en) | Date of publication |
| **Description** (de) | Erscheinungsdatum der ersten Ausdrucksform des Werks. |
| **Description** (en) | Date of publication of the first expression of a work. |
| **Attributes** | [`@agency`](#d17e5568)  [`@enriched`](#d17e5620)  [`@iso-date`](#attr.iso-date) |
| **Contained by** | [`work`](#d17e1378) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="iso-date" required="true"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e243"></a>
#### Event
|     |     |
| --- | --- |
| **Name** | `event`  |
| **See also** | [gndo:ConferenceOrEvent](https://d-nb.info/standards/elementset/gnd#id-50ee5a233a390c91e26b0d72b0a9a437) |
| **Label** (de) | Event |
| **Label** (en) | Event |
| **Description** (de) | NOCH NICHT IMPLEMENTIERT! |
| **Description** (en) | NOT YET IMPLEMENTED! |
| **Attributes** | [`@agency`](#attr.record.agency)  [`@enrich`](#d17e5700)  [`@gndo:uri`](#d17e5417)  [`@xml:id`](#d17e5720)   |
| **Contained by** | [`list`](#data-list) |
| **May contain** | [`<anyElement.narrow>`](#any-restricted)  [`dc:title`](#d17e2645)  [`dublicateGndIdentifier`](#dublicates)  [`gndo:abbreviatedName`](#d17e2856)  [`gndo:biographicalOrHistoricalInformation`](#d17e2985)  [`gndo:geographicAreaCode`](#elem.gndo.geographicAreaCode)  [`gndo:gndIdentifier`](#d17e3593)  [`gndo:gndSubjectCategory`](#d17e3634)  [`gndo:homepage`](#elem.gndo.homepage)  [`gndo:languageCode`](#d17e3736)  [`gndo:relatesTo`](#d17e4511)  [`gndo:temporaryName`](#d17e4681)  [`owl:sameAs`](#elem.owl.sameAs)  [`ref`](#elem.ref)  [`revision`](#revision)[`skos:note`](#elem.skos.note)  [`source`](#elem.source)   |

***Content Model***  
```xml
<content>
   <attribute name="xml:id" required="true"/>
   <attribute name="gndo:uri"/>
   <attribute name="agency"/>
   <attribute name="enrich"/>
   <anyOrder>
      <element name="gndo:abbreviatedName" repeatable="true"/>
      <element name="gndo:homepage" repeatable="true"/>
      <element name="gndo:temporaryName" repeatable="true"/>
      <element name="dc:title"/>
      <element name="gndo:biographicalOrHistoricalInformation" repeatable="true"/>
      <element name="dublicateGndIdentifier" repeatable="true"/>
      <element name="gndo:geographicAreaCode" repeatable="true"/>
      <element name="gndo:gndIdentifier" repeatable="true"/>
      <element name="gndo:gndSubjectCategory" repeatable="true"/>
      <element name="gndo:languageCode" repeatable="true"/>
      <element name="gndo:relatesTo" repeatable="true"/>
      <element name="ref" repeatable="true"/>
      <element name="owl:sameAs" repeatable="true"/>
      <element name="skos:note" repeatable="true"/>
      <element name="source" repeatable="true"/>
      <anyElement type="narrow" repeatable="true"/>
      <text/>
   </anyOrder>
   <element name="revision"/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e3332"></a>
#### Form des Werks und der Expression
|     |     |
| --- | --- |
| **Name** | `gndo:formOfWorkAndExpression`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#id-5c657a9503d0539ac24ff56d65a12e15](https://d-nb.info/standards/elementset/gnd#id-5c657a9503d0539ac24ff56d65a12e15) |
| **Label** (de) | Form des Werks und der Expression |
| **Label** (en) | Form of work and expression |
| **Description** (de) | ... |
| **Description** (en) | - |
| **Attributes** | [`@agency`](#d17e5568)  [`@enriched`](#d17e5620)  [`@gndo:ref`](#d17e5439)  [`@ref`](#d17e5735) |
| **Contained by** | [`work`](#d17e1378) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e3460"></a>
#### Funktion oder Rolle
|     |     |
| --- | --- |
| **Name** | `gndo:functionOrRole`  |
| **See also** | [gndo:functionOrRole](https://d-nb.info/standards/elementset/gnd#id-1dfb87c1d34f29969c315a7bb09990c5) |
| **Label** (de) | Funktion oder Rolle |
| **Label** (de) | Function or role |
| **Description** (de) | - |
| **Description** (de) | - |
| **Attributes** | [`@agency`](#d17e5568)  [`@enriched`](#d17e5620)  [`@gndo:term`](#d17e5459) |
| **Contained by** | [`person`](#person-record) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:term"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="agency-stmt"></a>
#### GND-Agentur
|     |     |
| --- | --- |
| **Name** | `agency`  |
| **Label** (de) | GND-Agentur |
| **Label** (en) | GND-agency |
| **Description** (de) | Informationen zur GND-Agentur, die die Datensammlung bearbeitet. |
| **Description** (en) | Information about the GND-agency working on the datacollection. |
| **Attributes** | [`@isil`](#d17e5379)   |
| **Contained by** | [`metadata`](#collection-metadata) |
| **May contain** | [`abstract`](#d17e4841)  [`respStmt`](#respStmt)[`title`](#d17e5317)   |

***Content Model***  
```xml
<content>
   <attribute name="isil" required="true"/>
   <anyOrder>
      <element name="title" required="true"/>
      <element name="abstract"/>
      <element name="respStmt" repeatable="true"/>
   </anyOrder>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e3593"></a>
#### GND-Identifier
|     |     |
| --- | --- |
| **Name** | `gndo:gndIdentifier`  |
| **Label** (de) | GND-Identifier |
| **Label** (en) | GND-Identifier |
| **Description** (de) | Der GND-Identifier eines in der GND eingetragenen Normdatensatzes sofern vorhanden |
| **Description** (en) | - |
| **Attributes** | [`@agency`](#d17e5568)  [`@cert`](#d17e5339)[`@enriched`](#d17e5620)   |
| **Contained by** | [`corporateBody`](#corporate-body-record)  [`entity`](#entity-record)  [`event`](#d17e243)  [`expression`](#d17e1235)  [`manifestation`](#d17e1304)  [`person`](#person-record)  [`place`](#d17e1061)  [`work`](#d17e1378) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="cert"/>
   <text/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="gndo.gndIdentifier" role="error">
      <sch:assert test="matches(./text(), '^([\w\d-])+$')">
         <sch:name/>(Invalid content): <sch:name/> must contain just one single GND-ID (eg. "118602802" or "2148150-7")!</sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="dublicates"></a>
#### GND-Identifier einer Dublette
|     |     |
| --- | --- |
| **Name** | `dublicateGndIdentifier`  |
| **Label** (de) | GND-Identifier einer Dublette |
| **Label** (en) | GND-Identifier of a dublicate |
| **Description** (de) | Der GND-Identifier einer möglichen Dublette. |
| **Description** (en) | - |
| **Attributes** | [`@agency`](#d17e5568)  [`@cert`](#d17e5339)[`@enriched`](#d17e5620)   |
| **Contained by** | [`corporateBody`](#corporate-body-record)  [`entity`](#entity-record)  [`event`](#d17e243)  [`expression`](#d17e1235)  [`manifestation`](#d17e1304)  [`person`](#person-record)  [`place`](#d17e1061)  [`work`](#d17e1378) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="cert"/>
   <text/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="entity:dublicateGndIdentifier" role="error">
      <sch:assert test="matches(./text(), '^([\w\d-])+$')">
         <sch:name/>(Invalid content): <sch:name/> must contain a valid GND-ID (eg. "118602802" or "2148150-7")!</sch:assert>
      <sch:assert test="parent::element()[@gndo:uri]">
         <sch:name/>(Missing GND-URI): It is mandatory to provide a GND-URI of the Entity recorded if <sch:name/> is used!</sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e3634"></a>
#### GND-Sachgruppe
|     |     |
| --- | --- |
| **Name** | `gndo:gndSubjectCategory`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#id-2677bed05208b15598ac658e68a09da7](https://d-nb.info/standards/elementset/gnd#id-2677bed05208b15598ac658e68a09da7) |
| **Label** (de) | GND-Sachgruppe |
| **Label** (en) | GND subject category |
| **Description** (de) | (GND-Sachgruppe) |
| **Description** (en) | - |
| **Attributes** | [`@agency`](#d17e5568)  [`@enriched`](#d17e5620)  [`@gndo:term`](#d17e5459) |
| **Contained by** | [`corporateBody`](#corporate-body-record)  [`entity`](#entity-record)  [`event`](#d17e243)  [`expression`](#d17e1235)  [`manifestation`](#d17e1304)  [`person`](#person-record)  [`place`](#d17e1061)  [`work`](#d17e1378) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:term" required="true"/>
   <text/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="gndo:gndSubjectCategory/@gndo:term" role="error">
      <sch:assert test="matches(., '(http|https)://d-nb.info/standards/vocab/gnd/gnd-sc#(.+)')">
                        (Missing or wrong vocabulary term): It is mandatory to provide an URI Descriptor from the [GND Sachgruppen Vocabulary](https://d-nb.info/standards/vocab/gnd/gnd-sc)!
                    </sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="person-record-dateOfBirth"></a>
#### Geburtsdatum (Person)
|     |     |
| --- | --- |
| **Name** | `gndo:dateOfBirth`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#id-8af5e67ec96dd1a475c480b9fb1302cf](https://d-nb.info/standards/elementset/gnd#id-8af5e67ec96dd1a475c480b9fb1302cf) |
| **Label** (de) | Geburtsdatum (Person) |
| **Label** (en) | Date of birth (Person) |
| **Description** (de) | Das Geburtsdatum der Person. |
| **Description** (en) | The birth date of a person. This can be a year, a year-month combination or a full date. |
| **Attributes** | [`@agency`](#d17e5568)  [`@enriched`](#d17e5620)  [`@iso-date`](#attr.iso-date) |
| **Contained by** | [`person`](#person-record) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="iso-date" required="true"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="person-record-placeOfBirth"></a>
#### Geburtsort (Person)
|     |     |
| --- | --- |
| **Name** | `gndo:placeOfBirth`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#id-5eaf40a63425b885f58c69694a6a8bf3](https://d-nb.info/standards/elementset/gnd#id-5eaf40a63425b885f58c69694a6a8bf3) |
| **Label** (de) | Geburtsort (Person) |
| **Label** (en) | Place of Birth (Person) |
| **Description** (de) | Der Geburtsort der Person. Über `@gndo:ref` kann ein Deskriptor (GND-URI) aus der GND erfasst werden. Steht kein GND-URI zur Verfügung erfolgt die Erfassung des Ortes als Freitext im Element. |
| **Description** (en) | - |
| **Attributes** | [`@agency`](#d17e5568)  [`@enriched`](#d17e5620)  [`@gndo:ref`](#d17e5439)  [`@ref`](#d17e5735) |
| **Contained by** | [`person`](#person-record) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <text/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="gndo:placeOfBirth">
      <sch:assert test="@gndo:ref" role="warning">[WARN] (Missing GND-URI): It is recommended to provide a GND-URI for `<sch:name/>` via `@gndo:ref`!</sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e1061"></a>
#### Geografikum
|     |     |
| --- | --- |
| **Name** | `place`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#id-a708231e5f676cb2d2aed717ac50d3c0](https://d-nb.info/standards/elementset/gnd#id-a708231e5f676cb2d2aed717ac50d3c0) |
| **Label** (de) | Geografikum |
| **Label** (en) | Place or geographic name |
| **Description** (de) | - |
| **Description** (en) | - |
| **Attributes** | [`@agency`](#attr.record.agency)  [`@enrich`](#d17e5700)  [`@gndo:type`](#d17e1167)  [`@gndo:uri`](#d17e5417)  [`@xml:id`](#d17e5720)   |
| **Contained by** | [`list`](#data-list) |
| **May contain** | [`<anyElement.narrow>`](#any-restricted)  [`dc:title`](#d17e2645)  [`dublicateGndIdentifier`](#dublicates)  [`geo:hasGeometry`](#d17e2764)  [`gndo:abbreviatedName`](#d17e2856)  [`gndo:biographicalOrHistoricalInformation`](#d17e2985)  [`gndo:dateOfEstablishment`](#d17e3105)  [`gndo:geographicAreaCode`](#elem.gndo.geographicAreaCode)  [`gndo:gndIdentifier`](#d17e3593)  [`gndo:gndSubjectCategory`](#d17e3634)  [`gndo:hierarchicalSuperiorOfPlaceOrGeographicName`](#d17e3673)  [`gndo:homepage`](#elem.gndo.homepage)  [`gndo:languageCode`](#d17e3736)  [`gndo:place`](#geographic-entity-place)  [`gndo:precedingPlaceOrGeographicName`](#d17e4101)  [`gndo:preferredName`](#d17e4196)  [`gndo:relatesTo`](#d17e4511)  [`gndo:succeedingPlaceOrGeographicName`](#d17e4626)  [`gndo:temporaryName`](#d17e4681)  [`gndo:variantName`](#elem.gndo.variantName.standard)  [`owl:sameAs`](#elem.owl.sameAs)  [`ref`](#elem.ref)  [`revision`](#revision)[`skos:note`](#elem.skos.note)  [`source`](#elem.source)   |

***Content Model***  
```xml
<content>
   <attribute name="gndo:type"/>
   <attribute name="xml:id" required="true"/>
   <attribute name="gndo:uri"/>
   <attribute name="agency"/>
   <attribute name="enrich"/>
   <anyOrder>
      <element name="gndo:dateOfEstablishment"/>
      <element name="gndo:homepage" repeatable="true"/>
      <element name="gndo:abbreviatedName" repeatable="true"/>
      <element name="geo:hasGeometry"/>
      <element name="gndo:hierarchicalSuperiorOfPlaceOrGeographicName"/>
      <element name="gndo:place"/>
      <element name="gndo:precedingPlaceOrGeographicName"/>
      <element name="gndo:preferredName"/>
      <element name="gndo:variantName" repeatable="true"/>
      <element name="gndo:succeedingPlaceOrGeographicName"/>
      <element name="gndo:temporaryName" repeatable="true"/>
      <element name="dc:title"/>
      <element name="gndo:biographicalOrHistoricalInformation" repeatable="true"/>
      <element name="dublicateGndIdentifier" repeatable="true"/>
      <element name="gndo:geographicAreaCode" repeatable="true"/>
      <element name="gndo:gndIdentifier" repeatable="true"/>
      <element name="gndo:gndSubjectCategory" repeatable="true"/>
      <element name="gndo:languageCode" repeatable="true"/>
      <element name="gndo:relatesTo" repeatable="true"/>
      <element name="ref" repeatable="true"/>
      <element name="owl:sameAs" repeatable="true"/>
      <element name="skos:note" repeatable="true"/>
      <element name="source" repeatable="true"/>
      <anyElement type="narrow" repeatable="true"/>
      <text/>
   </anyOrder>
   <element name="revision"/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="entity:place[not(@gndo:uri)]">
      <sch:assert test="gndo:preferredName" role="error">(Preferred Name missing): It is mandatory to provide a `gndo:preferredName` for a "<sch:name/>" Entity if no `@gndo:uri` is provided!</sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e2806"></a>
#### Geographische Breite
|     |     |
| --- | --- |
| **Name** | `wgs84:lat`  |
| **Label** (de) | Geographische Breite |
| **Description** (de) | Geographische Breite in der Dezimalschreibweise. |
| **Attributes** | [`@agency`](#d17e5568)  [`@enriched`](#d17e5620) |
| **Contained by** | [`geo:hasGeometry`](#d17e2764) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e2819"></a>
#### Geographische Länge
|     |     |
| --- | --- |
| **Name** | `wgs84:long`  |
| **Label** (de) | Geographische Länge |
| **Description** (de) | Geographische Länge in der Dezimalschreibweise. |
| **Attributes** | [`@agency`](#d17e5568)  [`@enriched`](#d17e5620) |
| **Contained by** | [`geo:hasGeometry`](#d17e2764) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="elem.gndo.geographicAreaCode"></a>
#### Geographischer Schwerpunkt
|     |     |
| --- | --- |
| **Name** | `gndo:geographicAreaCode`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#id-71a01b3adb2078ad01b4aba942e9ca4f](https://d-nb.info/standards/elementset/gnd#id-71a01b3adb2078ad01b4aba942e9ca4f) |
| **Label** (de) | Geographischer Schwerpunkt |
| **Label** (en) | Geographic Area Code |
| **Description** (de) | Ländercode(s) der Staat(en), denen die Entität zugeordnet werden kann (z.B. Lebensmittelpunkt bzw. Schwerpunkt ihres Wirkens bei Personen). Um die Datenqualität zu erhöhen und eine automatisierte Verarbeitung zu erleichtern, muss via `@gndo:term` ein Deskriptor aus dem GND Area Code Vokabular (https://d-nb.info/standards/vocab/gnd/geographic-area-code) angegeben werden! |
| **Description** (en) | - |
| **Attributes** | [`@agency`](#d17e5568)  [`@enriched`](#d17e5620)  [`@gndo:term`](#d17e5403) |
| **Contained by** | [`corporateBody`](#corporate-body-record)  [`entity`](#entity-record)  [`event`](#d17e243)  [`expression`](#d17e1235)  [`manifestation`](#d17e1304)  [`person`](#person-record)  [`place`](#d17e1061)  [`work`](#d17e1378) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:term" required="true"/>
   <text/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
                <!--<sch:rule context="gndo:geographicAreaCode" role="error">
                    <sch:assert test="@gndo:term or @term">
                        (Missing Code): It is mandatory to provide either a `@gndo:term` or a custom code via `@code` on `<sch:name/>`!
                    </sch:assert>
                </sch:rule>-->
                <!--<sch:rule context="gndo:geographicAreaCode/@term" role="warning">
                    <sch:assert test=". = 'ZZ'">
                        (Custom Terms will be ignored): Custom Terms in `@<sch:name/>` on gndo:geographicAreaCode will be handled by the GND-Agency as "unknown Geographic Area"!
                    </sch:assert>
                </sch:rule>-->
   <sch:rule context="gndo:geographicAreaCode/@gndo:term" role="error">
      <sch:assert test="matches(., '(http|https)://d-nb.info/standards/vocab/gnd/geographic-area-code#(.+)')">
                        (Missing vocabulary term): It is mandatory to provide an URI Descriptor from the [GND Area Code Vocabulary](https://d-nb.info/standards/vocab/gnd/geographic-area-code).
                    </sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e2764"></a>
#### Geometry eines geographischen Features
|     |     |
| --- | --- |
| **Name** | `geo:hasGeometry`  |
| **See also** | [https://opengeospatial.github.io/ogc-geosparql/geosparql11/spec.html#_property_geohasgeometry](https://opengeospatial.github.io/ogc-geosparql/geosparql11/spec.html#_property_geohasgeometry) |
| **Label** (de) | Geometry eines geographischen Features |
| **Label** (en) | geometry of a geographic feature |
| **Attributes** | [`@geo:source`](#d17e2788)   |
| **Contained by** | [`place`](#d17e1061) |
| **May contain** | [`wgs84:lat`](#d17e2806)  [`wgs84:long`](#d17e2819) |

***Content Model***  
```xml
<content>
   <attribute name="geo:source"/>
   <element name="wgs84:lat" required="true"/>
   <element name="wgs84:long" required="true"/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="geo:hasGeometry">
      <sch:assert test="@geo:source" role="warning">(Missing source): It is strongly recommended to provide an URL to the source of the encoded coordinates!</sch:assert>
   </sch:rule>
</sch:pattern>
 
```

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="wgs84:long | wgs84:lat">
      <sch:assert test="matches(text(), '(\+|\-)?\d{1,3}\.\d{5,6}')" role="error">(Wrong value encoding): The value of "<sch:name/>" must match '(\+|\-)?\d{1,3}\.\d{5,6}'! </sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e3491"></a>
#### Geschlecht
|     |     |
| --- | --- |
| **Name** | `gndo:gender`  |
| **See also** | [gndo:gender](https://d-nb.info/standards/elementset/gnd#id-f2abc8a45e359fd8e012d380966c7cb3) |
| **Label** (de) | Geschlecht |
| **Label** (en) | gender |
| **Description** (de) | Angaben zum Geschlecht einer Person. |
| **Description** (en) | - |
| **Attributes** | [`@agency`](#d17e5568)  [`@enriched`](#d17e5620)  [`@gndo:term`](#d17e3513) |
| **Contained by** | [`person`](#person-record) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:term" required="true"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="elem.owl.sameAs"></a>
#### Gleiche Entität
|     |     |
| --- | --- |
| **Name** | `owl:sameAs`  |
| **See also** | [https://www.w3.org/TR/owl-ref/#sameAs-def](https://www.w3.org/TR/owl-ref/#sameAs-def) |
| **Label** (de) | Gleiche Entität |
| **Label** (en) | Same as |
| **Description** (de) | HTTP-URI der selben Entität in einem anderen Normdatendienst. |
| **Description** (en) | links an individual to an individual. This statement indicates that two URI references actually refer to the same thing: the individuals have the same "identity". |
| **Attributes** | [`@agency`](#d17e5568)  [`@enriched`](#d17e5620) |
| **Contained by** | [`corporateBody`](#corporate-body-record)  [`entity`](#entity-record)  [`event`](#d17e243)  [`expression`](#d17e1235)  [`manifestation`](#d17e1304)  [`person`](#person-record)  [`place`](#d17e1061)  [`work`](#d17e1378) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e3133"></a>
#### Gründungs- und Auflösungsdatum
|     |     |
| --- | --- |
| **Name** | `gndo:dateOfEstablishmentAndTermination`  |
| **See also** | [gndo:dateOfEstablishment](https://d-nb.info/standards/elementset/gnd#id-9683c9bd5be4e0cfe0e44b28dfaca2a4) |
| **Label** (de) | Gründungs- und Auflösungsdatum |
| **Label** (en) | Date of establishment and termination |
| **Description** (de) | Zeitraum, innerhalb dessen die beschriebene Entität bestand. |
| **Description** (en) | - |
| **Attributes** | [`@agency`](#d17e5568)  [`@enriched`](#d17e5620)  [`@iso-from`](#attr.iso-from)  [`@iso-to`](#attr.iso-to) |
| **Contained by** | [`corporateBody`](#corporate-body-record) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="iso-from"/>
   <attribute name="iso-to"/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="gndo:dateOfEstablishment">
      <sch:assert test="@iso-from and @iso-to" role="error">(start and end date missing): It is mandatory to provide a star- and endpoint in time to encode a timespan like `<sch:name/>`!</sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e3105"></a>
#### Gründungsdatum
|     |     |
| --- | --- |
| **Name** | `gndo:dateOfEstablishment`  |
| **See also** | [gndo:dateOfEstablishment](https://d-nb.info/standards/elementset/gnd#id-9683c9bd5be4e0cfe0e44b28dfaca2a4) |
| **Label** (de) | Gründungsdatum |
| **Label** (en) | Date of establishment |
| **Description** (de) | Das Datum, an dem die beschriebene Entität entstanden ist bzw. gegründet wurde. |
| **Description** (en) | - |
| **Attributes** | [`@agency`](#d17e5568)  [`@enriched`](#d17e5620)  [`@iso-date`](#attr.iso-date) |
| **Contained by** | [`corporateBody`](#corporate-body-record)  [`place`](#d17e1061)  [`work`](#d17e1378) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="iso-date" required="true"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e3277"></a>
#### Herausgeber
|     |     |
| --- | --- |
| **Name** | `gndo:editor`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#id-c115e6fa70ea2ab59e0200caf2fd0129](https://d-nb.info/standards/elementset/gnd#id-c115e6fa70ea2ab59e0200caf2fd0129) |
| **Label** (de) | Herausgeber |
| **Label** (en) | Editor |
| **Description** (de) | ... |
| **Description** (en) | A person, family, or organization contributing to a resource by revising or elucidating the content, e.g., adding an introduction, notes, or other critical matter. An editor may also prepare a resource for production, publication, or distribution. For major revisions, adaptations, etc., that substantially change the nature and content of the original work, resulting in a new work, see author. |
| **Attributes** | [`@agency`](#d17e5568)  [`@enriched`](#d17e5620)  [`@gndo:ref`](#d17e5439)  [`@ref`](#d17e5735) |
| **Contained by** | [`work`](#d17e1378) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e3673"></a>
#### Hierarchisch übergeordnete geographische Einheit
|     |     |
| --- | --- |
| **Name** | `gndo:hierarchicalSuperiorOfPlaceOrGeographicName`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#id-cffe76ad73086417b37df3e58bfd9854](https://d-nb.info/standards/elementset/gnd#id-cffe76ad73086417b37df3e58bfd9854) |
| **Label** (de) | Hierarchisch übergeordnete geographische Einheit |
| **Label** (en) | Hierarchical superior of place or geographic name |
| **Description** (de) | Eine übergeordnete geographische Einheit (z.B. Körperschaft, Rechtssprechung) der beschriebenen Entität. |
| **Description** (en) | A hierarchically superordinate unit (corporate body, conference, jurisdiction) of the described unit (corporate body, conference, jurisdiction). |
| **Attributes** | [`@agency`](#d17e5568)  [`@enriched`](#d17e5620) |
| **Contained by** | [`place`](#d17e1061) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="elem.gndo.homepage"></a>
#### Homepage (GNDO)
|     |     |
| --- | --- |
| **Name** | `gndo:homepage`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#id-f406444d65129801d67c7d86a4460162](https://d-nb.info/standards/elementset/gnd#id-f406444d65129801d67c7d86a4460162) |
| **Label** (de) | Homepage (GNDO) |
| **Label** (en) | homepage (GNDO) |
| **Description** (de) | URL einer Webpräsenz der Person, z.B. von Wikipedia. |
| **Description** (en) | - |
| **Attributes** | [`@agency`](#d17e5568)  [`@enriched`](#d17e5620)  [`@iso-date`](#attr.iso-date) |
| **Contained by** | [`entity`](#entity-record)  [`event`](#d17e243)  [`place`](#d17e1061)  [`work`](#d17e1378) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="iso-date"/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e2596"></a>
#### Homepage (Kontakt)
|     |     |
| --- | --- |
| **Name** | `web`  |
| **Label** (de) | Homepage (Kontakt) |
| **Label** (en) | Homepage (Contact) |
| **Description** (de) | Ein URL zu einer offiziellen oder persönlichen Homepage. |
| **Description** (en) | - |
| **Contained by** | [`contact`](#contact-metadata) |

***Content Model***  
```xml
<content>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="elem.ref"></a>
#### Hyperlink
|     |     |
| --- | --- |
| **Name** | `ref`  |
| **Label** (de) | Hyperlink |
| **Label** (en) | Hyperlink |
| **Description** (de) | Ein Link zu einer Webressource, die weitere Informationen über die im Eintrag beschriebene Entität enthält. Der entsprechende URL wird entweder direkt als Text in `ref` oder via `@target` dokumentiert. |
| **Description** (en) | A link to another web ressource containing information about the described entity. |
| **Attributes** | [`@target`](#d17e5021) |
| **Contained by** | [`corporateBody`](#corporate-body-record)  [`entity`](#entity-record)  [`event`](#d17e243)  [`expression`](#d17e1235)  [`manifestation`](#d17e1304)  [`person`](#person-record)  [`place`](#d17e1061)  [`work`](#d17e1378) |

***Content Model***  
```xml
<content>
   <attribute name="target"/>
   <text/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="entity:ref[not(@target)]" role="error">
      <sch:assert test="matches(text(), '^(https|http)://(\S+)$')">(No explizit URL): The content of `<sch:name/>` is not a HTTP/HTTPS-URL! This is okay, but then you need to provide an HTTP/HTTPS-URL using `@target`!</sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e4565"></a>
#### In Beziehung stehendes Werk
|     |     |
| --- | --- |
| **Name** | `gndo:relatedWork`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#id-3a302d72b633c9edecea55b9e49f66b0](https://d-nb.info/standards/elementset/gnd#id-3a302d72b633c9edecea55b9e49f66b0) |
| **Label** (de) | In Beziehung stehendes Werk |
| **Label** (en) | Related work |
| **Description** (de) | ... |
| **Description** (en) | ... |
| **Attributes** | [`@agency`](#d17e5568)  [`@enriched`](#d17e5620)  [`@gndo:ref`](#d17e5439)  [`@ref`](#d17e5735) |
| **Contained by** | [`work`](#d17e1378) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="instance-of"></a>
#### Instanziierung von
|     |     |
| --- | --- |
| **Name** | `bf:instanceOf`  |
| **See also** | [bf:instantiates](http://bibfra.me/vocab/lite/instantiates) |
| **Label** (de) | Instanziierung von |
| **Label** (en) | Instance of |
| **Description** (de) | Verknüpfung auf ein Werk, das in der dokumentierten Manifestation instanziiert bzw. manifestiert wird. |
| **Description** (en) | Links to a Work the Instance described instantiates or manifests. |
| **Attributes** | [`@agency`](#d17e5568)  [`@enriched`](#d17e5620)  [`@gndo:ref`](#d17e5439)  [`@ref`](#d17e5735) |
| **Contained by** | [`manifestation`](#d17e1304) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <text/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="bf:instanceOf/@ref[starts-with(., '#')]">
      <sch:let name="id" value="substring-after(data(.), '#')"/>
      <sch:let name="target" value="root()//element()[@xml:id = $id]"/>
      <sch:assert test="$target[self::entity:work]" role="error"> Referenced Entity "<sch:value-of select="$id"/>" must be a work but is "<sch:value-of select="$target/name()"/>"! </sch:assert>
      <!--<sch:report test="." role="error"> Referenced Entity "<sch:value-of select="$id"/>" must be a work but is "<sch:value-of select="$target/name()"/>"! </sch:report>-->
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="contact-metadata"></a>
#### Kontaktinformationen
|     |     |
| --- | --- |
| **Name** | `contact`  |
| **Label** (de) | Kontaktinformationen |
| **Label** (en) | Contact |
| **Description** (de) | Informationen, die zur Kontaktaufnahme genutzt werden können. |
| **Description** (en) | - |
| **Contained by** | [`respStmt`](#respStmt) |
| **May contain** | [`address`](#d17e2576)  [`mail`](#d17e2536)  [`phone`](#d17e2556)  [`web`](#d17e2596) |

***Content Model***  
```xml
<content>
   <anyOrder>
      <element name="mail" required="true" repeatable="true"/>
      <element name="phone"/>
      <element name="address"/>
      <element name="web"/>
   </anyOrder>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e2479"></a>
#### Kurzbeschreibung (Sammlung)
|     |     |
| --- | --- |
| **Name** | `abstract`  |
| **Label** (de) | Kurzbeschreibung (Sammlung) |
| **Label** (en) | Abstract (Collection) |
| **Description** (de) | Kurzbeschreibung eines Sammlungsabschnitts. |
| **Description** (en) | - |
| **Contained by** | [`list`](#data-list) |

***Content Model***  
```xml
<content>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="corporate-body-record"></a>
#### Körperschaft
|     |     |
| --- | --- |
| **Name** | `corporateBody`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#id-39ad8031221791a9a0a5adafd88b8ff9](https://d-nb.info/standards/elementset/gnd#id-39ad8031221791a9a0a5adafd88b8ff9) |
| **Label** (de) | Körperschaft |
| **Label** (en) | Corporate Body |
| **Description** (de) | Ein Körperschaftsdatensatz zur potenziellen Ansetzung in der Gemeinsamen Normdatei (GND). |
| **Description** (en) | - |
| **Attributes** | [`@agency`](#attr.record.agency)  [`@enrich`](#d17e5700)  [`@gndo:type`](#d17e586)  [`@gndo:uri`](#d17e5417)  [`@xml:id`](#d17e5720)   |
| **Contained by** | [`list`](#data-list) |
| **May contain** | [`<anyElement.narrow>`](#any-restricted)  [`dc:title`](#d17e2645)  [`dublicateGndIdentifier`](#dublicates)  [`foaf:page`](#elem.foaf.page)  [`gndo:abbreviatedName`](#d17e2856)  [`gndo:affiliation`](#d17e2929)  [`gndo:biographicalOrHistoricalInformation`](#d17e2985)  [`gndo:dateOfEstablishment`](#d17e3105)  [`gndo:dateOfEstablishmentAndTermination`](#d17e3133)  [`gndo:geographicAreaCode`](#elem.gndo.geographicAreaCode)  [`gndo:gndIdentifier`](#d17e3593)  [`gndo:gndSubjectCategory`](#d17e3634)  [`gndo:languageCode`](#d17e3736)  [`gndo:placeOfBusiness`](#d17e3967)  [`gndo:precedingCorporateBody`](#d17e4133)  [`gndo:preferredName`](#d17e4196)  [`gndo:publication`](#d17e4368)  [`gndo:relatesTo`](#d17e4511)  [`gndo:succeedingCorporateBody`](#d17e4596)  [`gndo:temporaryName`](#d17e4681)  [`gndo:variantName`](#elem.gndo.variantName.standard)  [`owl:sameAs`](#elem.owl.sameAs)  [`ref`](#elem.ref)  [`revision`](#revision)[`skos:note`](#elem.skos.note)  [`source`](#elem.source)   |

***Content Model***  
```xml
<content>
   <attribute name="gndo:type"/>
   <attribute name="xml:id" required="true"/>
   <attribute name="gndo:uri"/>
   <attribute name="agency"/>
   <attribute name="enrich"/>
   <anyOrder>
      <element name="foaf:page" repeatable="true"/>
      <element name="gndo:abbreviatedName" repeatable="true"/>
      <element name="gndo:affiliation" repeatable="true"/>
      <element name="gndo:dateOfEstablishment"/>
      <element name="gndo:dateOfEstablishmentAndTermination"/>
      <element name="gndo:placeOfBusiness"/>
      <element name="gndo:precedingCorporateBody"/>
      <element name="gndo:preferredName"/>
      <element name="gndo:publication" repeatable="true"/>
      <element name="gndo:succeedingCorporateBody"/>
      <element name="gndo:temporaryName" repeatable="true"/>
      <element name="gndo:variantName" repeatable="true"/>
      <element name="dc:title"/>
      <element name="gndo:biographicalOrHistoricalInformation" repeatable="true"/>
      <element name="dublicateGndIdentifier" repeatable="true"/>
      <element name="gndo:geographicAreaCode" repeatable="true"/>
      <element name="gndo:gndIdentifier" repeatable="true"/>
      <element name="gndo:gndSubjectCategory" repeatable="true"/>
      <element name="gndo:languageCode" repeatable="true"/>
      <element name="gndo:relatesTo" repeatable="true"/>
      <element name="ref" repeatable="true"/>
      <element name="owl:sameAs" repeatable="true"/>
      <element name="skos:note" repeatable="true"/>
      <element name="source" repeatable="true"/>
      <anyElement type="narrow" repeatable="true"/>
      <text/>
   </anyOrder>
   <element name="revision"/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="entity:corporateBody[not(@gndo:uri)]">
      <sch:assert test="gndo:preferredName" role="error">(Preferred Name missing): It is mandatory to provide a `gndo:preferredName` for a "<sch:name/>" Entity if no `@gndo:uri` is provided!</sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="data-list"></a>
#### Liste (Daten Bereich)
|     |     |
| --- | --- |
| **Name** | `list`  |
| **Label** (de) | Liste (Daten Bereich) |
| **Label** (en) | List (Data Area) |
| **Description** (de) | - |
| **Description** (en) | - |
| **Attributes** | [`@id`](#d17e5475)   |
| **Contained by** | [`data`](#data-section) |
| **May contain** | [`abstract`](#d17e2479)  [`corporateBody`](#corporate-body-record)  [`entity`](#entity-record)  [`event`](#d17e243)  [`expression`](#d17e1235)  [`manifestation`](#d17e1304)  [`person`](#person-record)  [`place`](#d17e1061)  [`title`](#d17e5317)  [`work`](#d17e1378) |

***Content Model***  
```xml
<content>
   <attribute name="id"/>
   <element name="title"/>
   <element name="abstract"/>
   <anyOrder>
      <element name="corporateBody" repeatable="true"/>
      <element name="entity" repeatable="true"/>
      <element name="event" repeatable="true"/>
      <element name="expression" repeatable="true"/>
      <element name="manifestation" repeatable="true"/>
      <element name="person" repeatable="true"/>
      <element name="place" repeatable="true"/>
      <element name="work" repeatable="true"/>
   </anyOrder>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="entity:list/element()[@xml:id][@agency][not(@agency = 'ignore')]"
             role="error">
      <sch:assert test="entity:revision">(Missing revision description): If you are going to exchange your data with a GND Agency you need to provide a revision description for the Entry encoded!</sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e2536"></a>
#### Mailadresse
|     |     |
| --- | --- |
| **Name** | `mail`  |
| **Label** (de) | Mailadresse |
| **Label** (en) | Mailaddress |
| **Description** (de) | Email Adresse zur Kontaktaufnahme. |
| **Description** (en) | - |
| **Contained by** | [`contact`](#contact-metadata) |

***Content Model***  
```xml
<content>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e1304"></a>
#### Manifestation
|     |     |
| --- | --- |
| **Name** | `manifestation`  |
| **Label** (de) | Manifestation |
| **Label** (en) | Manifestation |
| **Description** (de) | Manifestation einer Ausdrucksform eines Werks nach FRBR. |
| **Description** (en) | Manifestation of an Expression of a Work (FRBR). |
| **Attributes** | [`@agency`](#attr.record.agency)  [`@enrich`](#d17e5700)  [`@gndo:uri`](#d17e5417)  [`@xml:id`](#d17e5720)   |
| **Contained by** | [`list`](#data-list) |
| **May contain** | [`<anyElement.narrow>`](#any-restricted)  [`bf:instanceOf`](#instance-of)  [`dc:title`](#d17e2645)  [`dublicateGndIdentifier`](#dublicates)  [`embodimentOf`](#d17e1339)  [`gndo:biographicalOrHistoricalInformation`](#d17e2985)  [`gndo:geographicAreaCode`](#elem.gndo.geographicAreaCode)  [`gndo:gndIdentifier`](#d17e3593)  [`gndo:gndSubjectCategory`](#d17e3634)  [`gndo:languageCode`](#d17e3736)  [`gndo:relatesTo`](#d17e4511)  [`owl:sameAs`](#elem.owl.sameAs)  [`ref`](#elem.ref)  [`revision`](#revision)[`skos:note`](#elem.skos.note)  [`source`](#elem.source)   |

***Content Model***  
```xml
<content>
   <attribute name="xml:id" required="true"/>
   <attribute name="gndo:uri"/>
   <attribute name="agency"/>
   <attribute name="enrich"/>
   <anyOrder>
      <element name="bf:instanceOf"/>
      <element name="embodimentOf"/>
      <element name="dc:title"/>
      <element name="gndo:biographicalOrHistoricalInformation" repeatable="true"/>
      <element name="dublicateGndIdentifier" repeatable="true"/>
      <element name="gndo:geographicAreaCode" repeatable="true"/>
      <element name="gndo:gndIdentifier" repeatable="true"/>
      <element name="gndo:gndSubjectCategory" repeatable="true"/>
      <element name="gndo:languageCode" repeatable="true"/>
      <element name="gndo:relatesTo" repeatable="true"/>
      <element name="ref" repeatable="true"/>
      <element name="owl:sameAs" repeatable="true"/>
      <element name="skos:note" repeatable="true"/>
      <element name="source" repeatable="true"/>
      <anyElement type="narrow" repeatable="true"/>
      <text/>
   </anyOrder>
   <element name="revision"/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="collection-metadata"></a>
#### Metadaten zur Datensammlung
|     |     |
| --- | --- |
| **Name** | `metadata`  |
| **Label** (de) | Metadaten zur Datensammlung |
| **Label** (en) | Data Collection Metadata |
| **Description** (de) | Bereich, in dem grundlegende Metadaten zu eine Datensammlung (z.B. eine Kurzbeschreibung der Sammlung, Angaben zum Datenlieferant, eine Revisionsbeschreibung der Sammlung etc.) dokumentiert werden kann. |
| **Description** (en) | Section where Metadata with regard to the Data Collection is described, eg. a brief description of the collection, information about the data provider, revision descriptions etc. |
| **Contained by** | [`collection`](#collection) |
| **May contain** | [`abstract`](#d17e4841)  [`agency`](#agency-stmt)  [`provider`](#data-provider)  [`respStmt`](#respStmt)  [`revision`](#revision)[`title`](#d17e5317)   |

***Content Model***  
```xml
<content>
   <anyOrder>
      <element name="title" required="true"/>
      <element name="abstract" required="true"/>
      <element name="respStmt" repeatable="true"/>
      <element name="provider" required="true"/>
      <element name="agency"/>
      <element name="revision" required="true"/>
   </anyOrder>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e4596"></a>
#### Nachfolgende Körperschaft (Körperschaft)
|     |     |
| --- | --- |
| **Name** | `gndo:succeedingCorporateBody`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#id-7f18db0698ca58f3697013b77021b18f](https://d-nb.info/standards/elementset/gnd#id-7f18db0698ca58f3697013b77021b18f) |
| **Label** (de) | Nachfolgende Körperschaft (Körperschaft) |
| **Label** (en) | Succeeding corporate body (Corporal Body) |
| **Description** (de) | Eine Nachfolgeinstitution der Körperschaft. |
| **Description** (en) | - |
| **Attributes** | [`@agency`](#d17e5568)  [`@enriched`](#d17e5620)  [`@gndo:ref`](#d17e5439)  [`@ref`](#d17e5735) |
| **Contained by** | [`corporateBody`](#corporate-body-record) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e4626"></a>
#### Nachfolgendes Geografikum
|     |     |
| --- | --- |
| **Name** | `gndo:succeedingPlaceOrGeographicName`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#id-73d2a305ccb4a9ec5a9cafbe6896f210](https://d-nb.info/standards/elementset/gnd#id-73d2a305ccb4a9ec5a9cafbe6896f210) |
| **Label** (de) | Nachfolgendes Geografikum |
| **Label** (en) | Succeeding place or geographic name |
| **Description** (de) | - |
| **Description** (en) | - |
| **Attributes** | [`@agency`](#d17e5568)  [`@enriched`](#d17e5620)  [`@gndo:ref`](#d17e5439)  [`@ref`](#d17e5735) |
| **Contained by** | [`place`](#d17e1061) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e4656"></a>
#### Nachname
|     |     |
| --- | --- |
| **Name** | `gndo:surname`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#id-9b68a0d2d741dbc2eb52518016001a5e](https://d-nb.info/standards/elementset/gnd#id-9b68a0d2d741dbc2eb52518016001a5e) |
| **Label** (de) | Nachname |
| **Label** (en) | Surname |
| **Description** (de) | Der Nach- oder Familienname, unter dem eine Person bekannt ist. |
| **Description** (en) | Last name of a person |
| **Contained by** | [`gndo:preferredName`](#person-record-preferredName)  [`gndo:variantName`](#person-record-variantName) |

***Content Model***  
```xml
<content>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e4906"></a>
#### Name (Verantworlichkeit)
|     |     |
| --- | --- |
| **Name** | `name`  |
| **Label** (de) | Name (Verantworlichkeit) |
| **Label** (en) | Name (Responsibility Statement) |
| **Description** (de) | Name der verantwortlichen Person oder Institution. |
| **Description** (en) | Name of a person or institution taking a certain responsibility with regard to a datacollection. |
| **Contained by** | [`respStmt`](#respStmt) |

***Content Model***  
```xml
<content>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e4237"></a>
#### Namenspräfix
|     |     |
| --- | --- |
| **Name** | `gndo:prefix`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#id-739a1f5f993193e1cd4a4f325f68417c](https://d-nb.info/standards/elementset/gnd#id-739a1f5f993193e1cd4a4f325f68417c) |
| **Label** (de) | Namenspräfix |
| **Label** (en) | Prefix |
| **Description** (de) | Namensteil, der dem Namen vorangestellt sind (z.B. Adelskennzeichnungen, "von", "zu" etc.). |
| **Description** (en) | Part of a name, which is prefixed. |
| **Contained by** | [`gndo:preferredName`](#person-record-preferredName)  [`gndo:variantName`](#person-record-variantName) |

***Content Model***  
```xml
<content>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e3818"></a>
#### Namenszusatz
|     |     |
| --- | --- |
| **Name** | `gndo:nameAddition`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#id-fcf6ee436a2053dc7acd89625d8a0ac7](https://d-nb.info/standards/elementset/gnd#id-fcf6ee436a2053dc7acd89625d8a0ac7) |
| **Label** (de) | Namenszusatz |
| **Label** (en) | Name addition |
| **Description** (de) | Zusätzliches Element des Namens, unter dem eine Person bekannt ist, z.B. "Graf von Wallmoden". |
| **Description** (en) | - |
| **Contained by** | [`gndo:preferredName`](#person-record-preferredName)  [`gndo:variantName`](#person-record-variantName) |

***Content Model***  
```xml
<content>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="geographic-entity-place"></a>
#### Ort
|     |     |
| --- | --- |
| **Name** | `gndo:place`  |
| **See also** | [gndo:place](https://d-nb.info/standards/elementset/gnd#id-6624b427d376fab4e8f2d55266f5d177) |
| **Label** (de) | Ort |
| **Label** (en) | Place |
| **Description** (de) | Ein Land, Staat, Provinz, Ort etc., wo die beschriebene Entität angesiedelt ist. Über `@gndo:ref` kann ein Deskriptor (GND-URI) aus der GND-Systematik für diesen Ort erfasst werden. |
| **Description** (en) | A country, state, province, etc., or place where the entity described is placed. |
| **Attributes** | [`@agency`](#d17e5568)  [`@enriched`](#d17e5620)  [`@gndo:ref`](#d17e5439)  [`@ref`](#d17e5735) |
| **Contained by** | [`place`](#d17e1061) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <text/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="gndo:place">
      <sch:assert test="@gndo:ref" role="warning">[WARN] (Missing GND-URI): It is recommended to provide a GND-URI for the encoded Place in `<sch:name/>` via `@gndo:ref`!</sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="person-record"></a>
#### Person (Record)
|     |     |
| --- | --- |
| **Name** | `person`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#id-07e558e11b6629c85c4cf0312a8d9acc](https://d-nb.info/standards/elementset/gnd#id-07e558e11b6629c85c4cf0312a8d9acc) |
| **Label** (de) | Person (Record) |
| **Label** (en) | Person (Record) |
| **Description** (de) | Ein Personendatensatz zur potenziellen Ansetzung in der Gemeinsamen Normdatei (GND). |
| **Description** (en) | - |
| **Attributes** | [`@agency`](#attr.record.agency)  [`@enrich`](#d17e5700)  [`@gndo:type`](#person-types)  [`@gndo:uri`](#d17e5417)  [`@xml:id`](#d17e5720)   |
| **Contained by** | [`list`](#data-list) |
| **May contain** | [`<anyElement.narrow>`](#any-restricted)  [`dc:title`](#d17e2645)  [`dublicateGndIdentifier`](#dublicates)  [`foaf:page`](#elem.foaf.page)  [`gndo:academicDegree`](#d17e2902)  [`gndo:affiliation`](#d17e2929)  [`gndo:biographicalOrHistoricalInformation`](#d17e2985)  [`gndo:dateOfBirth`](#person-record-dateOfBirth)  [`gndo:dateOfDeath`](#person-record-dateOfDeath)  [`gndo:fieldOfStudy`](#d17e3363)  [`gndo:functionOrRole`](#d17e3460)  [`gndo:gender`](#d17e3491)  [`gndo:geographicAreaCode`](#elem.gndo.geographicAreaCode)  [`gndo:gndIdentifier`](#d17e3593)  [`gndo:gndSubjectCategory`](#d17e3634)  [`gndo:languageCode`](#d17e3736)  [`gndo:periodOfActivity`](#prop-periodOfActivity)  [`gndo:placeOfActivity`](#person-record-placeOfActivity)  [`gndo:placeOfBirth`](#person-record-placeOfBirth)  [`gndo:placeOfDeath`](#person-record-placeOfDeath)  [`gndo:preferredName`](#person-record-preferredName)  [`gndo:professionOrOccupation`](#d17e4262)  [`gndo:pseudonym`](#d17e4321)  [`gndo:publication`](#d17e4368)  [`gndo:relatesTo`](#d17e4511)  [`gndo:titleOfNobility`](#d17e4726)  [`gndo:variantName`](#person-record-variantName)  [`owl:sameAs`](#elem.owl.sameAs)  [`ref`](#elem.ref)  [`revision`](#revision)[`skos:note`](#elem.skos.note)  [`source`](#elem.source)   |

***Content Model***  
```xml
<content>
   <attribute name="gndo:type"/>
   <attribute name="xml:id" required="true"/>
   <attribute name="gndo:uri"/>
   <attribute name="agency"/>
   <attribute name="enrich"/>
   <anyOrder>
      <element name="foaf:page" repeatable="true"/>
      <element name="gndo:academicDegree"/>
      <element name="gndo:affiliation" repeatable="true"/>
      <choice>
         <group>
            <element name="gndo:dateOfBirth"/>
            <element name="gndo:dateOfDeath"/>
         </group>
         <element name="gndo:periodOfActivity"/>
      </choice>
      <element name="gndo:fieldOfStudy" repeatable="true"/>
      <element name="gndo:functionOrRole" repeatable="true"/>
      <element name="gndo:gender"/>
      <element name="gndo:placeOfActivity" repeatable="true"/>
      <element name="gndo:placeOfBirth"/>
      <element name="gndo:placeOfDeath"/>
      <element name="gndo:preferredName"/>
      <element name="gndo:professionOrOccupation" repeatable="true"/>
      <element name="gndo:publication" repeatable="true"/>
      <element name="gndo:pseudonym" repeatable="true"/>
      <element name="gndo:titleOfNobility" repeatable="true"/>
      <element name="gndo:variantName" repeatable="true"/>
      <element name="dc:title"/>
      <element name="gndo:biographicalOrHistoricalInformation" repeatable="true"/>
      <element name="dublicateGndIdentifier" repeatable="true"/>
      <element name="gndo:geographicAreaCode" repeatable="true"/>
      <element name="gndo:gndIdentifier" repeatable="true"/>
      <element name="gndo:gndSubjectCategory" repeatable="true"/>
      <element name="gndo:languageCode" repeatable="true"/>
      <element name="gndo:relatesTo" repeatable="true"/>
      <element name="ref" repeatable="true"/>
      <element name="owl:sameAs" repeatable="true"/>
      <element name="skos:note" repeatable="true"/>
      <element name="source" repeatable="true"/>
      <anyElement type="narrow" repeatable="true"/>
      <text/>
   </anyOrder>
   <element name="revision"/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="entity:person[not(@gndo:uri)]">
      <sch:assert test="gndo:preferredName" role="error">(Preferred Name missing): It is mandatory to provide a `gndo:preferredName` for a "<sch:name/>" Entity if no `@gndo:uri` is provided!</sch:assert>
   </sch:rule>
</sch:pattern>
 
```

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="entity:person[not(@gndo:uri)]">
      <sch:assert test="gndo:geographicAreaCode" role="error">(Geographic Area Code is missing): It is mandatory to provide a `gndo:geographicAreaCode` for a "<sch:name/>" Entity if no `@gndo:uri` is provided!</sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e4075"></a>
#### Persönlicher Name
|     |     |
| --- | --- |
| **Name** | `gndo:personalName`  |
| **See also** | [gndo:personalName](https://d-nb.info/standards/elementset/gnd#id-f3724241883e63cad48b2f63bc27fe46) |
| **Label** (de) | Persönlicher Name |
| **Label** (en) | Personal name |
| **Description** (de) | Ein Name, unter dem die Person gemeinhin bekannt ist. Ein persönlicher Name wird dann angegeben, wenn die Person nicht durch einen Vor- oder Nachname benannt wird, z.B. "Aristoteles" |
| **Description** (en) | - |
| **Contained by** | [`gndo:preferredName`](#person-record-preferredName)  [`gndo:variantName`](#person-record-variantName) |

***Content Model***  
```xml
<content>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e4321"></a>
#### Pseudonym
|     |     |
| --- | --- |
| **Name** | `gndo:pseudonym`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#id-0a2caf81b26b3b75204250f4f16bdc39](https://d-nb.info/standards/elementset/gnd#id-0a2caf81b26b3b75204250f4f16bdc39) |
| **Label** (de) | Pseudonym |
| **Label** (en) | Pseudonym |
| **Description** (de) | Ein Pseudonym, unter dem die dokumentierte Person ebenfalls bekannt ist. |
| **Description** (en) | Links a person's real identity to an identity under which one or more persons act, e. g. write, compose or create art, but that is not the person's real name (i. e. a pseudonym). |
| **Attributes** | [`@agency`](#d17e5568)  [`@enriched`](#d17e5620)  [`@gndo:ref`](#d17e5439)  [`@ref`](#d17e5735) |
| **Contained by** | [`person`](#person-record) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <text/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="gndo:pseudonym">
      <sch:assert test="@gndo:ref or @ref" role="warning">(Missing Reference to Pseudonym Record): It is recommended to provide a GNDO-URI to a GND-Record of this Pseudonym or to a `person[@type='https://d-nb.info/standards/elementset/gnd#Pseudonym']` in this dataset using `@ref`.</sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e4368"></a>
#### Publikation
|     |     |
| --- | --- |
| **Name** | `gndo:publication`  |
| **See also** | [gndo:publication](https://d-nb.info/standards/elementset/gnd#id-73ff3a19c73bb6c5361ab7fa09f1f576) |
| **Label** (de) | Publikation |
| **Label** (en) | publication |
| **Description** (de) | Eine Publikation, die mit der Entität in Zusammenhang steht. Es kann sich hierbei (1) um eigene Titel der Entität (Person), (2) um Titel, die von der Entität (Person) herausgegeben worden sind und auch (3) um Titel über die Entität handeln. |
| **Description** (en) | - |
| **Attributes** | [`@agency`](#d17e5568)  [`@dnb:catalogue`](#d17e4402)  [`@enriched`](#d17e5620)  [`@gndo:ref`](#d17e5439)  [`@ref`](#d17e5735)  [`@role`](#d17e4416)   |
| **Contained by** | [`corporateBody`](#corporate-body-record)  [`person`](#person-record) |
| **May contain** | [`add`](#d17e2202)  [`date`](#d17e2619)[`gndo:firstAuthor`](#d17e3403)  [`title`](#d17e5317)   |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <attribute name="dnb:catalogue"/>
   <attribute name="role"/>
   <anyOrder>
      <choice>
         <anyOrder>
            <element name="gndo:firstAuthor" repeatable="true"/>
            <element name="title" required="true"/>
            <element name="add"/>
            <element name="date" required="true"/>
            <text/>
         </anyOrder>
         <text/>
      </choice>
   </anyOrder>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="gndo:publication">
      <sch:let name="ref" value="substring-after(data(@ref), '#')"/>
      <sch:let name="internal-work-record" value="//entity:work[@xml:id = $ref]"/>
      <sch:assert test="@gndo:ref or @dnb:catalogue or not(empty($internal-work-record))"
                  role="warning">[WARN] To identify a publication it is recommended to provide either a Reference 
                        to a Record in the DNB Catalogue (`@dnb:catalogue`) or an URI of a Work Entity in the GND (`@gndo:ref`).</sch:assert>
   </sch:rule>
</sch:pattern>
 
```

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="gndo:publication[@dnb:catalogue]">
      <sch:report test="@agency='remove'" role="warning">[WARN] (NO GND Data) Your request concerns data that is part of the DNB Catalogue. A GND-Agency doesn't have any access to these datasets.</sch:report>
   </sch:rule>
</sch:pattern>
 
```

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="gndo:publication[not(@role) or @role='author']">
      <sch:report test="gndo:firstAuthor" role="error">(Who is the author?) You didn't use `@role` which implies that the publication's author is actually the entity described! If so you must not use the `author` element again within the publication! Otherwise you need to be explizit about the Role of the entity described using `@role` on the publication!</sch:report>
   </sch:rule>
</sch:pattern>
 
```

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="gndo:publication[@role='about']">
      <sch:assert test="gndo:firstAuthor" role="error">(Missing author) If the publication is ABOUT the entity described you need to encode the primary author of the publication!</sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="elem.source"></a>
#### Quellenangabe
|     |     |
| --- | --- |
| **Name** | `source`  |
| **Label** (de) | Quellenangabe |
| **Label** (en) | Source |
| **Description** (de) | Angaben zu einer Quelle, die verwendet wurde, um die in diesem Eintrag gegebenen Informationen zu recherchieren. |
| **Description** (en) | A source which was used to identitfy the resource. |
| **Attributes** | [`@agency`](#d17e5568)  [`@enriched`](#d17e5620)  [`@url`](#d17e5216) |
| **Contained by** | [`corporateBody`](#corporate-body-record)  [`entity`](#entity-record)  [`event`](#d17e243)  [`expression`](#d17e1235)  [`manifestation`](#d17e1304)  [`person`](#person-record)  [`place`](#d17e1061)  [`work`](#d17e1378) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="url"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="revision"></a>
#### Revisionsbeschreibung
|     |     |
| --- | --- |
| **Name** | `revision`  |
| **Label** (de) | Revisionsbeschreibung |
| **Label** (en) | Revision Description |
| **Description** (de) | Statusinformationen zu einer Ressource (Datensammlung oder einzelner Entitätseintrag), die einzelne Bearbeitungsstände, Fragen oder anderweitige Anmerkungen zur Verarbeitung der entsprechenden Ressource dokumentiert. Die einzelnen Einträge (via `change`) werden ausgehend vom Datum von jung nach alt absteigend sortiert, sprich: Der jüngste Eintrag steht als Erstes in der Revisionsbeschreibung! |
| **Description** (en) | Description containing information about the editing status of the ressource. |
| **Attributes** | [`@status`](#rev-status)   |
| **Contained by** | [`corporateBody`](#corporate-body-record)  [`entity`](#entity-record)  [`event`](#d17e243)  [`expression`](#d17e1235)  [`manifestation`](#d17e1304)  [`metadata`](#collection-metadata)  [`person`](#person-record)  [`place`](#d17e1061)  [`work`](#d17e1378) |
| **May contain** | [`change`](#d17e2229) |

***Content Model***  
```xml
<content>
   <attribute name="status" required="true"/>
   <element name="change" repeatable="true"/>
   <text/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="entity:revision[@status = 'closed']" role="error">
      <sch:assert test="(entity:change[@status])[1][@status = ('published', 'withdrawn')]">(Missing prerequisites): You can't close a resource if it is not at least published or withdrawn!</sch:assert>
   </sch:rule>
</sch:pattern>
 
```

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="entity:revision[@status ='staged']" role="error">
      <sch:assert test="(entity:change[@status])[1][@status=('candidate', 'approved', 'embargoed', 'submitted', 'published')]">(Missing prerequisites): You can't stage a ressource as long as the ressource is not a candidate, approved or embargoed!</sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e2229"></a>
#### Revisionseintrag
|     |     |
| --- | --- |
| **Name** | `change`  |
| **Label** (de) | Revisionseintrag |
| **Label** (en) | Change |
| **Description** (de) | Zusammenfassende Beschreibung der vorgenommenen Änderung an einer Informationsressource (Sammlung oder Eintrag). Der aktuellste Eintrag steht immer am Anfang der Revisionsbeschreibung. |
| **Description** (en) | Summary of the change made to the informationressource. |
| **Attributes** | [`@status`](#change-status)[`@when`](#d17e5776)  [`@who`](#d17e2245)   |
| **Contained by** | [`revision`](#revision) |

***Content Model***  
```xml
<content>
   <attribute name="when" required="true"/>
   <attribute name="who" required="true"/>
   <attribute name="status"/>
   <text/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="entity:change[@when][preceding-sibling::entity:change]"
             role="error">
      <sch:let name="preceeding-change" value="preceding-sibling::entity:change[1]"/>
      <sch:assert test="xs:date($preceeding-change/@when) &gt; xs:date(@when) or xs:date($preceeding-change/@when) = xs:date(@when)">
                        (Youngest always on top!) A change entry of a younger date must be placed on top of older entries aka. the youngest is always the first in the list!
                    </sch:assert>
   </sch:rule>
</sch:pattern>
 
```

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="entity:data//entity:change[@status = ('candidate', 'cleared')]"
             role="warning">
      <sch:assert test="parent::entity:revision/parent::element()[@agency]">
                        [WARN] (Agency Action recommended): It is recommended to provide information on the action, a GND agency is expected to do with regard to the record! So please use `@agency` on the record to choose an action.
                    </sch:assert>
   </sch:rule>
</sch:pattern>
 
```

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="entity:change[@status = ('approved', 'submitted', 'published')]"
             role="error">
      <sch:let name="who-id" value="data(@who)"/>
      <sch:let name="resp-person"
               value="ancestor::entity:collection/entity:metadata//entity:respStmt[@id = $who-id]"/>
      <sch:assert test="$resp-person[parent::entity:agency]">
                        (Missing authority): Sorry <sch:value-of select="$resp-person/entity:name/text()"/>, you don't have permission to set the resources stage to "<sch:value-of select="data(@status)"/>"!
                    </sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e4889"></a>
#### Rolle (Verantwortlichkeit)
|     |     |
| --- | --- |
| **Name** | `resp`  |
| **Label** (de) | Rolle (Verantwortlichkeit) |
| **Label** (en) | Role (Responsibility Statement) |
| **Description** (de) | Eine Rolle, die der Person oder Institution im Rahmen der Bearbeitung einer Datensammlung zugeschrieben wird. |
| **Description** (en) | A role attributed to a person or institution with regard to a datacollection. |
| **Contained by** | [`respStmt`](#respStmt) |

***Content Model***  
```xml
<content>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e3967"></a>
#### Sitz (Körperschaft)
|     |     |
| --- | --- |
| **Name** | `gndo:placeOfBusiness`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#id-c8b65fe65d82aceeb3fd95511a2473e4](https://d-nb.info/standards/elementset/gnd#id-c8b65fe65d82aceeb3fd95511a2473e4) |
| **Label** (de) | Sitz (Körperschaft) |
| **Label** (en) | Place of Business (Corporal Body) |
| **Description** (de) | Der (Haupt)sitz der Körperschaft. |
| **Description** (en) | (Main)place of the Business. |
| **Attributes** | [`@agency`](#d17e5568)  [`@enriched`](#d17e5620)  [`@gndo:ref`](#d17e5439)  [`@ref`](#d17e5735) |
| **Contained by** | [`corporateBody`](#corporate-body-record) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e3736"></a>
#### Sprachraum
|     |     |
| --- | --- |
| **Name** | `gndo:languageCode`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#id-98caeb27b96d893f6910a632c81e948d](https://d-nb.info/standards/elementset/gnd#id-98caeb27b96d893f6910a632c81e948d) |
| **Label** (de) | Sprachraum |
| **Label** (en) | language code |
| **Description** (de) | Code(s) des Sprachraumes, dem die Entität zugeordnet werden kann. Um die Datenqualität zu erhöhen und eine automatisierte Verarbeitung zu erleichtern, kann via @gndo:term zsätzlich ein Deskriptor aus dem LOC ISO 693-2 Language Vokabular (http://id.loc.gov/vocabulary/iso639-2/) angegeben werden! |
| **Description** (en) | - |
| **Attributes** | [`@agency`](#d17e5568)  [`@enriched`](#d17e5620)  [`@gndo:term`](#d17e3757) |
| **Contained by** | [`corporateBody`](#corporate-body-record)  [`entity`](#entity-record)  [`event`](#d17e243)  [`expression`](#d17e1235)  [`manifestation`](#d17e1304)  [`person`](#person-record)  [`place`](#d17e1061)  [`work`](#d17e1378) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:term" required="true"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="person-record-dateOfDeath"></a>
#### Sterbedatum (Person)
|     |     |
| --- | --- |
| **Name** | `gndo:dateOfDeath`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#id-297e19a30370536f0d46cd56e69aa739](https://d-nb.info/standards/elementset/gnd#id-297e19a30370536f0d46cd56e69aa739) |
| **Label** (de) | Sterbedatum (Person) |
| **Label** (en) | Date of death (Person) |
| **Description** (de) | Das Sterbedatum der Person. |
| **Description** (en) | The death date of a person. This can be a year, a year-month combination or a full date. |
| **Attributes** | [`@agency`](#d17e5568)  [`@enriched`](#d17e5620)  [`@iso-date`](#attr.iso-date) |
| **Contained by** | [`person`](#person-record) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="iso-date" required="true"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="person-record-placeOfDeath"></a>
#### Sterbeort (Person)
|     |     |
| --- | --- |
| **Name** | `gndo:placeOfDeath`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#id-b10a5c75f3dc3de4ec82283b66486370](https://d-nb.info/standards/elementset/gnd#id-b10a5c75f3dc3de4ec82283b66486370) |
| **Label** (de) | Sterbeort (Person) |
| **Label** (en) | Place of death (Person) |
| **Description** (de) | Der Sterbeort der Person. Über `@gndo:ref` kann ein Deskriptor (GND-URI) aus der GND erfasst werden. Steht kein GND-URI zur Verfügung erfolgt die Erfassung des Ortes als Freitext im Element. |
| **Description** (en) | - |
| **Attributes** | [`@agency`](#d17e5568)  [`@enriched`](#d17e5620)  [`@gndo:ref`](#d17e5439)  [`@ref`](#d17e5735) |
| **Contained by** | [`person`](#person-record) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <text/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="gndo:placeOfDeath">
      <sch:assert test="@gndo:ref" role="warning">[WARN] (Missing GND-URI): It is recommended to provide a GND-URI for `<sch:name/>` via `@gndo:ref`!</sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e3363"></a>
#### Studienfach
|     |     |
| --- | --- |
| **Name** | `gndo:fieldOfStudy`  |
| **See also** | [gndo:fieldOfStudy](https://d-nb.info/standards/elementset/gnd?#id-73f6d3ea18bfa7b7d0bdd2144dee7582) |
| **Label** (de) | Studienfach |
| **Label** (en) | Field of study |
| **Description** (de) | Das Studienfach bzw. -gebiet einer Person. Über `@gndo:ref` kann ein entsprechendes Schlagwort aus der GND mit erfasst werden. |
| **Description** (en) | A person’s field of study. Use `@gndo:ref` to encode an additional GND Subject Category. |
| **Attributes** | [`@agency`](#d17e5568)  [`@enriched`](#d17e5620)  [`@gndo:ref`](#d17e5439)  [`@ref`](#d17e5735) |
| **Contained by** | [`person`](#person-record) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <text/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="gndo:fieldOfStudy">
      <sch:assert test="@gndo:ref" role="warning">[WARN](Missing Subject Category URI) It is recommended to provide an URI of a GND Subject Category using `@gndo:ref`.</sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e2556"></a>
#### Telefon
|     |     |
| --- | --- |
| **Name** | `phone`  |
| **Label** (de) | Telefon |
| **Label** (en) | phone |
| **Description** (de) | Telefonnummer zur Kontaktaufnahme |
| **Description** (en) | - |
| **Contained by** | [`contact`](#contact-metadata) |

***Content Model***  
```xml
<content>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e5317"></a>
#### Titel
|     |     |
| --- | --- |
| **Name** | `title`  |
| **Label** (de) | Titel |
| **Label** (en) | Title |
| **Description** (de) | Ein Titel für das Informationsobjekt, das in diesem Kontext beschrieben wird. |
| **Description** (en) | A title of the information-object, described in this very context. |
| **Contained by** | [`agency`](#agency-stmt)  [`gndo:publication`](#d17e4368)  [`list`](#data-list)  [`metadata`](#collection-metadata)  [`provider`](#data-provider) |

***Content Model***  
```xml
<content>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e2645"></a>
#### Titel (Record)
|     |     |
| --- | --- |
| **Name** | `dc:title`  |
| **See also** | [dc:title](http://purl.org/dc/elements/1.1/title) |
| **Label** (de) | Titel (Record) |
| **Label** (en) | title (record) |
| **Description** (de) | Ein Titel für die Ressource. `dc:title` wird dann zur Bezeichnung einer Entität verwendet, wenn keine Vorzugsbenennung dokumentiert wird bzw. werden soll. |
| **Description** (en) | A title of the record if no preferred or variant name is provided. |
| **Attributes** | [`@xml:lang`](#d17e5550) |
| **Contained by** | [`corporateBody`](#corporate-body-record)  [`entity`](#entity-record)  [`event`](#d17e243)  [`expression`](#d17e1235)  [`manifestation`](#d17e1304)  [`person`](#person-record)  [`place`](#d17e1061)  [`work`](#d17e1378) |

***Content Model***  
```xml
<content>
   <attribute name="xml:lang"/>
   <text/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="dc:title" role="warning">
      <sch:assert test="parent::node()[not(gndo:preferredName)]">(`dc:title` superfluous): <sch:name/> already contains a preferred name(`gndo:preferredName`) which makes `dc:title` superfluous!</sch:assert>
   </sch:rule>
</sch:pattern>
 
```

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="node()[parent::entity:list][self::entity:*][@gndo:uri or @xml:id][not(gndo:preferredName)][not(gndo:variantName)][not(self::entity:entity)]"
             role="warning">
      <sch:assert test="dc:title">[WARN] (fallback `dc:title` recommended): It is recommended to use `dc:title` to encode a fallback title for `<sch:name/>`!</sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e2202"></a>
#### Titelzusatz
|     |     |
| --- | --- |
| **Name** | `add`  |
| **Label** (de) | Titelzusatz |
| **Label** (en) | Addition to the title |
| **Description** (de) | Zusätze zum Titel im Rahmen der bibliographischen Angabe. |
| **Description** (en) | Additions to the title in the biblipgraphic reference. |
| **Contained by** | [`gndo:publication`](#d17e4368) |

***Content Model***  
```xml
<content>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="person-record-variantName"></a>
#### Variante Namensform (Person)
|     |     |
| --- | --- |
| **Name** | `gndo:variantName`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#id-7a7324f8ae0602c5fd0027e44275b5d6](https://d-nb.info/standards/elementset/gnd#id-7a7324f8ae0602c5fd0027e44275b5d6) |
| **Label** (de) | Variante Namensform (Person) |
| **Label** (en) | Variant Name (Person) |
| **Description** (de) | Eine alternative Namensform, unter der eine Person ebenfalls bekannt ist. |
| **Description** (en) | - |
| **Attributes** | [`@agency`](#d17e5568)  [`@enriched`](#d17e5620)  [`@xml:lang`](#d17e5550)   |
| **Contained by** | [`person`](#person-record) |
| **May contain** | [`gndo:counting`](#d17e3024)  [`gndo:epithetGenericNameTitleOrTerritory`](#d17e3307)[`gndo:forename`](#d17e3434)  [`gndo:nameAddition`](#d17e3818)  [`gndo:personalName`](#d17e4075)  [`gndo:prefix`](#d17e4237)  [`gndo:surname`](#d17e4656)   |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="xml:lang"/>
   <choice>
      <element name="gndo:personalName" required="true"/>
      <anyOrder>
         <element name="gndo:forename" required="true"/>
         <element name="gndo:prefix"/>
         <element name="gndo:surname" required="true"/>
         <element name="gndo:counting"/>
         <text/>
      </anyOrder>
   </choice>
   <anyOrder>
      <element name="gndo:nameAddition" repeatable="true"/>
      <element name="gndo:epithetGenericNameTitleOrTerritory" repeatable="true"/>
   </anyOrder>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="elem.gndo.variantName.standard"></a>
#### Variante Namensform (Standard)
|     |     |
| --- | --- |
| **Name** | `gndo:variantName`  |
| **See also** | [gndo:variantName](https://d-nb.info/standards/elementset/gnd#id-de90fe3e5aae8047bb551a1bc63abf17), [gndo:variantNameForTheCorporateBody](https://d-nb.info/standards/elementset/gnd#id-7a7324f8ae0602c5fd0027e44275b5d6), [gndo:variantNameForTheWork](https://d-nb.info/standards/elementset/gnd#id-4a1b3d3efe4ddec2e41e325b7cc57e88), [gndo:variantNameForThePlaceOrGeographicName](https://d-nb.info/standards/elementset/gnd#id-32e28dabd41a0474fa7bdfe901536e7a) |
| **Label** (de) | Variante Namensform (Standard) |
| **Label** (en) | Variant Name (Standard) |
| **Description** (de) | Eine alternative Namensform, um die beschriebenen Entität zu bezeichnen. |
| **Description** (en) | A variant name to denominate the documented Entity. |
| **Attributes** | [`@agency`](#d17e5568)  [`@enriched`](#d17e5620)  [`@xml:lang`](#d17e5550) |
| **Contained by** | [`corporateBody`](#corporate-body-record)  [`entity`](#entity-record)  [`place`](#d17e1061)  [`work`](#d17e1378) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="xml:lang"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="respStmt"></a>
#### Verantwortlichkeit (Metadaten)
|     |     |
| --- | --- |
| **Name** | `respStmt`  |
| **Label** (de) | Verantwortlichkeit (Metadaten) |
| **Label** (en) | Responsibility Statement (Metadata) |
| **Description** (de) | Angabe zur Rolle einer Person oder Institution in Hinblick auf die Bearbeitung einer Datensammlung. |
| **Description** (en) | A statement about responsibilities of a person or institution with regard to a datacollection. |
| **Attributes** | [`@id`](#d17e5475)   |
| **Contained by** | [`agency`](#agency-stmt)  [`metadata`](#collection-metadata)  [`provider`](#data-provider) |
| **May contain** | [`contact`](#contact-metadata)[`name`](#d17e4906)  [`resp`](#d17e4889)   |

***Content Model***  
```xml
<content>
   <attribute name="id" required="true"/>
   <element name="resp" required="true"/>
   <element name="name" required="true"/>
   <element name="contact"/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e2952"></a>
#### Verfasser
|     |     |
| --- | --- |
| **Name** | `gndo:author`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#id-91d732e5c5d458f32782b10467daec55](https://d-nb.info/standards/elementset/gnd#id-91d732e5c5d458f32782b10467daec55) |
| **Label** (de) | Verfasser |
| **Label** (en) | Author |
| **Description** (de) | (Verfasser) |
| **Description** (en) | A person, family, or organization responsible for creating a work that is primarily textual in content, regardless of media type (e.g., printed text, spoken word, electronic text, tactile text) or genre (e.g., poems, novels, screenplays, blogs). Use also for persons, etc., creating a new work by paraphrasing, rewriting, or adapting works by another creator such that the modification has substantially changed the nature and content of the original or changed the medium of expression. |
| **Attributes** | [`@agency`](#d17e5568)  [`@enriched`](#d17e5620)  [`@gndo:ref`](#d17e5439)  [`@ref`](#d17e5735) |
| **Contained by** | [`work`](#d17e1378) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e4133"></a>
#### Vorherige Körperschaft (Körperschaft)
|     |     |
| --- | --- |
| **Name** | `gndo:precedingCorporateBody`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#id-2a9d91576e259f07c488b9e0857c69bd](https://d-nb.info/standards/elementset/gnd#id-2a9d91576e259f07c488b9e0857c69bd) |
| **Label** (de) | Vorherige Körperschaft (Körperschaft) |
| **Label** (en) | Preceding corporate body (Corporal Body) |
| **Description** (de) | Eine Vorgängerinstitution der Körperschaft. |
| **Description** (en) | - |
| **Attributes** | [`@agency`](#d17e5568)  [`@enriched`](#d17e5620)  [`@gndo:ref`](#d17e5439)  [`@ref`](#d17e5735) |
| **Contained by** | [`corporateBody`](#corporate-body-record) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e4101"></a>
#### Vorheriges Geografikum
|     |     |
| --- | --- |
| **Name** | `gndo:precedingPlaceOrGeographicName`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#id-9c6196e848e7cf177f50c9033c07e164](https://d-nb.info/standards/elementset/gnd#id-9c6196e848e7cf177f50c9033c07e164) |
| **Label** (de) | Vorheriges Geografikum |
| **Label** (en) | Preceding place or geographic name |
| **Description** (de) | - |
| **Description** (en) | - |
| **Attributes** | [`@agency`](#d17e5568)  [`@enriched`](#d17e5620)  [`@gndo:ref`](#d17e5439)  [`@ref`](#d17e5735) |
| **Contained by** | [`place`](#d17e1061) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e3787"></a>
#### Vorlage
|     |     |
| --- | --- |
| **Name** | `gndo:literarySource`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#id-99e8bb7ad9b353ec0d8b7efe2283bc08](https://d-nb.info/standards/elementset/gnd#id-99e8bb7ad9b353ec0d8b7efe2283bc08) |
| **Label** (de) | Vorlage |
| **Label** (en) | Literary source |
| **Description** (de) | Die beschriebene Entität ist eine Instanziierung des verknüpften Werks. |
| **Description** (en) | The described entity is a realization of the related work. |
| **Attributes** | [`@agency`](#d17e5568)  [`@enriched`](#d17e5620)  [`@gndo:ref`](#d17e5439)  [`@ref`](#d17e5735) |
| **Contained by** | [`work`](#d17e1378) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e3434"></a>
#### Vorname
|     |     |
| --- | --- |
| **Name** | `gndo:forename`  |
| **See also** | [gndo:forename](https://d-nb.info/standards/elementset/gnd#id-dfd700ac86a76779dc050021c45b01a3) |
| **Label** (de) | Vorname |
| **Label** (en) | Forename |
| **Description** (de) | Ein oder mehrere Vornamen einer Person. |
| **Description** (en) | One or many prenames of person. |
| **Contained by** | [`gndo:preferredName`](#person-record-preferredName)  [`gndo:variantName`](#person-record-variantName) |

***Content Model***  
```xml
<content>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="person-record-preferredName"></a>
#### Vorzugsbenennung (Person)
|     |     |
| --- | --- |
| **Name** | `gndo:preferredName`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#id-904a3ccca0fde773a65d73946bf8a876](https://d-nb.info/standards/elementset/gnd#id-904a3ccca0fde773a65d73946bf8a876) |
| **Label** (de) | Vorzugsbenennung (Person) |
| **Label** (en) | Preferred Name (Person) |
| **Description** (de) | Die bevorzugte Namensform zur Denomination einer Person. |
| **Description** (en) | A preferred name to denominate a person. |
| **Attributes** | [`@agency`](#d17e5568)  [`@enriched`](#d17e5620)  [`@xml:lang`](#d17e5550)   |
| **Contained by** | [`person`](#person-record) |
| **May contain** | [`gndo:counting`](#d17e3024)  [`gndo:epithetGenericNameTitleOrTerritory`](#d17e3307)[`gndo:forename`](#d17e3434)  [`gndo:nameAddition`](#d17e3818)  [`gndo:personalName`](#d17e4075)  [`gndo:prefix`](#d17e4237)  [`gndo:surname`](#d17e4656)   |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="xml:lang"/>
   <choice>
      <element name="gndo:personalName" required="true"/>
      <anyOrder>
         <element name="gndo:forename" required="true"/>
         <element name="gndo:prefix"/>
         <element name="gndo:surname" required="true"/>
         <element name="gndo:counting"/>
         <text/>
      </anyOrder>
   </choice>
   <anyOrder>
      <element name="gndo:nameAddition" repeatable="true"/>
      <element name="gndo:epithetGenericNameTitleOrTerritory" repeatable="true"/>
   </anyOrder>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e4196"></a>
#### Vorzugsbenennung (Standard)
|     |     |
| --- | --- |
| **Name** | `gndo:preferredName`  |
| **See also** | [gndo:PlaceOrGeographicName](https://d-nb.info/standards/elementset/gnd#id-a708231e5f676cb2d2aed717ac50d3c0), [gndo:preferredNameForTheCorporateBody](https://d-nb.info/standards/elementset/gnd#id-904a3ccca0fde773a65d73946bf8a876), [gndo:preferredNameForTheWork](https://d-nb.info/standards/elementset/gnd#id-a979aab6cbe1246632b988f8d7713b41) |
| **Label** (de) | Vorzugsbenennung (Standard) |
| **Label** (en) | preferred name (default) |
| **Description** (de) | Eine bevorzugte Namensform, um die beschriebenen Entität zu bezeichnen. |
| **Description** (en) | A preferred name to denominate the documented Entity. |
| **Attributes** | [`@agency`](#d17e5568)  [`@enriched`](#d17e5620)  [`@xml:lang`](#d17e5550) |
| **Contained by** | [`corporateBody`](#corporate-body-record)  [`entity`](#entity-record)  [`place`](#d17e1061)  [`work`](#d17e1378) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="xml:lang"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="elem.foaf.page"></a>
#### Webseite
|     |     |
| --- | --- |
| **Name** | `foaf:page`  |
| **See also** | [foaf:page](http://xmlns.com/foaf/0.1/#term_page) |
| **Label** (de) | Webseite |
| **Label** (en) | Website |
| **Description** (de) | URL eines Dokuments mit näheren Informationen über die beschriebene Entität (z.B. Homepage, Wikipedia Seite etc.). |
| **Description** (en) | URL of a document about that thing, eg. a Wikipedia page. |
| **Attributes** | [`@agency`](#d17e5568)  [`@enriched`](#d17e5620)  [`@iso-date`](#attr.iso-date) |
| **Contained by** | [`corporateBody`](#corporate-body-record)  [`person`](#person-record) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="iso-date"/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e1378"></a>
#### Werk
|     |     |
| --- | --- |
| **Name** | `work`  |
| **See also** | [gndo:Work](https://d-nb.info/standards/elementset/gnd#id-c1e6e78875a240757c51a48091e75f13) |
| **Label** (de) | Werk |
| **Label** (en) | Work |
| **Description** (de) | Ein Werksdatensatz zur potenziellen Ansetzung in der Gemeinsamen Normdatei (GND). |
| **Description** (en) | - |
| **Attributes** | [`@agency`](#attr.record.agency)  [`@enrich`](#d17e5700)  [`@gndo:type`](#d17e1467)  [`@gndo:uri`](#d17e5417)  [`@xml:id`](#d17e5720)   |
| **Contained by** | [`list`](#data-list) |
| **May contain** | [`<anyElement.narrow>`](#any-restricted)  [`dc:title`](#d17e2645)  [`dublicateGndIdentifier`](#dublicates)  [`gndo:abbreviatedName`](#d17e2856)  [`gndo:author`](#d17e2952)  [`gndo:biographicalOrHistoricalInformation`](#d17e2985)  [`gndo:dateOfEstablishment`](#d17e3105)  [`gndo:dateOfPublication`](#d17e3174)  [`gndo:editor`](#d17e3277)  [`gndo:firstAuthor`](#d17e3403)  [`gndo:formOfWorkAndExpression`](#d17e3332)  [`gndo:geographicAreaCode`](#elem.gndo.geographicAreaCode)  [`gndo:gndIdentifier`](#d17e3593)  [`gndo:gndSubjectCategory`](#d17e3634)  [`gndo:homepage`](#elem.gndo.homepage)  [`gndo:languageCode`](#d17e3736)  [`gndo:literarySource`](#d17e3787)  [`gndo:preferredName`](#d17e4196)  [`gndo:relatedWork`](#d17e4565)  [`gndo:relatesTo`](#d17e4511)  [`gndo:variantName`](#elem.gndo.variantName.standard)  [`owl:sameAs`](#elem.owl.sameAs)  [`ref`](#elem.ref)  [`revision`](#revision)[`skos:note`](#elem.skos.note)  [`source`](#elem.source)   |

***Content Model***  
```xml
<content>
   <attribute name="gndo:type"/>
   <attribute name="xml:id" required="true"/>
   <attribute name="gndo:uri"/>
   <attribute name="agency"/>
   <attribute name="enrich"/>
   <anyOrder>
      <element name="gndo:abbreviatedName" repeatable="true"/>
      <element name="gndo:author" repeatable="true"/>
      <element name="gndo:dateOfEstablishment"/>
      <element name="gndo:dateOfPublication"/>
      <element name="gndo:editor" repeatable="true"/>
      <element name="gndo:firstAuthor"/>
      <element name="gndo:formOfWorkAndExpression" repeatable="true"/>
      <element name="gndo:homepage" repeatable="true"/>
      <element name="gndo:literarySource" repeatable="true"/>
      <element name="gndo:preferredName"/>
      <element name="gndo:relatedWork" repeatable="true"/>
      <element name="gndo:variantName" repeatable="true"/>
      <element name="dc:title"/>
      <element name="gndo:biographicalOrHistoricalInformation" repeatable="true"/>
      <element name="dublicateGndIdentifier" repeatable="true"/>
      <element name="gndo:geographicAreaCode" repeatable="true"/>
      <element name="gndo:gndIdentifier" repeatable="true"/>
      <element name="gndo:gndSubjectCategory" repeatable="true"/>
      <element name="gndo:languageCode" repeatable="true"/>
      <element name="gndo:relatesTo" repeatable="true"/>
      <element name="ref" repeatable="true"/>
      <element name="owl:sameAs" repeatable="true"/>
      <element name="skos:note" repeatable="true"/>
      <element name="source" repeatable="true"/>
      <anyElement type="narrow" repeatable="true"/>
      <text/>
   </anyOrder>
   <element name="revision"/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="entity:work[not(@gndo:uri)]">
      <sch:assert test="gndo:preferredName" role="error">(Preferred Name missing): It is mandatory to provide a `gndo:preferredName` for a "<sch:name/>" Entity if no `@gndo:uri` is provided!</sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="prop-periodOfActivity"></a>
#### Wirkungsdaten
|     |     |
| --- | --- |
| **Name** | `gndo:periodOfActivity`  |
| **Label** (de) | Wirkungsdaten |
| **Label** (en) | Period of activity |
| **Description** (de) | Zeitraum, innerhalb dessen die beschriebene Entität aktiv war. Wird nur dann erschlossen, wenn die Lebensdaten unbekannt sind. |
| **Description** (en) | A person’s known period of activity |
| **Attributes** | [`@agency`](#d17e5568)  [`@enriched`](#d17e5620)  [`@iso-from`](#attr.iso-from)  [`@iso-to`](#attr.iso-to) |
| **Contained by** | [`person`](#person-record) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="iso-from"/>
   <attribute name="iso-to"/>
   <text/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="gndo:periodOfActivity">
      <sch:assert test="@iso-from or @iso-to" role="error">(Point in time missing): It is mandatory to provide either a start- or endpoint in time (or both) to encode a timespan like `<sch:name/>`!</sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="person-record-placeOfActivity"></a>
#### Wirkungsort (Person)
|     |     |
| --- | --- |
| **Name** | `gndo:placeOfActivity`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#id-44ac981548f2d641daa2da3ff53f115a](https://d-nb.info/standards/elementset/gnd#id-44ac981548f2d641daa2da3ff53f115a) |
| **Label** (de) | Wirkungsort (Person) |
| **Label** (en) | Place of activity (Person) |
| **Description** (de) | Ein Wirkungsort, an dem die Person gewirkt oder gelebt (o.Ä.) hat. Über `@gndo:ref` kann ein Deskriptor (GND-URI) aus der GND erfasst werden. Steht kein GND-URI zur Verfügung erfolgt die Erfassung des Ortes als Freitext im Element. |
| **Description** (en) | A person’s or family’s place of activity |
| **Attributes** | [`@agency`](#d17e5568)  [`@enriched`](#d17e5620)  [`@gndo:ref`](#d17e5439)  [`@ref`](#d17e5735) |
| **Contained by** | [`person`](#person-record) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <text/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="gndo:placeOfActivity">
      <sch:assert test="@gndo:ref" role="warning">[WARN] (Missing GND-URI): It is recommended to provide a GND-URI for `<sch:name/>` via `@gndo:ref`!</sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e4681"></a>
#### Zeitweiser Name
|     |     |
| --- | --- |
| **Name** | `gndo:temporaryName`  |
| **See also** | [gndo:temporaryName](https://d-nb.info/standards/elementset/gnd#id-9501cd5820c216faa0cd3719c3c7e0b3), [gndo:temporaryNameOfTheConferenceOrEvent](https://d-nb.info/standards/elementset/gnd#id-347d719c10318cb52450ac95ffb98dc5), [gndo:temporaryNameOfTheCorporateBody](https://d-nb.info/standards/elementset/gnd#id-4c798ad163e2837706c7b03df7a2adde), [gndo:temporaryNameOfThePlaceOrGeographicName](https://d-nb.info/standards/elementset/gnd#id-5f19ab2748488f33cebe5cc993490a88) |
| **Label** (de) | Zeitweiser Name |
| **Label** (en) | Temporary name |
| **Description** (de) | Eine Namensform, die zeitweise zur Bezeichnung der dokumentierten Entität benutzt wurde. |
| **Description** (en) | A name used temporarily to denominate the entity encoded. |
| **Attributes** | [`@agency`](#d17e5568)  [`@enriched`](#d17e5620)  [`@gndo:ref`](#d17e5439)  [`@ref`](#d17e5735)  [`@xml:lang`](#d17e5550) |
| **Contained by** | [`corporateBody`](#corporate-body-record)  [`event`](#d17e243)  [`place`](#d17e1061) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <attribute name="xml:lang"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e2929"></a>
#### Zugehörigkeit
|     |     |
| --- | --- |
| **Name** | `gndo:affiliation`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#id-96bf02d246e0bc6ddd2b6f607ea1184c](https://d-nb.info/standards/elementset/gnd#id-96bf02d246e0bc6ddd2b6f607ea1184c) |
| **Label** (de) | Zugehörigkeit |
| **Description** (de) | Die Person oder Körperschaft ist zugehörig zu einer Körperschaft, oder ist mit einem Ort oder einem Event verbunden. |
| **Attributes** | [`@agency`](#d17e5568)  [`@enriched`](#d17e5620)  [`@gndo:ref`](#d17e5439)  [`@ref`](#d17e5735) |
| **Contained by** | [`corporateBody`](#corporate-body-record)  [`person`](#person-record) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e3024"></a>
#### Zählung
|     |     |
| --- | --- |
| **Name** | `gndo:counting`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#id-a3886e28760f72e469e4d24b67e363e5](https://d-nb.info/standards/elementset/gnd#id-a3886e28760f72e469e4d24b67e363e5) |
| **Label** (de) | Zählung |
| **Label** (en) | Counting |
| **Description** (de) | Eine Zählung als Namensbestandteil (z.B. in Ramses **II** oder Sethos **I**). |
| **Description** (en) | A Counting as Part of a proper name (eg. Ramesses **II** or Seti **II**). |
| **Attributes** | [`@agency`](#d17e5568)  [`@enriched`](#d17e5620) |
| **Contained by** | [`gndo:preferredName`](#person-record-preferredName)  [`gndo:variantName`](#person-record-variantName) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e1339"></a>
#### embodimentOf
|     |     |
| --- | --- |
| **Name** | `embodimentOf`  |
| **Attributes** | [`@agency`](#d17e5568)  [`@enriched`](#d17e5620)  [`@gndo:ref`](#d17e5439)  [`@ref`](#d17e5735) |
| **Contained by** | [`manifestation`](#d17e1304) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <text/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="entity:embodimentOf/@ref">
      <sch:let name="id" value="data(.)"/>
      <sch:let name="target" value="root()//element()[@xml:id = $id]"/>
      <sch:assert test="$target[self::entity:expression]" role="error"> Referenced Entity "<sch:value-of select="$id"/>" must be an expression but is "<sch:value-of select="$target/name()"/>"! </sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e5232"></a>
#### entityXML Store Metadaten
|     |     |
| --- | --- |
| **Name** | `store:store`  |
| **Label** (de) | entityXML Store Metadaten |
| **Label** (en) | entityXML Store metadata |
| **Description** (de) | Metadaten, die in einem entityXML Store automatisch erzeugt und zur Verwaltung des Datensatzes verwendet werden. |
| **Description** (en) | Metadata automatically generated by an entityXML Store to manage the dataset. |
| **Attributes** | [`@id`](#d17e5248)  [`@project`](#d17e5260)   |
| **Contained by** | [`entityXML`](#entity-xml) |
| **May contain** | [`store:workflow`](#d17e5272) |

***Content Model***  
```xml
<content>
   <attribute name="id"/>
   <attribute name="project"/>
   <element name="store:workflow"/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e5272"></a>
#### entityXML Store Workflow
|     |     |
| --- | --- |
| **Name** | `store:workflow`  |
| **Label** (de) | entityXML Store Workflow |
| **Label** (en) | entityXML Store workflow |
| **Description** (de) | Informationen zum entityXML Store Workflow des jeweiligen Informationsobjekts (Datensatz oder Record). |
| **Description** (en) | Information with regard to the entityXML Store Workflow of the Informationobject (dataset or record). |
| **Contained by** | [`store:store`](#d17e5232) |
| **May contain** | [`store:step`](#d17e5288) |

***Content Model***  
```xml
<content>
   <element name="store:step"/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e5288"></a>
#### entityXML Store Workflow Schritt
|     |     |
| --- | --- |
| **Name** | `store:step`  |
| **Label** (de) | entityXML Store Workflow Schritt |
| **Label** (en) | entityXML Store workflow Step |
| **Description** (de) | Ein Schritt im Managementworkflow. |
| **Description** (en) | A step of the management workflow. |
| **Attributes** | [`@name`](#d17e5302)  [`@timestamp`](#d17e5304) |
| **Contained by** | [`store:workflow`](#d17e5272) |

***Content Model***  
```xml
<content>
   <attribute name="name" required="true"/>
   <attribute name="timestamp" required="true"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e1265"></a>
#### realizationOf
|     |     |
| --- | --- |
| **Name** | `realizationOf`  |
| **Attributes** | [`@agency`](#d17e5568)  [`@enriched`](#d17e5620)  [`@gndo:ref`](#d17e5439)  [`@ref`](#d17e5735) |
| **Contained by** | [`expression`](#d17e1235) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <text/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="entity:realizationOf/@ref">
      <sch:let name="id" value="substring-after(data(.), '#')"/>
      <sch:let name="target" value="./root()//element()[@xml:id = $id]"/>
      <sch:assert test="$target[self::*:work]" role="error"> Referenced Entity "<sch:value-of select="$id"/>" must be a work but is "<sch:value-of select="$target/name()"/>"! </sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>


<a name="specs-attrs"></a>
### Attributspezifikationen

Derzeit umfasst das entityXML Schema **43** spezifizierte Attribute.

Die technische Spezifikation für Attribute umfasst folgende Eigenschaften: 

- **Name**: Der Attributsname bzw. XML-name, der das Attribut bezeichnet.
- **Datatype**: Angabe zum spezifizierten Datentyp des Attributs.
- **Label**: Ein oder mehrere Labels in Deutsch (und Englisch).
- **Description**: Eine oder mehrere Beschreibungen, die die Semantik des Attributs beschreiben.
- **Contained by**: Führt die Elemente auf, in denen das entsprechende Attrbut verwendet wird bzw. werden kann.
- **Predefined Values**: Falls spezifiziert, werden hier vordefinierte Werte angegeben.
- **Validation**: Falls spezifiziert, werden hier Validierungsregeln (z.B. Schematron Pattern) für das entsprechende Element angegeben.



<a name="d17e1960"></a>
#### ANY ATTRIBUTE
|     |     |
| --- | --- |
| **Name** | *Any name.*  |
| **Datatype** | string |
| **Label** (de) | ANY ATTRIBUTE |
| **Label** (en) | ANY ATTRIBUTE |
| **Description** (de) | Ein Attribut mit frei wählbarem **QName**. |
| **Description** (en) | - |
| **Contained by** | [`<anyElement.broad>`](#any-all)  [`<anyElement.narrow>`](#any-restricted) |

---

<p style="margin-bottom:60px"></p>

<a name="attr.record.agency"></a>
#### Agenturanfrage
|     |     |
| --- | --- |
| **Name** | `@agency`  |
| **Datatype** | string |
| **Label** (de) | Agenturanfrage |
| **Label** (en) | agency contact |
| **Description** (de) | Anfrage an die Agentur, welche Aktion in Hinblick auf den Eintrag durchgeführt werden soll. |
| **Description** (en) | - |
| **Contained by** | [`corporateBody`](#corporate-body-record)  [`entity`](#entity-record)  [`event`](#d17e243)  [`expression`](#d17e1235)  [`manifestation`](#d17e1304)  [`person`](#person-record)  [`place`](#d17e1061)  [`work`](#d17e1378) |

***Predefined Values***  (multiple choice)

 - **create**: (Eintrag in der GND neu anlegen) Der Eintrag soll als Normdatensatz neu in der GND angelegt werden.
 - **update**: (Normdatensatz mit Daten aus dem Eintrag erweitern) Der Normdatensatz soll durch neue Informationen angereichert werden.
 - **merge**: (Normdatensätze zusammenführen) Dubletten, die in dem Eintrag dokumentiert werden, sollen mit dem Normdatensatz, der durch den GND-URI des Eintrags (`@gndo:uri`) identifiziert ist, zusammengeführt werden.
 - **ignore**: (Eintrag ingonieren) Der Eintrag wird von der Agentur nicht bearbeitet.

---

<p style="margin-bottom:60px"></p>

<a name="d17e5568"></a>
#### Aktion (Agentur)
|     |     |
| --- | --- |
| **Name** | `@agency`  |
| **Datatype** | string |
| **Label** (de) | Aktion (Agentur) |
| **Label** (en) | Task (Agentur) |
| **Description** (de) | Anfrage an die Agentur, welche Aktion in Hinblick auf eine Property durchgeführt werden soll. |
| **Description** (en) | - |
| **Contained by** | [`bf:instanceOf`](#instance-of)  [`dublicateGndIdentifier`](#dublicates)  [`embodimentOf`](#d17e1339)  [`foaf:page`](#elem.foaf.page)  [`gndo:abbreviatedName`](#d17e2856)  [`gndo:academicDegree`](#d17e2902)  [`gndo:affiliation`](#d17e2929)  [`gndo:author`](#d17e2952)  [`gndo:biographicalOrHistoricalInformation`](#d17e2985)  [`gndo:counting`](#d17e3024)  [`gndo:dateOfBirth`](#person-record-dateOfBirth)  [`gndo:dateOfDeath`](#person-record-dateOfDeath)  [`gndo:dateOfEstablishment`](#d17e3105)  [`gndo:dateOfEstablishmentAndTermination`](#d17e3133)  [`gndo:dateOfPublication`](#d17e3174)  [`gndo:dateOfTermination`](#d17e3201)  [`gndo:editor`](#d17e3277)  [`gndo:fieldOfStudy`](#d17e3363)  [`gndo:firstAuthor`](#d17e3403)  [`gndo:formOfWorkAndExpression`](#d17e3332)  [`gndo:functionOrRole`](#d17e3460)  [`gndo:gender`](#d17e3491)  [`gndo:geographicAreaCode`](#elem.gndo.geographicAreaCode)  [`gndo:gndIdentifier`](#d17e3593)  [`gndo:gndSubjectCategory`](#d17e3634)  [`gndo:hierarchicalSuperiorOfPlaceOrGeographicName`](#d17e3673)  [`gndo:homepage`](#elem.gndo.homepage)  [`gndo:languageCode`](#d17e3736)  [`gndo:literarySource`](#d17e3787)  [`gndo:periodOfActivity`](#prop-periodOfActivity)  [`gndo:place`](#geographic-entity-place)  [`gndo:placeOfActivity`](#person-record-placeOfActivity)  [`gndo:placeOfBirth`](#person-record-placeOfBirth)  [`gndo:placeOfBusiness`](#d17e3967)  [`gndo:placeOfDeath`](#person-record-placeOfDeath)  [`gndo:precedingCorporateBody`](#d17e4133)  [`gndo:precedingPlaceOrGeographicName`](#d17e4101)  [`gndo:preferredName`](#person-record-preferredName)  [`gndo:preferredName`](#d17e4196)  [`gndo:professionOrOccupation`](#d17e4262)  [`gndo:pseudonym`](#d17e4321)  [`gndo:publication`](#d17e4368)  [`gndo:relatedWork`](#d17e4565)  [`gndo:relatesTo`](#d17e4511)  [`gndo:succeedingCorporateBody`](#d17e4596)  [`gndo:succeedingPlaceOrGeographicName`](#d17e4626)  [`gndo:temporaryName`](#d17e4681)  [`gndo:titleOfNobility`](#d17e4726)  [`gndo:variantName`](#person-record-variantName)  [`gndo:variantName`](#elem.gndo.variantName.standard)  [`owl:sameAs`](#elem.owl.sameAs)  [`realizationOf`](#d17e1265)  [`skos:note`](#elem.skos.note)  [`source`](#elem.source)  [`wgs84:lat`](#d17e2806)  [`wgs84:long`](#d17e2819) |

***Predefined Values***  (multiple choice)

 - **add**: (Eigenschaft anlegen) Die Eigenschaft soll dem GND-Normdatensatz des Records hinzugefügt werden.
 - **ignore**: (Eigenschaft ingonieren) Die Eigenschaft wird von der Agentur nicht bearbeitet.
 - **remove**: (Eigenschaft entfernen) Die Eigenschaft soll aus dem GND-Normdatensatz entfernt werden.

---

<p style="margin-bottom:60px"></p>

<a name="attr.iso-from"></a>
#### Anfangsdatum
|     |     |
| --- | --- |
| **Name** | `@iso-from`  |
| **Datatype** | date gYearMonth gYear  |
| **Label** (de) | Anfangsdatum |
| **Label** (en) | Startdate |
| **Description** (de) | Ein Anfangsdatum im ISO Format JJJJ-MM-TT, JJJJ-MM oder JJJJ |
| **Description** (en) | A startdate in ISO format YYYY-MM-DD, YYYY-MM or YYYY |
| **Contained by** | [`gndo:dateOfEstablishmentAndTermination`](#d17e3133)  [`gndo:periodOfActivity`](#prop-periodOfActivity) |

---

<p style="margin-bottom:60px"></p>

<a name="d17e5620"></a>
#### Angereicherte Eigenschaft
|     |     |
| --- | --- |
| **Name** | `@enriched`  |
| **Datatype** | anyURI |
| **Label** (de) | Angereicherte Eigenschaft |
| **Label** (en) | Enriched Property |
| **Description** (de) | Diese Eigenschaft ist automatisch mithilfe des angegebenen externen Datendienstes angereichert wurden! |
| **Description** (en) | This Property was automatically added to the record using the documented Dataservice! |
| **Contained by** | [`bf:instanceOf`](#instance-of)  [`dublicateGndIdentifier`](#dublicates)  [`embodimentOf`](#d17e1339)  [`foaf:page`](#elem.foaf.page)  [`gndo:abbreviatedName`](#d17e2856)  [`gndo:academicDegree`](#d17e2902)  [`gndo:affiliation`](#d17e2929)  [`gndo:author`](#d17e2952)  [`gndo:biographicalOrHistoricalInformation`](#d17e2985)  [`gndo:counting`](#d17e3024)  [`gndo:dateOfBirth`](#person-record-dateOfBirth)  [`gndo:dateOfDeath`](#person-record-dateOfDeath)  [`gndo:dateOfEstablishment`](#d17e3105)  [`gndo:dateOfEstablishmentAndTermination`](#d17e3133)  [`gndo:dateOfPublication`](#d17e3174)  [`gndo:dateOfTermination`](#d17e3201)  [`gndo:editor`](#d17e3277)  [`gndo:fieldOfStudy`](#d17e3363)  [`gndo:firstAuthor`](#d17e3403)  [`gndo:formOfWorkAndExpression`](#d17e3332)  [`gndo:functionOrRole`](#d17e3460)  [`gndo:gender`](#d17e3491)  [`gndo:geographicAreaCode`](#elem.gndo.geographicAreaCode)  [`gndo:gndIdentifier`](#d17e3593)  [`gndo:gndSubjectCategory`](#d17e3634)  [`gndo:hierarchicalSuperiorOfPlaceOrGeographicName`](#d17e3673)  [`gndo:homepage`](#elem.gndo.homepage)  [`gndo:languageCode`](#d17e3736)  [`gndo:literarySource`](#d17e3787)  [`gndo:periodOfActivity`](#prop-periodOfActivity)  [`gndo:place`](#geographic-entity-place)  [`gndo:placeOfActivity`](#person-record-placeOfActivity)  [`gndo:placeOfBirth`](#person-record-placeOfBirth)  [`gndo:placeOfBusiness`](#d17e3967)  [`gndo:placeOfDeath`](#person-record-placeOfDeath)  [`gndo:precedingCorporateBody`](#d17e4133)  [`gndo:precedingPlaceOrGeographicName`](#d17e4101)  [`gndo:preferredName`](#person-record-preferredName)  [`gndo:preferredName`](#d17e4196)  [`gndo:professionOrOccupation`](#d17e4262)  [`gndo:pseudonym`](#d17e4321)  [`gndo:publication`](#d17e4368)  [`gndo:relatedWork`](#d17e4565)  [`gndo:relatesTo`](#d17e4511)  [`gndo:succeedingCorporateBody`](#d17e4596)  [`gndo:succeedingPlaceOrGeographicName`](#d17e4626)  [`gndo:temporaryName`](#d17e4681)  [`gndo:titleOfNobility`](#d17e4726)  [`gndo:variantName`](#person-record-variantName)  [`gndo:variantName`](#elem.gndo.variantName.standard)  [`owl:sameAs`](#elem.owl.sameAs)  [`realizationOf`](#d17e1265)  [`skos:note`](#elem.skos.note)  [`source`](#elem.source)  [`wgs84:lat`](#d17e2806)  [`wgs84:long`](#d17e2819) |

---

<p style="margin-bottom:60px"></p>

<a name="d17e5171"></a>
#### Anmerkungsart
|     |     |
| --- | --- |
| **Name** | `@gndo:type`  |
| **Datatype** | string |
| **Label** (de) | Anmerkungsart |
| **Description** (de) | Angabe zur Art der Anmerkung, sprich, ob es sich um eine interne Anmerkung handelt. |
| **Contained by** | [`skos:note`](#elem.skos.note) |

***Predefined Values***  

 - **internal**: (Interne Anmerkung)Eine Anmerkung, die ausschließlich für interne Zwecke dokumentiert wird.

---

<p style="margin-bottom:60px"></p>

<a name="d17e5700"></a>
#### Anreicherung
|     |     |
| --- | --- |
| **Name** | `@enrich`  |
| **Datatype** | boolean |
| **Label** (de) | Anreicherung |
| **Label** (en) | enrichment |
| **Description** (de) | Der Record soll mit Daten eines externen Datendienstes angereichert werden. Hierfür wird ein zusätzliches Konversionsscript benötigt, dass die Konversionsroutinen zur verfügung stellt, um den Record anzureichern! |
| **Description** (en) | The Record is marked to be enriched by a separate enrichment routine provided by an additional conversion script! |
| **Contained by** | [`corporateBody`](#corporate-body-record)  [`entity`](#entity-record)  [`event`](#d17e243)  [`expression`](#d17e1235)  [`manifestation`](#d17e1304)  [`person`](#person-record)  [`place`](#d17e1061)  [`work`](#d17e1378) |

---

<p style="margin-bottom:60px"></p>

<a name="rev-status"></a>
#### Bearbeitungsphase
|     |     |
| --- | --- |
| **Name** | `@status`  |
| **Datatype** | string |
| **Label** (de) | Bearbeitungsphase |
| **Label** (en) | Phase |
| **Description** (de) | Die Phase der Bearbeitung, in der sich die Datensammlung oder der Eintrag momentan befindet. |
| **Description** (en) | The phase of a records editing-lifecycle. |
| **Contained by** | [`revision`](#revision) |

***Predefined Values***  

 - **opened**: (Geöffnet, *default*) Die Informationsressource wurde angelegt und befindet sich derzeit in der Bearbeitung durch den Datenlieferanten.
 - **staged**: (Auf dem Prüfstand) Die Informationsressource befindet sich derzeit in der Kontrolle durch eine GND-Agentur: Es wird geprüft, ob die geltenden Qualitätskriterien erfüllt sind. Mögliche Rückfragen und Vorschläge zur Anpassungen werden durch die GND-Agentur dokumentiert und den Datenlieferanten mitgeteilt.
 - **closed**: (Abgeschlossen) Alle Arbeiten an der Informationsressource wurden durchgeführt. Sie gilt als abgeschlossen.

---

<p style="margin-bottom:60px"></p>

<a name="change-status"></a>
#### Bearbeitungsstadium
|     |     |
| --- | --- |
| **Name** | `@status`  |
| **Datatype** | string |
| **Label** (de) | Bearbeitungsstadium |
| **Description** (de) | Der momentane Bearbeitungsstatus der Informationsressource. |
| **Contained by** | [`change`](#d17e2229) |

***Predefined Values***  

 - **draft**: (Draft, *default*) Die Informationsressource befindet sich in der Bearbeitung.
 - **candidate**: (Kandidat) Die Bearbeitung ist soweit abgeschlossen. Die Informationsressource gilt nun als potenzieller Kandidat zur Übertragung an die GND und ist somit bereit zur Prüfung durch eine GND-Agentur.
 - **embargoed**: (Gesperrt) Die Informationsressource ist derzeit gesperrt. Sie erfüllt entweder nicht die erforderlichen Qualitätskriterien oder ist aus einem anderen Grund, der in dem Änderungseintrag vermerkt ist, gesperrt. Sie muss entsprechend überarbeitet werden.
 - **withdrawn**: (Zurückgezogen) Die Informationsressource wurde zurückgezogen. Sie wird in Hinblick auf eine Übertragung an die GND ignoriert.
 - **cleared**: (Überarbeitet) Die Informationsressource wurde in Hinblick auf die geltenden Qualitätskriterien überarbeitet und gilt nun als überarbeiteter Kandidat, der erneut geprüft wird.
 - **approved**: (Angenommen) Die Prüfung der Informationsressource ist abgeschlossen. Sie erfüllt alle erforderlichen Qualitätsstandards und ist bereit zur Übertragung an die GND. Dieser Bearbeitungsschritt ist Mitarbeiter\*innen einer GND-Agentur vorbehalten.
 - **submitted**: (An GND geliefert) Die Daten der Informationsressource wurden an die GND zum Übertragen geliefert. Dieser Bearbeitungsschritt ist Mitarbeiter\*innen einer GND-Agentur vorbehalten.
 - **published**: (Veröffentlicht) Die Daten der Informationsressource wurden in der GND veröffentlicht. Dieser Bearbeitungsschritt ist Mitarbeiter\*innen einer GND-Agentur vorbehalten.

---

<p style="margin-bottom:60px"></p>

<a name="d17e4534"></a>
#### Beziehungstyp
|     |     |
| --- | --- |
| **Name** | `@gndo:type`  |
| **Datatype** | anyURI (*RNG Pattern Matching*, *RNG Pattern Matching*, *RNG Pattern Matching*) |
| **Label** (de) | Beziehungstyp |
| **Label** (en) | Type of Relation |
| **Description** (de) | Spezifiziert die Art der ausgewiesenen Beziehung entweder über einen **GND-URI**, einen **Term-URI aus einem GND-Vokabular** oder einen **Term-URI aus der GNDO**. |
| **Description** (en) | Spezifies the type of the encoded relation either using a **GND-URI**, a **GND-vocabulary descriptor URI** or **GNDO descriptor URI**. |
| **Contained by** | [`gndo:relatesTo`](#d17e4511) |

***Validation***  

```xml 
<param xmlns="http://relaxng.org/ns/structure/1.0" name="pattern">(https|http)://d-nb.info/standards/elementset/gnd#(.+)</param>
 
```

```xml 
<param xmlns="http://relaxng.org/ns/structure/1.0" name="pattern">(http|https)://d-nb.info/standards/vocab/gnd/(.+)</param>
 
```

```xml 
<param xmlns="http://relaxng.org/ns/structure/1.0" name="pattern">(http|https)://d-nb.info/gnd/(.+)</param>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e5339"></a>
#### Certainty
|     |     |
| --- | --- |
| **Name** | `@cert`  |
| **Datatype** | string |
| **Label** (de) | Certainty |
| **Description** (de) | Angabe zum Status der getroffenen Aussage. `@cert` hat einen vorgegebenen, exklusiven Wertebereich ("low", "middle", "high"). |
| **Contained by** | [`dublicateGndIdentifier`](#dublicates)  [`gndo:gndIdentifier`](#d17e3593) |

***Predefined Values***  

 - **low**: (unsicher) Die Aussage ist unsicher
 - **middle**: (wahrscheinlich) Die Aussage ist wahrscheinlich
 - **high**: (belegt) Die Aussage ist belegt

---

<p style="margin-bottom:60px"></p>

<a name="d17e4402"></a>
#### DNB Katalog
|     |     |
| --- | --- |
| **Name** | `@dnb:catalogue`  |
| **Datatype** | anyURI (*RNG Pattern Matching*) |
| **Label** (de) | DNB Katalog |
| **Description** (de) | Verknüpfung einer `gndo:publication` mit einem URL eines Eintrags im [Katalog der Deutschen Nationalbibliothek](https://portal.dnb.de/opac.htm). |
| **Contained by** | [`gndo:publication`](#d17e4368) |

***Validation***  

```xml 
<param xmlns="http://relaxng.org/ns/structure/1.0" name="pattern">(http|https)://d-nb.info/(.+)</param>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e5776"></a>
#### Datum
|     |     |
| --- | --- |
| **Name** | `@when`  |
| **Datatype** | date |
| **Label** (de) | Datum |
| **Description** (de) | Angabe des Datums der Änderung. |
| **Contained by** | [`change`](#d17e2229) |

---

<p style="margin-bottom:60px"></p>

<a name="attr.iso-to"></a>
#### Enddatum
|     |     |
| --- | --- |
| **Name** | `@iso-to`  |
| **Datatype** | date gYearMonth gYear  |
| **Label** (de) | Enddatum |
| **Label** (en) | Enddate |
| **Description** (de) | Ein Enddatum im ISO Format JJJJ-MM-TT, JJJJ-MM oder JJJJ |
| **Description** (en) | An enddate in ISO format YYYY-MM-DD, YYYY-MM or YYYY |
| **Contained by** | [`gndo:dateOfEstablishmentAndTermination`](#d17e3133)  [`gndo:periodOfActivity`](#prop-periodOfActivity) |

---

<p style="margin-bottom:60px"></p>

<a name="d17e3513"></a>
#### GND Gender Descriptor URI
|     |     |
| --- | --- |
| **Name** | `@gndo:term`  |
| **Datatype** | string |
| **Label** (de) | GND Gender Descriptor URI |
| **Description** (de) | Ein oder mehrere URIs von Termen aus dem GND Vokabular "Gender" (https://d-nb.info/standards/vocab/gnd/gender#). |
| **Contained by** | [`gndo:gender`](#d17e3491) |

***Predefined Values***  (multiple choice)

 - **https://d-nb.info/standards/vocab/gnd/gender#female**: *no description available*
 - **https://d-nb.info/standards/vocab/gnd/gender#male**: *no description available*
 - **https://d-nb.info/standards/vocab/gnd/gender#notKnown**: *no description available*

---

<p style="margin-bottom:60px"></p>

<a name="d17e5403"></a>
#### GND Geographic Area Code Term
|     |     |
| --- | --- |
| **Name** | `@gndo:term`  |
| **Datatype** | string |
| **Label** (de) | GND Geographic Area Code Term |
| **Description** (de) | URI eines Terms aus dem GND Vokabular *[Geographic Area Code](https://d-nb.info/standards/vocab/gnd/geographic-area-code)* |
| **Contained by** | [`gndo:geographicAreaCode`](#elem.gndo.geographicAreaCode) |

***Predefined Values***  

 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#NTHH**: (Neutral Zone (-1993))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA**: (Europe)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-AAAT**: (Austria (-12.11.1918))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-AD**: (Andorra)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-AL**: (Albania)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-AT**: (Austria)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-AT-1**: (Burgenland)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-AT-2**: (Carinthia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-AT-3**: (Lower Austria)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-AT-4**: (Upper Austria)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-AT-5**: (Salzburg)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-AT-6**: (Styria)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-AT-7**: (Tyrol)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-AT-8**: (Vorarlberg)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-AT-9**: (Vienna)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-AX**: (Ǻland Island)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-BA**: (Bosnia and Hercegovina)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-BE**: (Belgium)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-BG**: (Bulgaria)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-BY**: (Belarus)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH**: (Switzerland)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-AG**: (Aargau)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-AI**: (Appenzell Innerrhoden)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-AR**: (Appenzell Ausserrhoden)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-BE**: (Bern)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-BL**: (Basel-Landschaft)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-BS**: (Basel-Stadt)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-FR**: (Fribourg)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-GE**: (Geneva)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-GL**: (Glarus)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-GR**: (Grisons)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-JU**: (Jura)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-LU**: (Luzern)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-NE**: (Neuchâtel)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-NW**: (Nidwalden)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-OW**: (Obwalden)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-SG**: (St. Gallen)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-SH**: (Schaffhausen)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-SO**: (Solothurn)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-SZ**: (Schwyz)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-TG**: (Thurgau)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-TI**: (Ticino)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-UR**: (Uri)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-VD**: (Vaud)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-VS**: (Valais)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-ZG**: (Zug)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-ZH**: (Zürich)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CSHH**: (Czechoslovakia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CSXX**: (Serbia and Montenegro)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CY**: (Cyprus)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CZ**: (Czech Republic)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DDDE**: (Germany East)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DE**: (Germany)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DE-BB**: (Brandenburg)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DE-BE**: (Berlin)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DE-BW**: (Baden-Württemberg)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DE-BY**: (Bavaria)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DE-HB**: (Bremen)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DE-HE**: (Hesse)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DE-HH**: (Hamburg)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DE-MV**: (Mecklenburg-Vorpommern)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DE-NI**: (Lower Saxony)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DE-NW**: (North Rhine-Westphalia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DE-RP**: (Rhineland-Palatinate)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DE-SH**: (Schleswig-Holstein)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DE-SL**: (Saarland)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DE-SN**: (Saxony)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DE-ST**: (Saxony-Anhalt)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DE-TH**: (Thuringia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DK**: (Denmark)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DXDE**: (Germany (-1949))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-EE**: (Estonia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-ES**: (Spain)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-FI**: (Finland)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-FR**: (France)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-GB**: (Great Britain)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-GG**: (Guernsey)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-GI**: (Gibraltar)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-GR**: (Greece)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-HR**: (Croatia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-HU**: (Hungary)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-IE**: (Ireland)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-IM**: (Isle of Man)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-IS**: (Iceland)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-IT**: (Italy)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-IT-32**: (Trentino-Alto Adige Italy)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-JE**: (Jersey)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-LI**: (Liechtenstein)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-LT**: (Lithuania)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-LU**: (Luxembourg)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-LV**: (Latvia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-MC**: (Monaco)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-MD**: (Moldova)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-ME**: (Montenegro)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-MK**: (North Macedonia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-MT**: (Malta)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-NL**: (Netherlands)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-NO**: (Norway)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-PL**: (Poland)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-PT**: (Portugal)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-QV**: (Kosovo)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-RO**: (Romania)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-RS**: (Serbia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-RU**: (Russia (Federation))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-SE**: (Sweden)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-SI**: (Slovenia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-SK**: (Slovakia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-SM**: (San Marino)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-SUHH**: (Soviet Union)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-UA**: (Ukraine)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-VA**: (Vatican City)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-YUCS**: (Yugoslavia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB**: (Asia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-AE**: (United Arab Emirates)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-AF**: (Afghanistan)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-AM**: (Armenia (Republic))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-AZ**: (Azerbaijan)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-BD**: (Bangladesh)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-BH**: (Bahrain)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-BN**: (Brunei)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-BT**: (Bhutan)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-BUMM**: (Burma)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-CN**: (China)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-CN-54**: (Tibet (China))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-GE**: (Georgia (Republic))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-HK**: (Hong Kong)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-ID**: (Indonesia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-IL**: (Israel)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-IN**: (India)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-IQ**: (Iraq)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-IR**: (Iran)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-JO**: (Jordan)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-JP**: (Japan)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-KG**: (Kyrgyzstan)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-KH**: (Cambodia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-KP**: (Korea (North))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-KR**: (Korea (South))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-KW**: (Kuwait)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-KZ**: (Kazakhstan)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-LA**: (Laos)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-LB**: (Lebanon)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-LK**: (Sri Lanka)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-MM**: (Burma)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-MN**: (Mongolia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-MO**: (Macau (China : Special Administrative Region))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-MV**: (Maldives)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-MY**: (Malaysia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-NP**: (Nepal)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-OM**: (Oman)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-PH**: (Philippines)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-PK**: (Pakistan)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-QA**: (Qatar)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-SA**: (Saudi Arabia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-SG**: (Singapore)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-SKIN**: (Sikkim)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-SY**: (Syria)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-TH**: (Thailand)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-TJ**: (Tajikistan)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-TL**: (East Timor)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-TM**: (Turkmenistan)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-TPTL**: (East Timor)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-TR**: (Turkey)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-TW**: (Taiwan)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-UZ**: (Uzbekistan)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-VDVN**: (Vietnam (South))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-VN**: (Vietnam)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-YDYE**: (Yemen (Republic))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-YE**: (Yemen)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC**: (Africa)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-AIDJ**: (French Afars and Issas)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-AO**: (Angola)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-BF**: (Burkina Faso)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-BI**: (Burundi)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-BJ**: (Benin)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-BW**: (Botswana)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-CD**: (Congo (Democratic Republic))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-CF**: (Central African Republic)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-CG**: (Congo (Brazzaville))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-CI**: (Côte d'Ivoire)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-CM**: (Cameroon)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-CV**: (Cape Verde)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-DJ**: (Djibouti)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-DYBJ**: (Dahomey)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-DZ**: (Algeria)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-EG**: (Egypt)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-EH**: (Western Sahara)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-ER**: (Eritrea)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-ET**: (Ethiopia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-GA**: (Gabon)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-GH**: (Ghana)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-GM**: (Gambia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-GN**: (Guinea)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-GQ**: (Equatorial Guinea)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-GW**: (Guinea-Bissau)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-HVBF**: (Upper Volta)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-KE**: (Kenya)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-KM**: (Comoros)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-LR**: (Liberia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-LS**: (Lesotho)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-LY**: (Libya)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-MA**: (Morocco)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-MG**: (Madagascar)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-ML**: (Mali)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-MR**: (Mauritania)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-MU**: (Mauritius)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-MW**: (Malawi)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-MZ**: (Mozambique)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-NA**: (Namibia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-NE**: (Niger)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-NG**: (Nigeria)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-RHZW**: (Southern Rhodesia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-RW**: (Rwanda)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-SC**: (Seychelles)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-SD**: (Sudan)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-SL**: (Sierra Leone)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-SN**: (Senegal)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-SO**: (Somalia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-SS**: (South Sudan)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-ST**: (Sao Tome and Principe)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-SZ**: (Swaziland)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-TD**: (Chad)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-TG**: (Togo)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-TN**: (Tunisia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-TZ**: (Tanzania)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-UG**: (Uganda)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-YT**: (Mayotte)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-ZA**: (South Africa)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-ZM**: (Zambia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-ZRCD**: (Zaire)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-ZW**: (Zimbabwe)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD**: (America)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-AG**: (Antigua and Barbuda)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-AI**: (Anguilla)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-ANHH**: (Netherlands Antilles)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-AR**: (Argentina)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-AS**: (American Samoa)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-AW**: (Aruba)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-BB**: (Barbados)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-BL**: (Saint Barthélemy)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-BM**: (Bermuda Islands)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-BO**: (Bolivia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-BR**: (Brazil)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-BQ**: (Bonaire, Sint Eustatius and Saba)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-BS**: (Bahamas)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-BZ**: (Belize)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-CA**: (Canada)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-CL**: (Chile)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-CO**: (Colombia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-CR**: (Costa Rica)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-CU**: (Cuba)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-CW**: (Curaçao)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-DM**: (Dominica)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-DO**: (Dominican Republic)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-EC**: (Ecuador)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-GD**: (Grenada)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-GF**: (French Guiana)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-GP**: (Guadeloupe)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-GT**: (Guatemala)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-GY**: (Guyana)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-HN**: (Honduras)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-HT**: (Haiti)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-JM**: (Jamaica)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-KN**: (Saint Kitts-Nevis)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-KY**: (Cayman Islands)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-LC**: (Saint Lucia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-MF**: (Saint Martin, Northern)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-MQ**: (Martinique)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-MS**: (Montserrat)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-MX**: (Mexico)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-NI**: (Nicaragua)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-PA**: (Panama)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-PE**: (Peru)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-PM**: (Saint Pierre and Miquelon)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-PR**: (Puerto Rico)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-PY**: (Paraguay)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-PZPA**: (Panama Canal Zone)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-SR**: (Suriname)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-SV**: (El Salvador)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-SX**: (Sint Maarten (Dutch part))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-TC**: (Turks and Caicos Islands)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-TT**: (Trinidad and Tobago)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-US**: (United States)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-UY**: (Uruguay)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-VC**: (Saint Vincent and the Grenadines)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-VE**: (Venezuela)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-VG**: (British Virgin Islands)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-VI**: (Virgin Islands of the United States)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE**: (Australia, Oceania)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-AU**: (Australia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-CC**: (Cocos (Keeling) Islands)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-CK**: (Cook Islands)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-CTKI**: (Canton and Enderbury)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-CX**: (Christmas Island (Indian Ocean))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-FJ**: (Fiji)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-FM**: (Micronesia (Federated States))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-GEHH**: (Gilbert Islands)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-GU**: (Guam)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-KI**: (Kiribati)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-MH**: (Marshall Islands)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-MP**: (Northern Mariana Islands)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-NC**: (New Caledonia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-NF**: (Norfolk Island)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-NHVU**: (New Hebrides)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-NR**: (Nauru)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-NZ**: (New Zealand)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-PCHH**: (Pacific Islands (Trust Territory))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-PF**: (French Polynesia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-PG**: (Papua New Guinea)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-PW**: (Palau)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-SB**: (Solomon Islands)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-TK**: (Tokelau)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-TO**: (Tonga)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-TV**: (Tuvalu)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-VU**: (Vanuatu)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-WF**: (Wallis and Futuna)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-WS**: (Samoa)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XH**: (Arctic)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XI**: (Antarctic Ocean/Antarctica)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XI-AQ**: (Antarctica)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XI-BQAQ**: (British Antarctic Territory)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XI-FQHH**: (Terres australes et antarctiques françaises)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XI-HM**: (Heard and McDonald Islands)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XI-NQAQ**: (Queen Maud Land)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XK**: (Atlantic Ocean)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XK-BV**: (Bouvet Island)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XK-FK**: (Falkland Islands)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XK-FO**: (Faroe Islands)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XK-GL**: (Greenland)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XK-GS**: (South Georgia and the South Sandwich Islands)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XK-SH**: (Saint Helena)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XK-SJ**: (Svalbard and Jan Mayen)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XL**: (Indian Ocean)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XL-IO**: (British Indian Ocean Territory)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XL-RE**: (Réunion)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XL-TF**: (French Southern Territories (the))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XM**: (Pacific Ocean)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XM-JTUM**: (Johnston Atoll)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XM-MIUM**: (Midway Islands)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XM-NU**: (Niue)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XM-PN**: (Pitcairn Island)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XM-PUUM**: (American Territory in the Pacific (-1986))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XM-UM**: (United States Misc. Pacific Islands)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XM-WKUM**: (Wake Island)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XN**: (Outer Space)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XP**: (International Organizations)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XQ**: (World)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XR**: (Orient)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XS**: (Ancient Greece)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XT**: (Rome)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XU**: (Byzantine Empire)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XV**: (Ottoman Empire)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XW**: (Palestinian Arabs)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XX**: (Arab Countries)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XY**: (Jews)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XZ**: (Imaginary places)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#ZZ**: (Country unknown)

---

<p style="margin-bottom:60px"></p>

<a name="d17e5459"></a>
#### GND Term URI
|     |     |
| --- | --- |
| **Name** | `@gndo:term`  |
| **Datatype** | anyURI (*RNG Pattern Matching*) |
| **Label** (de) | GND Term URI |
| **Description** (de) | URI eines Terms entweder aus der **GND** (https://d-nb.info/gnd/) oder einem **GND Vokabular** (https://d-nb.info/standards/vocab/) |
| **Contained by** | [`gndo:functionOrRole`](#d17e3460)  [`gndo:gndSubjectCategory`](#d17e3634)  [`gndo:professionOrOccupation`](#d17e4262) |

***Validation***  

```xml 
<param xmlns="http://relaxng.org/ns/structure/1.0" name="pattern">(http|https)://d-nb.info/standards/vocab/gnd/(.+)</param>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e5439"></a>
#### GND-Referenz
|     |     |
| --- | --- |
| **Name** | `@gndo:ref`  |
| **Datatype** | anyURI (*RNG Pattern Matching*) |
| **Label** (de) | GND-Referenz |
| **Label** (en) | GND-Reference |
| **Description** (de) | Referenz auf eine Entität in der GND, identifiziert durch einen GND-URI. |
| **Description** (en) | Reference to an entity in the GND identified by a GND-URI. |
| **Contained by** | [`bf:instanceOf`](#instance-of)  [`embodimentOf`](#d17e1339)  [`gndo:affiliation`](#d17e2929)  [`gndo:author`](#d17e2952)  [`gndo:editor`](#d17e3277)  [`gndo:fieldOfStudy`](#d17e3363)  [`gndo:firstAuthor`](#d17e3403)  [`gndo:formOfWorkAndExpression`](#d17e3332)  [`gndo:literarySource`](#d17e3787)  [`gndo:place`](#geographic-entity-place)  [`gndo:placeOfActivity`](#person-record-placeOfActivity)  [`gndo:placeOfBirth`](#person-record-placeOfBirth)  [`gndo:placeOfBusiness`](#d17e3967)  [`gndo:placeOfDeath`](#person-record-placeOfDeath)  [`gndo:precedingCorporateBody`](#d17e4133)  [`gndo:precedingPlaceOrGeographicName`](#d17e4101)  [`gndo:professionOrOccupation`](#d17e4262)  [`gndo:pseudonym`](#d17e4321)  [`gndo:publication`](#d17e4368)  [`gndo:relatedWork`](#d17e4565)  [`gndo:relatesTo`](#d17e4511)  [`gndo:succeedingCorporateBody`](#d17e4596)  [`gndo:succeedingPlaceOrGeographicName`](#d17e4626)  [`gndo:temporaryName`](#d17e4681)  [`gndo:titleOfNobility`](#d17e4726)  [`realizationOf`](#d17e1265) |

***Validation***  

```xml 
<param xmlns="http://relaxng.org/ns/structure/1.0" name="pattern">(http|https)://d-nb.info/gnd/(.+)</param>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e5417"></a>
#### GND-URI
|     |     |
| --- | --- |
| **Name** | `@gndo:uri`  |
| **Datatype** | anyURI (*RNG Pattern Matching*) |
| **Label** (de) | GND-URI |
| **Label** (en) | GND-URI |
| **Description** (de) | Angabe des GND-HTTP URIs der beschriebenen Entität in der GND, sofern vorhanden. |
| **Description** (en) | - |
| **Contained by** | [`corporateBody`](#corporate-body-record)  [`entity`](#entity-record)  [`event`](#d17e243)  [`expression`](#d17e1235)  [`manifestation`](#d17e1304)  [`person`](#person-record)  [`place`](#d17e1061)  [`work`](#d17e1378) |

***Validation***  

```xml 
<param xmlns="http://relaxng.org/ns/structure/1.0" name="pattern">(http|https)://d-nb.info/gnd/(.+)</param>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e191"></a>
#### GNDO-Typ der beschriebenen Entität
|     |     |
| --- | --- |
| **Name** | `@gndo:type`  |
| **Datatype** | string |
| **Label** (de) | GNDO-Typ der beschriebenen Entität |
| **Description** (de) | URI eines Entitätstyps (Class) aus der [GNDO](https://d-nb.info/standards/elementset/gnd#). |
| **Contained by** | [`entity`](#entity-record) |

***Predefined Values***  

 - **https://d-nb.info/standards/elementset/gnd#CorporateBody**: (Körperschaft) [GNDO](https://d-nb.info/standards/elementset/gnd#id-39ad8031221791a9a0a5adafd88b8ff9)
 - **https://d-nb.info/standards/elementset/gnd#Company**: (Firma) [GNDO](https://d-nb.info/standards/elementset/gnd#id-1d31818b589bae5f506efcd36f8e4071)
 - **https://d-nb.info/standards/elementset/gnd#FictiveCorporateBody**: (Fiktive Körperschaft) [GNDO](https://d-nb.info/standards/elementset/gnd#id-233492de7966e18b7fe36583464ada24)
 - **https://d-nb.info/standards/elementset/gnd#MusicalCorporateBody**: (Musikalische Körperschaft) [GNDO](https://d-nb.info/standards/elementset/gnd#id-227522d76d555c9211e00f612ea79763)
 - **https://d-nb.info/standards/elementset/gnd#OrganOfCorporateBody**: (Organ einer Körperschaft) [GNDO](https://d-nb.info/standards/elementset/gnd#id-908a2b3cce6f09b3795666879fa1b13f)
 - **https://d-nb.info/standards/elementset/gnd#ProjectOrProgram**: (Projekt oder Programm) [GNDO](https://d-nb.info/standards/elementset/gnd#id-4dc1ef59dd262d466aeaf68091c1afdd)
 - **https://d-nb.info/standards/elementset/gnd#ReligiousAdministrativeUnit**: (Religiöse Verwaltungseinheit) [GNDO](https://d-nb.info/standards/elementset/gnd#id-4862583256b7553a04901cb9d78d2d8f)
 - **https://d-nb.info/standards/elementset/gnd#ReligiousCorporateBody**: (Religiöse Körperschaft) [GNDO](https://d-nb.info/standards/elementset/gnd#id-f5be3238b49f943d1458d99173b33295)
 - **https://d-nb.info/standards/elementset/gnd#ConferenceOrEvent**: (Konferenz oder Veranstaltung) [GNDO](https://d-nb.info/standards/elementset/gnd#id-50ee5a233a390c91e26b0d72b0a9a437)
 - **https://d-nb.info/standards/elementset/gnd#DifferentiatedPerson**: (Individualisierte Person) [GNDO](https://d-nb.info/standards/elementset/gnd#id-07e558e11b6629c85c4cf0312a8d9acc)
 - **https://d-nb.info/standards/elementset/gnd#CollectivePseudonym**: (Sammelpseudonym) [GNDO](https://d-nb.info/standards/elementset/gnd#id-e1a0c1b1bb1f0c6d862c101153127efb)
 - **https://d-nb.info/standards/elementset/gnd#Gods**: (Götter) [GNDO](https://d-nb.info/standards/elementset/gnd#id-4c3eb377f41e2e358d95fd66e85531f6)
 - **https://d-nb.info/standards/elementset/gnd#LiteraryOrLegendaryCharacter**: (Literarische oder Sagengestalt) [GNDO](https://d-nb.info/standards/elementset/gnd#id-561f9122a59eb9f11ca9ac9fbbb41015)
 - **https://d-nb.info/standards/elementset/gnd#Pseudonym**: (Pseudonym) [GNDO](https://d-nb.info/standards/elementset/gnd#id-4a1b77ecd57d7fe7074050aa7fddef3e)
 - **https://d-nb.info/standards/elementset/gnd#RoyalOrMemberOfARoyalHouse**: (Regierender Fürst oder Mitglied eines regierenden Fürstenhauses) [GNDO](https://d-nb.info/standards/elementset/gnd#id-48fef5513dcda9669ac3250908b3fc0e)
 - **https://d-nb.info/standards/elementset/gnd#Spirits**: (Geister) [GNDO](https://d-nb.info/standards/elementset/gnd#id-a53c2a79950d8502724d1e1a31ab19cb)
 - **https://d-nb.info/standards/elementset/gnd#PlaceOrGeographicName**: (Geografikum) [GNDO](https://d-nb.info/standards/elementset/gnd#id-a708231e5f676cb2d2aed717ac50d3c0)
 - **https://d-nb.info/standards/elementset/gnd#AdministrativeUnit**: (Verwaltungseinheit) [GNDO](https://d-nb.info/standards/elementset/gnd#id-1f0359b1603bcf7dfd0e822f7f4ffce7)
 - **https://d-nb.info/standards/elementset/gnd#BuildingOrMemorial**: (Bauwerk oder Denkmal) [GNDO](https://d-nb.info/standards/elementset/gnd#id-6873abeb35c905e0a6bd4279db13338e)
 - **https://d-nb.info/standards/elementset/gnd#Country**: (Land oder Staat) [GNDO](https://d-nb.info/standards/elementset/gnd#id-28d79d328de51fb38a288f6386717122)
 - **https://d-nb.info/standards/elementset/gnd#ExtraterrestrialTerritory**: (Extraterrestrikum) [GNDO](https://d-nb.info/standards/elementset/gnd#id-4a1b77ecd57d7fe7074050aa7fddef3e)
 - **https://d-nb.info/standards/elementset/gnd#FictivePlace**: (Fiktiver Ort) [GNDO](https://d-nb.info/standards/elementset/gnd#id-03854be83ea75e449883470169192ac1)
 - **https://d-nb.info/standards/elementset/gnd#MemberState**: (Gliedstaat) [GNDO](https://d-nb.info/standards/elementset/gnd#id-b9a80ea4ee95ebb62d0b78e7bc1baeb2)
 - **https://d-nb.info/standards/elementset/gnd#NameOfSmallGeographicUnitLyingWithinAnotherGeographicUnit**: (Kleinräumiges Geografikum innerhalb eines Ortes) [GNDO](https://d-nb.info/standards/elementset/gnd#id-d804f233455af5455ceea054be998d1f)
 - **https://d-nb.info/standards/elementset/gnd#NaturalGeographicUnit**: (Natürlich geografische Einheit) [GNDO](https://d-nb.info/standards/elementset/gnd#id-cdeb648dbe6d599c7f6f3ed98e7f3cdd)
 - **https://d-nb.info/standards/elementset/gnd#ReligiousTerritory**: (Religiöses Territorium) [GNDO](https://d-nb.info/standards/elementset/gnd#id-580efe99e43d92a022fe381e592afecf)
 - **https://d-nb.info/standards/elementset/gnd#TerritorialCorporateBodyOrAdministrativeUnit**: (Gebietskörperschaft oder Verwaltungseinheit) [GNDO](https://d-nb.info/standards/elementset/gnd#id-5752c1088a8f6f21005a54f1b3ac1edf)
 - **https://d-nb.info/standards/elementset/gnd#WayBorderOrLine**: (Weg, Grenze oder Linie) [GNDO](https://d-nb.info/standards/elementset/gnd#id-508b5841a6ebe341e738e53fbf1c7455)
 - **https://d-nb.info/standards/elementset/gnd#Work**: (Werk) [GNDO](https://d-nb.info/standards/elementset/gnd#id-c1e6e78875a240757c51a48091e75f13)
 - **https://d-nb.info/standards/elementset/gnd#Collection**: (Sammlung) [GNDO](https://d-nb.info/standards/elementset/gnd#id-cefc48c804ce704690b446462cb31aec)
 - **https://d-nb.info/standards/elementset/gnd#CollectiveManuscript**: (Sammelhandschrift) [GNDO](https://d-nb.info/standards/elementset/gnd#id-6a1601c81cac71d7324e45d4dcdc0771)
 - **https://d-nb.info/standards/elementset/gnd#Expression**: (Expression) [GNDO](https://d-nb.info/standards/elementset/gnd#id-3bd92dac383bb4fb6b9a636cf9bf9910)
 - **https://d-nb.info/standards/elementset/gnd#Manuscript**: (Schriftdenkmal) [GNDO](https://d-nb.info/standards/elementset/gnd#id-80ff91c42283532e5e51e45b11d3783a)
 - **https://d-nb.info/standards/elementset/gnd#MusicalWork**: (Werk der Musik) [GNDO](https://d-nb.info/standards/elementset/gnd#id-9696269e5379c76bd51afb23a34d3c10)
 - **https://d-nb.info/standards/elementset/gnd#ProvenanceCharacteristic**: (Provenienzmerkmal) [GNDO](https://d-nb.info/standards/elementset/gnd#id-f0a8b51b89f16454cda6c0e332225bfc)
 - **https://d-nb.info/standards/elementset/gnd#VersionOfAMusicalWork**: (Fassung eines Werks der Musik) [GNDO](https://d-nb.info/standards/elementset/gnd#id-931aecb58a23c5f3ef7148cd891c38ee)

---

<p style="margin-bottom:60px"></p>

<a name="d17e5475"></a>
#### ID
|     |     |
| --- | --- |
| **Name** | `@id`  |
| **Datatype** | ID |
| **Label** (de) | ID |
| **Description** (de) | Eine interne ID zur Identifikation des entsprechenden Elements. |
| **Contained by** | [`list`](#data-list)  [`respStmt`](#respStmt) |

---

<p style="margin-bottom:60px"></p>

<a name="d17e5379"></a>
#### ISIL/Bibliothekssiegel
|     |     |
| --- | --- |
| **Name** | `@isil`  |
| **Datatype** | string (*RNG Pattern Matching*) |
| **Label** (de) | ISIL/Bibliothekssiegel |
| **Label** (de) | ISIL/Acronym for libraries |
| **Description** (de) | Eindeutiger Identifikator der Organisation als ISIL (International Standard Identifier for Libraries and Related Organisations). |
| **Description** (de) | ISIL of an Organisation (International Standard Identifier for Libraries and Related Organisations). |
| **Contained by** | [`agency`](#agency-stmt) |

***Validation***  

```xml 
<param xmlns="http://relaxng.org/ns/structure/1.0" name="pattern">[A-Z]{1,4}-[a-zA-Z0-9\-/:]{1,11}</param>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="attr.iso-date"></a>
#### ISO-Datum
|     |     |
| --- | --- |
| **Name** | `@iso-date`  |
| **Datatype** | date gYearMonth gYear  |
| **Label** (de) | ISO-Datum |
| **Label** (en) | ISO-Date |
| **Description** (de) | Ein ISO Datum im Format JJJJ-MM-TT, JJJJ-MM oder JJJJ |
| **Description** (en) | An ISO date in the Format YYYY-MM-DD, YYYY-MM or YYYY |
| **Contained by** | [`foaf:page`](#elem.foaf.page)  [`gndo:dateOfBirth`](#person-record-dateOfBirth)  [`gndo:dateOfDeath`](#person-record-dateOfDeath)  [`gndo:dateOfEstablishment`](#d17e3105)  [`gndo:dateOfPublication`](#d17e3174)  [`gndo:dateOfTermination`](#d17e3201)  [`gndo:homepage`](#elem.gndo.homepage) |

---

<p style="margin-bottom:60px"></p>

<a name="d17e586"></a>
#### Körperschaftstyp
|     |     |
| --- | --- |
| **Name** | `@gndo:type`  |
| **Datatype** | string |
| **Label** (de) | Körperschaftstyp |
| **Label** (en) | Type of a Corporate Body |
| **Description** (de) | Typisierung der dokumentierten Körperschaft (z.B. als fiktive Körperschaft, Firma usw). Wenn kein `@gndo:type` vergeben wird, gilt die Körperschaft als [gndo:CorporateBody](https://d-nb.info/standards/elementset/gnd#id-39ad8031221791a9a0a5adafd88b8ff9). |
| **Description** (en) | Spezifies the Corporate Body encoded as eg. fictional corporate body, company etc.). If no `@gndo:type` is provided the default is [gndo:CorporateBody](https://d-nb.info/standards/elementset/gnd#id-39ad8031221791a9a0a5adafd88b8ff9). |
| **Contained by** | [`corporateBody`](#corporate-body-record) |

***Predefined Values***  

 - **https://d-nb.info/standards/elementset/gnd#Company**: (Firma) [GNDO](https://d-nb.info/standards/elementset/gnd#id-1d31818b589bae5f506efcd36f8e4071)
 - **https://d-nb.info/standards/elementset/gnd#FictiveCorporateBody**: (Fiktive Körperschaft) [GNDO](https://d-nb.info/standards/elementset/gnd#id-233492de7966e18b7fe36583464ada24)
 - **https://d-nb.info/standards/elementset/gnd#MusicalCorporateBody**: (Musikalische Körperschaft) [GNDO](https://d-nb.info/standards/elementset/gnd#id-227522d76d555c9211e00f612ea79763)
 - **https://d-nb.info/standards/elementset/gnd#OrganOfCorporateBody**: (Organ einer Körperschaft) [GNDO](https://d-nb.info/standards/elementset/gnd#id-908a2b3cce6f09b3795666879fa1b13f)
 - **https://d-nb.info/standards/elementset/gnd#ProjectOrProgram**: (Projekt oder Programm) [GNDO](https://d-nb.info/standards/elementset/gnd#id-4dc1ef59dd262d466aeaf68091c1afdd)
 - **https://d-nb.info/standards/elementset/gnd#ReligiousAdministrativeUnit**: (Religiöse Verwaltungseinheit) [GNDO](https://d-nb.info/standards/elementset/gnd#id-4862583256b7553a04901cb9d78d2d8f)
 - **https://d-nb.info/standards/elementset/gnd#ReligiousCorporateBody**: (Religiöse Körperschaft) [GNDO](https://d-nb.info/standards/elementset/gnd#id-f5be3238b49f943d1458d99173b33295)

---

<p style="margin-bottom:60px"></p>

<a name="d17e3757"></a>
#### Language Code URI
|     |     |
| --- | --- |
| **Name** | `@gndo:term`  |
| **Datatype** | anyURI (*Schematron Pattern*) |
| **Label** (de) | Language Code URI |
| **Description** (de) | Ein URI, bevorzugt aus dem [LOC ISO 693-2 Language Vocabulary](http://id.loc.gov/vocabulary/iso639-2/) |
| **Contained by** | [`gndo:languageCode`](#d17e3736) |

***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="gndo:languageCode/@gndo:term" role="warning">
      <sch:assert test="matches(., '(http|https)://id.loc.gov/vocabulary/iso639-2/(.+)')">(Missing <sch:name/>): It is recommendet to use an URI Descriptor 
                            from the LOC ISO 693-2 Language Vocabulary (http://id.loc.gov/vocabulary/iso639-2/).</sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e1167"></a>
#### Ortstyp
|     |     |
| --- | --- |
| **Name** | `@gndo:type`  |
| **Datatype** | string |
| **Label** (de) | Ortstyp |
| **Label** (en) | Type of a Place |
| **Description** (de) | Typisierung des dokumentierten Orts (z.B. als fiktiver Ort, Gebäude oder Land). Wenn kein `@gndo:type` vergeben wird, gilt der Ort als [gndo:PlaceOrGeographicName](https://d-nb.info/standards/elementset/gnd#id-a708231e5f676cb2d2aed717ac50d3c0). |
| **Description** (en) | Spezifies the place encoded as eg. fictional Place, Building or Country). If no `@gndo:type` is provided the default is [gndo:PlaceOrGeographicName](https://d-nb.info/standards/elementset/gnd#id-a708231e5f676cb2d2aed717ac50d3c0). |
| **Contained by** | [`place`](#d17e1061) |

***Predefined Values***  

 - **https://d-nb.info/standards/elementset/gnd#AdministrativeUnit**: (Verwaltungseinheit) [GNDO](https://d-nb.info/standards/elementset/gnd#id-1f0359b1603bcf7dfd0e822f7f4ffce7)
 - **https://d-nb.info/standards/elementset/gnd#BuildingOrMemorial**: (Bauwerk oder Denkmal) [GNDO](https://d-nb.info/standards/elementset/gnd#id-6873abeb35c905e0a6bd4279db13338e)
 - **https://d-nb.info/standards/elementset/gnd#Country**: (Land oder Staat) [GNDO](https://d-nb.info/standards/elementset/gnd#id-28d79d328de51fb38a288f6386717122)
 - **https://d-nb.info/standards/elementset/gnd#ExtraterrestrialTerritory**: (Extraterrestrikum) [GNDO](https://d-nb.info/standards/elementset/gnd#id-4a1b77ecd57d7fe7074050aa7fddef3e)
 - **https://d-nb.info/standards/elementset/gnd#FictivePlace**: (Fiktiver Ort) [GNDO](https://d-nb.info/standards/elementset/gnd#id-03854be83ea75e449883470169192ac1)
 - **https://d-nb.info/standards/elementset/gnd#MemberState**: (Gliedstaat) [GNDO](https://d-nb.info/standards/elementset/gnd#id-b9a80ea4ee95ebb62d0b78e7bc1baeb2)
 - **https://d-nb.info/standards/elementset/gnd#NameOfSmallGeographicUnitLyingWithinAnotherGeographicUnit**: (Kleinräumiges Geografikum innerhalb eines Ortes) [GNDO](https://d-nb.info/standards/elementset/gnd#id-d804f233455af5455ceea054be998d1f)
 - **https://d-nb.info/standards/elementset/gnd#NaturalGeographicUnit**: (Natürlich geografische Einheit) [GNDO](https://d-nb.info/standards/elementset/gnd#id-cdeb648dbe6d599c7f6f3ed98e7f3cdd)
 - **https://d-nb.info/standards/elementset/gnd#ReligiousTerritory**: (Religiöses Territorium) [GNDO](https://d-nb.info/standards/elementset/gnd#id-580efe99e43d92a022fe381e592afecf)
 - **https://d-nb.info/standards/elementset/gnd#TerritorialCorporateBodyOrAdministrativeUnit**: (Gebietskörperschaft oder Verwaltungseinheit) [GNDO](https://d-nb.info/standards/elementset/gnd#id-5752c1088a8f6f21005a54f1b3ac1edf)
 - **https://d-nb.info/standards/elementset/gnd#WayBorderOrLine**: (Weg, Grenze oder Linie) [GNDO](https://d-nb.info/standards/elementset/gnd#id-508b5841a6ebe341e738e53fbf1c7455)

---

<p style="margin-bottom:60px"></p>

<a name="person-types"></a>
#### Personentyp
|     |     |
| --- | --- |
| **Name** | `@gndo:type`  |
| **Datatype** | string |
| **Label** (de) | Personentyp |
| **Label** (en) | Type of a Person |
| **Description** (de) | Typisierung der dokumentierten Person (z.B. als fiktive Person, Gott oder royale Person). Wenn kein `@gndo:type` vergeben wird, gilt die Person als [gndo:DifferentiatedPerson](https://d-nb.info/standards/elementset/gnd#id-07e558e11b6629c85c4cf0312a8d9acc). |
| **Description** (en) | Spezifies the person encoded as eg. fictional character, god oder royal). If no `@gndo:type` is provided the default is [gndo:DifferentiatedPerson](https://d-nb.info/standards/elementset/gnd#id-07e558e11b6629c85c4cf0312a8d9acc). |
| **Contained by** | [`person`](#person-record) |

***Predefined Values***  

 - **https://d-nb.info/standards/elementset/gnd#CollectivePseudonym**: (Sammelpseudonym) [GNDO](https://d-nb.info/standards/elementset/gnd#id-e1a0c1b1bb1f0c6d862c101153127efb)
 - **https://d-nb.info/standards/elementset/gnd#Gods**: (Götter) [GNDO](https://d-nb.info/standards/elementset/gnd#id-4c3eb377f41e2e358d95fd66e85531f6)
 - **https://d-nb.info/standards/elementset/gnd#LiteraryOrLegendaryCharacter**: (Literarische oder Sagengestalt) [GNDO](https://d-nb.info/standards/elementset/gnd#id-561f9122a59eb9f11ca9ac9fbbb41015)
 - **https://d-nb.info/standards/elementset/gnd#Pseudonym**: (Pseudonym) [GNDO](https://d-nb.info/standards/elementset/gnd#id-4a1b77ecd57d7fe7074050aa7fddef3e)
 - **https://d-nb.info/standards/elementset/gnd#RoyalOrMemberOfARoyalHouse**: (Regierender Fürst oder Mitglied eines regierenden Fürstenhauses) [GNDO](https://d-nb.info/standards/elementset/gnd#id-48fef5513dcda9669ac3250908b3fc0e)
 - **https://d-nb.info/standards/elementset/gnd#Spirits**: (Geister) [GNDO](https://d-nb.info/standards/elementset/gnd#id-a53c2a79950d8502724d1e1a31ab19cb)

---

<p style="margin-bottom:60px"></p>

<a name="d17e5260"></a>
#### Projekt ID
|     |     |
| --- | --- |
| **Name** | `@project`  |
| **Datatype** | string |
| **Label** (de) | Projekt ID |
| **Description** (de) | Identifiers einer Projekts, mit dem der Datensatz in einem entityXML Store assoziiert ist. |
| **Contained by** | [`store:store`](#d17e5232) |

---

<p style="margin-bottom:60px"></p>

<a name="d17e4983"></a>
#### Provider-ID
|     |     |
| --- | --- |
| **Name** | `@id`  |
| **Datatype** | ID |
| **Label** (de) | Provider-ID |
| **Description** (de) | Der GND-Agentur Identifier des Datenproviders. Dieser Identifier wird in der Regel direkt von der GND-Agentur, bei der der Datenlieferant registriert ist, vergeben. |
| **Contained by** | [`provider`](#data-provider) |

---

<p style="margin-bottom:60px"></p>

<a name="d17e2788"></a>
#### Quelle der Koordinaten
|     |     |
| --- | --- |
| **Name** | `@geo:source`  |
| **Datatype** | anyURI |
| **Label** (de) | Quelle der Koordinaten |
| **Label** (en) | Source of Coordinates |
| **Description** (de) | Ein URL zur Quelle der documentierten Koordinaten. |
| **Description** (en) | An URL pointing to the source of the coordinates encoded. |
| **Contained by** | [`geo:hasGeometry`](#d17e2764) |

---

<p style="margin-bottom:60px"></p>

<a name="d17e5720"></a>
#### Record ID
|     |     |
| --- | --- |
| **Name** | `@xml:id`  |
| **Datatype** | ID |
| **Label** (de) | Record ID |
| **Description** (de) | Angabe eines Identifiers zur Identifikation eines Entitäteneintrags innerhalb der Datensammlung. Die Angabe einer ID via `@xml:id` ist verpflichtend! |
| **Contained by** | [`corporateBody`](#corporate-body-record)  [`entity`](#entity-record)  [`event`](#d17e243)  [`expression`](#d17e1235)  [`manifestation`](#d17e1304)  [`person`](#person-record)  [`place`](#d17e1061)  [`work`](#d17e1378) |

---

<p style="margin-bottom:60px"></p>

<a name="d17e5735"></a>
#### Referenz
|     |     |
| --- | --- |
| **Name** | `@ref`  |
| **Datatype** | anyURI (*Schematron Pattern*) |
| **Label** (de) | Referenz |
| **Description** (de) | Verweis auf die ID (`@xml:id`) eines Records in der selben entityXML Ressource oder auf einen HTTP-URI. |
| **Contained by** | [`bf:instanceOf`](#instance-of)  [`embodimentOf`](#d17e1339)  [`gndo:affiliation`](#d17e2929)  [`gndo:author`](#d17e2952)  [`gndo:editor`](#d17e3277)  [`gndo:fieldOfStudy`](#d17e3363)  [`gndo:firstAuthor`](#d17e3403)  [`gndo:formOfWorkAndExpression`](#d17e3332)  [`gndo:literarySource`](#d17e3787)  [`gndo:place`](#geographic-entity-place)  [`gndo:placeOfActivity`](#person-record-placeOfActivity)  [`gndo:placeOfBirth`](#person-record-placeOfBirth)  [`gndo:placeOfBusiness`](#d17e3967)  [`gndo:placeOfDeath`](#person-record-placeOfDeath)  [`gndo:precedingCorporateBody`](#d17e4133)  [`gndo:precedingPlaceOrGeographicName`](#d17e4101)  [`gndo:pseudonym`](#d17e4321)  [`gndo:publication`](#d17e4368)  [`gndo:relatedWork`](#d17e4565)  [`gndo:relatesTo`](#d17e4511)  [`gndo:succeedingCorporateBody`](#d17e4596)  [`gndo:succeedingPlaceOrGeographicName`](#d17e4626)  [`gndo:temporaryName`](#d17e4681)  [`gndo:titleOfNobility`](#d17e4726)  [`realizationOf`](#d17e1265) |

***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="@ref[starts-with(., '#')]" role="error">
      <sch:let name="id" value="substring-after(data(.), '#')"/>
      <sch:assert test="//element()[@xml:id = $id]">(Wrong ID): There is no ID "<sch:value-of select="$id"/>" in this resource! Your reference is most likely wrong!</sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e4416"></a>
#### Rolle der Entität (Publikation)
|     |     |
| --- | --- |
| **Name** | `@role`  |
| **Datatype** | string |
| **Label** (de) | Rolle der Entität (Publikation) |
| **Description** (de) | Hinweis darauf, welche Rolle die beschreibene Entität in Zusammenhang mit der Publikation spielt. |
| **Contained by** | [`gndo:publication`](#d17e4368) |

***Predefined Values***  

 - **author**: (Autor:in *default*) Die beschriebene Entität (Person) ist der/die Autor:in der Publikation.
 - **editor**: (Herausgeber:in) Die beschriebene Entität (Person) ist der/die Herausgeber:in der Publikation.
 - **about**: (About) Die Publikation handelt von der beschriebenen Entität.

---

<p style="margin-bottom:60px"></p>

<a name="d17e4294"></a>
#### Signifikanz
|     |     |
| --- | --- |
| **Name** | `@gndo:type`  |
| **Datatype** | string |
| **Label** (de) | Signifikanz |
| **Description** (de) | Angabe zur Signifikanz der Beschäftigung einer Person, d.h. ob es sich bei der angegebenen Beschäftigung um ihre hauptsächliche bzw. charakteristische Beschäfigung handelt. |
| **Contained by** | [`gndo:professionOrOccupation`](#d17e4262) |

***Predefined Values***  

 - **significant**: (charakteristisch) Der Beruf oder der Tätigkeitsbereich ist für die Person charakteristisch.

---

<p style="margin-bottom:60px"></p>

<a name="d17e5550"></a>
#### Sprache
|     |     |
| --- | --- |
| **Name** | `@xml:lang`  |
| **Datatype** | string |
| **Label** (de) | Sprache |
| **Label** (en) | Language |
| **Description** (de) | Die Sprache, in der die Information angegeben ist. |
| **Description** (en) | The language, the information is given in. |
| **Contained by** | [`dc:title`](#d17e2645)  [`gndo:abbreviatedName`](#d17e2856)  [`gndo:biographicalOrHistoricalInformation`](#d17e2985)  [`gndo:preferredName`](#person-record-preferredName)  [`gndo:preferredName`](#d17e4196)  [`gndo:temporaryName`](#d17e4681)  [`gndo:titleOfNobility`](#d17e4726)  [`gndo:variantName`](#person-record-variantName)  [`gndo:variantName`](#elem.gndo.variantName.standard)  [`skos:note`](#elem.skos.note) |

---

<p style="margin-bottom:60px"></p>

<a name="d17e5764"></a>
#### Term
|     |     |
| --- | --- |
| **Name** | `@term`  |
| **Datatype** | string |
| **Label** (de) | Term |
| **Description** (de) | - |
| **Contained by** |  |

---

<p style="margin-bottom:60px"></p>

<a name="d17e5216"></a>
#### URL
|     |     |
| --- | --- |
| **Name** | `@url`  |
| **Datatype** | string |
| **Label** (de) | URL |
| **Description** (de) | URL zu einer Quelle, falls vorhanden. |
| **Contained by** | [`source`](#elem.source) |

---

<p style="margin-bottom:60px"></p>

<a name="d17e2245"></a>
#### Verantwortliche Person
|     |     |
| --- | --- |
| **Name** | `@who`  |
| **Datatype** | IDREF |
| **Label** (de) | Verantwortliche Person |
| **Description** (de) | Die ID einer Person, die für die Änderung verantwortlich zeichnet. |
| **Contained by** | [`change`](#d17e2229) |

---

<p style="margin-bottom:60px"></p>

<a name="d17e1467"></a>
#### Werkstyp
|     |     |
| --- | --- |
| **Name** | `@gndo:type`  |
| **Datatype** | string |
| **Label** (de) | Werkstyp |
| **Label** (en) | Type of a Work |
| **Description** (de) | Typisierung des dokumentierten Werks (z.B. als Sammlung, Werk der Musik etc.). Wenn kein `@gndo:type` vergeben wird, gilt der Ort als [gndo:Work](https://d-nb.info/standards/elementset/gnd#id-c1e6e78875a240757c51a48091e75f13). |
| **Description** (en) | Spezifies the work encoded as eg. collection, musical work etc. If no `@gndo:type` is provided the default is [gndo:Work](https://d-nb.info/standards/elementset/gnd#id-c1e6e78875a240757c51a48091e75f13). |
| **Contained by** | [`work`](#d17e1378) |

***Predefined Values***  

 - **https://d-nb.info/standards/elementset/gnd#Collection**: (Sammlung) [GNDO](https://d-nb.info/standards/elementset/gnd#id-cefc48c804ce704690b446462cb31aec)
 - **https://d-nb.info/standards/elementset/gnd#CollectiveManuscript**: (Sammelhandschrift) [GNDO](https://d-nb.info/standards/elementset/gnd#id-6a1601c81cac71d7324e45d4dcdc0771)
 - **https://d-nb.info/standards/elementset/gnd#Expression**: (Expression) [GNDO](https://d-nb.info/standards/elementset/gnd#id-3bd92dac383bb4fb6b9a636cf9bf9910)
 - **https://d-nb.info/standards/elementset/gnd#Manuscript**: (Schriftdenkmal) [GNDO](https://d-nb.info/standards/elementset/gnd#id-80ff91c42283532e5e51e45b11d3783a)
 - **https://d-nb.info/standards/elementset/gnd#MusicalWork**: (Werk der Musik) [GNDO](https://d-nb.info/standards/elementset/gnd#id-9696269e5379c76bd51afb23a34d3c10)
 - **https://d-nb.info/standards/elementset/gnd#ProvenanceCharacteristic**: (Provenienzmerkmal) [GNDO](https://d-nb.info/standards/elementset/gnd#id-f0a8b51b89f16454cda6c0e332225bfc)
 - **https://d-nb.info/standards/elementset/gnd#VersionOfAMusicalWork**: (Fassung eines Werks der Musik) [GNDO](https://d-nb.info/standards/elementset/gnd#id-931aecb58a23c5f3ef7148cd891c38ee)

---

<p style="margin-bottom:60px"></p>

<a name="d17e5021"></a>
#### Zieladresse (Hyperlink)
|     |     |
| --- | --- |
| **Name** | `@target`  |
| **Datatype** | anyURI (*RNG Pattern Matching*) |
| **Label** (de) | Zieladresse (Hyperlink) |
| **Description** (de) | Zieladresse eines Hyperlinks, repräsentiert durch einen URL nach HTTP oder HTTPS Schema. |
| **Contained by** | [`ref`](#elem.ref) |

***Validation***  

```xml 
<param xmlns="http://relaxng.org/ns/structure/1.0" name="pattern">(https|http)://(\S+)</param>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e5248"></a>
#### entityXML Store ID
|     |     |
| --- | --- |
| **Name** | `@id`  |
| **Datatype** | string |
| **Label** (de) | entityXML Store ID |
| **Description** (de) | Identifier des Datensatzes in einem entityXML Store. |
| **Contained by** | [`store:store`](#d17e5232) |

---

<p style="margin-bottom:60px"></p>

<a name="d17e5302"></a>
#### name
|     |     |
| --- | --- |
| **Name** | `@name`  |
| **Datatype** | string |
| **Contained by** | [`store:step`](#d17e5288) |

---

<p style="margin-bottom:60px"></p>

<a name="d17e5304"></a>
#### timestamp
|     |     |
| --- | --- |
| **Name** | `@timestamp`  |
| **Datatype** | string |
| **Contained by** | [`store:step`](#d17e5288) |

---

<p style="margin-bottom:60px"></p>


<a name="serialisation"></a>
## Serialisierungen

Daten in entityXML lassen sich in verschiedene XML Stile aber auch andere Datenformate serialisieren:

<a name="docs_d14e2668"></a>
### XML


- **entityXML → entityXML (strict)**: [`entityxml.strict.xsl`](https://gitlab.gwdg.de/entities/entityxml/-/blob/master/scripts/xslt/entityxml.strict.xsl): Konversion, die alle *custom namespaces* aus einer entityXML Ressource entfernt. Sie ist fester Bestandteil des Ingestworkflows von Daten in die GND durch die Text+ GND Agentur.
- **entityXML → MARC-XML**: [`entityxml2marcxml.xsl`](https://gitlab.gwdg.de/entities/entityxml/-/blob/master/scripts/xslt/marcxml/entityxml2marcxml.xsl): Konversion, die entityXML Einträge in MARC-XML konvertiert. Sie ist fester Bestandteil des Ingestworkflows von Daten in die GND durch die Text+ GND Agentur.

<a name="docs_d14e2691"></a>
### JSON


- **entityXML → JSON**: [`entityxml2json.xsl`](https://gitlab.gwdg.de/entities/entityxml/-/blob/master/scripts/xslt/entityxml2json.xsl): JSON Serialisierung einer entityXML Ressource.

<a name="docs_d14e2705"></a>
### Markdown


- **entityXML → Markdown**: [`entityxml2md.xsl`](https://gitlab.gwdg.de/entities/entityxml/-/blob/master/scripts/xslt/entityxml2md.xsl): Markdown Liste mit einzelnen Einträgen aus einer entityXML Ressource

<a name="docs_d14e2720"></a>
### CSV


- **entityXML → CSV**: [`entityxml2csv.xsl`](https://gitlab.gwdg.de/entities/entityxml/-/blob/master/scripts/xslt/entityxml2csv.xsl) (Proof of Concept): CSV Liste aller Einträge einer entityXML Ressource. *nota bene*: Diese Konversion steckt noch in den Kinderschuhen.

<a name="revision-history"></a>
## Revisionen



##0.5.1 ALPHA (Nigtly) (2023-02-08)  

- separated record types.
- revised `revision` description model.
- cleaned `gndo:publication` and added simple bibliographic model.
- changed record-id to `@xml:id`
- added `bf:instanceOf` to `manifestation` entity.
- changed `gndo:page` to `gndo:homepage` (cf. v0.5.0 > v0.5.1 update script `/scripts/xslt/update/050-051.xsl`)
- added `gndo:succeedingPlaceOrGeographicName`, `gndo:temporaryName`, `gndo:titleOfNobility`, `gndo:abbreviatedName`, `gndo:fieldOfStudy` and `gndo:abbreviatedName`.
- added `gndo:relatesTo` property to all records (which has no direct equivalent in the GNDO but may be helpfull to map out relations between entities anyway).
- added `event` record which more or less translates to `gndo:ConferenceOrEvent` but is not fully moddled yet.
- changed `gndo:dateOfActivity` to `gndo:periodOfActivity` (cf. v0.5.0 > v0.5.1 update script `/scripts/xslt/update/050-051.xsl`)
- refined Validation on `gndo:publication` concerning `@ref`, `@gndo:ref`, `@dnb:catalogue`


##0.5.0 ALPHA (Latest) (2023-01-25)  

- **First published Version 0.5.0** (ALPHA) is now open for testing.
- Renamed former `project` element to `provider` and placed in `entityXML/collection`
- Changes on the Content Model of `entity`: ANY ELEMENT concept finetuned.
- Added `@url` to `source`
- Added `gndo:affiliation` to `person` and `corporateBody`
- Added `@dnb:catalogue` to `gndo:publication`
- Added `gndo:pseudonym` to `person`.
- Added `@gndo:type` to `person` to diferentiate subclasses like gods, fictional charcters etc.
- Modified `@ref` to either point to an internal ID or to an external URI.
- Added `@gndo:ref` to `gndo:professionOrOccupation` to make `@gndo:term` more clearer.


##0.3.2 ALPHA (Unpublished) (2022-11-18)  

- Cleaned Structure and introduced models
- Added Support for other schemes (like LIDO) using `rng:element/rng:anyName` concept.


##0.3.1 ALPHA (Unpublished) (2022-11-11)  

- Added manifestation and expression record.
- Added Property Class with agency features.


##0.3.0 ALPHA (Unpublished) (2022-10-05)  
Added store-namespace and schema.


##0.2.1 (Unpublished) (2022-07-06)  

- Added `work` Entity and provided MARC mapping.
- Added `@id` to `list` element.


##0.2 (Unpublished) (2022-05-10)  
Added GND-Reference abstract class.


##0.1.4 (Unpublished) (2022-04-21)  
Simplified Entity lists. Now every Record needs an ID (either `@id` or `@gndo:uri`).


##0.1.3 (Unpublished) (2022-04-20)  
Better revision description; entityXML now supports arbitrary elements within entity markup.


##0.1.2 (Unpublished) (2022-04-19)  
Structural Changes concerning project and collection container.


##0.1.1 (Unpublished) (2022-03-31)  
Reduced Content: Integrating just persons and places for now!


##0.1 (Unpublished) (2022-03-29)  
Created RELAX-NG Schema.

